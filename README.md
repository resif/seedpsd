# SeedPSD

This software is designed to be fully integrated into the main workflow of the 
[EPOS-France seismological data center (Résif-DC)](https://www.epos-france.fr/en/epos-france-actions/tta-si-s-seismological-information-system/). 
It processes metadata and daily data streams to calculate power spectral densities (PSD).

Hourly segmented PSDs are aggregated to show probabilistic distributions of the seismic noise levels. 
PSDs are calculated by the method described by [McNamara2004](https://docs.obspy.org/citations.html#mcnamara2004) 
using the [class PPSD of ObsPy](https://docs.obspy.org/packages/autogen/obspy.signal.spectral_estimation.PPSD.html) 
for a particular source (network/station/location/channel) and sampling rate combination. 
The calculations are stored in a database allowing PdF and spectrogram plots rendering on the fly.

Moreover, SeedPSD was:
- NOT designed as an interactive analysis tool and will therefore not replace tools like PQLX/SQLX for such use.
- inspired by [Mustang (EarthScope/IRIS)](https://service.iris.edu/mustang/) and was designed to be as compatible 
as possible with its API. However, since the SeedPSD engine is based on ObsPy, the two softwares diverge in several aspects.
- extended with a lot of configuration options in order to deploy it in other data centers.

The sources are distributed under the terms of [GPLv3 licence](https://choosealicense.com/licenses/gpl-3.0) and are 
available at https://gricad-gitlab.univ-grenoble-alpes.fr/OSUG/RESIF/seedpsd (and publicly mirrored at https://gitlab.com/resif/seedpsd)

## Documentation

### Quick steps

1. [Install](doc/install.adoc) or [Upgrade](doc/upgrade.adoc) or [Use a docker image](doc/docker.md)
2. [Configure](doc/config.adoc)
3. [First use](doc/first-steps.adoc)

### How it works

- [Full documentation](doc/index.md)
- [Known limitations](doc/known-limitations.md)

![overview](doc/modeling/seedpsd-overview.png)

#### Metadata Feeding

When a StationXML metadata is submitted, the SeedPSD metadata ingestion engine:

- extracts each epoch from it using a FDSN Station webservice, 
- extracts and stores in database the main values allowing to identify metadata changes (instrumental response, sampling rate, ...), 
- compares the metadata already present in the database with the new ones before inserting them and, in case of discrepancy, invalidates any statistics calculated from them.

#### Data feeding

When a MiniSEED data file is submitted, the SeedPSD data ingestion engine:

- checks the coverage and consistency of the metadata corresponding to the file, 
- initializes a PPSD object from the content of the MiniSEED file, 
- extracts and stores in database the main statistical values calculated by PPSD.

#### User request

When a HTTP request is submitted, the SeedPSD exploitation engine:

- extracts the metadata for the target (NSLC) from the inventory, 
- extracts from the database the statistical values corresponding to this target over the requested period (start/end), 
- initializes a PPSD object from these values, 
- uses this PPSD object to generate an image configured according to the options specified in the request parameters, 
- sends the generated image to the client that made the request.

## Contributing
If you're willing to contribute to this project, here are some tools that we are using to lint, test and format code.

### Pre-commit hooks
First of, install the [pre-commit](https://pre-commit.com/#usage) hooks.

To do so, activate the virtual env with the following command and run the install command for pre-commit:
```shell
uv run pre-commit install
```
Now, each time you'll want to commit, several tasks will be run to ensure certain standards of code quality:
- [ruff](https://beta.ruff.rs/docs/) : a linter and formatter for python files
- [djLint](https://www.djlint.com/) : a formatter for django templates
- [django-upgrade](https://github.com/adamchainz/django-upgrade/) : a tool that automatically upgrade jjango project code.

Configuration for pre-commit is located in `.pre-commit-config.yaml` file

## Authors

### Technical development
* [Bollard Philippe](https://orcid.org/0000-0001-9156-7819) (2021 - 2024: software architect)
* Panay Simon (2024 - now: maintainer)
* Touvier Jérôme (2020 - 2021: former contributor)

### Scientific advisors

* [Mordret Aurélien](https://orcid.org/0000-0002-7998-5417) (2023 - 2024)
* [Saurel Jean-Marie](https://orcid.org/0000-0003-1458-4193) (2021)
* [Stehly Laurent](https://orcid.org/0000-0002-1854-7157) (2022 - now)
* [Vergne Jérôme](https://orcid.org/0000-0003-1731-9360) (2021)
