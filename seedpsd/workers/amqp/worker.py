import verboselogs

from seedpsd.workers import SeedPsdWorker

logger = verboselogs.VerboseLogger(__name__)


class SeedPsdWorkerAMQP(SeedPsdWorker):
    """
    SeedPSD Worker class for AMQP with "RabbitMQ" backend
    """

    WORKER_DATA = "data"
    WORKER_METADATA = "metadata"

    def __init__(self, settings):
        logger.debug(f"SeedPsdWorkerAMQP.__init__({settings})")
        super().__init__(settings, parallel=False)

    @classmethod
    def factory(cls, worker_type, settings):
        """
        Worker factory
        :param worker_type: The type of worker to create (data|metadata)
        :param settings: The application settings
        """
        return NotImplemented
