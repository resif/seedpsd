import re

import verboselogs

from seedpsd.models.files.factory import MSEEDFileFactory
from seedpsd.workers.data.mseed import SeedPsdWorkerDataMseed
from seedpsd.workers.metadata import SeedPsdWorkerMetadata

logger = verboselogs.VerboseLogger(__name__)


class SeedPsdAMQPMessageProcessor:
    """
    Base AMQP message processor
    """

    def __init__(self, settings, simulate=False):
        """
        Initialize an AMQP Message processor for SeedPSD
        :param settings: The application settings
        :param simulate: Allows to not consume the message from queue and not register anything to database
        """
        logger.debug(f"SeedPsdMessageProcessor.__init__({settings}, {simulate})")
        self._settings = settings
        self._simulate = simulate

    def process(self, message, separator="\n"):
        """
        Process an AMQP message
        :param message: The message
        :param separator: The token separator (default='\n')
        """
        return NotImplemented

    @staticmethod
    def _decode_message(message):
        if isinstance(message, bytes):
            message = str(message, "utf-8")
        return message


class SeedPsdAMQPMessageProcessorData(SeedPsdAMQPMessageProcessor):
    """
    AMQP Message processor for data
    """

    def __init__(self, settings, simulate=False):
        """
        Initialize an AMQP Message processor for data
        :param settings: The application settings
        :param simulate: Allows to not consume the message from queue and not register anything to database
        """
        logger.debug(
            f"SeedPsdAMQPMessageProcessorData.__init__({settings}, {simulate})"
        )
        super().__init__(settings, simulate)
        self._worker = SeedPsdWorkerDataMseed(settings)

    def process(self, message, separator="\n"):
        """
        Process an AMQP message
        :param message: The message
        :param separator: The token separator (default='\n')
        """
        logger.debug(f"SeedPsdAMQPMessageProcessorData.process({message}, {separator})")

        # Decode the message
        message = self._decode_message(message)
        logger.info(f"Consuming AMQP message: {message}")

        # Process each token of the message
        for token in message.split(separator):
            try:
                mseed_file = MSEEDFileFactory.from_path(
                    token,
                    parent_path=self._settings.data_path,
                    sds=self._settings.data_sds,
                    extended=self._settings.data_extended,
                    raise_error=True,
                )
                self._worker.process_file(
                    mseed_file, force=True, simulate=self._simulate
                )
            except Exception:
                logger.warning(
                    f"Unable to process the following part of the AMQP message: '{token}'"
                )
                logger.exception("Unable to process the AMQP message")


class SeedPsdAMQPMessageProcessorMetadata(SeedPsdAMQPMessageProcessor):
    """
    AMQP Message processor for metadata
    """

    regex_token = re.compile(r"^(?P<network>[A-Z0-9]+)[._](?P<station>[A-Z0-9]+)$")

    def __init__(self, settings, simulate=False, sender=None):
        """
        Initialize an AMQP Message processor for metadata
        :param settings: The application settings
        :param simulate: Allows to not consume the message from queue and not register anything to database
        """
        logger.debug(
            f"SeedPsdAMQPMessageProcessorMetadata.__init__({settings}, {simulate})"
        )
        super().__init__(settings, simulate)
        self._sender = sender
        self._worker = SeedPsdWorkerMetadata(settings)

        if settings.amqp_metadata_update_data:
            self._worker_data = SeedPsdWorkerDataMseed(
                settings, db_engine=self._worker.db_engine()
            )

    def process(self, message, separator="\n"):
        """
        Process an AMQP message
        :param message: The message
        :param separator: The token separator (default='\n')
        """
        logger.debug(
            f"SeedPsdAMQPMessageProcessorMetadata.process({message}, {separator})"
        )

        # Decode the message
        message = self._decode_message(message)
        logger.info(f"Consuming AMQP message: {message}")

        # Process each token of the message
        for token in message.split(separator):
            match = self.regex_token.match(token)
            if match:
                mdict = match.groupdict()
                try:
                    self._worker.update(
                        mdict["network"], mdict["station"], simulate=self._simulate
                    )

                    if self._settings.amqp_metadata_update_data:
                        self._worker_data.update_orphan(
                            mdict["network"],
                            mdict["station"],
                            simulate=self._simulate,
                            delete=self._settings.amqp_metadata_delete_orphan,
                        )
                        self._update_dirty(
                            mdict["network"], mdict["station"], simulate=self._simulate
                        )
                except Exception:
                    logger.warning(
                        f"Unable to process the following part of the AMQP message: '{token}'"
                    )
                    logger.exception("Unable to process the AMQP message")
            else:
                logger.warning(f"Rejecting (a part of) the AMQP message: '{token}'")

    def _update_dirty(self, network, station, simulate=False):
        logger.debug(
            f"SeedPsdAMQPMessageProcessorMetadata._update_dirty({network}, {station}, {simulate})"
        )
        if self._sender:
            # Find MSEED dirty files
            dirty_files = self._worker_data._find_dirty_files(
                network, station, delete=self._settings.amqp_metadata_delete_dirty
            )

            # Send an AMQP message for each dirty file
            if dirty_files and not simulate:
                for dirty_file in dirty_files:
                    self._sender.send(dirty_file.file_name)
