import verboselogs

from seedpsd.workers.amqp.processors import (
    SeedPsdAMQPMessageProcessorData,
    SeedPsdAMQPMessageProcessorMetadata,
)
from seedpsd.workers.amqp.worker import SeedPsdWorkerAMQP

from .receivers import (
    SeedPsdAmqpReceiverRabbitmqData,
    SeedPsdAmqpReceiverRabbitmqMetadata,
)
from .senders import SeedPsdAmqpSenderRabbitmqData

logger = verboselogs.VerboseLogger(__name__)


class SeedPsdWorkerRabbitmq(SeedPsdWorkerAMQP):
    """
    SeedPSD Worker class for AMQP with "RabbitMQ" backend
    """

    def __init__(self, settings):
        logger.debug(f"SeedPsdWorkerRabbitmq.__init__({settings})")
        super().__init__(settings)

    @classmethod
    def factory(cls, worker_type, settings):
        """
        Worker factory for AMQP with "RabbitMQ" backend
        :param worker_type: The type of worker to create (data|metadata)
        :param settings: The application settings
        """
        logger.debug(f"SeedPsdWorkerRabbitmq.factory({worker_type}, {settings})")

        if worker_type == cls.WORKER_DATA:
            return SeedPsdWorkerRabbitmqData(settings)
        if worker_type == cls.WORKER_METADATA:
            return SeedPsdWorkerRabbitmqMetadata(settings)
        return None


class SeedPsdWorkerRabbitmqData(SeedPsdWorkerRabbitmq):
    """
    Worker data for AMQP with "RabbitMQ" backend
    """

    def __init__(self, settings):
        logger.debug(f"SeedPsdWorkerRabbitmqData.__init__({settings})")
        super().__init__(settings)

    def start(self, simulate=False):
        """
        Listen an AMQP queue for data
        :param simulate: Enable the 'simulation' mode (do not consume the AMQP messages)
        """
        logger.debug(f"SeedPsdWorkerRabbitmqData.start({simulate})")
        processor = SeedPsdAMQPMessageProcessorData(self._settings, simulate)
        return SeedPsdAmqpReceiverRabbitmqData(
            processor, self._settings, simulate
        ).receive()


class SeedPsdWorkerRabbitmqMetadata(SeedPsdWorkerRabbitmq):
    """
    Worker metadata for AMQP with "RabbitMQ" backend
    """

    def __init__(self, settings):
        logger.debug(f"SeedPsdWorkerRabbitmqMetadata.__init__({settings})")
        super().__init__(settings)

    def start(self, simulate=False):
        """
        Listen an AMQP queue for metadata
        :param simulate: Enable the 'simulation' mode (do not consume the AMQP messages)
        """
        logger.debug(f"SeedPsdWorkerRabbitmqMetadata.start({simulate})")
        sender = SeedPsdAmqpSenderRabbitmqData(self._settings, simulate)
        processor = SeedPsdAMQPMessageProcessorMetadata(
            self._settings, simulate, sender
        )
        return SeedPsdAmqpReceiverRabbitmqMetadata(
            processor, self._settings, simulate
        ).receive()
