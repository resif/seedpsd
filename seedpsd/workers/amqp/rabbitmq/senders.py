import pika
import verboselogs

logger = verboselogs.VerboseLogger(__name__)


class SeedPsdAmqpSenderRabbitmq:
    """
    Base AMQP message handler with "RabbitMQ" backend
    """

    def __init__(self, settings, simulate=False):
        self._settings = settings
        self._simulate = simulate

        self._queue_name = None
        self._queue_routing = None
        self._queue_durable = None

    def send(self, message=None):
        """
        Connect to RabbitMQ
        :param message: The message to send
        :param routing_key: The routing key
        :return:
        """
        logger.debug("SeedPsdAmqpSenderRabbitmqData.send(%s)", message)

        # Start connection
        logger.info("Connecting AMQP sender to %s", self._settings.amqp_server_url)
        credentials = pika.PlainCredentials(
            self._settings.amqp_user, self._settings.amqp_password
        )
        parameters = pika.ConnectionParameters(
            self._settings.amqp_server_url,
            self._settings.amqp_port,
            self._settings.amqp_vhost,
            credentials,
        )
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()

        # Initialize queue
        logger.verbose("Declaring the '%s' queue", self._queue_name)
        channel.queue_declare(
            queue=self._queue_name,
            durable=self._queue_durable,
        )

        # Send message
        logger.info(
            "Sending message '%s' on '%s' queue using '%s' routing",
            message,
            self._queue_name,
            self._queue_name,
        )
        channel.basic_publish(
            exchange="",
            routing_key=self._queue_name,
            body=message,
            properties=pika.BasicProperties(delivery_mode=pika.DeliveryMode.Persistent),
        )

        # Close connection
        connection.close()


class SeedPsdAmqpSenderRabbitmqData(SeedPsdAmqpSenderRabbitmq):
    def __init__(self, settings, simulate=False):
        super().__init__(settings, simulate)
        self._queue_name = self._settings.amqp_data_queue_name
        self._queue_routing = self._settings.amqp_data_queue_routing
        self._queue_durable = self._settings.amqp_data_queue_durable


class SeedPsdAmqpSenderRabbitmqMetadata(SeedPsdAmqpSenderRabbitmq):
    def __init__(self, settings, simulate=False):
        super().__init__(settings, simulate)
        self._queue_name = self._settings.amqp_metadata_queue_name
        self._queue_routing = self._settings.amqp_metadata_queue_routing
        self._queue_durable = self._settings.amqp_metadata_queue_durable
