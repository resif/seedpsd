from datetime import timedelta

import verboselogs
from tablib import Dataset

from . import SeedPsdWorker

logger = verboselogs.VerboseLogger(__name__)


class SeedPsdWorkerValue(SeedPsdWorker):
    """
    SeedPSD Worker class for value extraction
    """

    def __init__(self, settings=None):
        logger.debug(f"SeedPsdWorkerValue.__init__({settings})")
        super().__init__(settings, parallel=False)

    @staticmethod
    def build_filename(ppsd, output_format, output_type):
        """
        Build a file name for a PPSD object
        :param ppsd: The PPSD object
        :param output_format: The output format
        :param output_type: The output type
        :return: The file name
        """
        try:
            start_used = ppsd.current_times_used[0].isoformat()
            end_used = (
                ppsd.current_times_used[-1] + timedelta(seconds=ppsd.ppsd_length)
            ).isoformat()
            return f"{ppsd.id}_{start_used}_{end_used}_{ppsd.sampling_rate}Hz_{output_type}.{output_format}"
        except Exception:
            return f"{ppsd.id}_{ppsd.sampling_rate}Hz_{output_type}.{output_format}"

    @staticmethod
    def build_header(ppsd, output_format):
        """
        Add header lines to a text output
        :param ppsd: The PPSD object
        :param output_format: The output format
        :return: The formatted header
        """
        header = f"{ppsd.id} at {ppsd.sampling_rate}Hz from {ppsd.current_times_used[0].isoformat()} to {(ppsd.current_times_used[-1] + timedelta(seconds=ppsd.ppsd_length)).isoformat()}"

        if output_format != "json":
            block = "#############################################################################################\n"
            block += f"## {header} \n"
            block += "#############################\n"
            return block
        return ""

    # RENDER PSD VALUES ################################################################################################

    def export(
        self,
        ppsd,
        output_format,
        output_type,
        x_min=None,
        x_max=None,
        y_min=None,
        y_max=None,
        z_min=None,
        z_max=None,
    ):
        """
        Export values
        :param ppsd: The PPSD object
        :param output_format: The output format
        :param output_type: The output type
        :param x_min: The minimum value of X (period)
        :param x_max: The maximum value of X (period)
        :param y_min: The minimum value of Y (amplitude)
        :param y_max: The maximum value of Y (amplitude)
        :param z_min: The minimum value of Z (percentage)
        :param z_max: The maximum value of Z (percentage)
        :return: The formatted dataset
        """

        # Render the values
        if output_type == "psd" and output_format != "npz":
            return self.export_psd_values(ppsd, output_format)
        if output_type == "mean":
            return self.export_mean_values(ppsd, output_format)
        if output_type == "mode":
            return self.export_mode_values(ppsd, output_format)
        if output_type == "histogram":
            return self.export_histogram_values(
                ppsd, output_format, x_min, x_max, y_min, y_max, z_min, z_max
            )
        if output_type == "spectrogram":
            return self.export_spectrogram_values(
                ppsd, output_format, x_min, x_max, y_min, y_max, z_min, z_max
            )
        msg = f"Unsupported value type: '{output_type}"
        raise Exception(msg)

    @staticmethod
    def export_psd_values(ppsd, output_format):
        """
        Export PSD values using a CSV/JSON format
        :param ppsd: The PPSD object
        :param output_format: The output format
        :return: The formatted dataset
        """

        # Compute the values
        ppsd.calculate_histogram()

        # Initialize the dataset
        data = Dataset()
        headers = ["Timestamp", "Values"]

        # Get data
        data.append_col(ppsd.times_processed)
        data.append_col(ppsd.psd_values)

        # Set the headers
        data.headers = headers

        # Sort the dataset
        data = data.sort("Timestamp")

        # Render the dataset
        return data.export(output_format)

    @staticmethod
    def export_mean_values(ppsd, output_format):
        """
        Export PSD mean values using a CSV/JSON format
        :param ppsd: The PPSD object
        :param output_format: The output format
        :return: The formatted dataset
        """

        # Compute the values
        ppsd.calculate_histogram()

        # Initialize the dataset
        data = Dataset()
        headers = ["Period", "Mean"]

        # Get data
        data_periods, data_means = ppsd.get_mean()
        data.append_col(data_periods)
        data.append_col(data_means)

        # Set the headers
        data.headers = headers

        # Sort the dataset
        data = data.sort("Period")

        # Render the dataset
        return data.export(output_format)

    @staticmethod
    def export_mode_values(ppsd, output_format):
        """
        Export PSD mode values using a CSV/JSON format
        :param ppsd: The PPSD object
        :param output_format: The output format
        :return: The formatted dataset
        """

        # Compute the values
        ppsd.calculate_histogram()

        # Initialize the dataset
        data = Dataset()
        headers = ["Period", "Mode"]

        # Get data
        data_periods, data_modes = ppsd.get_mode()
        data.append_col(data_periods)
        data.append_col(data_modes)

        # Set the headers
        data.headers = headers

        # Sort the dataset
        data = data.sort("Period")

        # Render the dataset
        return data.export(output_format)

    # RENDER HISTOGRAM VALUES ##########################################################################################

    @staticmethod
    def export_histogram_values(
        ppsd, output_format, x_min, x_max, y_min, y_max, z_min, z_max
    ):
        """
        Export histogram values using a CSV/JSON format
        :param ppsd: The PPSD object
        :param output_format: The output format
        :param x_min: The minimum value of X (period)
        :param x_max: The maximum value of X (period)
        :param y_min: The minimum value of Y (amplitude)
        :param y_max: The maximum value of Y (amplitude)
        :param z_min: The minimum value of Z (percentage)
        :param z_max: The maximum value of Z (percentage)
        :return: The formatted dataset
        """

        # Compute the values
        ppsd.calculate_histogram()

        # Initialize the dataset
        data = Dataset()
        headers = ["Period", "Amplitude", "Percentage"]

        # Get data
        values = ppsd.get_histogram_values(x_min, x_max, y_min, y_max, z_min, z_max)
        data.append_col(values[:, 0])
        data.append_col(values[:, 1])
        data.append_col(values[:, 2])

        # Set the headers
        data.headers = headers

        # Sort the dataset
        data = data.sort("Period")

        # Render the dataset
        return data.export(output_format)

    # RENDER SPECTROGRAM VALUES ########################################################################################

    @staticmethod
    def export_spectrogram_values(
        ppsd, output_format, x_min, x_max, y_min, y_max, z_min, z_max
    ):
        """
        Render spectrogram values using a CSV/JSON format
        :param ppsd: The PPSD object
        :param output_format: The output format
        :param x_min: The minimum value of X (period)
        :param x_max: The maximum value of X (period)
        :param y_min: The minimum value of Y (amplitude)
        :param y_max: The maximum value of Y (amplitude)
        :param z_min: The minimum value of Z (percentage)
        :param z_max: The maximum value of Z (percentage)
        :return: The formatted dataset
        """
