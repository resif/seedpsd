import os
import platform

import verboselogs
from obspy import __version__ as obspy_version
from obspy.clients.fdsn import Client, RoutingClient
from obspy.clients.fdsn.header import URL_MAPPINGS, FDSNException, platform_

from seedpsd import __version__ as seedpsd_version
from seedpsd.models.db.session import DBSession
from seedpsd.settings.workers import Settings
from seedpsd.tools.inventory import InventoryFactory

logger = verboselogs.VerboseLogger(__name__)


class SeedPsdWorker:
    """
    Base class for workers
    """

    def __init__(self, settings=None, db_engine=None, parallel=False, max_cpu=None):
        logger.debug(
            f"SeedPsdWorker.__init__({settings}, {db_engine}, {parallel}, {max_cpu})"
        )
        self._settings = settings or Settings()

        # Parallelization
        if max_cpu:
            self.n_cores = min(len(os.sched_getaffinity(0)), int(max_cpu))
        else:
            self.n_cores = len(os.sched_getaffinity(0))

        if parallel and self.n_cores > 1:
            logger.info(f"Parallelization ENABLED. Set CPU limit to {self.n_cores}")
            self._parallel = parallel
        elif parallel and self.n_cores == 1:
            logger.warning("Only 1 CPU detected. Parallelization DISABLED")
            self._parallel = False
        else:
            logger.verbose("Parallelization DISABLED")
            self._parallel = False

        # Database
        self._db_pool = db_engine

        # ObsPy client
        self._client = None

    # MULTIPROCESSING ##################################################################################################

    def pre_fork(self):
        """
        Reset the database engine and session when starting a new process
        """
        logger.debug("SeedPsdWorker.pre_fork()")

        # Close DB engine
        if self._db_pool:
            logger.verbose("Pre-fork: Reset DB engine")
            self._db_pool.dispose()

    def post_fork(self):
        """
        Reset the database engine and session when starting a new process
        """
        logger.debug("SeedPsdWorker.post_fork()")

        # Close DB engine
        if self._db_pool:
            logger.verbose("Post-fork: Reset DB engine")
            # self._db_pool.dispose(close=False)
            self._db_pool = None

        # Close ObsPy client
        if self._client:
            logger.verbose("Post-fork: Close ObsPy client")
            self._client = None

    # DATABASE #########################################################################################################

    def db_engine(self, tenant_schema=None):
        """
        Initialize the database engine (singleton)

        :param tenant_schema: The tenant schema to use
        :return: A database engine
        """
        logger.debug(
            f"SeedPsdWorker.db_engine({tenant_schema}) | parallel={self._parallel}"
        )

        # Pool support with parallel mode
        use_pool = (
            self._settings.database_pool_with_parallel if self._parallel else True
        )

        # Initialize the pool
        if not self._db_pool:
            self._db_pool = DBSession.engine_factory(self._settings, use_pool=use_pool)

        # Configure the pool
        return DBSession.engine_factory(
            self._settings,
            tenant_schema=tenant_schema,
            engine=self._db_pool,
            use_pool=use_pool,
        )

    def db_session(self, engine=None, tenant_schema=None):
        """
        Get a database session

        :param engine: The engine to use
        :param tenant_schema: The tenant schema to use
        :return: A database session
        """
        logger.debug(f"SeedPsdWorker.db_session({engine}, {tenant_schema})")
        engine = engine or self.db_engine(tenant_schema)
        return DBSession.factory(engine)

    # METADATA INVENTORY ###############################################################################################

    @property
    def client(self):
        """
        Initialize the ObsPy client
        """
        if not self._client:
            # Set debug flag
            debug = self._settings.fdsn_client_debug

            # Build custom user-agent
            user_agent = f"SeedPSD/{seedpsd_version} (ObsPy/{obspy_version}, {platform_}, Python {platform.python_version()})"

            # Initialize the Client
            if str(self._settings.fdsn_client).lower() in (
                "eida-routing",
                "iris-federator",
            ):
                logger.verbose(
                    f"Initializing FDSN routing client: {self._settings.fdsn_client}"
                )
                self._client = RoutingClient(
                    str(self._settings.fdsn_client).lower(), debug=debug
                )

            elif str(self._settings.fdsn_client).upper() in URL_MAPPINGS:
                logger.verbose(
                    f"Initializing FDSN client: {self._settings.fdsn_client}"
                )
                self._client = Client(
                    str(self._settings.fdsn_client).upper(),
                    debug=debug,
                    user_agent=user_agent,
                )

            elif "http" in str(self._settings.fdsn_client).lower():
                logger.verbose(
                    f"Initializing FDSN client: {self._settings.fdsn_client}"
                )
                self._client = Client(
                    str(self._settings.fdsn_client).lower(),
                    debug=debug,
                    user_agent=user_agent,
                )

            else:
                logger.warning("Unknown '%s' FDSN client", self._settings.fdsn_client)
                msg = f"Unknown '{self._settings.fdsn_client}' FDSN client"
                raise Exception(msg)

        return self._client

    def _get_inventory(
        self,
        network=None,
        station=None,
        location=None,
        channel=None,
        level="channel",
        start_before=None,
        end_after=None,
        raise_error=False,
    ):
        """
        Get an inventory filtered by network, station, channel

        :param network: (Optional) Network code
        :param station: (Optional) Station code
        :param location: (Optional) Location code
        :param channel: (Optional) Channel code
        :param level: (Optional) Level (network|station|channel|response) - Default:channel
        :param start_before: (Optional) Select results staring before this date
        :param end_after: (Optional) Select results ending after this date
        :param raise_error: (Optional) Raise errors
        :return: An inventory
        """
        logger.debug(
            f"SeedPsdWorker._get_inventory({network}, {station}, {location}, {channel}, {level}, {start_before}, {end_after}, {raise_error})"
        )

        try:
            if network:
                source = network
                if station:
                    source += f".{station}"
                    if channel:
                        source += f".{location or ''}.{channel}"

                logger.verbose(f"{source} : Fetching metadata at level '{level}'")

            if self._settings.metadata_path is not None:
                return InventoryFactory.from_filesystem(
                    self._settings.metadata_path, level, network, station
                )
            return InventoryFactory.from_webservice(
                self.client,
                level,
                network,
                station,
                location,
                channel,
                start_before,
                end_after,
            )
        except FDSNException as e:
            logger.debug(e)
            if raise_error:
                raise
            return None
