import os

import numpy as np
import verboselogs
from multiprocess.pool import Pool

from seedpsd.models.db.shared.file import File
from seedpsd.models.db.shared.source import Source
from seedpsd.models.db.tenant.epoch import Epoch
from seedpsd.models.db.tenant.gap import Gap
from seedpsd.models.db.tenant.stream import Stream
from seedpsd.models.db.tenant.trace import Trace
from seedpsd.models.files.factory import NPZFileFactory
from seedpsd.models.files.npz import NPZFile
from seedpsd.tools.common import datetime_now

from . import SeedPsdWorkerData

logger = verboselogs.VerboseLogger(__name__)


class SeedPsdWorkerDataNPZ(SeedPsdWorkerData):
    """
    SeedPSD Worker class for NPZ files
    """

    def __init__(self, settings, db_engine=None, parallel=False, max_cpu=None):
        logger.debug(
            f"SeedPsdWorkerDataNPZ.__init__({settings}, {db_engine}, {parallel}, {max_cpu})"
        )
        super().__init__(settings, db_engine, parallel, max_cpu)

    # ENTRY POINT: PROCESS NPZ FILE ####################################################################################

    def process_path(
        self,
        path,
        force_processed=False,
        force_dirty=False,
        simulate=False,
        debug=False,
    ):
        """
        Load NPZ files from a path
        :param path: The path to load
        :param force_processed: Force update of already processed NPZ files
        :param force_dirty: Force update of dirty NPZ files
        :param simulate: Enable the 'simulation' mode (do not alter the database)
        """
        logger.debug(
            f"SeedPsdWorkerDataNPZ.process_path({path}, {force_processed}, {force_dirty}, {simulate}, {debug})"
        )
        if os.path.isdir(path):
            logger.info(f"Processing directory '{path}'...")

            if self._parallel:
                paths = set()
                for dirpath, _dirs, files in os.walk(path, followlinks=True):
                    for filename in sorted(files):
                        logger.verbose(f"Find file: '{filename}'")
                        paths.add(os.path.join(dirpath, filename))

                self.pre_fork()
                with Pool(self.n_cores, self.post_fork()) as pool:
                    pool.map(
                        lambda file: self.process_file(
                            file,
                            force_processed=force_processed,
                            force_dirty=force_dirty,
                            simulate=simulate,
                            debug=debug,
                        ),
                        paths,
                    )
                    return None

            else:
                for dirpath, _dirs, files in os.walk(path, followlinks=True):
                    for filename in sorted(files):
                        logger.verbose(f"Find file: '{filename}'")
                        self.process_file(
                            os.path.join(dirpath, filename),
                            force_processed=force_processed,
                            force_dirty=force_dirty,
                            simulate=simulate,
                            debug=debug,
                        )
                return None
        else:
            return self.process_file(
                path,
                force_processed=force_processed,
                force_dirty=force_dirty,
                simulate=simulate,
                debug=debug,
            )

    def process_file(
        self, npz, force_processed=False, force_dirty=False, simulate=False, debug=False
    ):
        """
        Process a NPZ file

        :param npz: The file (path or NPZFile instance)
        :param force_processed: Force update of already processed NPZ files
        :param force_dirty: Force update of dirty NPZ files
        :param simulate: Enable the 'simulation' mode (do not alter the database)

        Try to load the NPZ file
        - detect the 'dirty' flag. Skip the file if needed
        - get metadata from WebService
        - initialize a PPSD object from the NPZ

        Identify the period of data contained by the NPZ
        - sort the times_data
        - search the related File objects for the start and end days
        - clean existing data

        For each time_data:
        - search or create the related File object
        - create a Stream object

        For each time_gap:
        - search the related Stream object
        - create a Gap object

        For each time_processed + psd_value:
        - search the related Stream object
        - create a Trace object

        Finally, do bulk inserts in database, with the following order:
        - Files
        - Streams
        - Gaps
        - Traces
        """
        logger.debug(
            f"SeedPsdWorkerDataNPZ.process_file({npz}, {force_processed}, {force_dirty}, {simulate}, {debug})"
        )
        # Initialize a container for the Mseed file
        if isinstance(npz, str):
            npz_file = NPZFileFactory.from_path(npz, raise_error=True)
        elif isinstance(npz, NPZFile):
            npz_file = npz
        elif debug:
            msg = f"Unsupported type {type(npz)}"
            raise Exception(msg)
        else:
            npz_file = None

        # Do not continue if we don't have a NPZFile instance
        if not npz_file:
            return False

        # Try to load the file
        if force_dirty or not npz_file.is_dirty:
            # Processing a dirty file
            if npz_file.is_dirty:
                logger.warning(
                    f"The data or metadata has been updated since creation of file {npz_file.file_name}. It may produce wrong statistics!"
                )

            # Load metadata
            npz_metadata = self._get_inventory(
                npz_file.network,
                npz_file.station,
                npz_file.location,
                npz_file.channel,
                level="response",
            )

            # Load a PPSD instance from the NPZ
            ppsd = npz_file.ppsd(npz_metadata)

            # Unable to read the file
            if not ppsd:
                logger.warning("Unable to read the '%s' file", npz_file.file_name)
                msg = f"Unable to read the '{npz_file.file_name}' file"
                raise Exception(msg)

            # Else, the file has been successfully opened
            logger.info(f"Reading PSD informations from {npz_file.file_name}")

            # Sort times_data
            times_data = sorted(ppsd.times_data, key=lambda tup: tup[0])

            # Get PPSD start and end dates
            npz_start = times_data[0][0].date
            npz_end = times_data[-1][0].date
            logger.info(
                f"{npz_file.file_name} contains PSD data processed from MSEED files between {npz_start} and {npz_end}"
            )

            # Get the source
            db_engine_shared = self.db_engine().connect()
            with self.db_session(db_engine_shared) as db_session:
                source = db_session.find_one(
                    Source.find(
                        npz_file.network,
                        npz_file.station,
                        npz_file.location,
                        npz_file.channel,
                        npz_start,
                        npz_end,
                    )
                )
                source_id = source.id
                tenant_schema = source.network.schema
                db_session.rollback()
            db_engine_shared.close()

            # If DB is not up-to-date, do not process the file
            if not source_id:
                logger.warning(
                    f"Missing metadata for {npz_file.file_name}. Please update the database and reprocess this file."
                )
                return None
            # Use the DB schema dedicated to this tenant
            db_engine_tenant = self.db_engine(tenant_schema).connect()
            with self.db_session(db_engine_tenant) as db_session:
                try:
                    # Find Files related to start/end of NPZ
                    file_start = db_session.find_one(
                        File.find(source=source_id, date=npz_start)
                    )
                    file_end = db_session.find_one(
                        File.find(source=source_id, date=npz_end)
                    )

                    if (
                        not force_processed
                        and file_start is not None
                        and file_start.processed
                        and file_end is not None
                        and file_end.processed
                    ):
                        logger.info(f"Skipping changes from {npz_file.file_name}")
                    else:
                        logger.info(f"Loading changes from {npz_file.file_name}")

                        # Clean existing data
                        db_session.execute(
                            File.delete(source=source_id, date=npz_start)
                        )
                        db_session.execute(File.delete(source=source_id, date=npz_end))
                        db_session.execute(
                            File.delete(
                                source=source_id, before=npz_end, after=npz_start
                            )
                        )

                        # Extract content from the PPSD
                        data = {}
                        self._load_files_streams(
                            ppsd, data, times_data, source_id, npz_file, db_session
                        )
                        self._load_gaps(ppsd, data)
                        self._load_traces(ppsd, data)

                        # Save the objects
                        db_session.bulk_save_objects(data["files"].values())
                        for stream_day in data["streams"]:
                            db_session.bulk_save_objects(data["streams"][stream_day])
                        db_session.bulk_save_objects(data["gaps"])
                        db_session.bulk_save_objects(data["traces"])

                    # Save all stuff in database
                    if not simulate:
                        db_session.commit()
                    else:
                        logger.warning(
                            "Simulation mode. No changes will be saved in database!"
                        )
                        db_session.rollback()
                except Exception:
                    db_session.rollback()
                    db_session.close()
                    db_engine_tenant.close()
                    raise

            # Close DB connection
            db_engine_tenant.close()
            return None

        # Data/Metadata has been updated since the file creation
        logger.warning(f"Ignoring the dirty file {npz_file.file_name}")
        return None

    def _load_files_streams(
        self, ppsd, data, times_data, source_id, npz_file, db_session
    ):
        """
        Load files and streams from PPSD

        :param ppsd: The PPSD object
        :param data: The data container
        :param times_data: The sorted times data
        :param source_id: The source ID
        :param npz_file: The NPZFile object
        :param db_session: The DB session
        :return: The updated data container
        """
        logger.debug("SeedPsdWorkerDataNPZ._load_files_streams()")
        data["files"] = {}
        data["streams"] = {}

        epochs = db_session.find_all(Epoch.find(source=source_id))
        epochs = set(epochs)

        for time_data in times_data:
            # Build the related MSED file name
            file_name = (
                f"{ppsd.id}.D.{time_data[0].year}.{str(time_data[0].julday).zfill(3)}"
            )

            # Get or Create the file
            if file_name not in data["files"]:
                file = File(name=file_name, date=time_data[0])
                file.source_id = source_id
                file.date_created = npz_file.date_created
                file.date_processed = datetime_now()
                file.processed = True
                data["files"][file_name] = file

            # Create a stream
            stream = Stream(time_data[0], time_data[1], ppsd.sampling_rate)
            stream.file_id = data["files"][file_name].id
            stream.source_id = source_id

            # Link the stream to an epoch
            epoch_found = False
            for epoch in epochs:
                if epoch.start < stream.end and (
                    not epoch.end or epoch.end > stream.start
                ):
                    stream.epoch_id = epoch.id
                    stream.valid = True
                    epoch_found = True
                    break
            if not epoch_found:
                logger.warning(
                    f"Unable to find an epoch related to the stream '{stream}'"
                )
                stream.epoch_id = None
                stream.valid = False

            stream_date = stream.start.date()
            if stream_date not in data["streams"]:
                data["streams"][stream_date] = set()
            data["streams"][stream_date].add(stream)

        return data

    def _load_gaps(self, ppsd, data):
        """
        Load gaps from PPSD

        :param ppsd: The PPSD object
        :param data: The data container
        :return: The updated data container
        """
        logger.debug("SeedPsdWorkerDataNPZ._load_gaps()")
        data["gaps"] = set()

        for time_gap in ppsd.times_gaps:
            gap = Gap(time_gap[0], time_gap[1])

            stream_found = False
            gap_date = gap.start.date()

            if gap_date in data["streams"]:
                for stream in data["streams"][gap_date]:
                    if (
                        time_gap[0] <= stream.end_utc
                        and time_gap[1] >= stream.start_utc
                    ):
                        gap.stream_id = stream.id
                        stream_found = True
                        break

            if stream_found:
                data["gaps"].add(gap)
            else:
                logger.warning(f"Skipping gap between {gap.start} and {gap.end}")

        return data

    def _load_traces(self, ppsd, data):
        """
        Load traces from PPSD

        :param ppsd: The PPSD object
        :param data: The data container
        :return: The updated data container
        """
        logger.debug("SeedPsdWorkerDataNPZ._load_traces()")
        data["traces"] = set()

        for time_processed, psd_values in zip(
            ppsd.times_processed, ppsd.psd_values, strict=False
        ):
            trace = Trace(time_processed)
            trace.data = np.asarray(psd_values, dtype=float)

            stream_found = False
            trace_date = trace.timestamp.date()

            if trace_date in data["streams"]:
                for stream in data["streams"][trace_date]:
                    if stream.end_utc >= time_processed >= stream.start_utc:
                        trace.stream_id = stream.id
                        stream_found = True
                        break

            if stream_found:
                data["traces"].add(trace)
            else:
                logger.warning(f"Skipping data at {time_processed}")

        return data
