import os
import warnings
from urllib.error import URLError

import numpy as np
import pendulum
import verboselogs
from lxml.etree import XMLSyntaxError
from multiprocess.pool import Pool
from sqlalchemy.exc import IntegrityError

from seedpsd.errors import SkippedTraceError
from seedpsd.models.db.shared.file import File
from seedpsd.models.db.shared.network import Network
from seedpsd.models.db.shared.source import Source
from seedpsd.models.db.tenant.epoch import Epoch
from seedpsd.models.db.tenant.gap import Gap
from seedpsd.models.db.tenant.stream import Stream
from seedpsd.models.db.tenant.trace import Trace
from seedpsd.models.files.factory import MSEEDFileFactory, MSEEDFilePathFactory
from seedpsd.models.files.mseed import MSEEDFile
from seedpsd.tools.common import date_today, datetime_now
from seedpsd.tools.ppsd import PPSDFactory
from seedpsd.workers.value import SeedPsdWorkerValue

from . import SeedPsdWorkerData

logger = verboselogs.VerboseLogger(__name__)


class SeedPsdWorkerDataMseed(SeedPsdWorkerData):
    """
    SeedPSD Worker class for MSEED files
    """

    def __init__(self, settings, db_engine=None, parallel=False, max_cpu=None):
        logger.debug(
            f"SeedPsdWorkerDataMseed.__init__({settings}, {db_engine}, {parallel}, {max_cpu})"
        )
        super().__init__(settings, db_engine, parallel, max_cpu)

    @staticmethod
    def debug_psd_values(ppsd, output_format="csv", output_type="psd", output_path="."):
        file_name = SeedPsdWorkerValue.build_filename(ppsd, output_format, output_type)
        logger.info(f"Exporting PSD values into '{file_name}' file")
        with open(os.path.join(output_path, file_name), "w") as output_file:
            output_file.write(SeedPsdWorkerValue.build_header(ppsd, output_format))
            output_file.write(SeedPsdWorkerValue.export_psd_values(ppsd, output_format))

    # ENTRY POINT: UPDATE DIRTY FILES ##################################################################################

    def _find_dirty_files(
        self,
        network=None,
        station=None,
        location=None,
        channel=None,
        year=None,
        delete=False,
    ):
        """
        Find and reprocess dirty MSEED files needing update
        :param network: Filter by a network code
        :param station: Filter by a station code
        :param location: Filter by a location code
        :param channel: Filter by a channel code
        :param channel: Filter by a year
        :param delete: Enable the 'delete' mode (delete old dirty files from database)
        """
        logger.debug(
            f"SeedPsdWorkerDataMseed._find_dirty_files({network}, {station}, {location}, {channel}, {year}, {delete})"
        )

        # Find dirty files (unprocessed or without metadata) but exclude orphans
        params = {"dirty": True, "orphan": False}

        # Filter dirty files for a year
        if year:
            params["year"] = year

        # Initialize the list to return
        mseed_files = []

        # Use the shared DB schema
        db_engine = self.db_engine().connect()
        with self.db_session(db_engine) as db_session:
            # Search by NSLC
            if network or station or location or channel:
                # Find the sources
                sources_id = db_session.find_all(
                    Source.find_id(network, station, location, channel)
                )

                # No sources found
                if not sources_id:
                    logger.warning("No sources found")
                    return []
                params["source"] = sources_id

            # Search in DB with file.status != 'processed'
            dirty_files = db_session.find_all(File.find(**params))
            logger.debug(f"{len(dirty_files)} dirty files found in database")

            # Build path for each file
            for dirty_file in dirty_files:
                logger.verbose(f"The file '{dirty_file.name}' needs update")
                mseed_file = MSEEDFileFactory.from_database(
                    dirty_file,
                    parent_path=self._settings.data_path,
                    sds=self._settings.data_sds,
                    extended=self._settings.data_extended,
                )

                if (
                    self._settings.ppsd_only_allowed_channels
                    and not self._settings.ppsd_is_allowed_channel(mseed_file.channel)
                ):
                    if delete:
                        logger.warning(
                            f"{mseed_file.file_name} : Non-seismic channels are not supported. Deleting this file."
                        )
                        db_session.delete(dirty_file)
                    else:
                        logger.warning(
                            f"{mseed_file.file_name} : Non-seismic channels are not supported. Ignoring this file."
                        )

                elif not mseed_file.is_readable:
                    if delete and (
                        dirty_file.date_created is None
                        or (
                            dirty_file.date_created is not None
                            and pendulum.instance(dirty_file.date_created)
                            < pendulum.now().subtract(months=3)
                        )
                    ):
                        logger.warning(
                            f"{mseed_file.file_name} : The file is not readable. Deleting this file."
                        )
                        db_session.delete(dirty_file)
                    else:
                        logger.warning(
                            f"{mseed_file.file_name} : The file is not readable. Ignoring this file."
                        )

                else:
                    logger.verbose(f"{mseed_file.file_name}' : The file needs update")
                    mseed_files.append(mseed_file)

            # Save changes in database
            try:
                db_session.commit()
            except Exception:
                db_session.rollback()

        # Close DB connection
        db_engine.close()

        logger.info(
            f"{len(mseed_files)} dirty files found in database requiring updating"
        )
        return mseed_files

    def update_dirty(
        self,
        network=None,
        station=None,
        location=None,
        channel=None,
        year=None,
        simulate=False,
        debug=False,
        delete=False,
    ):
        """
        Find and reprocess dirty MSEED files needing update
        :param network: Filter by a network code
        :param station: Filter by a station code
        :param location: Filter by a location code
        :param channel: Filter by a channel code
        :param channel: Filter by a year
        :param simulate: Enable the 'simulation' mode (do not alter the database)
        :param debug: Enable the 'debug' mode
        :param delete: Enable the 'delete' mode (delete old dirty files from database)
        """
        logger.debug(
            f"SeedPsdWorkerDataMseed.update_dirty({network}, {station}, {location}, {channel}, {year}, {simulate}, {debug}, {delete})"
        )

        # Search in DB with file.status != 'processed'
        mseed_dirty_files = self._find_dirty_files(
            network, station, location, channel, year, delete
        )

        # Reprocess each file
        if mseed_dirty_files:
            if self._parallel:
                self.pre_fork()
                with Pool(self.n_cores, self.post_fork()) as pool:
                    pool.map(
                        lambda mseed_dirty_file: self.process_file(
                            mseed_dirty_file, simulate=simulate, debug=debug
                        ),
                        mseed_dirty_files,
                    )

            else:
                for mseed_dirty_file in mseed_dirty_files:
                    self.process_file(mseed_dirty_file, simulate=simulate, debug=debug)

            return True
        logger.debug("No file to update")
        return False

    # ENTRY POINT: UPDATE ORPHANS ######################################################################################

    def update_orphan(
        self,
        network=None,
        station=None,
        location=None,
        channel=None,
        year=None,
        simulate=False,
        debug=False,
        delete=False,
    ):
        """
        Find orphan files and link them to related Source
        :param network: Filter by a network code
        :param station: Filter by a station code
        :param location: Filter by a location code
        :param channel: Filter by a channel code
        :param channel: Filter by a year
        :param simulate: Enable the 'simulation' mode (do not alter the database)
        :param delete: Enable the deletion of really orphan files with still missing metadata
        """
        logger.debug(
            f"SeedPsdWorkerDataMseed.update_orphan({network}, {station}, {location}, {channel}, {year}, {simulate}, {debug}, {delete})"
        )

        # Find dirty files (unprocessed or without metadata)
        params = {"orphan": True}

        # Search by NSLC and/or year
        if network or station or location or channel or year:
            params["name"] = (
                f"{network or '%'}.{station or '%'}.{location or '%'}.{channel or '%'}.D.{year or '%'}.%"
            )

        # Use the shared DB schema
        db_engine = self.db_engine().connect()
        with self.db_session(db_engine) as db_session:
            # Search in DB with no source linked to
            orphan_files = db_session.find_all(File.find(**params))
            if not orphan_files:
                logger.info("No orphan file found in database.")
                return False

            # Reprocess each file
            logger.info(f"{len(orphan_files)} orphan files found in database")

            for orphan_file in orphan_files:
                logger.verbose(
                    f"Trying to find a Source for orphan file '{orphan_file.name}'"
                )

                match = MSEEDFileFactory.regex_filename.match(orphan_file.name)
                if match:
                    mdict = match.groupdict()
                    source = db_session.find_one(
                        Source.find(
                            mdict["network"],
                            mdict["station"],
                            mdict["location"],
                            mdict["channel"],
                        )
                    )
                    if source:
                        orphan_file.source = source
                        db_session.add(orphan_file)
                        logger.info(
                            f"The file '{orphan_file.name}' is now linked to the source '{source}'."
                        )
                    elif (
                        self._settings.ppsd_only_allowed_channels
                        and not self._settings.ppsd_is_allowed_channel(mdict["channel"])
                    ):
                        logger.warning(
                            f"Non-seismic channel are not supported: Deleting orphan file '{orphan_file.name}'."
                        )
                        db_session.delete(orphan_file)
                    elif delete and (
                        orphan_file.date_created is None
                        or (
                            orphan_file.date_created is not None
                            and pendulum.instance(orphan_file.date_created)
                            < pendulum.now().subtract(months=3)
                        )
                    ):
                        logger.warning(
                            f"No source found then deletion requested for the file '{orphan_file.name}'."
                        )
                        db_session.delete(orphan_file)
                    else:
                        logger.warning(
                            f"Unable to find a source for the file '{orphan_file.name}'. Please update metadata and try again."
                        )
                else:
                    logger.error(
                        f"Unable to extract a NSLC from the file name '{orphan_file.name}'."
                    )

            if not simulate:
                db_session.commit()
            else:
                logger.warning("Simulation mode. No changes will be saved in database!")
                db_session.rollback()

        # Close DB connection
        db_engine.close()

        return True

    # ENTRY POINT: PROCESS FILES #######################################################################################

    def process_nslc(
        self,
        year=None,
        network=None,
        station=None,
        location=None,
        channel=None,
        force=False,
        simulate=False,
        debug=False,
    ):
        """
        Load MSEED from disk (find by year + NSLC)

        :param year: Filter by year
        :param network: Filter by a network code
        :param station: Filter by a station code
        :param location: Filter by a location code
        :param channel: Filter by a channel code
        :param force: Force update of already processed files
        :param simulate: Enable the 'simulation' mode (do not alter the database)
        """
        logger.debug(
            f"SeedPsdWorkerDataMseed.process_nslc({year}, {network}, {station}, {location}, {channel}, {force}, {simulate}, {debug})"
        )

        # Initializing a list of MSEED files to process
        mseed_files = []

        # Initializing a list of station codes
        stations = station.split(",") if station else [None]

        # Initializing a list of location codes
        locations = location.split(",") if location else []

        # Initializing a list of channel codes
        channels = channel.split(",") if channel else [None]

        # Open a database connection
        db_engine = self.db_engine().connect()
        with self.db_session(db_engine) as db_session:
            # For each network with registered metadata
            for db_network in db_session.find_all(Network.find(code=network)):
                # Use network extended code if needed
                network_code = (
                    db_network.extended_code
                    if self._settings.data_extended
                    else db_network.code
                )

                # Use specific year(s)
                if year:
                    years = year.split(",")

                # Use all years of network activity
                elif db_network.end:
                    years = range(db_network.year, db_network.end.year + 1)

                # Use all years from network start to the current year
                else:
                    years = range(db_network.year, date_today().year + 1)

                # Build and search files inside paths
                for data_year in years:
                    for station_code in stations:
                        for channel_code in channels:
                            parent_path = MSEEDFilePathFactory.factory(
                                self._settings.data_path,
                                data_year,
                                network_code,
                                station_code,
                                channel_code,
                                self._settings.data_sds,
                            )
                            logger.info(
                                f"Searching MSEED files into '{parent_path}' ..."
                            )

                            for dirpath, _dirs, files in os.walk(
                                parent_path, followlinks=True
                            ):
                                for file in sorted(files):
                                    mseed_file = MSEEDFileFactory.from_path(
                                        os.path.join(dirpath, file)
                                    )
                                    if mseed_file:
                                        logger.info(
                                            f"Loading MSEED file: '{mseed_file.file_name}'"
                                        )
                                        if not locations or (
                                            locations
                                            and mseed_file.location in locations
                                        ):
                                            mseed_files.append(mseed_file)
                                        else:
                                            logger.debug(
                                                f"Ignoring '{mseed_file.file_name}': wrong location code"
                                            )

            # Close DB session
            db_session.rollback()

        # Close DB connection
        db_engine.close()

        # Processing all found files
        if self._parallel:
            self.pre_fork()
            with Pool(self.n_cores, self.post_fork()) as pool:
                pool.map(
                    lambda mseed_file: self.process_file(
                        mseed_file, force=force, simulate=simulate, debug=debug
                    ),
                    mseed_files,
                )
        else:
            for mseed_file in mseed_files:
                self.process_file(
                    mseed_file, force=force, simulate=simulate, debug=debug
                )

    def process_path(self, path, force=False, simulate=False, debug=False):
        """
        Load MSEED files from a path
        :param path: The path to load
        :param force: Force update of already processed files
        :param simulate: Enable the 'simulation' mode (do not alter the database)
        """
        logger.debug(
            f"SeedPsdWorkerDataMseed.process_path({path}, {force}, {simulate}, {debug})"
        )
        if os.path.isdir(path):
            logger.info(f"Processing directory '{path}'...")

            if self._parallel:
                paths = set()
                for dirpath, _dirs, files in os.walk(path, followlinks=True):
                    for filename in sorted(files):
                        logger.debug(f"Find file: '{filename}'")
                        paths.add(os.path.join(dirpath, filename))

                self.pre_fork()
                with Pool(self.n_cores, self.post_fork()) as pool:
                    pool.map(
                        lambda file: self.process_file(
                            file, force=force, simulate=simulate, debug=debug
                        ),
                        paths,
                    )
                    return None

            else:
                for dirpath, _dirs, files in os.walk(path, followlinks=True):
                    for filename in sorted(files):
                        logger.debug(f"Find file: '{filename}'")
                        self.process_file(
                            os.path.join(dirpath, filename),
                            force=force,
                            simulate=simulate,
                            debug=debug,
                        )
                return None
        else:
            return self.process_file(path, force=force, simulate=simulate, debug=debug)

    def process_files(self, files, force=False, simulate=False, debug=False):
        """
        Load MSEED files from a list of files
        :param files: The path to load
        :param force: Force update of already processed files
        :param simulate: Enable the 'simulation' mode (do not alter the database)
        """
        logger.debug(
            f"SeedPsdWorkerDataMseed.process_files([files], {force}, {simulate}, {debug})"
        )
        if self._parallel:
            self.pre_fork()
            with Pool(self.n_cores, self.post_fork()) as pool:
                pool.map(
                    lambda file: self.process_file(
                        str(file).strip(), force=force, simulate=simulate, debug=debug
                    ),
                    files,
                )

        else:
            for file in sorted(files):
                self.process_file(
                    str(file).strip(), force=force, simulate=simulate, debug=debug
                )

    def process_file(self, mseed, force=False, simulate=False, debug=False):
        """
        Load a MSEED files
        :param mseed: The file (path or MSEEDFile instance)
        :param force: Force update of already processed files
        :param simulate: Enable the 'simulation' mode (do not alter the database)
        """
        logger.debug(
            f"SeedPsdWorkerDataMseed.process_file({mseed}, {force}, {simulate}, {debug})"
        )
        try:
            # Initialize a container for the Mseed file
            if isinstance(mseed, str):
                mseed_file = MSEEDFileFactory.from_path(
                    mseed,
                    parent_path=self._settings.data_path,
                    sds=self._settings.data_sds,
                    extended=self._settings.data_extended,
                    raise_error=True,
                )
            elif isinstance(mseed, MSEEDFile):
                mseed_file = mseed
            elif debug:
                msg = f"Unsupported type {type(mseed)}"
                raise Exception(msg)
            else:
                mseed_file = None

            # Do not continue if we don't have a MSEEDFILE instance
            if not mseed_file:
                logger.debug(f"{mseed} is not an instance of MSEEDFile")
                return False

            # Do not continue if the channel is not seismic
            if (
                self._settings.ppsd_only_allowed_channels
                and not self._settings.ppsd_is_allowed_channel(mseed_file.channel)
            ):
                logger.warning(
                    f"{mseed_file.file_name} : Non-seismic channels are not supported. Ignoring this file."
                )
                return False

            # Do not continue if the file is not readable
            if not mseed_file.is_readable:
                logger.warning(
                    f"{mseed_file.file_name} : The file is not readable. Ignoring this file."
                )
                return False

            # Init DB session
            db_engine_shared = self.db_engine().connect()
            db_session = self.db_session(db_engine_shared)

            # Get or create the file in DB
            file = db_session.find_one(File.find(name=mseed_file.file_name))
            if not file:
                file = File(name=mseed_file.file_name, date=mseed_file.date)
                db_session.add(file)

            # Get the source
            source = db_session.find_one(
                Source.find(
                    mseed_file.network,
                    mseed_file.station,
                    mseed_file.location,
                    mseed_file.channel,
                    mseed_file.date,
                    mseed_file.date,
                )
            )

            # If DB is not up-to-date, do not process the file
            if not source:
                logger.warning(
                    f"{mseed_file.file_name} : Unknown data source. Please update the metadata into SeedPSD and reprocess this file."
                )

                # Update the properties of the file
                file.date_processed = datetime_now()
                file.processed = False
                file.available_metadata = False

                # Save in database
                if not simulate:
                    db_session.commit()
                else:
                    logger.warning(
                        f"{mseed_file.file_name} : Simulation mode. No changes will be saved in database!"
                    )
                    db_session.rollback()

                # Close DB session
                db_session.close()
                db_engine_shared.close()

            # Process a file only when needed/asked
            elif force or not file.processed or not file.available_metadata:
                # Get metadata
                inventory = self._get_inventory(
                    network=mseed_file.network,
                    station=mseed_file.station,
                    location=mseed_file.location,
                    channel=mseed_file.channel,
                    level="response",
                )

                # No metadata found
                if not inventory:
                    logger.warning(
                        f"{mseed_file.file_name} : No metadata available for this file"
                    )

                    # Update the properties of the file
                    file.source_id = source.id
                    file.date_processed = datetime_now()
                    file.processed = True
                    file.available_metadata = False

                    # Save in database
                    if not simulate:
                        db_session.commit()
                    else:
                        logger.warning(
                            f"{mseed_file.file_name} : Simulation mode. No changes will be saved in database!"
                        )
                        db_session.rollback()

                    # Close DB session
                    db_session.close()
                    db_engine_shared.close()

                # There are metadata for this file
                else:
                    # Detach objects from shared DB session
                    tenant_schema = source.network.schema
                    db_session.expunge_all()

                    # Close shared DB session
                    db_session.rollback()
                    db_session.close()
                    db_engine_shared.close()

                    # Read the MSEED data
                    mseed_data = mseed_file.read()

                    # Unable to read the file
                    if not mseed_data:
                        logger.warning(
                            f"{mseed_file.file_name} : Unable to read the file"
                        )
                        msg = f"Unable to read the '{mseed_file.file_name}' file"
                        raise Exception(msg)

                    # The file is readable
                    nb_streams = len(mseed_data)

                    # Merge overlapped streams
                    logger.debug(f"{file.name} : Loading {nb_streams} stream(s).")
                    if nb_streams > 1:
                        mseed_data._cleanup()
                        nb_streams_after = len(mseed_data)
                        if nb_streams_after != nb_streams:
                            logger.warning(
                                f"{file.name} : Cleaning overlaps ({nb_streams_after} stream(s) remaining)."
                            )

                    # Use the DB schema dedicated to this tenant
                    db_engine_tenant = self.db_engine(tenant_schema).connect()
                    try:
                        with self.db_session(db_engine_tenant) as db_session_tenant:
                            try:
                                # Attach shared DB session objects to this tenant DB session
                                db_session_tenant.merge(source)

                                # Update the properties of the file
                                file = db_session_tenant.find_one(
                                    File.find(name=mseed_file.file_name)
                                )
                                if not file:
                                    file = File(
                                        name=mseed_file.file_name, date=mseed_file.date
                                    )
                                    db_session_tenant.add(file)
                                file.source_id = source.id
                                file.date_processed = datetime_now()
                                file.processed = True
                                file.available_metadata = True
                                db_session_tenant.flush()

                                # Delete existing data
                                db_session_tenant.execute(Stream.delete(file=file))

                                # Loading epochs
                                epochs = db_session_tenant.find_all(
                                    Epoch.find(source=source)
                                )
                                epochs = set(epochs)

                                # Parse each stream
                                for data in mseed_data:
                                    logger.info(
                                        f"{file.name} : Reading stream from {data.stats.starttime} to {data.stats.endtime} ({data.stats.sampling_rate}Hz | {data.stats.npts} samples)"
                                    )

                                    # Create a PPSD object from MSEED
                                    try:
                                        ppsd = PPSDFactory.factory(
                                            data.stats,
                                            inventory,
                                            use_defaults=self._settings.ppsd_use_defaults,
                                            check_trace=True,
                                        )

                                        with warnings.catch_warnings(
                                            record=True
                                        ) as catched_warnings:
                                            ppsd.add(data)

                                        for catched_warning in catched_warnings:
                                            logger.debug(catched_warning)

                                        # self.debug_psd_values(ppsd)
                                    except SkippedTraceError as e:
                                        logger.debug(e)
                                        continue
                                    except Exception:
                                        logger.exception(
                                            f"{file.name} : Unable to load PPSD traces and gaps from file."
                                        )
                                        raise

                                    # Creates a Stream object
                                    stream = Stream(
                                        data.stats.starttime,
                                        data.stats.endtime,
                                        data.stats.sampling_rate,
                                    )
                                    stream.source_id = source.id
                                    stream.file_id = file.id

                                    # Link a stream to the epoch
                                    epoch_found = False
                                    for epoch in epochs:
                                        if epoch.start < stream.end and (
                                            not epoch.end or epoch.end > stream.start
                                        ):
                                            stream.epoch_id = epoch.id
                                            stream.valid = True
                                            epoch_found = True
                                            break
                                    if not epoch_found:
                                        logger.warning(
                                            f"{file.name} : Unable to find an epoch related to the stream '{stream}'"
                                        )
                                        stream.epoch_id = None
                                        stream.valid = False

                                    # Save the stream
                                    db_session_tenant.add(stream)
                                    db_session_tenant.flush()

                                    # Extracts gaps only from PPSD
                                    for time_gap in ppsd.times_gaps:
                                        if time_gap[0] < time_gap[1]:
                                            gap = Gap(time_gap[0], time_gap[1])
                                            gap.stream_id = stream.id
                                            db_session_tenant.add(gap)
                                        else:
                                            logger.warning(
                                                f"{file.name} : Ignoring overlap between {time_gap[1]} and {time_gap[0]}"
                                            )
                                    db_session_tenant.flush()

                                    # Extracts times_processed and binned_psds from PPSD
                                    for time_processed, psd_values in zip(
                                        ppsd.times_processed,
                                        ppsd.psd_values,
                                        strict=False,
                                    ):
                                        trace = Trace(time_processed)
                                        trace.data = np.asarray(psd_values, dtype=float)
                                        trace.stream_id = stream.id
                                        db_session_tenant.add(trace)
                                    db_session_tenant.flush()

                                # Save DB changes
                                if not simulate:
                                    db_session_tenant.commit()
                                else:
                                    logger.warning(
                                        f"{file.name} : Simulation mode. No changes will be saved in database!"
                                    )
                                    db_session_tenant.rollback()
                            except Exception:
                                db_session_tenant.rollback()
                                raise

                        # Close the DB connection
                        db_engine_tenant.close()
                    except Exception:
                        db_engine_tenant.close()
                        raise

            # Already processed file
            else:
                logger.warning("%s : Already processed. Ignoring this file.", file.name)

                # Close DB session
                db_session.close()
                db_engine_shared.close()
        except (URLError, TimeoutError):
            logger.warning("%s : MSEEDFile creation failed due to network error", mseed)
        except XMLSyntaxError:
            logger.warning(
                "%s : Getting inventory failed for file. ws_tation not responding",
                mseed,
            )
            db_session.close()
        except IntegrityError:
            logger.exception("Integrity Error raised for file : %s", mseed)
            db_session.close()

            db_engine_shared = self.db_engine().connect()
            with self.db_session(db_engine_shared) as db_session:
                file = db_session.find_one(File.find(name=mseed_file.file_name))
                file.processed = False
                db_session.commit()
        except Exception:
            logger.exception("File processing failed : %s", mseed)
            if debug:
                raise
