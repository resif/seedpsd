import copy
import warnings
from collections import OrderedDict

import verboselogs
from multiprocess.pool import Pool
from obspy.core.inventory.channel import Channel
from obspy.core.inventory.response import (
    InstrumentPolynomial,
    InstrumentSensitivity,
    PolesZerosResponseStage,
)

from seedpsd.models.db import create_tenant_schema, create_tenant_tables
from seedpsd.models.db.shared.network import Network
from seedpsd.models.db.shared.source import Source
from seedpsd.models.db.tenant.epoch import Epoch
from seedpsd.tools.common import is_temporary_network, to_date

from . import SeedPsdWorker

logger = verboselogs.VerboseLogger(__name__)


class SeedPsdWorkerMetadata(SeedPsdWorker):
    """
    SeedPSD Worker class for Epoch
    """

    def __init__(self, settings, db_engine=None, parallel=False, max_cpu=None):
        logger.debug(
            f"SeedPsdWorkerMetadata.__init__({settings}, {db_engine}, {parallel}, {max_cpu})"
        )
        super().__init__(settings, db_engine, parallel, max_cpu)

    # ENTRY POINT: UPDATE ##############################################################################################

    def update(
        self,
        network_code=None,
        station_code=None,
        location_code=None,
        channel_code=None,
        simulate=False,
    ):
        """
        Load/Update epochs from the inventory
        :param network_code: Filter epochs by a network code
        :param station_code: Filter epochs by a station code
        :param location_code: Filter epochs by a location code
        :param channel_code: Filter epochs by a channel code
        :param simulate: Enable the 'simulation' mode (do not alter the database)
        """
        logger.debug(
            f"SeedPsdWorkerMetadata.update({network_code}, {station_code}, {location_code}, {channel_code}, {simulate})"
        )

        # Get the list of stations
        inventory = self._get_inventory(
            network=network_code,
            station=station_code,
            location=location_code,
            channel=channel_code,
            level="station",
        )
        if inventory:
            networks_stations = set()
            network_schemas = set()

            logger.info("Initializing database schemas...")
            db_engine_shared = self.db_engine().connect()
            with self.db_session(db_engine_shared) as db_session:
                for network in inventory.networks:
                    # Compute tenant schema name
                    if is_temporary_network(network.code):
                        tenant_schema = (
                            f"network_{network.code}{to_date(network.start_date).year}"
                        )
                    else:
                        tenant_schema = f"network_{network.code}"

                    # Find or create network in database
                    db_network = Network.factory(
                        db_session, network.code, network.start_date, network.end_date
                    )

                    # Create DB schema only for new networks
                    if not db_network.schema_exists:
                        db_session.add(db_network)
                        db_session.flush()

                        # Track schema name
                        network_schemas.add(tenant_schema)
                        create_tenant_schema(db_session, tenant_schema)
                        db_session.commit()

                    # Iterate over stations
                    for station in network:
                        networks_stations.add((network.code, station.code))

                # Close DB session
                if network_schemas:
                    db_session.commit()
                else:
                    db_session.rollback()

            # Closing DB connection
            db_engine_shared.close()

            # Create DB structure only for new networks
            for schema in network_schemas:
                db_engine_tenant = self.db_engine(schema)
                with self.db_session(db_engine_tenant) as db_session:
                    logger.verbose(
                        f"Initializing database structure for schema '{schema}'"
                    )
                    create_tenant_tables(db_engine_tenant, schema)
                    db_session.commit()
                db_engine_tenant.dispose()

            # Process stations
            if self._parallel:
                self.pre_fork()
                with Pool(self.n_cores, self.post_fork()) as pool:
                    pool.map(
                        lambda net_sta: self._update_station(
                            net_sta[0],
                            net_sta[1],
                            location_code,
                            channel_code,
                            simulate=simulate,
                        ),
                        networks_stations,
                    )
            else:
                for n_code, s_code in networks_stations:
                    self._update_station(
                        n_code, s_code, location_code, channel_code, simulate
                    )
        else:
            logger.warning(
                "No metadata found for %s %s %s %s!",
                network_code,
                station_code,
                location_code,
                channel_code,
            )

    def _update_station(
        self,
        network_code,
        station_code,
        location_code=None,
        channel_code=None,
        simulate=False,
    ):
        """
        Load/Update epochs from the inventory
        :param network_code: Filter epochs by a network code
        :param station_code: Filter epochs by a station code
        :param location_code: Filter epochs by a location code
        :param channel_code: Filter epochs by a channel code
        :param simulate: Enable the 'simulation' mode (do not alter the database)
        """
        logger.debug(
            f"SeedPsdWorkerMetadata._update_station({network_code}, {station_code}, {location_code}, {channel_code}, {simulate})"
        )
        # Get the list of stations
        inventory = self._get_inventory(
            network=network_code,
            station=station_code,
            location=location_code,
            channel=channel_code,
            level="response",
        )
        if inventory:
            for network in inventory.networks:
                # Compute tenant schema name
                if is_temporary_network(network.code):
                    tenant_schema = (
                        f"network_{network.code}{to_date(network.start_date).year}"
                    )
                else:
                    tenant_schema = f"network_{network.code}"

                # Use the DB schema dedicated to this tenant
                db_engine_tenant = self.db_engine(tenant_schema).connect()
                try:
                    with self.db_session(db_engine_tenant) as db_session:
                        # Get or create the network
                        db_network = Network.factory(
                            db_session,
                            network.code,
                            network.start_date,
                            network.end_date,
                        )
                        db_session.flush()

                        # Initialize a dict to store epochs by source
                        epochs_by_source = {}

                        # Initialize a set to store skipped channels (non-seismic)
                        skipped_channels = set()

                        # Analyze each station from XML
                        for station in network.stations:
                            logger.info(
                                f"Reading epochs for station '{network.code}.{station.code}' (from {station.start_date} to {station.end_date or 'now'})"
                            )

                            # Analyze each channel from XML
                            for channel in station.channels:
                                # Ignore non-seismic channels
                                if (
                                    self._settings.ppsd_only_allowed_channels
                                    and not self._settings.ppsd_is_allowed_channel(
                                        channel.code
                                    )
                                ):
                                    logger.verbose(
                                        f"{network.code}.{station.code}.{channel.location_code}.{channel.code}: Ignoring non-seismic channel ({channel.start_date} - {channel.end_date or 'now'})"
                                    )
                                    skipped_channels.add(
                                        f"{network.code}.{station.code}.{channel.location_code}.{channel.code}"
                                    )

                                else:
                                    logger.verbose(
                                        f"{network.code}.{station.code}.{channel.location_code}.{channel.code}: Reading metadata from {channel.start_date} to {channel.end_date or 'now'}"
                                    )

                                    # Get the source
                                    source = Source.factory(
                                        db_session,
                                        db_network,
                                        station.code,
                                        channel.location_code,
                                        channel.code,
                                        channel.start_date,
                                        channel.end_date,
                                    )
                                    if source.nslc not in epochs_by_source:
                                        epochs_by_source[source.nslc] = {
                                            "source": source,
                                            "epochs": set(),
                                            "new_epochs": set(),
                                            "added": 0,
                                            "updated": 0,
                                            "deleted": 0,
                                            "total": 0,
                                        }

                                    # Find the epoch
                                    epoch = db_session.find_first(
                                        Epoch.find(
                                            source=source, start=channel.start_date
                                        )
                                    )

                                    # Update the existing epoch
                                    if epoch:
                                        updated = epoch.update(
                                            end=channel.end_date,
                                            instrument=self._channel_to_dict(channel),
                                        )
                                        if updated:
                                            logger.verbose(
                                                f"{source.nslc}: Updating epoch '{epoch.start} - {epoch.end or 'now'}'"
                                            )
                                            epochs_by_source[source.nslc][
                                                "updated"
                                            ] += 1
                                        else:
                                            logger.verbose(
                                                f"{source.nslc}: Found epoch '{epoch.start} - {epoch.end or 'now'}'"
                                            )

                                        epochs_by_source[source.nslc]["epochs"].add(
                                            epoch
                                        )
                                        epochs_by_source[source.nslc]["total"] += 1

                                    # Or create a new epoch
                                    else:
                                        epoch = Epoch(
                                            channel.start_date,
                                            channel.end_date,
                                            self._channel_to_dict(channel),
                                        )
                                        logger.verbose(
                                            f"{source.nslc}: Creating epoch '{epoch.start} - {epoch.end or 'now'}'"
                                        )
                                        epochs_by_source[source.nslc]["new_epochs"].add(
                                            epoch
                                        )
                                        epochs_by_source[source.nslc]["added"] += 1
                                        epochs_by_source[source.nslc]["total"] += 1

                        # Analyze epochs
                        for nslc in epochs_by_source:
                            # Clean obsolete epochs
                            for epoch in epochs_by_source[nslc]["source"].epochs:
                                if epoch not in epochs_by_source[nslc]["epochs"]:
                                    logger.verbose(
                                        f"{nslc}: Deleting epoch '{epoch.start} - {epoch.end or 'now'}'"
                                    )
                                    epochs_by_source[nslc]["deleted"] += 1
                                    db_session.delete(epoch)

                            # Add created epochs
                            for epoch in epochs_by_source[nslc]["new_epochs"]:
                                logger.verbose(
                                    f"{nslc}: Inserting epoch '{epoch.start} - {epoch.end or 'now'}'"
                                )
                                epoch.source = epochs_by_source[nslc]["source"]
                                db_session.add(epoch)

                            # Analyze epochs
                            logger.info(
                                f"{nslc}: {epochs_by_source[nslc]['total']} epoch(s) ({epochs_by_source[nslc]['added']} added, {epochs_by_source[nslc]['updated']} updated, {epochs_by_source[nslc]['deleted']} deleted)"
                            )

                        # Listing excluded channels
                        for skipped_channel in skipped_channels:
                            logger.warning(f"{skipped_channel}: Skipped (non-seismic)")

                        # Save in database
                        if not simulate:
                            db_session.commit()
                        else:
                            logger.warning(
                                "Simulation mode. No changes will be saved in database!"
                            )
                            db_session.rollback()

                    # Close connection
                    db_engine_tenant.close()
                except Exception:
                    db_session.rollback()
                    db_engine_tenant.close()
                    raise

    # OBSPY INVENTORY RESPONSE SERIALIZERS #############################################################################
    ####################################################################################################################

    @staticmethod
    def _obspy_to_dict(obj, excludes=()):
        """
        Convert an ObsPy inventory/response object into a dict
        :param obj: The object to convert
        :param excludes: The list of attributes to excludes
        :rtype: dict
        """
        logger.debug(f"SeedPsdWorkerMetadata._obspy_to_dict({obj}, {excludes})")
        d = copy.deepcopy(obj).__dict__
        for k in list(d.keys()):
            if k.startswith("_") or "description" in k or d[k] is None or k in excludes:
                # logger.debug(f">> Removing {k}")
                del d[k]

        return d

    @staticmethod
    def _instrument_polynomial_to_dict(instrument_polynomial):
        """
        Convert an InstrumentPolynomial object into a dict
        :param instrument_polynomial: The object to convert
        :rtype: OrderedDict
        """
        logger.debug(
            f"SeedPsdWorkerMetadata._instrument_polynomial_to_dict({instrument_polynomial})"
        )
        if not isinstance(instrument_polynomial, InstrumentPolynomial):
            msg = f"'{type(instrument_polynomial)}' is not supported by _instrument_polynomial_to_dict"
            raise Exception(msg)

        excludes = (
            "resource_id",
            "name",
        )
        polynomial = SeedPsdWorkerMetadata._obspy_to_dict(
            instrument_polynomial, excludes
        )

        if instrument_polynomial.approximation_type is not None:
            polynomial["approximation_type"] = instrument_polynomial.approximation_type

        return OrderedDict(sorted(polynomial.items(), key=lambda obj: obj[0]))

    @staticmethod
    def _instrument_sensitivity_to_dict(instrument_sensitivity):
        """
        Convert an InstrumentSensitivity object into a dict
        :param instrument_sensitivity: The object to convert
        :rtype: OrderedDict
        """
        logger.debug(
            f"SeedPsdWorkerMetadata._instrument_sensitivity_to_dict({instrument_sensitivity})"
        )
        if not isinstance(instrument_sensitivity, InstrumentSensitivity):
            msg = f"'{type(instrument_sensitivity)}' is not supported by _instrument_sensitivity_to_dict"
            raise Exception(msg)

        sensitivity = SeedPsdWorkerMetadata._obspy_to_dict(instrument_sensitivity)
        return OrderedDict(sorted(sensitivity.items(), key=lambda obj: obj[0]))

    @staticmethod
    def _poles_zeros_to_dict(poles_zeros):
        """
        Convert an PolesZerosResponseStage object into a dict
        :param poles_zeros: The object to convert
        :rtype: OrderedDict
        """
        logger.debug(f"SeedPsdWorkerMetadata._poles_zeros_to_dict({poles_zeros})")
        if not isinstance(poles_zeros, PolesZerosResponseStage):
            msg = f"'{type(poles_zeros)}' is not supported by _poles_zeros_to_dict"
            raise Exception(msg)

        excludes = (
            "resource_id",
            "resource_id2",
            "name",
            "stage_sequence_number",
        )
        paz = SeedPsdWorkerMetadata._obspy_to_dict(poles_zeros, excludes)

        if poles_zeros.normalization_frequency is not None:
            paz["normalization_frequency"] = poles_zeros.normalization_frequency

        if poles_zeros.pz_transfer_function_type is not None:
            paz["pz_transfer_function_type"] = poles_zeros.pz_transfer_function_type

        paz["zeros"] = [str(zero) for zero in poles_zeros.zeros]
        paz["poles"] = [str(pole) for pole in poles_zeros.poles]

        return OrderedDict(sorted(paz.items(), key=lambda obj: obj[0]))

    @staticmethod
    def _channel_to_dict(channel):
        """
        Convert a Channel object into a dict
        :param channel: The object to convert
        :rtype: OrderedDict
        """
        logger.debug(f"SeedPsdWorkerMetadata._channel_to_dict({channel})")
        if not isinstance(channel, Channel):
            msg = f"'{type(channel)}' is not supported by _channel_to_dict"
            raise Exception(msg)

        excludes = (
            "start_date",
            "end_date",
            "restricted_status",
            "data_availability",
            "response",
            "comments",
            "external_references",
            "types",
            "sensor",
            "pre_amplifier",
            "data_logger",
            "response",
        )
        cha = SeedPsdWorkerMetadata._obspy_to_dict(channel, excludes)

        if channel.azimuth is not None:
            cha["azimuth"] = channel.azimuth

        if channel.dip is not None:
            cha["dip"] = channel.dip

        if channel.water_level is not None:
            cha["water_level"] = channel.water_level

        if channel.sample_rate is not None:
            cha["sample_rate"] = channel.sample_rate

        if channel.clock_drift_in_seconds_per_sample is not None:
            cha["clock_drift_in_seconds_per_sample"] = (
                channel.clock_drift_in_seconds_per_sample
            )

        if channel.response is not None:
            response = {}
            if channel.response.instrument_sensitivity:
                response["sensitivity"] = (
                    SeedPsdWorkerMetadata._instrument_sensitivity_to_dict(
                        channel.response.instrument_sensitivity
                    )
                )

            if channel.response.instrument_polynomial:
                response["polynomial"] = (
                    SeedPsdWorkerMetadata._instrument_polynomial_to_dict(
                        channel.response.instrument_polynomial
                    )
                )

            try:
                with warnings.catch_warnings(record=True) as catched_warnings:
                    paz = channel.response.get_paz()
                    response["poles_zeros"] = (
                        SeedPsdWorkerMetadata._poles_zeros_to_dict(paz)
                    )

                for catched_warning in catched_warnings:
                    logger.debug(catched_warning)

            except Exception:
                pass

            if response:
                cha["response"] = OrderedDict(
                    sorted(response.items(), key=lambda obj: obj[0])
                )
        else:
            msg = "No response found!"
            raise Exception(msg)

        return OrderedDict(sorted(cha.items(), key=lambda obj: obj[0]))
