import bisect
import os
from datetime import timedelta

import verboselogs
from obspy.core import Stats
from sqlalchemy import and_

from seedpsd.errors import NoDataError
from seedpsd.models.db.shared.source import Source
from seedpsd.models.db.tenant.gap import Gap
from seedpsd.models.db.tenant.stream import Stream
from seedpsd.models.db.tenant.trace import Trace
from seedpsd.tools.common import datetime_today, to_datetime
from seedpsd.tools.ppsd import PPSDFactory

from . import SeedPsdWorker

logger = verboselogs.VerboseLogger(__name__)


class SeedPsdWorkerPPSD(SeedPsdWorker):
    """
    SeedPSD Worker class for PPSD
    """

    def __init__(self, settings=None, db_engine=None):
        logger.debug(f"SeedPsdWorkerPPSD.__init__({settings}, {db_engine})")
        super().__init__(settings, db_engine, parallel=False)

    # LOAD FROM DATABASE
    ####################################################################################################################

    def get_sources(self, network, station, location, channel, start_time, end_time):
        """
        Get all sources
        :param network: The network code
        :param station: The station code
        :param location: The location code
        :param channel: The channel code
        :param start_time: The start datetime
        :param end_time: The end datetime
        """
        logger.debug(
            f"SeedPsdWorkerPPSD.get_sources({network}, {station}, {location}, {channel}, {start_time}, {end_time})"
        )

        # Get the source
        db_engine = self.db_engine().connect()
        with self.db_session() as db_session:
            sources = db_session.find_all(
                Source.find(network, station, location, channel, start_time, end_time)
            )
        db_engine.close()

        if not sources:
            logger.info(
                f"Unable to find a source for {network}.{station}.{location}.{channel}"
            )
            msg = (
                f"Unable to find a source for {network}.{station}.{location}.{channel}"
            )
            raise NoDataError(msg)

        return sources

    def from_database(
        self, source, start_time, end_time, with_outdated=False, mode="split"
    ):
        """
        Build PPSD for a source from database
        :param source: The Source object (or a list of)
        :param start_time: The start datetime
        :param end_time: The end datetime
        :param with_outdated: Do not use only valid streams (default=False)
        :param mode: The algorithm mode (default='split')
        """
        logger.debug(
            f"SeedPsdWorkerPPSD.from_database({source}, {start_time}, {end_time}, {with_outdated}, {mode})"
        )

        if isinstance(source, (list, tuple)):
            if len(source) == 1:
                source = source[0]
            else:
                msg = "PPSD building is allowed only for a single Source object"
                raise Exception(msg)
        elif not isinstance(source, Source):
            msg = "PPSD building is allowed only for a single Source object"
            raise Exception(msg)

        if mode == "split":
            return self._from_database_split(
                source, start_time, end_time, with_outdated
            )
        if mode == "grouped":
            return self._from_database_grouped(
                source, start_time, end_time, with_outdated
            )
        msg = f"Unknown mode '{mode}' for SeedPsdWorkerPPSD.from_database()"
        raise Exception(msg)

    def _from_database_split(self, source, start_time, end_time, with_outdated=False):
        """
        Build a PPSD instance from database (using 'split' algorithm)
        :param source: The source
        :param start_time: The start datetime
        :param end_time: The end datetime
        :param with_outdated: Do not use only valid streams (default=False)
        :param ppsds: List of PPSD objects (empty by default)
        """
        logger.debug(
            f"SeedPsdWorkerPPSD._from_database_split({source}, {start_time}, {end_time}, {with_outdated})"
        )

        # Get inventory
        inventory = self._get_inventory(
            network=source.network.code,
            station=source.station,
            location=source.location,
            channel=source.channel,
            level="response",
        )

        # Initialize PPSD
        ppsds = []
        ppsd = None
        date_processed = None
        dirty = False

        # Use the DB schema dedicated to this tenant
        db_engine = self.db_engine(source.network.schema).connect()
        with self.db_session(db_engine) as db_session:
            # Get the streams
            where_params = [
                Stream.start <= to_datetime(end_time),
                Stream.end >= to_datetime(start_time),
                Stream.source == source,
            ]
            if not with_outdated:
                where_params.append(Stream.valid == True)  # noqa: E712
            stream_statement = (
                Stream.select().where(and_(*where_params)).order_by(Stream.start)
            )

            streams = db_session.find_all(stream_statement)
            if not streams:
                logger.info(f"Unable to find Streams for {source.nslc}")
                msg = f"Unable to find Streams for {source.nslc}"
                raise NoDataError(msg)

            # For each stream
            for stream in streams:
                # Start a new PPSD object when the sampling rate changes
                if ppsd is not None and ppsd.sampling_rate != stream.sampling_rate:
                    if date_processed:
                        ppsd.seedpsd_date_processed = date_processed
                    ppsd.seedpsd_dirty = dirty
                    ppsds.append(ppsd)

                    ppsd = None
                    date_processed = None
                    dirty = False

                # Initialize a new PPSD object if needed
                if ppsd is None:
                    stats = Stats(
                        {
                            "network": source.network.code,
                            "station": source.station,
                            "location": source.location or "",
                            "channel": source.channel,
                            "sampling_rate": stream.sampling_rate,
                        }
                    )
                    ppsd = PPSDFactory.factory(
                        stats, inventory, use_defaults=self._settings.ppsd_use_defaults
                    )

                # Add the stream times to the PPSD instance
                ppsd._times_data.extend([(stream.start_utc.ns, stream.end_utc.ns)])

                # Add gaps
                gaps = db_session.find_all(Gap.find(stream=stream))
                ppsd._times_gaps.extend(
                    [(gap.start_utc.ns, gap.end_utc.ns) for gap in gaps]
                )

                # Add traces
                traces = db_session.find_all(Trace.find(stream=stream))
                for trace in traces:
                    t = trace.timestamp_utc.ns
                    ind = bisect.bisect(ppsd._times_processed, t)
                    ppsd._times_processed.insert(ind, t)
                    ppsd._binned_psds.insert(ind, trace.data)

                # Compute last update
                if date_processed:
                    date_processed = max(date_processed, stream.file.date_processed)
                else:
                    date_processed = stream.file.date_processed

                # Check outdated calculation
                if not stream.valid:
                    dirty = True

            # Add the last PPSD object to the returned list
            if ppsd is not None:
                if date_processed:
                    ppsd.seedpsd_date_processed = date_processed
                ppsd.seedpsd_dirty = dirty
                ppsds.append(ppsd)

            # Close DB session
            db_session.rollback()

        # Close DB connection
        db_engine.close()

        return ppsds

    def _from_database_grouped(self, source, start_time, end_time, with_outdated=False):
        """
        Build a PPSD instance from database (using 'grouped' algorithm)
        :param source: The source
        :param start_time: The start datetime
        :param end_time: The end datetime
        :param with_outdated: Do not use only valid streams (default=False)
        """
        logger.debug(
            f"SeedPsdWorkerPPSD._from_database_grouped({source}, {start_time}, {end_time}, {with_outdated})"
        )

        # Get inventory
        inventory = self._get_inventory(
            network=source.network.code,
            station=source.station,
            location=source.location,
            channel=source.channel,
            level="response",
        )
        # TODO : Use 'start' and 'end' parameters to restrict requested metadata on a specific time range
        if not inventory:
            logger.warning("Unable to find metadata for %s", source.nslc)
            msg = f"Unable to find metadata for {source.nslc}"
            raise NoDataError(msg)

        # Use the DB schema dedicated to this tenant
        db_engine = self.db_engine(source.network.schema).connect()
        with self.db_session(db_engine) as db_session:
            # Get the streams
            where_params = [
                Stream.start <= to_datetime(end_time),
                Stream.end >= to_datetime(start_time),
                Stream.source == source,
            ]
            if not with_outdated:
                where_params.append(Stream.valid == True)  # noqa: E712
            stream_statement = (
                Stream.select().where(and_(*where_params)).order_by(Stream.start)
            )

            streams = db_session.find_all(stream_statement)
            if not streams:
                logger.info(f"Unable to find Streams for {source.nslc}")
                msg = f"Unable to find Streams for {source.nslc}"
                raise NoDataError(msg)

            # Sort by sampling rate
            sampling_rates = {}
            for stream in streams:
                if stream.sampling_rate not in sampling_rates:
                    sampling_rates[stream.sampling_rate] = {
                        "stream_ids": [],
                        "times_data": [],
                    }
                sampling_rates[stream.sampling_rate]["stream_ids"].append(stream.id)
                sampling_rates[stream.sampling_rate]["times_data"].append(
                    (stream.start_utc.ns, stream.end_utc.ns)
                )

            # For each sampling rate
            ppsds = []
            for sampling_rate in sampling_rates:
                # Initialize a new PPSD instance
                stats = Stats(
                    {
                        "network": source.network.code,
                        "station": source.station,
                        "location": source.location or "",
                        "channel": source.channel,
                        "sampling_rate": sampling_rate,
                    }
                )
                ppsd = PPSDFactory.factory(
                    stats, inventory, use_defaults=self._settings.ppsd_use_defaults
                )

                # Add the stream times to the PPSD instance
                ppsd._times_data.extend(sampling_rates[sampling_rate]["times_data"])

                # Add gaps
                gaps = db_session.find_all(
                    Gap.find(stream=sampling_rates[sampling_rate]["stream_ids"])
                )
                ppsd._times_gaps.extend(
                    [(gap.start_utc.ns, gap.end_utc.ns) for gap in gaps]
                )

                # Add traces
                traces = db_session.find_all(
                    Trace.find(stream=sampling_rates[sampling_rate]["stream_ids"])
                )
                for trace in traces:
                    t = trace.timestamp_utc.ns
                    ind = bisect.bisect(ppsd._times_processed, t)
                    ppsd._times_processed.insert(ind, t)
                    ppsd._binned_psds.insert(ind, trace.data)

                ppsds.append(ppsd)

            # Close DB session
            db_session.rollback()

        # Close DB connection
        db_engine.close()

        return ppsds

    # LOAD FROM NPZ
    ####################################################################################################################

    # SAVE TO NPZ
    ####################################################################################################################

    @staticmethod
    def save_to_npz(ppsd, file_name, file_path="/tmp"):
        """
        Save a PPSD object into a NPZ file
        :param ppsd: The PPSD object
        :param file_name: The file name
        :param file_path: The file path
        """
        if ppsd is not None:
            try:
                # Check existence of output directory
                os.makedirs(file_path, exist_ok=True)

                # Save NPZ file
                logger.info(
                    f"Saving {ppsd.id} to '{os.path.join(file_path, file_name)}'"
                )
                ppsd.save_npz(os.path.join(file_path, file_name))
            except Exception:
                logger.exception(
                    f"An error occurred during save of {ppsd.id} to NPZ file"
                )
                return False
            else:
                return True
        else:
            return False

    # SAVE TO DATABASE
    ####################################################################################################################

    # TOOLS
    ####################################################################################################################

    @staticmethod
    def npz_filename(ppsd):
        """
        Build a NPZ file name for a PPSD object
        :param ppsd: The PPSD object
        :return: The file name
        """
        try:
            start_used = ppsd.current_times_used[0].isoformat()
            end_used = (
                ppsd.current_times_used[-1] + timedelta(seconds=ppsd.ppsd_length)
            ).isoformat()
            return f"{ppsd.id}_{start_used}_{end_used}_{ppsd.sampling_rate}Hz.npz"
        except Exception:
            return f"{ppsd.id}_{ppsd.sampling_rate}Hz.npz"

    @staticmethod
    def _date_time(d):
        """
        Build an datetime
        :param d: A datetime or 'currentutcday'
        :return: A datetime
        """
        if d == "currentutcday":
            return datetime_today()
        return to_datetime(d)
