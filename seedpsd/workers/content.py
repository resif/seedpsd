import verboselogs

from seedpsd.models.db.shared.file import File
from seedpsd.models.db.shared.network import Network
from seedpsd.models.db.shared.source import Source

# from seedpsd.models.db.tenant.epoch import Epoch
# from seedpsd.models.db.tenant.stream import Stream
from . import SeedPsdWorker

logger = verboselogs.VerboseLogger(__name__)


class SeedPsdWorkerContent(SeedPsdWorker):
    """
    SeedPSD Worker class for using content
    """

    def __init__(self, settings, db_engine=None):
        logger.debug(f"SeedPsdWorkerContent.__init__({settings}, {db_engine})")
        super().__init__(settings, db_engine, parallel=False)

    # ENTRY POINT: LIST FILE ###########################################################################################

    def list_file(
        self,
        network=None,
        station=None,
        location=None,
        channel=None,
        date=None,
        before=None,
        after=None,
        source=None,
    ):
        """
        List files form database
        :param network: The network code
        :param station: The station code
        :param location: The location code
        :param channel: The channel code
        :param date: The file at a date
        :param before: Find file before a date
        :param after: Find file after a date
        :param after: Find file related to a (list of) source
        :return: list(File)
        """
        logger.debug(
            f"SeedPsdWorkerContent.list_file({network}, {station}, {location}, {channel}, {date}, {before}, {after}, {source})"
        )

        # with session_factory(settings=self._settings) as db_session:
        with self.db_session(self.db_engine()) as db_session:
            if not source and (network or station or location or channel):
                source = db_session.find_all(
                    Source.find(network, station, location, channel)
                )

            return db_session.find_all(
                File.find(source=source, date=date, before=before, after=after)
            )

    # ENTRY POINT: LIST NETWORK ########################################################################################

    def list_network(
        self,
        code=None,
        year=None,
        start=None,
        start_before=None,
        start_after=None,
        end=None,
        end_before=None,
        end_after=None,
        temporary=None,
    ):
        """
        List networks from database
        :param code: Filter by code
        :param year: Filter by year
        :param start: Filter networks by a start datetime
        :param start_before: networks epochs starting before a datetime
        :param start_after: networks epochs starting after a datetime
        :param end: Filter networks by an end datetime
        :param end_before: Filter networks ending before a datetime
        :param end_after: Filter networks ending after a datetime
        :param temporary: Filter by temporary/permanent status
        :return: list(Epoch)
        """
        logger.debug(
            f"SeedPsdWorkerContent.list_network({code}, {year}, {start}, {start_before}, {start_after}, {end}, {end_before}, {end_after}, {temporary})"
        )

        # with session_factory(settings=self._settings) as db_session:
        with self.db_session(self.db_engine()) as db_session:
            return db_session.find_all(
                Network.find(
                    code=code,
                    year=year,
                    temporary=temporary,
                    start=start,
                    start_before=start_before,
                    start_after=start_after,
                    end=end,
                    end_before=end_before,
                    end_after=end_after,
                )
            )

    # ENTRY POINT: LIST SOURCE #########################################################################################

    def list_source(
        self,
        network=None,
        station=None,
        location=None,
        channel=None,
        start=None,
        end=None,
    ):
        """
        List sources form database
        :param network: The network code
        :param station: The station code
        :param location: The location code
        :param channel: The channel code
        :param start: The start datetime
        :param end: The end datetime
        :return: list(Seed)
        """
        logger.debug(
            f"SeedPsdWorkerContent.list_source({network}, {station}, {location}, {channel}, {start}, {end})"
        )

        # with session_factory(settings=self._settings) as db_session:
        with self.db_session(self.db_engine()) as db_session:
            return db_session.find_all(
                Source.find(network, station, location, channel, start, end)
            )
