import logging
import os
from datetime import timedelta
from io import BytesIO

import matplotlib.pyplot as plt
import numpy as np
import pendulum
import verboselogs
from matplotlib.backends.backend_agg import FigureCanvasAgg
from matplotlib.backends.backend_pdf import FigureCanvasPdf
from matplotlib.backends.backend_svg import FigureCanvasSVG
from matplotlib.pyplot import close as pyplot_close
from matplotlib.pyplot import get_cmap
from obspy import UTCDateTime
from obspy.imaging.cm import pqlx
from PIL import Image

from seedpsd.errors import NoDataError
from seedpsd.tools.common import date_today

from . import SeedPsdWorker

logger = verboselogs.VerboseLogger(__name__)


class SeedPsdWorkerPlot(SeedPsdWorker):
    """
    SeedPSD Worker class for plot
    """

    def __init__(self, settings=None, matplotlib_loglevel=logging.WARNING):
        logger.debug(f"SeedPsdWorkerPlot.__init__({settings})")
        super().__init__(settings, parallel=False)

        try:
            logging.getLogger("matplotlib.font_manager").disabled = True
            logging.getLogger("matplotlib").setLevel(matplotlib_loglevel)
        except Exception:
            pass

    # PLOT
    ####################################################################################################################

    def build_plot(self, ppsd, plot_settings, plot_type):
        """
        Build a plot form PPSD data
        :param ppsd: THe PPSD instance
        :param plot_settings: The plot settings
        :param plot_type: The plot type ('histogram' | 'spectrogram')
        """
        logger.debug(f"SeedPsdWorkerPlot.plot({ppsd}, {plot_settings}, {plot_type})")

        if plot_type == "histogram":
            return self.build_plot_histogram(ppsd, plot_settings)
        if plot_type == "spectrogram":
            return self.build_plot_spectrogram(ppsd, plot_settings)
        msg = f"Unknown '{plot_type}' plot type"
        raise Exception(msg)

    # PLOT HISTOGRAM
    ####################################################################################################################

    def build_plot_histogram(self, ppsd, plot_settings):
        """
        Plot a histogram from PPSD data
        :param ppsd: THe PPSD instance
        :param plot_settings: The plot settings
        """
        logger.debug(f"SeedPsdWorkerPlot.plot_histogram({ppsd}, {plot_settings})")

        # Check availability of PSD on selected time range before going on
        if not ppsd._times_processed:
            msg = "No PSD available on the selected time range"
            raise NoDataError(msg)

        # Restrict time range inside the PPSD object (for Y axis)
        start_used, end_used = self._resize_ppsd(ppsd, plot_settings)

        # Set limits on X axis
        x_min = min(
            plot_settings.plot_x_min(ppsd.ppsd_params),
            plot_settings.plot_x_max(ppsd.ppsd_params),
        )
        x_max = max(
            plot_settings.plot_x_min(ppsd.ppsd_params),
            plot_settings.plot_x_max(ppsd.ppsd_params),
        )

        period_lim = (
            (x_max, x_min) if plot_settings.plot_x_frequency else (x_min, x_max)
        )

        # Build the plot
        figure = ppsd.plot(
            show_coverage=plot_settings.plot_coverage,
            show_histogram=True,
            show_percentiles=plot_settings.plot_percentiles,
            show_noise_models=plot_settings.plot_noise_models,
            grid=plot_settings.plot_grid,
            show=False,
            max_percentage=plot_settings.plot_colormap_max,
            period_lim=period_lim,
            show_mode=plot_settings.plot_mode,
            show_mean=plot_settings.plot_mean,
            cmap=self._plot_colormap(plot_settings),
            cumulative=plot_settings.plot_cumulative,
            xaxis_frequency=plot_settings.plot_x_frequency,
        )

        # Are we using "dirty" data?
        if hasattr(ppsd, "seedpsd_dirty") and ppsd.seedpsd_dirty:
            # self._plot_annotate_dirty()
            dirty = True
        else:
            dirty = False

        # Image cosmetics: dimensions, titles, margins, ...
        self._resize_plot(figure, plot_settings)

        # Footer
        if hasattr(ppsd, "seedpsd_date_processed"):
            self._plot_footer(
                figure,
                plot_settings,
                date_processed=ppsd.seedpsd_date_processed,
                dirty=dirty,
            )
        else:
            self._plot_footer(figure, plot_settings)

        return figure

    # PLOT SPECTROGRAM
    ####################################################################################################################

    def build_plot_spectrogram(self, ppsd, plot_settings):
        """
        Plot a spectrogram from PPSD data
        :param ppsd: THe PPSD instance
        :param plot_settings: The plot settings
        """
        logger.debug(f"SeedPsdWorkerPlot.plot_spectrogram({ppsd}, {plot_settings})")

        # Check availability of PSD on selected time range before going on
        if not ppsd._times_processed:
            msg = "No PSD available on the selected time range"
            raise NoDataError(msg)

        # Compute histogram on the selected time range
        start = UTCDateTime(plot_settings.start_time)
        end = UTCDateTime(plot_settings.end_time)
        ppsd.calculate_histogram(starttime=start, endtime=end)

        # Build the plot
        figure = ppsd.plot_spectrogram(
            cmap=self._plot_colormap(plot_settings),
            clim=(plot_settings.plot_colormap_min, plot_settings.plot_colormap_max),
            grid=plot_settings.plot_grid,
            show=False,
        )

        # Get plot object
        ax = figure.get_axes()[0]

        # Invert Y-axis
        if plot_settings.plot_y_invert:
            ax.invert_yaxis()

        # Are we using "dirty" data?
        if hasattr(ppsd, "seedpsd_dirty") and ppsd.seedpsd_dirty:
            # self._plot_annotate_dirty()
            dirty = True
        else:
            dirty = False

        # Image cosmetics: dimensions, titles, margins, ...
        self._resize_plot(figure, plot_settings)

        # Header
        scaled_font = plot_settings.plot_font_size * self._scale_factor(
            figure, plot_settings
        )
        ax.set_title(ppsd._get_plot_title(), fontsize=scaled_font)

        # Footer
        if hasattr(ppsd, "seedpsd_date_processed"):
            self._plot_footer(
                figure,
                plot_settings,
                date_processed=ppsd.seedpsd_date_processed,
                dirty=dirty,
            )
        else:
            self._plot_footer(figure, plot_settings)

        return figure

    # SAVE / RENDER
    ####################################################################################################################

    def save(self, figure, plot_settings, output_path, close=True):
        """
        Save a plot object into a file
        :param figure: The figure object
        :param plot_settings: The plot settings
        :param output_path: The output path
        :param close: Close the figure after saving
        """
        logger.debug(
            f"SeedPsdWorkerPlot.save({figure}, {plot_settings}, {output_path}, {close})"
        )
        if isinstance(figure, (list, tuple)):
            return self._save_merged(figure, plot_settings, output_path)
        figure.savefig(
            output_path,
            format=plot_settings.image_format,
            dpi=plot_settings.image_dpi,
            transparent=plot_settings.image_transparent,
            # bbox_inches='tight',
        )
        if close:
            pyplot_close(figure)
            return None
        return None

    def _save_merged(self, figures, plot_settings, output_path, close=True):
        """
        Merge and save a list of plot objects into a file
        :param figures: The figure objects
        :param plot_settings: The plot settings
        :param output_path: The output path
        :param close: Close the figures after saving
        """
        logger.debug(
            f"SeedPsdWorkerPlot._save_merged({figures}, {plot_settings}, {output_path}, {close})"
        )

        if plot_settings.image_format in ("jpg", "jpeg", "png"):
            image_mode = "RGBA" if plot_settings.image_format == "png" else "RGB"

            image_format = (
                "jpeg"
                if plot_settings.image_format == "jpg"
                else plot_settings.image_format
            )

            image_merged = Image.new(
                image_mode,
                (
                    int(plot_settings.image_width),
                    int(plot_settings.image_height) * len(figures),
                ),
            )

            index = 0
            for figure in figures:
                with BytesIO() as figure_file:
                    self.save(figure, plot_settings, figure_file)
                    image = Image.open(figure_file)
                    image_merged.paste(
                        image, (0, index * int(plot_settings.image_height))
                    )
                    index += 1
                if close:
                    pyplot_close(figure)

            image_merged.save(output_path, image_format)

        elif plot_settings.image_format == "pdf":
            from PyPDF3 import PdfFileMerger, PdfFileReader

            image_merged = PdfFileMerger()

            for figure in figures:
                with BytesIO() as figure_file:
                    self.save(figure, plot_settings, figure_file)
                    image_merged.append(PdfFileReader(figure_file, "rb"))
                if close:
                    pyplot_close(figure)

            image_merged.write(output_path)

        else:
            msg = f"Image merging is not available with format '{plot_settings.image_format}'"
            raise Exception(msg)

    @staticmethod
    def render(figure, plot_settings, close=True):
        """
        Render the plot directly
        :param figure: The figure object
        :param plot_settings: The plot settings
        :return: Image content
        :param close: Close the figure after saving
        """
        logger.debug(f"SeedPsdWorkerPlot.render({figure}, {plot_settings}, {close})")

        image_format = plot_settings.image_format
        output = BytesIO()
        if image_format == "png":
            FigureCanvasAgg(figure).print_png(output)
        elif image_format in ("jpg", "jpeg"):
            FigureCanvasAgg(figure).print_jpg(output)
        elif image_format == "pdf":
            FigureCanvasPdf(figure).print_pdf(output)
        elif image_format == "svg":
            FigureCanvasSVG(figure).print_svg(output)

        if close:
            pyplot_close(figure)

        return output.getvalue()

    # TOOLS
    ####################################################################################################################

    @staticmethod
    def filename(ppsd, plot_settings, plot_type):
        """
        Build a file name for a plot
        :param ppsd: The PPSD object
        :param plot_settings: The plot settings
        :param plot_type: The plot type
        :return: The file name
        """
        logger.debug(
            f"SeedPsdWorkerPlot.filename({ppsd}, {plot_settings}, {plot_type})"
        )

        # Merged file for multiple sampling rate
        if isinstance(ppsd, (list, tuple)):
            start_time = pendulum.instance(plot_settings.start_time).isoformat()
            end_time = pendulum.instance(plot_settings.end_time).isoformat()
            return f"{ppsd[0].id}_{start_time}_{end_time}_{plot_type}_merged.{plot_settings.image_format}"

        # Unique sampling rate
        start_used = ppsd.current_times_used[0].isoformat()
        end_used = (
            ppsd.current_times_used[-1] + timedelta(seconds=ppsd.ppsd_length)
        ).isoformat()
        return f"{ppsd.id}_{start_used}_{end_used}_{plot_type}_{ppsd.sampling_rate}Hz.{plot_settings.image_format}"

    @staticmethod
    def mime_type(plot_settings):
        """
        Return the mime type related to the image format
        :param plot_settings: The plot settings
        :return: The MIME type
        :rtype: str
        """
        logger.debug(f"SeedPsdWorkerPlot.mime_type({plot_settings})")

        image_format = str(plot_settings.image_format).lower()
        if image_format == "png":
            mime_type = "image/png"
        elif image_format in ("jpg", "jpeg"):
            mime_type = "image/jpeg"
        elif image_format == "pdf":
            mime_type = "application/pdf"
        elif image_format == "svg":
            mime_type = "image/svg+xml"
        else:
            logger.debug(f"Unsupported image format '{image_format}'")
            mime_type = None

        return mime_type

    @staticmethod
    def _resize_ppsd(ppsd, plot_settings):
        """
        Resize the PPSD data according to the plot settings
        :param ppsd: The PPSD object
        :param plot_settings: The plot settings
        :return: starttime, endtime
        """
        logger.debug(f"SeedPsdWorkerPlot._resize_ppsd({ppsd}, {plot_settings})")

        # Extract needed values from parameters
        start_time = plot_settings.start_time
        end_time = plot_settings.end_time
        y_min = plot_settings.plot_y_min
        y_max = plot_settings.plot_y_max

        # Set limits on Y axis
        num_bins = int((y_max - y_min) / 1.0)
        ppsd._db_bin_edges = np.linspace(y_min, y_max, num_bins + 1, endpoint=True)

        # Compute histogram on the selected time range
        start = UTCDateTime(start_time)
        end = UTCDateTime(end_time)
        ppsd.calculate_histogram(starttime=start, endtime=end)

        # Additional processing for the spectrogram (FIXME)
        ind = [n for n, t in enumerate(ppsd.times_processed) if start <= t <= end]
        ppsd._times_processed = [ppsd._times_processed[i] for i in ind]
        ppsd._binned_psds = [ppsd._binned_psds[i] for i in ind]

        # Get time range of the data used
        start_used = ppsd.current_times_used[0].isoformat()
        end_used = (
            ppsd.current_times_used[-1] + timedelta(seconds=ppsd.ppsd_length)
        ).isoformat()
        logger.debug(f"> Resized from {start} - {end} to {start_used} - {end_used}")

        return start_used, end_used

    def _resize_plot(self, figure, plot_settings):
        """
        Resize a plot figure according to the plot settings
        :param figure: The figure object
        :param plot_settings: The plot settings
        :return: The figure
        """
        logger.debug(f"SeedPsdWorkerPlot._resize_plot({figure}, {plot_settings})")

        # Set the dimensions of the image
        width = plot_settings.image_width / figure.dpi
        height = plot_settings.image_height / figure.dpi
        figure.set_size_inches(width, height)

        # Set the fontsize
        for axe in figure.axes:
            axe.tick_params(axis="both", labelsize=plot_settings.plot_font_size)
            axe.xaxis.get_label().set_fontsize(plot_settings.plot_font_size)
            axe.yaxis.get_label().set_fontsize(plot_settings.plot_font_size)
            axe.title.set_fontsize(plot_settings.plot_font_size)

        # plt.tight_layout(rect=[0.08, 0.1, 1.0, 0.95])
        return figure

    @staticmethod
    def _scale_factor(figure, plot_settings):
        """
        Calculate the scale factor
        :param figure: The figure object
        :param plot_settings: The plot settings
        :return: The figure
        """
        logger.debug(f"SeedPsdWorkerPlot._scale_factor({figure}, {plot_settings})")
        return plot_settings.image_dpi / figure.dpi

    @staticmethod
    def _plot_annotate_dirty():
        plt.annotate(
            "OUTDATED calculations",
            xy=(0.7, 0.05),
            xycoords="figure fraction",
            style="oblique",
            color="red",
            weight="bold",
            bbox={"facecolor": "yellow", "alpha": 0.5, "pad": 4},
        )

    def _plot_footer(
        self,
        figure,
        plot_settings,
        text=None,
        pad=5,
        color="grey",
        date_processed=None,
        dirty=False,
    ):
        """
        Adds a footer bar to matplotlib plots
        :param figure: The figure object
        :param plot_settings: The plot settings
        :param text: The text of the footer bar
        :param pad: The padding of the footer bar, in pixels (default=5)
        :param color: The color of the footer bar (default='grey')
        :param date_processed: The last update datetime
        :param dirty: Some calculations are outdated (default=False)
        """
        logger.debug(
            f"SeedPsdWorkerPlot._plot_footer({figure}, {plot_settings}, {text}, {pad}, {color}, {date_processed}, {dirty})"
        )

        # Compute footer size
        figure_width_in, figure_height_in = figure.get_size_inches()
        footer_height_px = (plot_settings.plot_font_size * 1.33) + (2 * pad)
        footer_height_in = footer_height_px / figure.dpi

        # Add a grey rectangle in background of the footer
        rectangle_height_ratio = footer_height_in / figure_height_in
        footer = plt.Rectangle(
            (0, 0),
            1,
            rectangle_height_ratio,
            transform=figure.transFigure,
            clip_on=False,
        )
        footer.set_color(color)
        figure.axes[0].add_patch(footer)

        # Build the footer text
        if not text:
            text = "Generated"

            # Add the DC name
            if self._settings.plot_footer_dc:
                text += f" by {self._settings.plot_footer_dc}"

            # Add the build date
            text += f" on {date_today().strftime('%Y-%m-%d')}"

            # Add the update date
            if date_processed:
                text += (
                    f" | Calculations updated on {date_processed.strftime('%Y-%m-%d')}"
                )
                if dirty:
                    text += " (Outdated)"

        # Add the footer text
        plt.figtext(
            0.01,
            0.01,
            text,
            ha="left",
            color="white",
            fontsize=plot_settings.plot_font_size,
        )

        # Add a license logo
        if self._settings.plot_footer_license:
            logo_path = os.path.abspath(
                os.path.join(
                    os.path.dirname(__file__),
                    "static",
                    "license",
                    f"{str(self._settings.plot_footer_license).lower()}.png",
                )
            )
            try:
                with Image.open(logo_path) as logo:
                    # Resize the logo
                    scale_factor = self._scale_factor(figure, plot_settings)
                    (logo_width_px, logo_height_px) = logo.size
                    logo_ratio = logo_width_px / logo_height_px
                    logo_resized = logo.resize(
                        (
                            int(footer_height_px * logo_ratio * scale_factor),
                            int(footer_height_px * scale_factor),
                        )
                    )

                    # Add the logo on the bottom right
                    logo_x_px = int(
                        (figure_width_in * figure.dpi * scale_factor)
                        - logo_resized.width
                    )
                    figure.figimage(logo_resized, logo_x_px, 0)
            except Exception:
                logger.warning(
                    f"Unable to load the license file '{self._settings.plot_footer_license}.png"
                )

        return figure

    @staticmethod
    def _plot_colormap(plot_settings):
        """
        Get the colourmap according to the plot settings
        :param plot_settings: The plot settings
        :return: The colourmap object
        """
        logger.debug(f"SeedPsdWorkerPlot._plot_colormap({plot_settings})")

        if plot_settings.plot_colormap == "pqlx":
            return pqlx
        return get_cmap(plot_settings.plot_colormap)

    @staticmethod
    def colormaps():
        cmaps = plt.colormaps()
        cmaps.append("pqlx")
        return sorted(cmaps)
