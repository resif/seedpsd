import verboselogs

from seedpsd.tools.common import str2bool

from . import PlotSettings

logger = verboselogs.VerboseLogger(__name__)


class SpectrogramPlotSettings(PlotSettings):
    def __init__(
        self,
        settings,
        start_time,
        end_time,
        image_format=None,
        image_transparent=None,
        image_dpi=None,
        image_width=None,
        image_height=None,
        plot_colormap=None,
        plot_colormap_min=None,
        plot_colormap_max=None,
        plot_font_size=None,
        plot_grid=None,
        plot_y_invert=None,
    ):
        logger.debug(
            f"SpectrogramPlotSettings({settings}, {start_time}, {end_time}, "
            f"{image_format}, {image_transparent}, {image_dpi}, {image_width}, {image_height}, "
            f"{plot_colormap}, {plot_colormap_min}, {plot_colormap_max}, "
            f"{plot_font_size}, {plot_grid}, {plot_y_invert})"
        )

        super().__init__(
            settings,
            start_time,
            end_time,
            image_format,
            image_transparent,
            image_dpi,
            image_width,
            image_height,
            plot_colormap,
            plot_colormap_min,
            plot_colormap_max,
            plot_font_size,
            plot_grid,
        )

        # Spectrogram plot parameters
        self._plot_y_invert = plot_y_invert

    # SPECTROGRAM PLOT PARAMETERS ######################################################################################

    @property
    def plot_colormap(self):
        if self._plot_colormap is not None:
            return self._plot_colormap
        return self._settings.plot_spectrogram_colormap

    @property
    def plot_colormap_min(self):
        if self._plot_colormap_min is not None:
            return int(self._plot_colormap_min)
        return self._settings.plot_spectrogram_colormap_min

    @property
    def plot_colormap_max(self):
        if self._plot_colormap_max is not None:
            return int(self._plot_colormap_max)
        return self._settings.plot_spectrogram_colormap_max

    @property
    def plot_grid(self):
        if self._plot_grid is not None:
            return str2bool(self._plot_grid)
        return self._settings.plot_spectrogram_grid

    @property
    def plot_y_invert(self):
        if self._plot_y_invert is not None:
            return str2bool(self._plot_y_invert)
        return self._settings.plot_spectrogram_y_invert
