import verboselogs

from seedpsd.errors import InvalidParameterValueError, MissingParameterError
from seedpsd.tools.common import str2bool, to_datetime

logger = verboselogs.VerboseLogger(__name__)


class PlotSettings:
    def __init__(
        self,
        settings,
        start_time,
        end_time,
        image_format=None,
        image_transparent=None,
        image_dpi=None,
        image_width=None,
        image_height=None,
        plot_colormap=None,
        plot_colormap_min=None,
        plot_colormap_max=None,
        plot_font_size=None,
        plot_grid=None,
    ):
        logger.debug(
            f"PlotSettings({settings}, {start_time}, {end_time}, "
            f"{image_format}, {image_transparent}, {image_dpi}, {image_width}, {image_height}, "
            f"{plot_colormap}, {plot_colormap_min}, {plot_colormap_max}, {plot_font_size}, {plot_grid})"
        )

        # Settings
        self._settings = settings

        # Time period selected
        self._start_time = start_time
        self._end_time = end_time

        # Image parameters
        self._image_format = image_format
        self._image_transparent = image_transparent
        self._image_dpi = image_dpi
        self._image_width = image_width
        self._image_height = image_height

        # Plot parameters
        self._plot_colormap = plot_colormap
        self._plot_colormap_min = plot_colormap_min
        self._plot_colormap_max = plot_colormap_max
        self._plot_font_size = plot_font_size
        self._plot_grid = plot_grid

    # TIME PERIOD ######################################################################################################

    @property
    def start_time(self):
        if self._start_time is not None:
            return to_datetime(self._start_time)
        return None

    @property
    def start_year(self):
        if self.start_time is not None:
            return self.start_time.year
        return None

    @property
    def end_time(self):
        if self._end_time is not None:
            return to_datetime(self._end_time)
        return None

    @property
    def end_year(self):
        if self.end_time is not None:
            return self.end_time.year
        return None

    # IMAGE PARAMETERS #################################################################################################

    @property
    def image_format(self):
        if self._image_format is not None:
            return str(self._image_format).lower()
        return self._settings.plot_image_format

    @property
    def image_transparent(self):
        if self._image_transparent is not None:
            return str2bool(self._image_transparent)
        return self._settings.plot_image_transparent

    @property
    def image_dpi(self):
        if self._image_dpi is not None:
            return int(self._image_dpi)
        return self._settings.plot_image_dpi

    @property
    def image_width(self):
        if self._image_width is not None:
            return int(self._image_width)
        return self._settings.plot_image_width

    @property
    def image_height(self):
        if self._image_height is not None:
            return int(self._image_height)
        return self._settings.plot_image_height

    # PLOT PARAMETERS ##################################################################################################

    @property
    def plot_font_size(self):
        if self._plot_font_size is not None:
            return int(self._plot_font_size)
        return self._settings.plot_font_size

    # METHODS ##########################################################################################################

    def check(self, raise_error=True):
        logger.debug(f"Query.check({raise_error})")

        try:
            if self._start_time is None:
                msg = "starttime"
                raise MissingParameterError(msg)
            if not self.start_time:
                msg = "starttime"
                raise InvalidParameterValueError(msg, self._start_time)

            if self._end_time is None:
                msg = "endtime"
                raise MissingParameterError(msg)
            if not self.end_time:
                msg = "endtime"
                raise InvalidParameterValueError(msg, self._end_time)

            if self.image_dpi is None or self.image_dpi < 75 or self.image_dpi > 300:
                msg = "dpi"
                raise InvalidParameterValueError(msg, self.image_dpi)

            if (
                self.image_width is None
                or self.image_width < 640
                or self.image_width > 2000
            ):
                msg = "width"
                raise InvalidParameterValueError(msg, self.image_width)

            if (
                self.image_height is None
                or self.image_height < 480
                or self.image_height > 2000
            ):
                msg = "height"
                raise InvalidParameterValueError(msg, self.image_height)

            return True

        except Exception:
            if raise_error:
                raise
            return False
