import verboselogs

from seedpsd.tools.common import str2bool

from . import PlotSettings

logger = verboselogs.VerboseLogger(__name__)


class HistogramPlotSettings(PlotSettings):
    def __init__(
        self,
        settings,
        start_time,
        end_time,
        image_format=None,
        image_transparent=None,
        image_dpi=None,
        image_width=None,
        image_height=None,
        plot_colormap=None,
        plot_colormap_min=None,
        plot_colormap_max=None,
        plot_font_size=None,
        plot_grid=None,
        plot_coverage=None,
        plot_cumulative=None,
        plot_mean=None,
        plot_mode=None,
        plot_noise_models=None,
        plot_percentiles=None,
        plot_x_frequency=None,
        plot_x_min=None,
        plot_x_max=None,
        plot_y_min=None,
        plot_y_max=None,
    ):
        logger.debug(
            f"HistogramPlotSettings({settings}, {start_time}, {end_time}, "
            f"{image_format}, {image_transparent}, {image_dpi}, {image_width}, {image_height}, "
            f"{plot_colormap}, {plot_colormap_min}, {plot_colormap_max}, {plot_font_size}, {plot_grid}, "
            f"{plot_coverage}, {plot_cumulative}, {plot_mean}, {plot_mode}, {plot_noise_models}, "
            f"{plot_percentiles}, {plot_x_frequency}, {plot_x_min}, {plot_x_max}, {plot_y_min}, {plot_y_max})"
        )

        super().__init__(
            settings,
            start_time,
            end_time,
            image_format,
            image_transparent,
            image_dpi,
            image_width,
            image_height,
            plot_colormap,
            plot_colormap_min,
            plot_colormap_max,
            plot_font_size,
            plot_grid,
        )

        # Histogram plot parameters
        self._plot_coverage = plot_coverage
        self._plot_cumulative = plot_cumulative
        self._plot_mean = plot_mean
        self._plot_mode = plot_mode
        self._plot_noise_models = plot_noise_models
        self._plot_percentiles = plot_percentiles
        self._plot_x_frequency = plot_x_frequency
        self._plot_x_min = plot_x_min
        self._plot_x_max = plot_x_max
        self._plot_y_min = plot_y_min
        self._plot_y_max = plot_y_max

    # HISTOGRAM PLOT PARAMETERS ########################################################################################

    @property
    def plot_colormap(self):
        if self._plot_colormap is not None:
            return self._plot_colormap
        return self._settings.plot_histogram_colormap

    @property
    def plot_colormap_min(self):
        if self._plot_colormap_min is not None:
            return int(self._plot_colormap_min)
        return self._settings.plot_histogram_colormap_min

    @property
    def plot_colormap_max(self):
        if self._plot_colormap_max is not None:
            return int(self._plot_colormap_max)
        return self._settings.plot_histogram_colormap_max

    @property
    def plot_coverage(self):
        if self._plot_coverage is not None:
            return str2bool(self._plot_coverage)
        return self._settings.plot_histogram_coverage

    @property
    def plot_cumulative(self):
        if self._plot_cumulative is not None:
            return str2bool(self._plot_cumulative)
        return self._settings.plot_histogram_cumulative

    @property
    def plot_grid(self):
        if self._plot_grid is not None:
            return str2bool(self._plot_grid)
        return self._settings.plot_histogram_grid

    @property
    def plot_mean(self):
        if self._plot_mean is not None:
            return str2bool(self._plot_mean)
        return self._settings.plot_histogram_mean

    @property
    def plot_mode(self):
        if self._plot_mode is not None:
            return str2bool(self._plot_mode)
        return self._settings.plot_histogram_mode

    @property
    def plot_noise_models(self):
        if self._plot_noise_models is not None:
            return str2bool(self._plot_noise_models)
        return self._settings.plot_histogram_noise_models

    @property
    def plot_percentiles(self):
        if self._plot_percentiles is not None:
            return str2bool(self._plot_percentiles)
        return self._settings.plot_histogram_percentiles

    @property
    def plot_x_frequency(self):
        if self._plot_x_frequency is not None:
            return str2bool(self._plot_x_frequency)
        return self._settings.plot_histogram_x_frequency

    def plot_x_min(self, ppsd_params=None):
        if self._plot_x_min is not None:
            return float(self._plot_x_min)
        if ppsd_params is not None and "period_limits" in ppsd_params:
            return ppsd_params["period_limits"][0]
        return self._settings.plot_histogram_x_min

    def plot_x_max(self, ppsd_params=None):
        if self._plot_x_max is not None:
            return float(self._plot_x_max)
        if ppsd_params is not None and "period_limits" in ppsd_params:
            return ppsd_params["period_limits"][1]
        return self._settings.plot_histogram_x_max

    @property
    def plot_y_min(self):
        if self._plot_y_min is not None:
            return float(self._plot_y_min)
        return self._settings.plot_histogram_y_min

    @property
    def plot_y_max(self):
        if self._plot_y_max is not None:
            return float(self._plot_y_max)
        return self._settings.plot_histogram_y_max
