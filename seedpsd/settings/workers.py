import logging
import os
from pathlib import Path

import verboselogs
from dotenv import dotenv_values
from simple_singleton import SingletonArgs

from seedpsd import __version__ as seedpsd_version
from seedpsd.errors import EmptySettingValueError, UndefinedSettingValueError
from seedpsd.tools.common import is_allowed_channel, str2bool

logger = verboselogs.VerboseLogger(__name__)
__all__ = ["Settings"]


class Settings(metaclass=SingletonArgs):
    def __init__(
        self, config_path=None, config_file=".env", debug=False, prefix="SEEDPSD"
    ):
        logger.debug(f"Settings.__init__({config_path}, {debug})")

        # Prefix
        self.PREFIX = prefix

        # Search path for .env or settings.ini file
        if config_path is None:
            config_path = Path(__file__).parent.parent.parent.resolve()
        elif not isinstance(config_path, Path):
            config_path = Path(config_path)

        if config_path.is_dir():
            config_path = config_path / config_file

        logger.debug(f"Try to load settings file from '{config_path}'")
        self._config = {
            **dotenv_values(config_path),
            **os.environ,
        }

        # Initialize the internal cache of setting values
        self._values = {}

        # Use debug flag from CLI
        if debug:
            self._values["log_stacktrace"] = debug

    def _get_value(
        self,
        key,
        env_key=None,
        default=None,
        mandatory=False,
        as_type=None,
        filter=None,
    ):
        """
        Get a setting value

        :param key: Setting key
        :param env_key: ENVVAR key
        :param default: Default value
        :param mandatory: Mandatory flag
        :param as_type: Type casting
        :param filter: Filter callback
        :return: Setting value
        """

        # Build ENVVAR key
        if not env_key:
            env_key = f"{self.PREFIX}_{str(key).upper()}"
        elif not env_key.startswith(self.PREFIX):
            env_key = f"{self.PREFIX}_{str(env_key).upper()}"

        # First search for a setting key
        if key not in self._values:
            try:
                # Get the value from environment/file
                value = self._config[env_key]

                # Check mandatory
                if value == "":
                    if mandatory:
                        raise EmptySettingValueError(env_key)
                    value = default() if callable(default) else default

                # Optional conversion
                if as_type and as_type is bool:
                    value = str2bool(value)
                elif as_type:
                    value = as_type(value)
                elif filter and callable(filter):
                    value = filter(value)

                # Save the value
                self._values[key] = value

            except KeyError as e:
                # Key not found but this parameter is mandatory
                if mandatory:
                    raise UndefinedSettingValueError(env_key) from e

                # Key not found but this parameter is optional : Use the default value
                if callable(default):
                    value = default()
                    logger.debug(
                        f"{env_key} is not configured. Using '{value}' by default."
                    )
                    self._values[key] = value
                else:
                    logger.debug(
                        f"{env_key} is not configured. Using '{default}' by default."
                    )
                    self._values[key] = default

        # Return cached value
        return self._values[key]

    @property
    def env_config(self):
        conf = {}
        for key in self._config:
            if str(key).startswith(self.PREFIX):
                conf[key] = self._config[key]
        return conf

    @property
    def full_config(self):
        conf = {}
        for key in dir(self):
            if not key.startswith("_") and key not in (
                "env_config",
                "full_config",
                "ppsd_is_allowed_channel",
            ):
                try:
                    conf[key] = getattr(self, key)
                except UndefinedSettingValueError:
                    conf[key] = None
        return conf

    # AMQP #############################################################################################################

    @property
    def amqp_server_url(self):
        return self._get_value("amqp_server_url", mandatory=True)

    @property
    def amqp_port(self):
        return self._get_value("amqp_port", default=5672, as_type=int)

    @property
    def amqp_vhost(self):
        return self._get_value("amqp_vhost", default="/")

    @property
    def amqp_user(self):
        return self._get_value("amqp_user", mandatory=True)

    @property
    def amqp_password(self):
        return self._get_value("amqp_password", mandatory=True)

    @property
    def amqp_heartbeat(self):
        return self._get_value("amqp_heartbeat", default=2400, as_type=int)

    @property
    def amqp_timeout(self):
        return self._get_value("amqp_timeout", default=1200, as_type=int)

    @property
    def amqp_log_level(self):
        return self._get_value("amqp_log_level", default=logging.WARNING)

    @property
    def amqp_data_queue_name(self):
        return self._get_value("amqp_data_queue_name", default="seedpsd-data")

    @property
    def amqp_data_queue_routing(self):
        return self._get_value("amqp_data_queue_routing", default="miniseed")

    @property
    def amqp_data_queue_durable(self):
        return self._get_value("amqp_data_queue_durable", default=True, as_type=bool)

    @property
    def amqp_exchange_name(self):
        return self._get_value("amqp_exchange_name", default="post-integration")

    @property
    def amqp_exchange_type(self):
        return self._get_value("amqp_exchange_type", default="direct")

    @property
    def amqp_exchange_durable(self):
        return self._get_value("amqp_exchange_durable", default=True, as_type=bool)

    @property
    def amqp_metadata_queue_name(self):
        return self._get_value("amqp_metadata_queue_name", default="seedpsd-metadata")

    @property
    def amqp_metadata_queue_routing(self):
        return self._get_value("amqp_metadata_queue_routing", default="stationxml")

    @property
    def amqp_metadata_queue_durable(self):
        return self._get_value(
            "amqp_metadata_queue_durable", default=True, as_type=bool
        )

    @property
    def amqp_metadata_update_data(self):
        return self._get_value("amqp_metadata_update_data", default=False, as_type=bool)

    @property
    def amqp_metadata_delete_orphan(self):
        return self._get_value(
            "amqp_metadata_delete_orphan", default=False, as_type=bool
        )

    @property
    def amqp_metadata_delete_dirty(self):
        return self._get_value(
            "amqp_metadata_delete_dirty", default=False, as_type=bool
        )

    # API URL ##########################################################################################################

    @property
    def api_path(self):
        def _filter(value):
            value = str(value).rstrip("/")
            if not value.startswith("/"):
                value = f"/{value}"
            return value

        return self._get_value("api_path", default="/", filter=_filter)

    # DATA #############################################################################################################

    @property
    def data_path(self):
        return self._get_value("data_path", env_key="path_data", mandatory=True)

    @property
    def data_sds(self):
        return self._get_value(
            "data_sds", env_key="use_sds", default=False, as_type=bool
        )

    @property
    def data_extended(self):
        return self._get_value(
            "data_extended", env_key="use_extended", default=False, as_type=bool
        )

    # DATABASE #########################################################################################################

    @property
    def database_host(self):
        return self._get_value("database_host", mandatory=True)

    @property
    def database_port(self):
        return self._get_value("database_port")

    @property
    def database_name(self):
        return self._get_value("database_name", mandatory=True)

    @property
    def database_user(self):
        return self._get_value("database_user", mandatory=True)

    @property
    def database_pass(self):
        return self._get_value("database_pass")

    @property
    def database_driver(self):
        return self._get_value("database_driver", default="psycopg")

    @property
    def database_uri(self):
        # Driver
        uri = "postgresql"
        if self.database_driver:
            uri += f"+{self.database_driver}"

        # User
        uri += f"://{self.database_user}"

        # Password (optional)
        if self.database_pass:
            uri += f":{self.database_pass}"

        # Host
        uri += f"@{self.database_host}"

        # Port (optional)
        if self.database_port:
            uri += f":{self.database_port}"

        # Database name
        uri += f"/{self.database_name}"

        return uri

    @property
    def database_pool_type(self):
        def _filter(value):
            return str(value).lower()

        return self._get_value("database_pool_type", default="queue", filter=_filter)

    @property
    def database_pool_size_min(self):
        return self._get_value("database_pool_size_min", default=5, as_type=int)

    @property
    def database_pool_size_max(self):
        return self._get_value("database_pool_size_max", default=10, as_type=int)

    @property
    def database_pool_recycle(self):
        return self._get_value("database_pool_recycle", default=-1, as_type=int)

    @property
    def database_pool_timeout(self):
        return self._get_value("database_pool_timeout", default=30, as_type=int)

    @property
    def database_pool_pre_ping(self):
        return self._get_value("database_pool_pre_ping", default=True, as_type=bool)

    @property
    def database_pool_with_parallel(self):
        return self._get_value(
            "database_pool_with_parallel", default=False, as_type=bool
        )

    # EXAMPLE CUSTOMIZATION ############################################################################################

    @property
    def example_network(self):
        return self._get_value("example_network")

    @property
    def example_station(self):
        return self._get_value("example_station")

    @property
    def example_location(self):
        return self._get_value("example_location")

    @property
    def example_channel(self):
        return self._get_value("example_channel")

    @property
    def example_start(self):
        return self._get_value("example_start")

    @property
    def example_end(self):
        return self._get_value("example_end")

    # FDSN OBSPY CLIENT ################################################################################################

    @property
    def fdsn_client(self):
        return self._get_value("fdsn_client", mandatory=True)

    @property
    def fdsn_client_debug(self):
        return self._get_value("fdsn_client_debug", default=False, as_type=bool)

    # LOCALIZATION #####################################################################################################

    @property
    def locale_default(self):
        return self._get_value("locale_default", default="en")

    @property
    def locale_available(self):
        def _filter(value):
            if isinstance(value, str):
                return str(value).split(",")
            return value

        return self._get_value("locale_available", default=["en"], filter=_filter)

    @property
    def locale_path(self):
        return self._get_value(
            "locale_path",
            default=str(Path(__file__).parent.parent.joinpath("locale").resolve()),
        )

    # LOGGING ##########################################################################################################

    @property
    def log_path(self):
        return self._get_value("log_path")

    @property
    def log_stacktrace(self):
        return self._get_value("log_stacktrace", default=False, as_type=bool)

    @property
    def log_level(self):
        return self._get_value("log_level", default=logging.INFO)

    @property
    def sql_log_level(self):
        return self._get_value("sql_log_level", default=logging.WARNING)

    # METADATA #########################################################################################################

    @property
    def metadata_path(self):
        return self._get_value("metadata_path", env_key="path_metadata")

    # PLOT CUSTOMIZATION ###############################################################################################

    @property
    def plot_footer_dc(self):
        return self._get_value("plot_footer_dc", default="SeedPSD")

    @property
    def plot_footer_license(self):
        return self._get_value("plot_footer_license")

    @property
    def plot_image_format(self):
        return self._get_value("plot_image_format", default="png")

    @property
    def plot_image_transparent(self):
        return self._get_value("plot_image_transparent", default=True, as_type=bool)

    @property
    def plot_image_dpi(self):
        return self._get_value("plot_image_dpi", default=100, as_type=int)

    @property
    def plot_image_width(self):
        return self._get_value("plot_image_width", default=900, as_type=int)

    @property
    def plot_image_height(self):
        return self._get_value("plot_image_height", default=600, as_type=int)

    @property
    def plot_font_size(self):
        return self._get_value("plot_font_size", default=10, as_type=int)

    # PLOT HISTOGRAM CUSTOMIZATION #####################################################################################

    @property
    def plot_histogram_colormap(self):
        return self._get_value("plot_histogram_colormap", default="viridis_r")

    @property
    def plot_histogram_colormap_min(self):
        return self._get_value("plot_histogram_colormap_min", as_type=int)

    @property
    def plot_histogram_colormap_max(self):
        return self._get_value("plot_histogram_colormap_max", default=30, as_type=int)

    @property
    def plot_histogram_grid(self):
        return self._get_value("plot_histogram_grid", default=True, as_type=bool)

    @property
    def plot_histogram_coverage(self):
        return self._get_value("plot_histogram_coverage", default=True, as_type=bool)

    @property
    def plot_histogram_percentiles(self):
        return self._get_value(
            "plot_histogram_percentiles", default=False, as_type=bool
        )

    @property
    def plot_histogram_noise_models(self):
        return self._get_value(
            "plot_histogram_noise_models", default=True, as_type=bool
        )

    @property
    def plot_histogram_mode(self):
        return self._get_value("plot_histogram_mode", default=False, as_type=bool)

    @property
    def plot_histogram_mean(self):
        return self._get_value("plot_histogram_mean", default=False, as_type=bool)

    @property
    def plot_histogram_cumulative(self):
        return self._get_value("plot_histogram_cumulative", default=False, as_type=bool)

    @property
    def plot_histogram_x_frequency(self):
        return self._get_value(
            "plot_histogram_x_frequency", default=False, as_type=bool
        )

    @property
    def plot_histogram_x_min(self):
        return self._get_value("plot_histogram_x_min", default=0.01, as_type=float)

    @property
    def plot_histogram_x_max(self):
        return self._get_value("plot_histogram_x_max", default=179, as_type=float)

    @property
    def plot_histogram_y_min(self):
        return self._get_value("plot_histogram_y_min", default=-200, as_type=float)

    @property
    def plot_histogram_y_max(self):
        return self._get_value("plot_histogram_y_max", default=-50, as_type=float)

    # PLOT SPECTROGRAM CUSTOMIZATION ###################################################################################

    @property
    def plot_spectrogram_colormap(self):
        return self._get_value("plot_spectrogram_colormap", default="jet")

    @property
    def plot_spectrogram_colormap_min(self):
        return self._get_value("plot_spectrogram_colormap_min", as_type=int)

    @property
    def plot_spectrogram_colormap_max(self):
        return self._get_value("plot_spectrogram_colormap_max", as_type=int)

    @property
    def plot_spectrogram_grid(self):
        return self._get_value("plot_spectrogram_grid", default=True, as_type=bool)

    @property
    def plot_spectrogram_y_invert(self):
        return self._get_value("plot_spectrogram_y_invert", default=True, as_type=bool)

    @property
    def plot_spectrogram_y_min(self):
        return self._get_value("plot_spectrogram_y_min", default=-200, as_type=float)

    @property
    def plot_spectrogram_y_max(self):
        return self._get_value("plot_spectrogram_y_max", default=-50, as_type=float)

    # PPSD CUSTOMIZATION ###############################################################################################

    @property
    def ppsd_use_defaults(self):
        return self._get_value("ppsd_use_defaults", default=True, as_type=bool)

    @property
    def ppsd_only_allowed_channels(self):
        return self._get_value("ppsd_only_allowed_channels", default=True, as_type=bool)

    @property
    def ppsd_allowed_channel_bands(self):
        return self._get_value("ppsd_allowed_channel_bands", default="BCDEFGHLMPQRSTUV")

    @property
    def ppsd_allowed_channel_instruments(self):
        return self._get_value("ppsd_allowed_channel_instruments", default="DGHJLMNP")

    @property
    def ppsd_allowed_channel_orientations(self):
        return self._get_value(
            "ppsd_allowed_channel_orientations", default="ABCDEFHINORTUVWZ123"
        )

    def ppsd_is_allowed_channel(self, code):
        if self.ppsd_only_allowed_channels:
            return is_allowed_channel(
                code,
                self.ppsd_allowed_channel_bands,
                self.ppsd_allowed_channel_instruments,
                self.ppsd_allowed_channel_orientations,
            )
        # logging.warning('Allowing ALL channels')
        return True

    # SENTRY LOGGING ###################################################################################################

    @property
    def sentry_dsn(self):
        return self._get_value("sentry_dsn")

    @property
    def sentry_tracing(self):
        return self._get_value("sentry_tracing", default=False, as_type=bool)

    @property
    def sentry_environment(self):
        return self._get_value("sentry_environment")

    @property
    def sentry_sample_rate(self):
        return self._get_value("sentry_sample_rate")

    @property
    def sentry_traces_sample_rate(self):
        return self._get_value("sentry_traces_sample_rate")

    @property
    def sentry_profiles_sample_rate(self):
        return self._get_value("sentry_profiles_sample_rate")

    # SKIN CUSTOMIZATION ###############################################################################################

    @property
    def skin_name(self):
        return self._get_value("skin_name")

    @property
    def skin_path(self):
        return self._get_value("skin_path")

    # VERSION & COMMIT #################################################################################################

    @property
    def version(self):
        return self._get_value("version", default=seedpsd_version)

    @property
    def commit(self):
        def get_commit():
            commit = None
            try:
                commit_file_path = (
                    Path(__file__).parent.parent.parent.joinpath("commit.txt").resolve()
                )
                if commit_file_path.exists():
                    with open(commit_file_path) as commit_file:
                        commit = commit_file.readline()

                return commit
            except OSError:
                pass

        return self._get_value("commit", default=get_commit)
