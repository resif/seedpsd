from pathlib import Path

import verboselogs
from alembic.config import Config

logger = verboselogs.VerboseLogger(__name__)


class AlembicSettings(Config):
    def __init__(self, settings):
        logger.debug(f"AlembicSettings.__init__({settings})")
        super().__init__()
        self.set_main_option(
            "script_location",
            str(Path(__file__).parent.parent.joinpath("alembic").resolve()),
        )
        self.set_main_option(
            "file_template", "%%(year)d-%%(month).2d-%%(day).2d_%%(rev)s"
        )
        self.set_main_option("sqlalchemy.url", settings.database_uri)
        self._seedpsd_settings = settings
