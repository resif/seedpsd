# encoding: utf8
"""${message}

Revision ID: ${up_revision}
Revises: ${down_revision | comma,n}
Create Date: ${create_date}

"""
import logging
from alembic import op
import sqlalchemy as sa
${imports if imports else ""}


# revision identifiers, used by Alembic.
revision = ${repr(up_revision)}
down_revision = ${repr(down_revision)}
branch_labels = ${repr(branch_labels)}
depends_on = ${repr(depends_on)}


# ## INSTRUCTIONS FOR DEVELOPMENT ##
# 1. At present, Alembic is not able to cleanly detect changes to the 'tenant' schemas. These operations must be written by hand. Get inspired by previous revisions!
# 2. Please adjust the lines generated in the blocks below according to the instructions.
# 3. Please integrate data migration operations with schema migration operations.
# 4. Please test migration in both directions.


# UPGRADE ##############################################################################################################

def upgrade():

    # Get connection
    connection = op.get_bind()
    
    # Migrate shared schema
    upgrade_shared(connection)

    # Migrate each network
    for network in connection.execute(sa.text(f"SELECT id, schema FROM shared.network")):
        upgrade_tenant(connection, network.schema, network.id)


def upgrade_shared(connection):
    logging.info(f"Migrating schema: shared")

    # ## INSTRUCTIONS FOR DEVELOPMENT ##
    # 1. Please keep here only the operations concerning the 'shared' schema.
    # 2. Please move below all the operations concerning the 'tenant' schema

    ${upgrades if upgrades else "pass"}


def upgrade_tenant(connection, schema, network_id):
    logging.info(f"Migrating schema: {schema}")

    # ## INSTRUCTIONS FOR DEVELOPMENT ##
    # 1. Please move here all the operations concerning the 'tenant' schema
    # 2. Please replace in each operation "schema='tenant'" by "schema=schema"

    pass


# DOWNGRADE ############################################################################################################

def downgrade():

    # Get connection
    connection = op.get_bind()
    
    # Migrate shared schema
    downgrade_shared(connection)

    # Migrate each network
    for network in connection.execute(sa.text(f"SELECT id, schema FROM shared.network")):
        downgrade_tenant(connection, network.schema, network.id)


def downgrade_shared(connection):
    logging.info(f"Migrating schema: shared")

    # ## INSTRUCTIONS FOR DEVELOPMENT ##
    # 1. Please keep here only the operations concerning the 'shared' schema.
    # 2. Please move below all the operations concerning the 'tenant' schema

    ${downgrades if downgrades else "pass"}


def downgrade_tenant(connection, schema, network_id):
    logging.info(f"Migrating schema: {schema}")

    ### INSTRUCTIONS FOR DEVELOPMENT ###
    # 1. Please move here all the operations concerning the 'tenant' schema
    # 2. Please replace in each operation "schema='tenant'" by "schema=schema"

    pass
