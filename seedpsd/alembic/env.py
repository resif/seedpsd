from alembic import context
from sqlalchemy import text

# Import all models
from seedpsd.models.db import *

# Add your model's MetaData object here for 'autogenerate' support
from seedpsd.models.db.base import Base
from seedpsd.models.db.session import DBSession

# This is the Alembic Config object, which provides access to the values within the .ini file in use.
# Other values from the config, defined by the needs of env.py, can be acquired:
# my_important_option = config.get_main_option("my_important_option")
# ... etc.
config = context.config

target_metadata = Base.metadata


# Customize the rendering of some types
# From https://stackoverflow.com/questions/30132370/trouble-when-using-alembic-with-sqlalchemy-utils
#      https://alembic.sqlalchemy.org/en/latest/autogenerate.html#affecting-the-rendering-of-types-themselves
def render_item(type_, obj, autogen_context):
    """Apply custom rendering for selected items."""

    if type_ == "type" and obj.__class__.__module__.startswith("sqlalchemy_utils."):
        autogen_context.imports.add(
            f"from {obj.__class__.__module__} import {obj.__class__.__name__}"
        )
        return f"{obj.__class__.__name__}()"

    if type_ == "type" and obj.__class__.__module__.startswith("sqlalchemy_utc."):
        autogen_context.imports.add(
            f"from {obj.__class__.__module__} import {obj.__class__.__name__}"
        )
        if hasattr(obj, "timezone"):
            return f"{obj.__class__.__name__}(timezone={obj.timezone})"
        return f"{obj.__class__.__name__}()"

    # default rendering for other objects
    return False


# Filter available objects from data model classes
def include_object_from_schemas(schemas):
    def include_object(object, name, type_, reflected, compare_to):
        if type_ == "schema":
            return name in schemas
        if type_ == "table":
            return object.schema in schemas
        return True

    return include_object


# Filter available objects from database
def include_name_from_schemas(schemas):
    def include_name(name, type_, parent_names):
        if type_ == "schema":
            return name in schemas
        return True

    return include_name


# Main migration workflow
def run_migrations_online():
    # Configure a connection
    engine = DBSession.engine_factory(
        settings=config._seedpsd_settings,
        use_pool=False,
    )

    # Find a tenant schema name
    try:
        with engine.connect() as connection:
            tenant_schema = connection.execute(
                text("SELECT schema FROM shared.network ORDER BY schema")
            ).scalar()
    except Exception:
        tenant_schema = None

    # If database has already a tenant
    if tenant_schema:
        # Configure a specialized connection
        engine_tenant = DBSession.engine_factory(
            settings=config._seedpsd_settings,
            use_pool=False,
            tenant_schema=tenant_schema,
        )
        # Use database to reflect existing structure into metadata
        with engine_tenant.connect() as connection:
            connection.dialect.default_schema_name = tenant_schema
            connection.execute(
                text(f"SET search_path TO {tenant_schema}, shared, public")
            )
            connection.commit()
            Base.metadata.reflect(connection, schema="tenant")

        # Configure the migration context
        with engine.connect() as connection:
            context.configure(
                connection=connection,
                target_metadata=target_metadata,
                render_item=render_item,
                include_name=include_name_from_schemas(
                    ["shared", tenant_schema]
                ),  # Filter from database
                include_object=include_object_from_schemas(
                    ["shared", "tenant"]
                ),  # Filter from code
                include_schemas=True,
                version_table_schema="public",
                transaction_per_migration=True,
            )
            with context.begin_transaction():
                context.run_migrations()

    # Database has no tenant schema, use only the 'shared' schema to detect pobbile migrations
    else:
        # Use database to reflect existing structure into metadata
        Base.metadata.reflect(engine)

        # Configure the migration context
        with engine.connect() as connection:
            context.configure(
                connection=connection,
                target_metadata=target_metadata,
                render_item=render_item,
                include_name=include_name_from_schemas(["shared"]),
                include_object=include_object_from_schemas(["shared"]),
                include_schemas=True,
                version_table_schema="public",
                transaction_per_migration=True,
            )
            with context.begin_transaction():
                context.run_migrations()


# Unused: Migrations as SQL file
def run_migrations_offline():
    """Run migrations in 'offline' mode.

    This configures the context with just a URL
    and not an Engine, though an Engine is acceptable
    here as well.  By skipping the Engine creation
    we don't even need a DBAPI to be available.

    Calls to context.execute() here emit the given string to the
    script output.

    """
    url = config.get_main_option("sqlalchemy.url")
    context.configure(
        url=url,
        target_metadata=target_metadata,
        literal_binds=True,
        dialect_opts={"paramstyle": "named"},
    )

    with context.begin_transaction():
        context.run_migrations()


if context.is_offline_mode():
    run_migrations_offline()
else:
    run_migrations_online()
