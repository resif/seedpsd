"""Cleanup database structure after multi-tenancy migration

Revision ID: cc2560834a33
Revises: 385990f03d31
Create Date: 2024-01-31 14:36:26.769632

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "cc2560834a33"
down_revision = "385990f03d31"
branch_labels = None
depends_on = None


def upgrade():
    engine = op.get_bind()
    inspector = sa.inspect(engine)

    # Create uniqueness constraint on 'shared.source'
    if not inspector.has_index("source", "uq_source_network_id", schema="shared"):
        op.create_unique_constraint(
            op.f("uq_source_network_id"),
            "source",
            ["network_id", "station", "location", "channel"],
            schema="shared",
        )

    # Drop table 'overlap' on every schema
    schemas = engine.execute(sa.text("SELECT schema FROM shared.network")).scalars()
    for schema in schemas:
        if inspector.has_table("overlap", schema=schema):
            op.drop_table("overlap", schema=schema)


def downgrade():
    pass
