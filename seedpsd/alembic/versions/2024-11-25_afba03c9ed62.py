"""Location code must be non-nullable

Revision ID: afba03c9ed62
Revises: cc2560834a33
Create Date: 2024-11-25 09:59:47.808125

"""

import logging

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "afba03c9ed62"
down_revision = "cc2560834a33"
branch_labels = None
depends_on = None


def upgrade():
    # Get connection
    connection = op.get_bind()
    # Migrate shared schema
    upgrade_shared(connection)


def upgrade_shared(connection):
    logging.info("Migrating schema: shared")

    op.alter_column(
        "source",
        "location",
        existing_type=sa.VARCHAR(),
        nullable=False,
        schema="shared",
    )


def downgrade():
    # Get connection
    connection = op.get_bind()

    # Migrate shared schema
    downgrade_shared(connection)


def downgrade_shared(connection):
    logging.info("Migrating schema: shared")

    op.alter_column(
        "source", "location", existing_type=sa.VARCHAR(), nullable=True, schema="shared"
    )
