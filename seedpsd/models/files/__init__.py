import os

import pendulum
import verboselogs

from seedpsd.tools.common import is_temporary_network

logger = verboselogs.VerboseLogger(__name__)


class SeedPsdFile:
    """
    Base class for File classes
    """

    def __init__(
        self,
        year,
        network,
        station,
        location,
        channel,
        quality="D",
        file_name=None,
        parent_path=None,
    ):
        """
        Initialize an SeedPsdFile

        :param year: The year
        :param network: The network code
        :param station: The station code
        :param location: The location code
        :param channel: The channel code
        :param quality: The quality code (fixed to 'D')
        :param file_name: The file name
        :param parent_path: The parent path
        :rtype: SeedPsdFile
        """
        logger.debug(
            f"SeedPsdFile.__init__({year}, {network}, {station}, {location}, {channel}, {quality}, {file_name}, {parent_path})"
        )
        self.year = year
        self.network = network
        self.station = station
        self.location = location
        self.channel = channel
        self.quality = quality
        self._file_name = file_name
        self._parent_path = parent_path

    @property
    def file_name(self):
        """
        Get the file name

        :rtype: str
        """
        if self._file_name:
            return self._file_name
        return self._generated_file_name

    @property
    def _generated_file_name(self):
        """
        Build a file name

        :rtype: str
        """
        return NotImplemented

    @property
    def file_path(self):
        """
        Build the file path

        :rtype: str
        """
        if self._parent_path:
            return os.path.join(self._parent_path, self.file_name)
        msg = f"No parent path for file '{self.file_name}'"
        raise Exception(msg)

    @property
    def exists(self):
        """
        Check if the file exist

        :rtype: bool
        """
        return os.path.exists(self.file_path)

    @property
    def is_readable(self):
        """
        Check if the file is readable

        :rtype: bool
        """
        return os.path.exists(self.file_path) and os.access(self.file_path, os.R_OK)

    @property
    def is_writeable(self):
        """
        Check if the file is writeable

        :rtype: bool
        """
        return os.path.exists(self.file_path) and os.access(self.file_path, os.W_OK)

    @property
    def date_created(self):
        if self.is_readable:
            try:
                ctime = os.path.getctime(self.file_path)
                return pendulum.from_timestamp(ctime)
            except OSError:
                pass
        return None

    @property
    def is_temporary_network(self):
        """
        Return True if the network is temporary

        :rtype: bool
        """
        return is_temporary_network(self.network)
