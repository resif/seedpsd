import os
import re
from pathlib import Path, PurePath

import verboselogs
from fdsnnetextender import FdsnNetExtender

from seedpsd.tools.common import is_temporary_network

from .mseed import MSEEDFile
from .npz import NPZFile

logger = verboselogs.VerboseLogger(__name__)


# MSEED FILE PATH FACTORY ##############################################################################################
########################################################################################################################


class MSEEDFilePathFactory:
    """
    MSEEDFile path factory class
    """

    @staticmethod
    def factory(parent_path, year, network, station=None, channel=None, sds=False):
        """
        Build a full path for a validated data directory

        :param parent_path: The parent path
        :param year: The year
        :param network: The network code
        :param station: The station code
        :param channel: The channel code
        :param sds: Use SDS path pattern
        :return: The data file path
        :rtype: str
        """
        logger.debug(
            f"MSEEDFilePathFactory.factory({parent_path}, {year}, {network}, {station}, {channel}, {sds})"
        )

        path = parent_path
        path = (
            os.path.join(path, str(year), network)
            if sds
            else os.path.join(path, network, str(year))
        )

        if station:
            path = os.path.join(path, station)
            if channel:
                path = os.path.join(path, f"{channel}.D")

        logger.verbose(f"> Generated path: {path}")
        return path

    @staticmethod
    def from_id(parent_path, mseed_id, raise_error=False, sds=False, extended=True):
        """
        Initialize a MSEEDFile path from a MSEED identifier

        :param parent_path: The parent path
        :param mseed_id: The MSEED identifier
        :param raise_error: Raise an Exception on error
        :param sds: Use SDS path pattern
        :param extended: Use extended network code
        :rtype: MSEEDFile
        """
        logger.debug(
            f"MSEEDFilePathFactory.from_id({parent_path}, {mseed_id}, {raise_error}, {sds})"
        )

        match = MSEEDFileFactory.regex_filename.match(mseed_id)
        if match:
            mdict = match.groupdict()
            if extended and is_temporary_network(mdict["network"]):
                network_code = FdsnNetExtender().extend(
                    mdict["network"], f"{mdict['year']}-01-01"
                )
            else:
                network_code = mdict["network"]
            return MSEEDFilePathFactory.factory(
                parent_path,
                mdict["year"],
                network_code,
                mdict["station"],
                mdict["channel"],
                sds=sds,
            )
        if raise_error:
            msg = f"Unable to match an ID pattern for '{mseed_id}'"
            raise Exception(msg)
        logger.warning(f"Unrecognized ID pattern for '{mseed_id}'")
        return None

    @staticmethod
    def from_database(parent_path, file, sds=False, extended=True):
        """
        Initialize a MSEEDFile path from a File instance

        :param parent_path: The parent path
        :param file: The File object
        :param sds: Use SDS path pattern
        :param extended: Use extended network code
        :return: The data file path
        :rtype: str
        """
        logger.debug(
            f"MSEEDFilePathFactory.from_file({parent_path}, {file}, {sds}, {extended})"
        )
        source = file.source
        network_code = source.network.extended_code if extended else source.network.code
        return MSEEDFilePathFactory.factory(
            parent_path,
            file.year,
            network_code,
            source.station,
            source.channel,
            sds=sds,
        )


# MSEED FILE FACTORY ###################################################################################################
########################################################################################################################


class MSEEDFileFactory:
    """
    MSEEDFile factory class
    """

    regex_filename = re.compile(
        r"^(?P<network>[A-Z0-9]+)\.(?P<station>[A-Z0-9]+)\.(?P<location>[A-Z0-9]*)\.(?P<channel>[A-Z0-9]+)\.(?P<quality>[A-Z]{1})\.(?P<year>[0-9]{4})\.(?P<day>[0-9]{3})$"
    )

    @staticmethod
    def factory(
        day,
        year,
        network,
        station,
        location,
        channel,
        quality="D",
        file_name=None,
        parent_path=None,
    ):
        """
        Initialize a MSEEDFile object

        :param day: The day
        :param year: The year
        :param network: The network code
        :param station: The station code
        :param location: The location code
        :param channel: The channel code
        :param quality: The quality code
        :param file_name: The file name (Optional)
        :param parent_path: The parent path (Optional)
        :rtype: MSEEDFile
        """
        logger.debug(
            f"MSEEDFileFactory.factory({day}, {year}, {network}, {station}, {location}, {channel}, {quality}, {file_name}, {parent_path})"
        )
        return MSEEDFile(
            day=day,
            year=year,
            network=network,
            station=station,
            location=location,
            channel=channel,
            quality=quality,
            file_name=file_name,
            parent_path=parent_path,
        )

    @staticmethod
    def from_path(
        file_path, parent_path=None, sds=False, extended=True, raise_error=False
    ):
        """
        Initialize a MSEEDFile object from a filename

        :param file_path: The MSEED file path
        :param parent_path: The optional parent path
        :param sds: Use SDS path pattern
        :param extended: Use extended network code
        :param raise_error: Raise an Exception on error
        :rtype: MSEEDFile
        """
        logger.debug(
            f"MSEEDFileFactory.from_path({file_path}, {parent_path}, {sds}, {extended}, {raise_error})"
        )

        # Strip file_path from whitespace and special characters
        file_path = file_path.strip()

        # Extract the filename from the path
        filename = PurePath(file_path).name

        # Check the filename against a regex pattern
        match = MSEEDFileFactory.regex_filename.match(filename)
        if match:
            # Extract parts from the filename
            mdict = match.groupdict()

            # Check if file exists, extract and use the parent path
            if Path(file_path).exists():
                parent = PurePath(file_path).parent

            # Else, try to use the parent_path
            elif parent_path and Path(parent_path, file_path).exists():
                parent = PurePath(parent_path)

            # Else, try to build the parent path
            else:
                parent = MSEEDFilePathFactory.from_id(
                    parent_path, filename, sds=sds, extended=extended
                )

            # Initialize the MSEEDFile object
            mseed_file = MSEEDFileFactory.factory(
                day=mdict["day"],
                year=mdict["year"],
                network=mdict["network"],
                station=mdict["station"],
                location=mdict["location"],
                channel=mdict["channel"],
                quality=mdict["quality"],
                file_name=filename,
                parent_path=parent,
            )

            # The file exists
            if mseed_file.exists:
                logger.verbose(f"{filename} : Using file from '{parent}'")
                return mseed_file

            # The file does NOT exist
            if raise_error:
                msg = f"Unable to find the file {filename} in '{parent}'"
                raise Exception(msg)
            logger.warning(f"{filename} : Unable to find the file in '{parent}'")
            return None

        # The filename does NOT match the regex pattern
        if raise_error:
            msg = f"Unrecognized filename pattern for '{file_path}'"
            raise Exception(msg)
        logger.warning(f"{file_path} : Unrecognized filename pattern")
        return None

    @staticmethod
    def from_database(file, parent_path, sds=False, extended=True):
        """
        Initialize a MSEEDFile object from a File instance

        :param file: The File object
        :param parent_path: The parent path
        :param sds: Use SDS path pattern
        :param extended: Use extended network code
        :rtype: MSEEDFile
        """
        logger.debug(
            f"MSEEDFileFactory.from_database({file}, {parent_path}, {sds}, {extended})"
        )

        # Extract the source
        source = file.source
        if not source:
            logger.warning(
                f"{file.name} : The file is not linked to a source in database."
            )
            return None

        # Build the path
        path = MSEEDFilePathFactory.from_database(
            parent_path, file, sds=sds, extended=extended
        )

        return MSEEDFileFactory.factory(
            day=file.day,
            year=file.year,
            network=source.network.code,
            station=source.station,
            location=source.location,
            channel=source.channel,
            parent_path=path,
            file_name=file.name,
        )


# NPZ FILE FACTORY #####################################################################################################
########################################################################################################################


class NPZFileFactory:
    """
    NPZFile factory class
    """

    regex_filename = re.compile(
        r"^(?P<network>[A-Z0-9]+)\.(?P<station>[A-Z0-9]+)\.(?P<location>[A-Z0-9]*)\.(?P<channel>[A-Z0-9]+)\.(?P<quality>[A-Z]{1})\.(?P<year>[0-9]{4})\.npz$"
    )
    regex_id = re.compile(
        r"^(?P<network>[A-Z0-9]+)\.(?P<station>[A-Z0-9]+)\.(?P<location>[A-Z0-9]*)\.(?P<channel>[A-Z0-9]+)\.(?P<quality>[A-Z]{1})\.(?P<year>[0-9]{4})$"
    )

    @staticmethod
    def factory(
        year,
        network,
        station,
        location,
        channel,
        quality="D",
        file_name=None,
        parent_path=None,
    ):
        """
        Initialize a NPZFile object

        :param year: The year
        :param network: The network code
        :param station: The station code
        :param location: The location code
        :param channel: The channel code
        :param quality: The channel quality (fixed to 'D')
        :param file_name: The file name (optional)
        :param parent_path: The parent path (optional)
        :rtype: NPZFile
        """
        logger.debug(
            f"NPZFileFactory.factory({year}, {network}, {station}, {location}, {channel}, {quality}, {file_name}, {parent_path})"
        )
        return NPZFile(
            year=year,
            network=network,
            station=station,
            location=location,
            channel=channel,
            quality=quality,
            file_name=file_name,
            parent_path=parent_path,
        )

    @staticmethod
    def from_path(file_path, raise_error=False):
        """
        Initialize a NPZFile object from a NPZ filename

        :param file_path: The NPZ file path
        :param raise_error: Raise an Exception on error
        :rtype: NPZFile
        """
        logger.debug(f"NPZFileFactory.from_path({file_path}, {raise_error})")

        # Extract the filename from the path
        filename = PurePath(file_path).name
        parent = PurePath(file_path).parent

        # Check the filename against a regex pattern
        match = NPZFileFactory.regex_filename.match(filename)
        if match:
            # Extract parts from the filename
            mdict = match.groupdict()

            return NPZFileFactory.factory(
                year=mdict["year"],
                network=mdict["network"],
                station=mdict["station"],
                location=mdict["location"],
                channel=mdict["channel"],
                quality=mdict["quality"],
                file_name=filename,
                parent_path=parent,
            )
        if raise_error:
            msg = f"Unable to match a filename pattern for '{file_path}'"
            raise Exception(msg)
        logger.warning(f"Unrecognized filename pattern for '{file_path}'")
        return None

    @staticmethod
    def from_id(npz_id, raise_error=False):
        """
        Initialize a NPZFile object from a NPZ identifier

        :param npz_id: The NPZ identifier
        :param raise_error: Raise an Exception on error
        :rtype: NPZFile
        """
        logger.debug(f"NPZFileFactory.from_id({npz_id}, {raise_error})")

        # Check the NPZ id against a regex pattern
        match = NPZFileFactory.regex_id.match(npz_id)
        if match:
            # Extract parts from the filename
            mdict = match.groupdict()

            return NPZFileFactory.factory(
                year=mdict["year"],
                network=mdict["network"],
                station=mdict["station"],
                location=mdict["location"],
                channel=mdict["channel"],
                quality=mdict["quality"],
            )
        if raise_error:
            msg = f"Unable to match an ID pattern for '{npz_id}'"
            raise Exception(msg)
        logger.warning(f"Unrecognized ID pattern for '{npz_id}'")
        return None

    @staticmethod
    def from_mseed(mseed):
        """
        Initialize a NPZFile object from a MSEEDFile

        :param mseed: The NPZ file
        :rtype: NPZFile
        """
        logger.debug(f"NPZFileFactory.from_mseed({mseed})")
        return NPZFileFactory.factory(
            year=mseed.year,
            network=mseed.network,
            station=mseed.station,
            location=mseed.location,
            channel=mseed.channel,
            quality=mseed.quality,
        )

    @staticmethod
    def from_mseed_filename(file_path, raise_error=False):
        """
        Initialize a NPZFile object from a MSEED filename

        :param file_path: The MSEED file path
        :param raise_error: Raise an Exception on error
        :rtype: NPZFile
        """
        logger.debug(f"NPZFileFactory.from_mseed_filename({file_path}, {raise_error})")

        # Extract the filename from the path
        filename = PurePath(file_path).name

        # Check the filename against a regex pattern
        match = NPZFileFactory.regex_filename.match(filename)
        if match:
            # Extract parts from the filename
            mdict = match.groupdict()

            return NPZFileFactory.factory(
                year=mdict["year"],
                network=mdict["network"],
                station=mdict["station"],
                location=mdict["location"],
                channel=mdict["channel"],
                quality=mdict["quality"],
            )
        if raise_error:
            msg = f"Unable to match a filename pattern for '{file_path}'"
            raise Exception(msg)
        logger.warning(f"Unrecognized filename pattern for '{file_path}'")
        return None

    # @staticmethod
    # def from_directory(npz_dir):
    #     logger.debug(f"NPZFileFactory.from_directory({npz_dir})")
    #     return [NPZFileFactory.from_filename(filename) for filename in os.listdir(npz_dir)]
