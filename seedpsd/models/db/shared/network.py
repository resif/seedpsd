import uuid

import verboselogs
from sqlalchemy import (
    Boolean,
    Column,
    Date,
    Index,
    Integer,
    PrimaryKeyConstraint,
    String,
    UniqueConstraint,
    and_,
    null,
    or_,
    select,
)
from sqlalchemy.orm import relationship
from sqlalchemy_utils import UUIDType

from seedpsd.models.db.base import Base
from seedpsd.tools.common import date_2100, is_temporary_network, to_date

__all__ = ("Network",)
logger = verboselogs.VerboseLogger(__name__)


class Network(Base):
    __tablename__ = "network"
    __table_args__ = (
        PrimaryKeyConstraint("id"),
        UniqueConstraint("schema"),
        Index("ix_network_code", "code"),
        Index("ix_network_year", "year"),
        Index("ix_network_start", "start"),
        {"schema": "shared"},
    )
    id = Column(UUIDType(), default=uuid.uuid4)
    code = Column(String, nullable=False)
    year = Column(Integer, nullable=False)
    start = Column(Date, nullable=False)
    _end = Column("end", Date)
    temporary = Column(Boolean, nullable=False)
    schema = Column(String, nullable=False)

    sources = relationship(
        "Source", back_populates="network", cascade="all", passive_deletes=True
    )

    @property
    def extended_code(self):
        """
        Get the extended code of the network
        :rtype: str
        """
        if self.temporary and self.year is not None:
            return f"{self.code}{self.year}"
        return self.code

    @property
    def end(self):
        """
        Get the end date
        :rtype: date
        """
        return self._end

    @end.setter
    def end(self, value):
        """
        Set the end date
        :param value: The value
        """
        if value is not None and to_date(value) < date_2100:
            self._end = value
        else:
            self._end = None

    def __repr__(self):
        return f"<Network {self.id} | {self.extended_code} | {self.code} | {self.start} | {self.end} | Temporary={self.temporary}>"

    def __init__(self, code, start, end):
        """
        Create a Source object
        :param code: The code
        :param start: The start time
        :param end: The end time
        """
        logger.debug(f"Network({code}, {start}, {end})")
        self.id = uuid.uuid4()
        self.code = code
        self.start = to_date(start)
        self.end = to_date(end)

        self.year = self.start.year
        self.temporary = is_temporary_network(code)

        if self.temporary:
            self.schema = f"network_{self.code}{self.year}"
        else:
            self.schema = f"network_{self.code}"

    # SQL STATEMENT BUILDING ###########################################################################################

    @classmethod
    def find_id(
        cls,
        code=None,
        year=None,
        start=None,
        start_before=None,
        start_after=None,
        end=None,
        end_before=None,
        end_after=None,
        temporary=None,
    ):
        """
        Build a statement for selecting ID of networks
        :param code: Filter by code
        :param year: Filter by year
        :param start: Filter network starting at a date
        :param start_before: Filter network starting before a date
        :param start_after: Filter network starting after a date
        :param end: Filter network ending at a date
        :param end_before: Filter network ending before a date
        :param end_after: Filter network ending after a date
        :param temporary: Filter by temporary/permanent status
        :return: Select statement
        """
        logger.debug(
            f"Network.find_id({code}, {year}, {start}, {start_before}, {start_after}, {end}, {end_before}, {end_after}, {temporary})"
        )
        statement = select(cls.id)
        return cls.find(
            code,
            year,
            start,
            start_before,
            start_after,
            end,
            end_before,
            end_after,
            temporary,
            statement,
        )

    @classmethod
    def find(
        cls,
        code=None,
        year=None,
        start=None,
        start_before=None,
        start_after=None,
        end=None,
        end_before=None,
        end_after=None,
        temporary=None,
        statement=None,
    ):
        """
        Build a statement for selecting networks
        :param code: Filter by code
        :param year: Filter by year
        :param start: Filter network starting at a date
        :param start_before: Filter network starting before a date
        :param start_after: Filter network starting after a date
        :param end: Filter network ending at a date
        :param end_before: Filter network ending before a date
        :param end_after: Filter network ending after a date
        :param temporary: Filter by temporary/permanent status
        :param statement: An initialized statement
        :return: Select statement
        """
        logger.debug(
            f"Network.find({code}, {year}, {start}, {start_before}, {start_after}, {end}, {end_before}, {end_after}, {temporary}, {statement})"
        )

        if statement is None:
            statement = select(cls)

        # Build WHERE part of statement
        # Initialize a container for WHERE conditions
        params = []

        if code and code != "*":
            params.append(cls._where_wildcard(code, cls.code))

        if year:
            params.append(cls.year == year)

        if start:
            params.append(cls.start == to_date(start))
        if start_before:
            params.append(cls.start < to_date(start_before))
        if start_after:
            params.append(cls.start > to_date(start_after))

        if end:
            end = to_date(end)
            if end > date_2100:
                end = null()
            params.append(cls._end == end)
        if end_before:
            params.append(cls._end < to_date(end_before))
        if end_after:
            params.append(or_(cls._end > to_date(end_after), cls._end == null()))

        if temporary is not None:
            params.append(cls.temporary == temporary)

        # Add the WHERE conditions to the SQL statement
        if params:
            statement = (
                statement.where(and_(*params))
                if len(params) > 1
                else statement.where(*params)
            )

        # Build ORDER BY part of statement
        return statement.order_by(cls.code, cls.year)

    # LOAD FROM DB OR CREATE ###########################################################################################

    @classmethod
    def factory(cls, db_session, code=None, start=None, end=None, add_session=True):
        """
        Load from database or create a Source object
        :param db_session: The database session
        :param code: The network code
        :param start: The network start date
        :param end: The network end date
        :param add_session: Add the created object to the DB session
        :rtype: Network
        """
        logger.debug(
            f"Network.factory({db_session}, {code}, {start}, {end}, {add_session})"
        )
        network = db_session.find_one(cls.find(code, year=to_date(start).year))
        if not network:
            logger.info(f"> Creating network {code}")
            network = cls(code, start, end)
            network.schema_exists = False
            if add_session:
                db_session.add(network)

        elif network.temporary:
            network.schema_exists = True
            logger.info(f"> Updating network {network.extended_code}")

            network.end = to_date(end)
            if add_session:
                db_session.add(network)

        else:
            network.schema_exists = True

        return network
