import verboselogs
from sqlalchemy import create_engine
from sqlalchemy.exc import MultipleResultsFound
from sqlalchemy.orm import Session, sessionmaker
from sqlalchemy.pool import NullPool, QueuePool, StaticPool

from seedpsd.errors import TooManyResultsError

logger = verboselogs.VerboseLogger(__name__)


class DBSession(Session):
    def find_all(self, statement):
        return self.execute(statement).scalars().all()

    def find_first(self, statement):
        return self.execute(statement).scalars().first()

    def find_one(self, statement, or_none=True):
        try:
            if or_none:
                return self.execute(statement).scalars().one_or_none()
            return self.execute(statement).scalars().one()
        except MultipleResultsFound:
            msg = "source"
            raise TooManyResultsError(msg)

    # FACTORIES ########################################################################################################

    @classmethod
    def factory(cls, engine):
        logger.debug(f"DBSession.factory({engine})")
        return sessionmaker(bind=engine, class_=cls)()

    @staticmethod
    def engine_factory(
        settings,
        use_pool=True,
        read_only=False,
        deferrable=False,
        isolation_level=None,
        tenant_schema=None,
        engine=None,
    ):
        logger.debug(
            f"DBSession.engine_factory({settings}, {use_pool}, {read_only}, {deferrable}, {isolation_level}, {tenant_schema}, {engine})"
        )

        execution_options = {}
        if read_only:
            execution_options["postgresql_readonly"] = True
            execution_options["isolation_level"] = "AUTOCOMMIT"

        elif isolation_level and isolation_level in (
            "AUTOCOMMIT",
            "READ COMMITTED",
            "READ UNCOMMITTED",
            "REPEATABLE READ",
            "SERIALIZABLE",
        ):
            execution_options["isolation_level"] = isolation_level

        if deferrable:
            execution_options["postgresql_deferrable"] = True

        if tenant_schema:
            execution_options["schema_translate_map"] = {"tenant": tenant_schema}
        else:
            execution_options["schema_translate_map"] = None

        if engine:
            logger.debug(f"Configuring the pool with: {execution_options}")
            return engine.execution_options(**execution_options)

        params = {}

        if execution_options:
            params["execution_options"] = execution_options

        # Pool configuration
        params["pool_pre_ping"] = settings.database_pool_pre_ping
        params["pool_recycle"] = settings.database_pool_recycle

        if not use_pool or settings.database_pool_type == "null":
            params["poolclass"] = NullPool
            logger.debug(f"Creating a NullPool with: {params}")
        elif settings.database_pool_type == "static":
            params["poolclass"] = StaticPool
            logger.debug(f"Creating a StaticPool with: {params}")
        else:
            params["poolclass"] = QueuePool
            params["pool_size"] = settings.database_pool_size_min
            params["max_overflow"] = settings.database_pool_size_max
            params["pool_timeout"] = settings.database_pool_timeout
            logger.debug(f"Creating a QueuePool with: {params}")

        return create_engine(settings.database_uri, **params)

    # DEBUG ############################################################################################################

    def debug_statement(self, statement):
        try:
            logger.debug(
                f"Statement: {statement.compile(self.get_bind(), compile_kwargs={'literal_binds': True})}"
            )
        except Exception:
            logger.debug(f"Statement: {statement}")
