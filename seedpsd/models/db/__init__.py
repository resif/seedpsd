import verboselogs
from sqlalchemy.schema import CreateSchema

from .base import Base, metadata
from .session import DBSession
from .shared import *
from .tenant import *

logger = verboselogs.VerboseLogger(__name__)


def create_tenant_schema(db_session, schema, commit=False):
    logger.debug(f"create_tenant_schema({db_session}, {schema}, {commit})")

    # Create schema
    db_session.execute(CreateSchema(schema, if_not_exists=True))

    if commit:
        db_session.commit()


def create_tenant_tables(db_session, schema, commit=False):
    logger.debug(f"create_tenant_tables({db_session}, {schema}, {commit})")

    # Create tables
    tables = []
    for table in metadata.tables.values():
        if table.schema == "tenant":
            tables.append(table)

    metadata.create_all(bind=db_session, tables=tables)

    if commit:
        db_session.commit()
