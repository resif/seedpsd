import uuid

import verboselogs
from obspy import UTCDateTime
from sqlalchemy import (
    Column,
    DateTime,
    ForeignKey,
    Index,
    PrimaryKeyConstraint,
    and_,
    select,
)
from sqlalchemy.orm import relationship

from seedpsd.models.db.base import Base
from seedpsd.tools.common import to_datetime

from .stream import Stream

__all__ = ("Gap",)
logger = verboselogs.VerboseLogger(__name__)


class Gap(Base):
    __tablename__ = "gap"
    __table_args__ = (
        PrimaryKeyConstraint("stream_id", "start"),
        Index("ix_gap_stream", "stream_id"),
    )

    # stream_id = Column(ForeignKey("tenant.stream.id", onupdate="CASCADE", ondelete="CASCADE"), nullable=False)
    stream_id = Column(
        ForeignKey("stream.id", onupdate="CASCADE", ondelete="CASCADE"), nullable=False
    )
    stream = relationship("Stream", back_populates="gaps")

    start = Column(DateTime(), nullable=False)
    end = Column(DateTime(), nullable=False)

    @property
    def start_utc(self):
        """
        Get the start datetime as an UTCDateTime
        :rtype: UTCDateTime
        """
        return UTCDateTime(self.start)

    @property
    def end_utc(self):
        """
        Get the end datetime as an UTCDateTime
        :rtype: UTCDateTime
        """
        return UTCDateTime(self.end)

    def __repr__(self):
        return f"<Gap {self.stream_id} | {self.start} | {self.end}>"

    def __init__(self, start, end):
        """
        Create a Gap object
        :param start: The start datetime
        :param end: The end datetime
        :rtype: Gap
        """
        logger.debug(f"Gap({start}, {end})")
        self.start = to_datetime(start)
        self.end = to_datetime(end)

    # SQL STATEMENT BUILDING ###########################################################################################

    @classmethod
    def find(
        cls,
        stream=None,
        start=None,
        start_before=None,
        start_after=None,
        end=None,
        end_before=None,
        end_after=None,
        statement=None,
    ):
        """
        Build a statement for selecting gaps
        :param stream: Filter gap related to a stream object
        :param start: Filter gap starting at a datetime
        :param start_before: Filter gap starting before a datetime
        :param start_after: Filter gap starting after a datetime
        :param end: Filter gap ending at a datetime
        :param end_before: Filter gap ending before a datetime
        :param end_after: Filter gap ending after a datetime
        :param statement: An initialized statement
        :return: Select statement
        """
        logger.debug(
            f"Gap.find({stream}, {start}, {start_before}, {start_after}, {end}, {end_before}, {end_after}, {statement})"
        )

        if statement is None:
            statement = select(cls)

        # Build WHERE part of statement
        # Initialize a container for WHERE conditions
        params = []

        # Build WHERE conditions
        if stream:
            if isinstance(stream, Stream):
                params.append(cls.stream == stream)
            elif isinstance(stream, uuid.UUID):
                params.append(cls.stream_id == stream)
            elif isinstance(stream, (list, tuple, set)):
                ids = []
                for strm in stream:
                    if isinstance(strm, Stream):
                        ids.append(strm.id)
                    elif isinstance(strm, uuid.UUID):
                        ids.append(strm)
                params.append(cls.stream_id.in_(ids))

        if start:
            params.append(cls.start == to_datetime(start))
        if start_before:
            params.append(cls.start < to_datetime(start_before))
        if start_after:
            params.append(cls.start > to_datetime(start_after))

        if end:
            params.append(cls.end == to_datetime(end))
        if end_before:
            params.append(cls.end < to_datetime(end_before))
        if end_after:
            params.append(cls.end > to_datetime(end_after))

        # Add the WHERE conditions to the SQL statement
        if params:
            statement = (
                statement.where(and_(*params))
                if len(params) > 1
                else statement.where(*params)
            )

        # Build ORDER BY part of statement
        return statement.order_by(cls.start)

    # LOAD FROM DB OR CREATE ###########################################################################################

    @classmethod
    def factory(cls, db_session, stream=None, start=None, end=None, add_session=True):
        """
        Load from database or create a Gap object
        :param db_session: The database session
        :param stream: The related stream object
        :param start: The start datetime
        :param end: The end datetime
        :param add_session: Add the created object to the DB session
        :rtype: Gap
        """
        logger.debug(
            f"Gap.factory({db_session}, {stream}, {start}, {end}, {add_session})"
        )
        gap = db_session.find_one(cls.find(stream=stream, start=start, end=end))
        if not gap:
            logger.debug("> Creating new Gap")
            gap = cls(start, end)
            if stream:
                if isinstance(stream, uuid.UUID):
                    gap.stream_id = stream
                else:
                    gap.stream = stream
            if add_session:
                db_session.add(gap)
        return gap
