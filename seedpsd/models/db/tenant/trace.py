import uuid

import verboselogs
from obspy import UTCDateTime
from sqlalchemy import (
    Column,
    DateTime,
    Float,
    ForeignKey,
    Index,
    PrimaryKeyConstraint,
    and_,
    select,
)
from sqlalchemy.dialects.postgresql import ARRAY
from sqlalchemy.orm import relationship

from seedpsd.models.db.base import Base
from seedpsd.tools.common import to_datetime

from .stream import Stream

__all__ = ("Trace",)
logger = verboselogs.VerboseLogger(__name__)


class Trace(Base):
    __tablename__ = "trace"
    __table_args__ = (
        PrimaryKeyConstraint("stream_id", "timestamp"),
        Index("ix_trace_stream", "stream_id"),
    )

    # stream_id = Column(ForeignKey("tenant.stream.id", onupdate="CASCADE", ondelete="CASCADE"), nullable=False)
    stream_id = Column(
        ForeignKey("stream.id", onupdate="CASCADE", ondelete="CASCADE"), nullable=False
    )
    stream = relationship("Stream", back_populates="traces")

    timestamp = Column(DateTime(), nullable=False)
    data = Column(ARRAY(Float))  # deferred(Column(ARRAY(Float)))

    @property
    def timestamp_utc(self):
        """
        Get the datetime as an UTCDateTime
        :rtype: UTCDateTime
        """
        return UTCDateTime(self.timestamp)

    def __repr__(self):
        return f"<Trace {self.stream_id} | {self.timestamp}>"

    def __init__(self, timestamp):
        """
        Create a Trace object
        :param timestamp: The datetime
        """
        logger.debug(f"Trace({timestamp})")
        self.timestamp = to_datetime(timestamp)

    # SQL STATEMENT BUILDING ###########################################################################################

    @classmethod
    def find(cls, stream=None, timestamp=None, before=None, after=None, statement=None):
        """
        Build a statement for selecting traces
        :param stream: Filter trace related to a stream object
        :param timestamp: Filter trace at a datetime
        :param before: Filter trace before a datetime
        :param after: Filter trace after a datetime
        :param statement: An initialized statement
        :return: Select statement
        """
        logger.debug(
            f"Trace.find({stream}, {timestamp}, {before}, {after}, {statement})"
        )

        if statement is None:
            statement = select(cls)

        # Build WHERE part of statement
        # Initialize a container for WHERE conditions
        params = []

        # Build WHERE conditions
        if stream:
            if isinstance(stream, Stream):
                params.append(cls.stream == stream)
            elif isinstance(stream, uuid.UUID):
                params.append(cls.stream_id == stream)
            elif isinstance(stream, (list, tuple, set)):
                ids = []
                for strm in stream:
                    if isinstance(strm, Stream):
                        ids.append(strm.id)
                    elif isinstance(strm, uuid.UUID):
                        ids.append(strm)
                params.append(cls.stream_id.in_(ids))

        if timestamp:
            params.append(cls.timestamp == to_datetime(timestamp))
        if before:
            params.append(cls.timestamp < to_datetime(before))
        if after:
            params.append(cls.timestamp > to_datetime(after))

        # Add the WHERE conditions to the SQL statement
        if params:
            statement = (
                statement.where(and_(*params))
                if len(params) > 1
                else statement.where(*params)
            )

        # Build ORDER BY part of statement
        return statement.order_by(cls.timestamp)

    # LOAD FROM DB OR CREATE ###########################################################################################

    @classmethod
    def factory(cls, db_session, stream=None, timestamp=None, add_session=True):
        """
        Load from database or create a Trace object
        :param db_session: The database session
        :param stream: The related Stream object
        :param timestamp: The datetime
        :param add_session: Add the created object to the DB session
        :rtype: Trace
        """
        logger.debug(
            f"Trace.factory({db_session}, {stream}, {timestamp}, {add_session})"
        )
        trace = db_session.find_one(cls.find(stream=stream, timestamp=timestamp))
        if not trace:
            logger.debug("> Creating new Trace")
            trace = cls(timestamp)
            if stream:
                if isinstance(stream, uuid.UUID):
                    trace.stream_id = stream
                else:
                    trace.stream = stream
            if add_session:
                db_session.add(trace)
        return trace
