import uuid

import verboselogs
from obspy import UTCDateTime
from sqlalchemy import (
    Column,
    DateTime,
    ForeignKey,
    Index,
    PrimaryKeyConstraint,
    and_,
    null,
    or_,
    select,
)
from sqlalchemy.orm import deferred, relationship
from sqlalchemy_utils import JSONType, UUIDType

from seedpsd.models.db.base import Base
from seedpsd.models.db.shared.source import Source
from seedpsd.tools.common import datetime_2100, to_datetime

__all__ = ("Epoch",)
logger = verboselogs.VerboseLogger(__name__)


class Epoch(Base):
    __tablename__ = "epoch"
    __table_args__ = (
        PrimaryKeyConstraint("id"),
        Index("ix_epoch_source", "source_id"),
    )

    id = Column(UUIDType(), default=uuid.uuid4)  # FIXME

    source_id = Column(
        ForeignKey("shared.source.id", onupdate="CASCADE", ondelete="CASCADE"),
        nullable=False,
    )
    source = relationship("Source", back_populates="epochs")

    start = Column(DateTime(), nullable=False)
    _end = Column("end", DateTime())

    instrument = deferred(Column(JSONType))

    streams = relationship(
        "Stream", back_populates="epoch", cascade="all", passive_deletes=True
    )

    @property
    def start_utc(self):
        """
        Get the start datetime as an UTCDateTime
        :rtype: UTCDateTime
        """
        return UTCDateTime(self.start)

    @property
    def end_utc(self):
        """
        Get the end datetime as an UTCDateTime
        :rtype: UTCDateTime
        """
        if self._end:
            return UTCDateTime(self._end)
        return None

    @property
    def end(self):
        """
        Get the end datetime
        :rtype: datetime
        """
        return self._end

    @end.setter
    def end(self, value):
        """
        Set the end datetime
        :param value: The value
        """
        if value is not None and value < datetime_2100():
            self._end = value
        else:
            self._end = None

    def __init__(self, start, end, instrument):
        logger.debug(f"Epoch({start}, {end}, {instrument})")
        self.id = uuid.uuid4()
        self.start = to_datetime(start)
        self.end = to_datetime(end)
        self.instrument = instrument

    def __repr__(self):
        return f"<Epoch {self.id} | {self.source.nslc if self.source else self.source_id} | {self.start} | {self.end}>"

    def update(self, end=None, instrument=None):
        """
        Update the epoch
        :param end: The end datetime
        :param instrument: The instrument properties
        :return: True if the epoch has been updated, else False
        :rtype: bool
        """
        logger.debug(f"Epoch.update({end}, {instrument})")
        changed = False
        resized = False

        # Handles infinity endtime
        end = to_datetime(end)
        if end is not None and end > datetime_2100():
            end = None

        # Detect resize of epoch
        if self.end != end:
            logger.verbose("End date mismatch. Epoch needs update!")
            self.end = end
            resized = True

        # Detect change of metadata
        if instrument is not None and self.instrument != instrument:
            logger.verbose("Channel metadata mismatch. Epoch needs update!")
            self.instrument = instrument
            changed = True

        # Update linked streams if needed
        if changed or resized:
            for stream in self.streams:
                # Metadata changed: Invalidate streams
                if changed:
                    logger.warning(f"Epoch {self} changed: invalidating {stream}")
                    stream.invalidate()

                # Epoch resized: Unlink streams
                if resized and (
                    stream.end < self.start
                    or (self.end is not None and stream.start > self.end)
                ):
                    logger.warning(f"Epoch {self} resized: unlinking {stream}")
                    stream.epoch = None

        return changed or resized

    # SQL STATEMENT BUILDING ###########################################################################################

    @classmethod
    def find_id(
        cls,
        source=None,
        start=None,
        start_before=None,
        start_after=None,
        end=None,
        end_before=None,
        end_after=None,
    ):
        """
        Build a statement for selecting ID of epochs
        :param _id: Filter epoch by an ID
        :param source: Filter epoch related to a source object
        :param start: Filter epoch starting at a datetime
        :param start_before: Filter epoch starting before a datetime
        :param start_after: Filter epoch starting after a datetime
        :param end: Filter epoch ending at a datetime
        :param end_before: Filter epoch ending before a datetime
        :param end_after: Filter epoch ending after a datetime
        :return: Select statement
        """
        logger.debug(
            f"Epoch.find_id({source}, {start}, {start_before}, {start_after}, {end}, {end_before}, {end_after})"
        )
        statement = select(cls.id)
        return cls.find(
            source,
            start,
            start_before,
            start_after,
            end,
            end_before,
            end_after,
            statement,
        )

    @classmethod
    def find(
        cls,
        source=None,
        start=None,
        start_before=None,
        start_after=None,
        end=None,
        end_before=None,
        end_after=None,
        statement=None,
    ):
        """
        Build a statement for selecting epochs
        :param source: Filter epoch related to a source object
        :param start: Filter epoch starting at a datetime
        :param start_before: Filter epoch starting before a datetime
        :param start_after: Filter epoch starting after a datetime
        :param end: Filter epoch ending at a datetime
        :param end_before: Filter epoch ending before a datetime
        :param end_after: Filter epoch ending after a datetime
        :return: Select statement
        :return: Select statement
        """
        logger.debug(
            f"Epoch.find({source}, {start}, {start_before}, {start_after}, {end}, {end_before}, {end_after}, {statement})"
        )

        if statement is None:
            statement = select(cls)

        params = []

        if source:
            if isinstance(source, Source):
                params.append(cls.source == source)
            elif isinstance(source, uuid.UUID):
                params.append(cls.source_id == source)
            elif isinstance(source, (list, tuple, set)):
                ids = []
                for src in source:
                    if isinstance(src, Source):
                        ids.append(src.id)
                    elif isinstance(src, uuid.UUID):
                        ids.append(src)
                params.append(cls.source_id.in_(ids))

        if start:
            params.append(cls.start == to_datetime(start))
        if end:
            end = to_datetime(end)
            if end > datetime_2100():
                end = null()
            params.append(cls._end == end)

        if start_before:
            params.append(cls.start < to_datetime(start_before))
        if start_after:
            params.append(cls.start > to_datetime(start_after))
        if end_before:
            params.append(and_(cls._end != null(), cls._end < to_datetime(end_before)))
        if end_after:
            params.append(or_(cls._end > to_datetime(end_after), cls._end == null()))

        # Add the WHERE conditions to the SQL statement
        if params:
            statement = (
                statement.where(and_(*params))
                if len(params) > 1
                else statement.where(*params)
            )

        # Build ORDER BY part of statement
        return statement.order_by(cls.source_id, cls.start)
