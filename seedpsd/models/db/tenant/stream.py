import uuid

import verboselogs
from obspy import UTCDateTime
from sqlalchemy import (
    Boolean,
    Column,
    DateTime,
    Float,
    ForeignKey,
    Index,
    PrimaryKeyConstraint,
    and_,
    delete,
    select,
    true,
)
from sqlalchemy.orm import relationship
from sqlalchemy_utils import UUIDType

from seedpsd.models.db.base import Base
from seedpsd.models.db.shared.file import File
from seedpsd.models.db.shared.source import Source
from seedpsd.tools.common import to_datetime

__all__ = ("Stream",)
logger = verboselogs.VerboseLogger(__name__)


class Stream(Base):
    __tablename__ = "stream"
    __table_args__ = (
        PrimaryKeyConstraint("id"),
        Index("ix_stream_source", "source_id"),
        Index("ix_stream_file", "file_id"),
        Index("ix_stream_epoch", "epoch_id"),
    )

    id = Column(UUIDType(), default=uuid.uuid4, nullable=False)

    file_id = Column(
        ForeignKey("shared.file.id", onupdate="CASCADE", ondelete="CASCADE"),
        nullable=False,
    )
    file = relationship("File", back_populates="streams")

    source_id = Column(
        ForeignKey("shared.source.id", onupdate="CASCADE", ondelete="CASCADE"),
        nullable=False,
    )
    source = relationship("Source", back_populates="streams")

    # epoch_id = Column(ForeignKey("tenant.epoch.id", onupdate="CASCADE", ondelete="SET NULL"), nullable=True)
    epoch_id = Column(
        ForeignKey("epoch.id", onupdate="CASCADE", ondelete="SET NULL"), nullable=True
    )
    epoch = relationship("Epoch", back_populates="streams")

    valid = Column(Boolean, default=True, server_default=true(), nullable=False)
    start = Column(DateTime(), nullable=False)
    end = Column(DateTime(), nullable=False)
    sampling_rate = Column(Float, nullable=False)

    traces = relationship(
        "Trace",
        back_populates="stream",
        cascade="all, delete-orphan",
        passive_deletes=True,
    )
    gaps = relationship(
        "Gap",
        back_populates="stream",
        cascade="all, delete-orphan",
        passive_deletes=True,
    )

    @property
    def start_utc(self):
        """
        Get the start datetime as an UTCDateTime
        :rtype: UTCDateTime
        """
        return UTCDateTime(self.start)

    @property
    def end_utc(self):
        """
        Get the end datetime as an UTCDateTime
        :rtype: UTCDateTime
        """
        return UTCDateTime(self.end)

    def __repr__(self):
        return f"<Stream {self.id} | {self.source.nslc if self.source else self.source_id} | {self.start} | {self.end} | {self.sampling_rate}>"

    def __init__(self, start, end, sampling_rate):
        """
        Create a Stream object
        :param start: The start datetime
        :param end: The end datetime
        :param sampling_rate: The sample rate
        """
        logger.debug(f"Stream({start}, {end}, {sampling_rate})")
        self.id = uuid.uuid4()
        self.start = to_datetime(start)
        self.end = to_datetime(end)
        self.sampling_rate = sampling_rate

    def invalidate(self):
        """
        Invalidate the stream
        """
        logger.debug("Stream.invalidate()")

        # Flag the stream as invalid
        self.valid = False

        # Flag source files as invalid
        self.file.invalidate()

        return True

    # SQL STATEMENT BUILDING ###########################################################################################

    @classmethod
    def delete(
        cls,
        source=None,
        sampling_rate=None,
        start=None,
        start_before=None,
        start_after=None,
        end=None,
        end_before=None,
        end_after=None,
        file=None,
    ):
        """
        Build a statement for deleting streams
        :param source: Filter stream related to a source object
        :param sampling_rate: Filter stream by sampling rate
        :param start: Filter stream starting at a datetime
        :param start_before: Filter stream starting before a datetime
        :param start_after: Filter stream starting after a datetime
        :param end: Filter stream ending at a datetime
        :param end_before: Filter stream ending before a datetime
        :param end_after: Filter stream ending after a datetime
        :param file: Filter stream related to a file object
        :return: Select statement
        """
        logger.debug(
            f"Stream.delete({source}, {sampling_rate}, {start}, {start_before}, {start_after}, {end}, {end_before}, {end_after}, {file})"
        )
        statement = delete(cls)
        return cls.find(
            source,
            sampling_rate,
            start,
            start_before,
            start_after,
            end,
            end_before,
            end_after,
            file,
            statement,
            ordered=False,
        )

    @classmethod
    def find_id(
        cls,
        source=None,
        sampling_rate=None,
        start=None,
        start_before=None,
        start_after=None,
        end=None,
        end_before=None,
        end_after=None,
        file=None,
    ):
        """
        Build a statement for selecting ID of streams
        :param source: Filter stream related to a source object
        :param sampling_rate: Filter stream by sampling rate
        :param start: Filter stream starting at a datetime
        :param start_before: Filter stream starting before a datetime
        :param start_after: Filter stream starting after a datetime
        :param end: Filter stream ending at a datetime
        :param end_before: Filter stream ending before a datetime
        :param end_after: Filter stream ending after a datetime
        :param file: Filter stream related to a file object
        :return: Select statement
        """
        logger.debug(
            f"Stream.find_id({source}, {sampling_rate}, {start}, {start_before}, {start_after}, {end}, {end_before}, {end_after}, {file})"
        )
        statement = select(cls.id)
        return cls.find(
            source,
            sampling_rate,
            start,
            start_before,
            start_after,
            end,
            end_before,
            end_after,
            file,
            statement,
        )

    @classmethod
    def find(
        cls,
        source=None,
        sampling_rate=None,
        start=None,
        start_before=None,
        start_after=None,
        end=None,
        end_before=None,
        end_after=None,
        file=None,
        statement=None,
        ordered=True,
    ):
        """
        Build a statement for selecting streams
        :param source: Filter stream related to a source object
        :param sampling_rate: Filter stream by sampling rate
        :param start: Filter stream starting at a datetime
        :param start_before: Filter stream starting before a datetime
        :param start_after: Filter stream starting after a datetime
        :param end: Filter stream ending at a datetime
        :param end_before: Filter stream ending before a datetime
        :param end_after: Filter stream ending after a datetime
        :param file: Filter stream related to a file object
        :param statement: An initialized statement
        :return: Select statement
        """
        logger.debug(
            f"Stream.find({source}, {sampling_rate}, {start}, {start_before}, {start_after}, {end}, {end_before}, {end_after}, {file}, {statement})"
        )

        if statement is None:
            statement = select(cls)

        params = []

        if source:
            if isinstance(source, Source):
                params.append(cls.source == source)
            elif isinstance(source, uuid.UUID):
                params.append(cls.source_id == source)
            elif isinstance(source, (list, tuple, set)):
                ids = []
                for src in source:
                    if isinstance(src, Source):
                        ids.append(src.id)
                    elif isinstance(src, uuid.UUID):
                        ids.append(src)
                params.append(cls.source_id.in_(ids))

        if file:
            if isinstance(file, File):
                params.append(cls.file == file)
            elif isinstance(file, uuid.UUID):
                params.append(cls.file_id == file)
            elif isinstance(file, (list, tuple, set)):
                ids = []
                for f in file:
                    if isinstance(f, File):
                        ids.append(f.id)
                    elif isinstance(f, uuid.UUID):
                        ids.append(f)
                params.append(cls.file_id.in_(ids))

        if sampling_rate is not None:
            params.append(cls.sampling_rate == sampling_rate)

        if start:
            params.append(cls.start == to_datetime(start))
        if start_before:
            params.append(cls.start < to_datetime(start_before))
        if start_after:
            params.append(cls.start > to_datetime(start_after))

        if end:
            params.append(cls.end == to_datetime(end))
        if end_before:
            params.append(cls.end < to_datetime(end_before))
        if end_after:
            params.append(cls.end > to_datetime(end_after))

        # Add the WHERE conditions to the SQL statement
        if params:
            statement = (
                statement.where(and_(*params))
                if len(params) > 1
                else statement.where(*params)
            )

        # Build ORDER BY part of statement
        if ordered:
            statement = statement.order_by(cls.start)
            # statement = statement.order_by(Network.code, Network.year, Source.station, Source.location, Source.channel, Stream.start)

        return statement

    # LOAD FROM DB OR CREATE ###########################################################################################

    @classmethod
    def factory(
        cls,
        db_session,
        source=None,
        sampling_rate=None,
        start=None,
        end=None,
        add_session=True,
    ):
        """
        Load from database or create a Stream object
        :param db_session: The database session
        :param source: The related Source object
        :param sampling_rate: The sampling rate
        :param start: The start datetime
        :param end: The end datetime
        :param add_session: Add the created object to the DB session
        :rtype: Stream
        """
        logger.debug(
            f"Stream.factory({db_session}, {source}, {sampling_rate}, {start}, {end}, {add_session})"
        )
        stream = db_session.find_one(
            cls.find(source=source, sampling_rate=sampling_rate, start=start, end=end)
        )
        if not stream:
            logger.debug("> Creating new Stream")
            stream = cls(start, end, sampling_rate)
            if source:
                if isinstance(source, Source):
                    stream.source = source
                elif isinstance(source, uuid.UUID):
                    stream.source_id = source
            if add_session:
                db_session.add(stream)
        return stream
