from sqlalchemy import inspect, or_, select
from sqlalchemy.orm import declarative_base
from sqlalchemy.schema import MetaData


class CustomBase:
    @classmethod
    def select(cls):
        """Initiate a query on this model.
        Example::
            User.select().order_by(User.username)
        """
        return select(cls)

    @staticmethod
    def _where_wildcard(value, property):
        if "," in value:
            conditions = []
            for token in value.split(","):
                if "?" in token or "*" in token:
                    conditions.append(
                        property.like(token.replace("?", "_").replace("*", "%"))
                    )
                elif token in ("--", "=="):
                    conditions.append(property.is_(None))
                else:
                    conditions.append(property == token)
            return or_(*conditions)
        if "?" in value or "*" in value:
            return property.like(value.replace("?", "_").replace("*", "%"))
        if value is None or value in ("--", "=="):
            return property.is_(None)
        return property == value

    @property
    def is_detached(self):
        return inspect(self).detached


# Recommended naming convention used by Alembic, as various different database
# providers will autogenerate vastly different names making migrations more
# difficult. See: http://alembic.readthedocs.org/en/latest/naming.html
NAMING_CONVENTION = {
    "ix": "ix_%(column_0_label)s",
    "uq": "uq_%(table_name)s_%(column_0_name)s",
    "ck": "ck_%(table_name)s_%(constraint_name)s",
    "fk": "fk_%(table_name)s_%(column_0_name)s_%(referred_table_name)s",
    "pk": "pk_%(table_name)s",
}

metadata = MetaData(naming_convention=NAMING_CONVENTION, schema="tenant")
Base = declarative_base(cls=CustomBase, metadata=metadata)


def get_shared_metadata():
    meta = MetaData(naming_convention=NAMING_CONVENTION)
    for table in Base.metadata.tables.values():
        if table.schema != "tenant":
            table.tometadata(meta)
    return meta


def get_tenant_specific_metadata():
    meta = MetaData(naming_convention=NAMING_CONVENTION, schema="tenant")
    for table in Base.metadata.tables.values():
        if table.schema == "tenant":
            table.tometadata(meta)
    return meta
