import numpy as np
import verboselogs
from obspy.core import UTCDateTime

from seedpsd.errors import SkippedTraceError

# ==== PATCH ====
# Use a local copy of PPSD extracted from ObsPy v1.4.1
# until the next version of ObsPy is compatible with SQLAlchemy > 2.0
# from obspy.signal import PPSD as ObspyPPSD
from .spectral_estimation import PPSD as ObspyPPSD  # noqa: N811

# ===============

logger = verboselogs.VerboseLogger(__name__)


########################################################################################################################
# PPSD Customization
####################


class CustomPPSD(ObspyPPSD):
    def __check_histogram(self):
        """
        Duplicated method (not callable from a child-class)
        """
        # check if any data has been added yet
        if self._current_hist_stack is None:
            if self._times_processed:
                self.calculate_histogram()
            else:
                msg = "No data accumulated"
                raise Exception(msg)

    def get_histogram_values(
        self,
        x_min=None,
        x_max=None,
        y_min=None,
        y_max=None,
        z_min=None,
        z_max=None,
        xaxis_frequency=False,
    ):
        """
        Get values from histogram

        Note: Extracted from PPSD.plot() and PPSD._plot_histogram()
        :param x_min: X minimum value
        :param x_max: X maximum value
        :param y_min: Y minimum value
        :param y_max: Y maximum value
        :param z_min: Z minimum value
        :param z_max: Z maximum value
        :param xaxis_frequency: Use frequencies on X axis (default=False)
        :return: Values of the histogram
        """

        # Initialize the histogram
        self.__check_histogram()

        # Get coordinates (X, Y)
        xedges = self.period_xedges
        if xaxis_frequency:
            xedges = 1.0 / xedges

        coordinates = np.meshgrid(xedges, self.db_bin_edges)

        # Get percentages (Z)
        percentages = (
            self.current_histogram * 100.0 / (self.current_histogram_count or 1)
        )

        # Filter coordinates with percentages
        data_x = []
        data_y = []
        data_z = []
        for row_x, row_y, row_z in zip(
            coordinates[0], coordinates[1], percentages.T, strict=False
        ):
            for x, y, z in zip(row_x, row_y, row_z, strict=False):
                # Check empty Z (no color point at these coordinates)
                if not z:
                    continue

                # Check X limits
                if x_min is not None and x < x_min:
                    continue
                if x_max is not None and x > x_max:
                    continue

                # Check Y limits
                if y_min is not None and y < y_min:
                    continue
                if y_max is not None and y > y_max:
                    continue

                # Check Z limits
                if z_min is not None and z < z_min:
                    continue
                if z_max is not None and z > z_max:
                    continue

                # Append this tuple to data
                data_x.append(x)
                data_y.append(y)
                data_z.append(z)

        return np.column_stack((data_x, data_y, data_z))

    def _plot_histogram(self, fig, draw=False, filename=None):
        """
        Patched method for plotting the histogram with a transparent background
        """
        import matplotlib.pyplot as plt

        ax = fig.axes[0]
        xlim = ax.get_xlim()
        if "quadmesh" in fig.ppsd:
            fig.ppsd.pop("quadmesh").remove()

        if fig.ppsd.cumulative:
            data = self.current_histogram_cumulative * 100.0
        else:
            # avoid divison with zero in case of empty stack
            data = self.current_histogram * 100.0 / (self.current_histogram_count or 1)

        xedges = self.period_xedges
        if fig.ppsd.xaxis_frequency:
            xedges = 1.0 / xedges

        if "meshgrid" not in fig.ppsd:
            fig.ppsd.meshgrid = np.meshgrid(xedges, self.db_bin_edges)

        # Begin patch: Replace '0' values by 'NaN' to get a transparent background
        data_t = data.T
        data_t[data_t == 0] = np.nan

        ppsd = ax.pcolormesh(
            fig.ppsd.meshgrid[0],
            fig.ppsd.meshgrid[1],
            data_t,
            cmap=fig.ppsd.cmap,
            zorder=-1,
        )
        # End patch

        fig.ppsd.quadmesh = ppsd

        if "colorbar" not in fig.ppsd:
            cb = plt.colorbar(ppsd, ax=ax)
            cb.mappable.set_clim(*fig.ppsd.color_limits)
            cb.set_label(fig.ppsd.label)
            fig.ppsd.colorbar = cb

        if fig.ppsd.max_percentage is not None:
            ppsd.set_clim(*fig.ppsd.color_limits)

        if fig.ppsd.grid:
            color = {"color": "0.7"} if fig.ppsd.cmap.name == "viridis" else {}
            ax.grid(True, which="major", **color)
            ax.grid(True, which="minor", **color)

        ax.set_xlim(*xlim)

        if filename is not None:
            plt.savefig(filename)
        elif draw:
            with np.errstate(under="ignore"):
                plt.draw()
        return fig

    def _get_plot_title(self):
        """
        Patched method for building the title with a more precise time period
        """
        start = UTCDateTime(ns=self._times_processed[0]).strftime("%Y-%m-%dT%H:%M:%S")
        end = UTCDateTime(ns=self._times_processed[-1]).strftime("%Y-%m-%dT%H:%M:%S")
        title = f"{self.id} | {self.sampling_rate}Hz | {start} - {end} | "

        total = len(self._times_processed)
        if self.current_histogram_count != total:
            title += f"{self.current_histogram_count}/{total} segments"
        else:
            title += f"{total} segments"
        return title


########################################################################################################################
# PPSD Factory
##############


class PPSDFactory:
    @staticmethod
    def factory(
        stats, inventory, use_defaults=False, check_trace=False, _class=CustomPPSD
    ):
        """
        Build an instance of PPSD with parameters defined regarding channel type

        :param stats: The Stats dictionary of an ObsPy Trace object
        :param inventory: The inventory
        :param use_defaults: (Optional) Use default values for PPSD parameters: ppsd_length, ... (default=False)
        :param check_trace: (Optional) Check the trace (default=False)
        :param _class: (Optional) The class to use in place of PPSD
        :return: An instance of the PPSD class
        """
        logger.debug(
            f"PPSDFactory.factory({stats}, {inventory}, {use_defaults}, {_class})"
        )

        # Get the PPSD parameters regarding channel type
        params = PPSDFactory.ppsd_params(stats, use_defaults)

        # Check the trace
        if check_trace:
            # Use default value if ppsd_length is not set
            ppsd_length = params.get("ppsd_length", 3600.0)

            # Detect shorter traces
            if stats.starttime + ppsd_length - stats.delta > stats.endtime:
                logger.warning(
                    f"Skipping stream ({stats.starttime} - {stats.endtime}): Shorter than the 'ppsd_length' ({ppsd_length} seconds)."
                )
                raise SkippedTraceError(stats)

            # Detect nfft computation incompatibility
            if stats.sampling_rate * ppsd_length / 4 == 1:
                logger.warning(
                    f"Skipping stream ({stats.starttime} - {stats.endtime}): Incompatible sampling rate ({stats.sampling_rate} Hz) with the 'ppsd_length' ({ppsd_length} seconds)."
                )
                raise SkippedTraceError(stats)

        # Initialize a PPSD instance
        ppsd = _class(stats, metadata=inventory, **params)
        ppsd.ppsd_params = params
        return ppsd

    @staticmethod
    def ppsd_params(stats, use_defaults=False):
        """
        Build PPSD() parameters regarding channel type

        :param stats: The Stats dictionary of an ObsPy Trace object
        :param use_defaults: (Optional) Use default values for PPSD parameters (default=False)
        :return: A dictionary with parameters to use during the PPSD instantiation
        """
        logger.debug(f"PPSDFactory.ppsd_params({stats}, {use_defaults})")

        # Initialize an empty dictionary
        params = {}

        # Using customization
        if not use_defaults:
            logger.debug(
                f"{stats.network}.{stats.station}.{stats.location or ''}.{stats.channel} : Customizing PPSD parameters..."
            )

            # D: Sample rate >= 250Hz to < 1000Hz (corner period < 10s)
            if stats.channel.startswith("D"):
                params["ppsd_length"] = 3600.0 * 0.5
                params["period_smoothing_width_octaves"] = 1.0
                params["period_step_octaves"] = 1 / 4
                params["period_limits"] = (
                    2 / stats.sampling_rate,
                    params["ppsd_length"] / 4,
                )

            # C: Sample rate >= 250Hz to < 1000Hz (corner period >= 10s)
            elif stats.channel.startswith("C"):
                params["ppsd_length"] = 3600.0 * 0.25
                params["period_smoothing_width_octaves"] = 1.0
                params["period_step_octaves"] = 1 / 4
                params["period_limits"] = (
                    2 / stats.sampling_rate,
                    params["ppsd_length"] / 4,
                )

            # E: Sample rate >= 80Hz to < 250Hz (corner period < 10s)
            # H: Sample rate >= 80Hz to < 250Hz (corner period >= 10s)
            elif stats.channel.startswith("E") or stats.channel.startswith("H"):
                params["ppsd_length"] = 3600.0 * 1
                params["period_smoothing_width_octaves"] = 1 / 2
                params["period_step_octaves"] = 1 / 8
                params["period_limits"] = (
                    2 / stats.sampling_rate,
                    params["ppsd_length"] / 12,
                )

            # B: Sample rate >= 10Hz to < 80Hz
            # M: Sample rate >= 1Hz to < 10Hz
            elif stats.channel.startswith("B") or stats.channel.startswith("M"):
                params["ppsd_length"] = 3600.0 * 2
                params["period_smoothing_width_octaves"] = 1 / 2
                params["period_step_octaves"] = 1 / 32
                params["period_limits"] = (
                    2 / stats.sampling_rate,
                    params["ppsd_length"] / 24,
                )

            # L: Sample rate ~= 1Hz
            # V: Sample rate ~= 0.1Hz
            elif stats.channel.startswith("L") or stats.channel.startswith("V"):
                params["ppsd_length"] = 3600.0 * 24 - 1000.0
                params["period_smoothing_width_octaves"] = 1 / 4
                params["period_step_octaves"] = 1 / 64
                params["period_limits"] = (
                    2 / stats.sampling_rate,
                    params["ppsd_length"] / 48,
                )
                params["overlap"] = 0

            # Other cases
            else:
                logger.debug(
                    f"{stats.network}.{stats.station}.{stats.location or ''}.{stats.channel} : Unsupported channel type ({stats.channel}), using PPSD default values."
                )

        # Using default values
        else:
            logger.debug(
                f"{stats.network}.{stats.station}.{stats.location or ''}.{stats.channel} : Forcing using PPSD default values."
            )

        # Log customized PPSD parameters
        if params:
            logger.verbose(
                f"{stats.network}.{stats.station}.{stats.location or ''}.{stats.channel} : Customizing PPSD parameters with {params}"
            )

        return params
