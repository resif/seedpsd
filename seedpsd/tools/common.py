import re
from datetime import UTC, date, datetime

import pendulum
from obspy.core import UTCDateTime

regex_temp_network = re.compile("^[XYZ0-9][A-Z0-9]?$", flags=re.IGNORECASE)


def is_allowed_channel(code, bands="", instruments="", orientations=""):
    """
    Check if a channel code defines an allowed channel or not
    :param code: The channel code
    :param bands: The allowed channel bands
    :param instruments: The allowed channel instruments
    :param orientations: The allowed channel orientations
    :return: True if the channel is allowed, else False
    :rtype: bool
    """
    if bands and instruments and orientations:
        regex_allowed_channel = re.compile(
            f"^(?P<band>[{bands}])(?P<instrument>[{instruments}])(?P<orientation>[{orientations}])$",
            flags=re.IGNORECASE,
        )
        return bool(regex_allowed_channel.match(code))
    return False


def is_temporary_network(code):
    """
    Check if a network code defines a temporary network or not
    :param code: The network code
    :return: True if the network is temporary, else False
    :rtype: bool
    """
    return bool(regex_temp_network.match(code))


def value_or_default(value, default=None):
    """
    Return the value or a default value
    :param value: The value
    :param default: The default value
    :return: Return the value if not None, else the default
    """
    if value is not None:
        return value
    return default


def str2bool(value):
    """
    Return the boolean value
    :param value: The value
    :rtype: boolean
    """
    if value is None or isinstance(value, bool):
        return value
    return value.lower() in ("yes", "true", "t", "1", "on", "show")


def to_datetime(value, tzinfo=None):
    """
    Convert the value as a datetime
    :param value: The value to convert
    :rtype: datetime
    """
    if value is None:
        return value
    if isinstance(value, pendulum.DateTime):
        return value
    if isinstance(value, UTCDateTime):
        value = value.datetime
    elif isinstance(value, date):
        value = datetime.combine(value, datetime.min.time(), tzinfo=tzinfo)
    elif isinstance(value, str):
        if value.lower() == "currentutcday":
            return datetime_today(tzinfo)
        value = pendulum.parse(value)
        if not tzinfo:
            return value.naive()
        return value.astimezone(tzinfo)
    return pendulum.instance(value, tzinfo)


def to_date(value):
    """
    Convert the value as a date
    :param value: The value to convert
    :rtype: date
    """
    if value is None:
        return value
    if isinstance(value, (pendulum.DateTime, datetime)):
        value = value.date()
    elif isinstance(value, UTCDateTime):
        value = value.date
    elif isinstance(value, pendulum.Date):
        return value
    elif isinstance(value, str):
        value = (
            date_today()
            if value.lower() == "currentutcday"
            else pendulum.parse(value).date()
        )
    return value


def date_today():
    """
    Return the date of today
    :rtype: date
    """
    return datetime.now(tz=UTC).date()


def datetime_today(tzinfo=None):
    """
    Return the datetime of today at 00:00:00
    :rtype: datetime
    """
    if tzinfo == "utc":
        tzinfo = UTC

    today = datetime.now(tz=UTC).date()
    return datetime.combine(today, datetime.min.time(), tzinfo=tzinfo)


def datetime_now(tzinfo=None):
    """
    Return the datetime of today at current time
    :rtype: datetime
    """
    if tzinfo == "utc":
        tzinfo = UTC

    return datetime.now(tzinfo)


def datetime_2100(tzinfo=None):
    """
    Return the datetime of 2100-01-01 at 00:00:00
    :rtype: datetime
    """
    if tzinfo == "utc":
        tzinfo = UTC

    return datetime(2100, 1, 1, tzinfo=tzinfo)


date_2100 = date(2100, 1, 1)
