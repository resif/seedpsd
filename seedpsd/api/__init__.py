import logging
from pathlib import Path

from pyramid.config import Configurator
from pyramid.session import SignedCookieSessionFactory

from seedpsd.models.db.session import DBSession
from seedpsd.settings.workers import Settings
from seedpsd.workers.content import SeedPsdWorkerContent
from seedpsd.workers.plot import SeedPsdWorkerPlot
from seedpsd.workers.ppsd import SeedPsdWorkerPPSD
from seedpsd.workers.value import SeedPsdWorkerValue


def init_wsgi_api(
    settings,
    config_path=None,
    reload=False,
    debug=False,
    locale=None,
    locale_domain="api_templates",
):
    """This function returns a Pyramid WSGI application."""
    # Application settings
    settings or Settings(config_path, debug=debug)

    # Pyramid settings
    pyramid_settings = {
        "pyramid.reload_all": "true" if reload else "false",
        "pyramid.debug_all": "true"
        if settings.log_level in (logging.DEBUG, "DEBUG")
        else "false",
        "pyramid.default_locale_name": locale or settings.locale_default,
        "jinja2.i18n.domain": locale_domain,
    }

    def seedpsd_routes(config):
        config.add_route("histogram", "/histogram")
        config.add_route("spectrogram", "/spectrogram")
        config.add_route("value", "/value")
        config.add_route("coverage", "/coverage")

        config.add_route("wadl", "/application.wadl")
        config.add_route("version", "/version")
        config.add_route("health", "/health")
        config.add_route("healthcheck", "/healthcheck")
        config.add_route("home", "/")
        config.add_static_view("static", "seedpsd:api/static", cache_max_age=3600)

    with Configurator(settings=pyramid_settings) as config:
        # Template engine
        config.include("pyramid_jinja2")
        config.include("pyramid_layout")

        # Routes
        config.include(seedpsd_routes, route_prefix=settings.api_path)

        # Session
        config.set_session_factory(SignedCookieSessionFactory("SeedPSD"))

        # Registry
        config.registry["settings"] = settings
        config.registry["db_engine"] = DBSession.engine_factory(
            settings, read_only=True
        )
        config.registry["worker_ppsd"] = SeedPsdWorkerPPSD(
            settings, config.registry["db_engine"]
        )
        config.registry["worker_plot"] = SeedPsdWorkerPlot(
            settings, config.registry["db_engine"]
        )
        config.registry["worker_content"] = SeedPsdWorkerContent(
            settings, config.registry["db_engine"]
        )
        config.registry["worker_value"] = SeedPsdWorkerValue(settings)
        config.registry["default_locale"] = pyramid_settings.get(
            "pyramid.default_locale_name"
        )

        # Localization
        if isinstance(settings.locale_path, Path):
            config.add_translation_dirs(settings.locale_path.as_posix())
        else:
            config.add_translation_dirs(settings.locale_path)

        # Skin assets/templates overriding
        if settings.skin_name:
            skin_path = f"seedpsd:api/skins/{settings.skin_name}/"
        elif settings.skin_path:
            skin_path = settings.skin_path
        else:
            skin_path = None

        if skin_path:
            config.override_asset(
                to_override="seedpsd:api/static/", override_with=skin_path
            )
            config.override_asset(
                to_override="seedpsd:api/templates/", override_with=skin_path
            )

        # Application
        config.scan(categories=["pyramid", "pyramid_layout"])

    return config.make_wsgi_app()
