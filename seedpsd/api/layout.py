from pyramid_layout.layout import layout_config


@layout_config(template="seedpsd:api/templates/layout.jinja2")
class MainLayout:
    def __init__(self, context, request):
        self.context = context
        self.request = request

    @property
    def settings(self):
        return self.request.registry.get("settings")

    @property
    def locales(self):
        return self.settings.locale_available
