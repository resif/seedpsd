from pyramid.httpexceptions import HTTPOk
from pyramid.view import view_config

from . import BaseView


@view_config(route_name="healthcheck")
class HealthView(BaseView):
    """
    The purpose of this view is to check simple availability of the web service
    k8s probes will fetch this endpoint to see if the web server is deployed/available
    """

    def __call__(self):
        return HTTPOk("OK")
