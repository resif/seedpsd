from seedpsd.errors import MissingParameterError
from seedpsd.models.db.session import DBSession


class BaseView:
    def __init__(self, request):
        self.request = request

    @property
    def session(self):
        return self.request.session

    @property
    def settings(self):
        return self.request.registry.get("settings")

    def db_engine(self, tenant_schema=None):
        if tenant_schema:
            return DBSession.engine_factory(
                self.settings,
                tenant_schema=tenant_schema,
                engine=self.request.registry.get("db_engine"),
            )

        return self.request.registry.get("db_engine")

    def db_session(self, engine=None, tenant_schema=None):
        engine = engine or self.db_engine(tenant_schema)
        return DBSession.factory(engine)

    @property
    def locale(self):
        locale = self._get_parameter("locale", ["lang"])
        default = self.request.registry.get("default_locale")
        if locale:
            self.session["locale"] = locale
            return locale
        if "locale" in self.session:
            return self.session.get("locale", default)
        return default

    # INTERNAL METHODS #################################################################################################

    def _get_parameter(self, name, aliases=None, mandatory=False, default=None):
        if aliases is None:
            aliases = []
        if name in self.request.params:
            value = self.request.params.get(name, default)
            if value is not None and value != "":
                return value
            return None
        if aliases:
            for alias in aliases:
                if alias in self.request.params:
                    value = self.request.params.get(alias, default)
                    if value is not None and value != "":
                        return value
                    return None

        if mandatory:
            raise MissingParameterError(name)
        return default
