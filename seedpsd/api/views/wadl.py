from pyramid.view import view_config


@view_config(route_name="wadl", renderer="seedpsd:api/templates/views/wadl.jinja2")
class WadlView:
    def __init__(self, request):
        self.request = request

    def __call__(self):
        self.request.response.content_type = "text/xml"
        return {}
