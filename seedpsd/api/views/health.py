import logging

import verboselogs
from alembic.script import ScriptDirectory
from alembicverify.util import get_current_revision, get_head_revision
from obspy import __version__ as obspy_version
from pyramid.httpexceptions import HTTPOk, HTTPServiceUnavailable
from pyramid.view import view_config
from sqlalchemy.exc import DatabaseError

from seedpsd.settings import AlembicSettings

from . import BaseView

logger = verboselogs.VerboseLogger(__name__)


@view_config(route_name="health", header="User-Agent:kube-probe/.*")
@view_config(route_name="health", renderer="seedpsd:api/templates/views/health.jinja2")
class HealthView(BaseView):
    def __call__(self):
        logger.debug(self.request)

        # Configure Alembic
        alembic_cfg = AlembicSettings(self.settings)
        script = ScriptDirectory.from_config(alembic_cfg)
        engine = self.db_engine()

        # Try to access DB
        db_available = True
        db_status = False

        # Compare DB revision with model revision
        try:
            head_revision = get_head_revision(alembic_cfg, engine, script)
            current_revision = get_current_revision(alembic_cfg, engine, script)
            db_status = head_revision == current_revision
        except DatabaseError:
            logging.exception("Database connection failed")
            db_available = False
            current_revision = None

        # Kubernetes probe:
        if "kube-probe" in self.request.user_agent:
            if not db_available:
                return HTTPServiceUnavailable("Database unavailable")
            if not db_status:
                return HTTPServiceUnavailable("Database needs upgrade")
            return HTTPOk("Ready")

        # Browser request

        # Change return code if DB is not ready
        if not db_available or not db_status:
            self.request.response.status = 503

        # Get locale
        self.request._LOCALE_ = self.locale

        # Set template variables
        return {
            "locale": self.locale,
            "obspy_version": obspy_version,
            "seedpsd_version": self.settings.version,
            "seedpsd_commit": self.settings.commit,
            "seedpsd_loglevel": logging.getLevelName(
                logging.getLogger("seedpsd").level
            ),
            "db_available": db_available,
            "db_status": db_status,
            "db_version": current_revision,
            "db_pool_status": engine.pool.status(),
            "db_loglevel": logging.getLevelName(logging.getLogger("sqlalchemy").level),
            "debug": self.settings.log_stacktrace,
            "command": "health",
        }
