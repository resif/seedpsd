import traceback
from io import BytesIO, StringIO
from tempfile import NamedTemporaryFile
from zipfile import ZIP_DEFLATED, ZipFile

import verboselogs
from pyramid.httpexceptions import (
    HTTPBadRequest,
    HTTPException,
    HTTPNoContent,
    HTTPNotFound,
    HTTPServerError,
)
from pyramid.response import Response
from pyramid.view import view_config

from seedpsd.errors import (
    InvalidParameterValueError,
    MissingParameterError,
    NoDataError,
)

from . import QueryView

logger = verboselogs.VerboseLogger(__name__)


@view_config(route_name="value", renderer="seedpsd:api/templates/views/value.jinja2")
class ValueView(QueryView):
    def __call__(self):
        logger.debug(self.request)

        # Process a query
        if self.request.params and "locale" not in self.request.params:
            try:
                # Check output options
                if self.format == "npz" and self.type != "psd":
                    msg = f"{self.type} values can not be exported using the NPZ output format."
                    raise HTTPBadRequest(msg)
                if self.type not in ("psd", "mean", "mode", "histogram", "spectrogram"):
                    msg = f"Unsupported type of value: '{self.type}'"
                    raise HTTPBadRequest(msg)
                if self.format not in ("npz", "csv", "json"):
                    msg = f"Unsupported format of value: '{self.format}'"
                    raise HTTPBadRequest(msg)

                # Get the source(s)
                sources = self.get_sources()
                if not sources:
                    msg = f"No source found for {self.network or ''}.{self.station or ''}.{self.location or ''}.{self.channel or ''}"
                    raise NoDataError(msg)

                # Get the PPSD objects
                ppsds = []
                for source in sources:
                    try:
                        ppsds.extend(self.get_ppsds(source))
                    except NoDataError as e:
                        logger.verbose(e)

                # No PPSD
                if not ppsds:
                    msg = f"No PSD values found for {self.network or ''}.{self.station or ''}.{self.location or ''}.{self.channel or ''}"
                    raise NoDataError(msg)

                # Multiple PPSD objects
                if len(ppsds) > 1:
                    # Initialize the output container
                    output = BytesIO()

                    # For each PPSD, build a file and add it to the Zip
                    with ZipFile(output, "a", ZIP_DEFLATED, False) as zip_output:
                        if self.format == "npz":
                            for ppsd in ppsds:
                                with NamedTemporaryFile() as tmp_output:
                                    psd_filename = self._render(ppsd, tmp_output)
                                    zip_output.write(tmp_output.name, psd_filename)
                        else:
                            for ppsd in ppsds:
                                with StringIO() as tmp_output:
                                    psd_filename = self._render(ppsd, tmp_output)
                                    zip_output.writestr(
                                        psd_filename, tmp_output.getvalue()
                                    )

                    # Render the output
                    output.seek(0)
                    filename = f"{self.network or ''}.{self.station or ''}.{self.location or ''}.{self.channel or ''}_{self.start_time}_{self.end_time}_{self.type}_{self.format}.zip"
                    return Response(
                        body_file=output,
                        content_type="application/zip",
                        content_disposition=f'attachement; filename="{filename}"',
                    )

                # Unique PPSD object
                if self.format == "npz":
                    output = BytesIO()
                    filename = self._render(ppsds[0], output)
                    output.seek(0)
                    return Response(
                        body_file=output,
                        content_type=self._mime_type(),
                        content_disposition=f'filename="{filename}"',
                    )
                output = StringIO()
                filename = self._render(ppsds[0], output)
                output.seek(0)
                return Response(
                    body=output.getvalue(),
                    charset="utf8",
                    content_type=self._mime_type(),
                    content_disposition=f'filename="{filename}"',
                )

            except NoDataError as e:
                nodata = self.request.params.get("nodata", None)
                if nodata is not None:
                    raise HTTPNotFound(str(e))
                raise HTTPNoContent(str(e))
            except InvalidParameterValueError as e:
                raise HTTPBadRequest(str(e))
            except MissingParameterError as e:
                raise HTTPBadRequest(str(e))
            except HTTPException:
                raise
            except Exception as e:
                logger.exception("Unmanaged exception was raised")
                raise HTTPServerError(str(e), comment=traceback.format_exc())

        # No query parameters, display the help HTML page
        else:
            self.request._LOCALE_ = self.locale
            return {
                "locale": self.locale,
                "command": "value",
            }

    # RENDERING ########################################################################################################

    def _render(self, ppsd, output):
        """
        Render the values using the requested format
        :param ppsd: The PPSD object
        :param output: The output container
        :return: The filename
        """

        # Render the values
        if self.type == "histogram":
            self._render_histogram(ppsd, output)
        elif self.type == "spectrogram":
            self._render_spectrogram(ppsd, output)
        elif self.type == "mean":
            self._render_mean(ppsd, output)
        elif self.type == "mode":
            self._render_mode(ppsd, output)
        elif self.type == "psd" and self.format == "npz":
            self._render_npz(ppsd, output)
        else:
            self._render_psd(ppsd, output)

        # Render the filename
        return self._filename(ppsd)

    def _mime_type(self):
        """
        Get the MIME type for the requested format
        :return: A MIME type
        """
        mime_type = ""
        if self.format == "npz":
            mime_type = "application/x-npz"
        elif self.format == "json":
            mime_type = "application/json"
        elif self.format == "csv":
            mime_type = "text/csv"
        elif self.format == "ascii":
            mime_type = "text/plain"

        return mime_type

    def _filename(self, ppsd):
        """
        Build a file name for a PPSD object
        :param ppsd: The PPSD object
        :return: The file name
        """
        return self.worker_value.build_filename(ppsd, self.format, self.type)

    # RENDER PSD VALUES ################################################################################################

    @staticmethod
    def _render_npz(ppsd, output):
        """
        Render a NPZ
        :param ppsd: The PPSD object
        :param output: The output container
        """

        # Export the PPSD into a NPZ
        ppsd.save_npz(output)

    def _render_psd(self, ppsd, output):
        """
        Render PSD values using a CSV/JSON format
        :param ppsd: The PPSD object
        :param output: The output container
        """

        # Add headers
        output.write(self.worker_value.build_header(ppsd, self.format))

        # Render the dataset
        output.write(self.worker_value.export_psd_values(ppsd, self.format))

    def _render_mean(self, ppsd, output):
        """
        Render PSD mean values using a CSV/JSON format
        :param ppsd: The PPSD object
        :param output: The output container
        """

        # Add headers
        output.write(self.worker_value.build_header(ppsd, self.format))

        # Render the dataset
        output.write(self.worker_value.export_mean_values(ppsd, self.format))

    def _render_mode(self, ppsd, output):
        """
        Render PSD mode values using a CSV/JSON format
        :param ppsd: The PPSD object
        :param output: The output container
        """

        # Add headers
        output.write(self.worker_value.build_header(ppsd, self.format))

        # Render the dataset
        output.write(self.worker_value.export_mode_values(ppsd, self.format))

    # RENDER HISTOGRAM VALUES ##########################################################################################

    def _render_histogram(self, ppsd, output):
        """
        Render histogram values using a CSV/JSON format
        :param ppsd: The PPSD object
        :param output: The output container
        """

        # Add headers
        output.write(self.worker_value.build_header(ppsd, self.format))

        # Render the dataset
        output.write(
            self.worker_value.export_histogram_values(
                ppsd,
                self.format,
                self.x_min,
                self.x_max,
                self.y_min,
                self.y_max,
                self.z_min,
                self.z_max,
            )
        )

    # RENDER SPECTROGRAM VALUES ########################################################################################

    def _render_spectrogram(self, ppsd, output):
        """
        Render spectrogram values using a CSV/JSON format
        :param ppsd: The PPSD object
        :param output: The output container
        :return: The filename
        """

    # OPTIONAL PARAMETERS ##############################################################################################

    @property
    def type(self):
        return self._get_parameter("type", default="psd")

    @property
    def format(self):
        return self._get_parameter("format", default="csv")

    @property
    def x_min(self):
        value = self._get_parameter("x_min", ["xmin"])
        if value is not None and value != "":
            return float(value)
        return value

    @property
    def x_max(self):
        value = self._get_parameter("x_max", ["xmax"])
        if value is not None and value != "":
            return float(value)
        return value

    @property
    def y_min(self):
        value = self._get_parameter("y_min", ["ymin"])
        if value is not None and value != "":
            return float(value)
        return value

    @property
    def y_max(self):
        value = self._get_parameter("y_max", ["ymax"])
        if value is not None and value != "":
            return float(value)
        return value

    @property
    def z_min(self):
        value = self._get_parameter("z_min", ["zmin"])
        if value is not None and value != "":
            return float(value)
        return value

    @property
    def z_max(self):
        value = self._get_parameter("z_max", ["zmax"])
        if value is not None and value != "":
            return float(value)
        return value
