import traceback

import verboselogs
from pyramid.httpexceptions import (
    HTTPBadRequest,
    HTTPException,
    HTTPNoContent,
    HTTPNotFound,
    HTTPServerError,
)
from pyramid.view import view_config

from seedpsd.errors import (
    InvalidParameterValueError,
    MissingParameterError,
    NoDataError,
)
from seedpsd.settings import HistogramPlotSettings
from seedpsd.tools.common import str2bool

from . import PlotView

logger = verboselogs.VerboseLogger(__name__)


@view_config(
    route_name="histogram", renderer="seedpsd:api/templates/views/histogram.jinja2"
)
class HistogramView(PlotView):
    def __call__(self):
        logger.debug(self.request)

        # Process a query
        if (
            self.request.params
            and "locale" not in self.request.params
            and "customize" not in self.request.params
        ):
            try:
                # Get the plot settings
                plot_settings = HistogramPlotSettings(
                    self.settings,
                    start_time=self.start_time,
                    end_time=self.end_time,
                    image_format=self.image_format,
                    image_transparent=self.image_transparent,
                    image_dpi=self.image_dpi,
                    image_width=self.image_width,
                    image_height=self.image_height,
                    plot_colormap=self.plot_colormap,
                    plot_colormap_min=self.plot_colormap_min,
                    plot_colormap_max=self.plot_colormap_max,
                    plot_font_size=self.plot_font_size,
                    plot_grid=self.plot_grid,
                    plot_coverage=self.plot_coverage,
                    plot_cumulative=self.plot_cumulative,
                    plot_mean=self.plot_mean,
                    plot_mode=self.plot_mode,
                    plot_noise_models=self.plot_noise_models,
                    plot_percentiles=self.plot_percentiles,
                    plot_x_frequency=self.plot_x_frequency,
                    plot_x_min=self.plot_x_min,
                    plot_x_max=self.plot_x_max,
                    plot_y_min=self.plot_y_min,
                    plot_y_max=self.plot_y_max,
                )
                return self._render(plot_settings, "histogram")

            except NoDataError as e:
                nodata = self.request.params.get("nodata", None)
                if nodata is not None:
                    raise HTTPNotFound(str(e))
                raise HTTPNoContent(str(e))
            except InvalidParameterValueError as e:
                raise HTTPBadRequest(str(e))
            except MissingParameterError as e:
                raise HTTPBadRequest(str(e))
            except HTTPException:
                raise
            except Exception as e:
                logger.exception("Histogram plot failed")
                raise HTTPServerError(str(e), comment=traceback.format_exc())

        # No query parameters, display the help HTML page
        else:
            self.request._LOCALE_ = self.locale
            return {
                "locale": self.locale,
                "colormaps": self.worker_plot.colormaps(),
                "default_colormap": "viridis_r",
                "network": self._get_parameter("network", ["net"], mandatory=False),
                "station": self._get_parameter("station", ["sta"], mandatory=False),
                "location": self._get_parameter("location", ["loc"], mandatory=False),
                "channel": self._get_parameter("channel", ["cha"], mandatory=False),
                "command": "histogram",
                "settings": self.settings,
            }

    # HISTOGRAM PLOT PARAMETERS ########################################################################################

    @property
    def plot_coverage(self):
        return str2bool(self._get_parameter("coverage", ["show_coverage"]))

    @property
    def plot_cumulative(self):
        return str2bool(self._get_parameter("cumulative"))

    # @property
    # def plot_histogram(self):
    #     return str2bool(self._get_parameter("histogram", ["show_histogram", "plot.histogram"]))

    @property
    def plot_mean(self):
        return str2bool(self._get_parameter("mean", ["show_mean"]))

    @property
    def plot_mode(self):
        return str2bool(self._get_parameter("mode", ["show_mode", "plot.minmodemax"]))

    @property
    def plot_noise_models(self):
        return str2bool(
            self._get_parameter(
                "noise_models",
                ["noise", "show_noise_models", "noisemodels", "plot.model"],
            )
        )

    @property
    def plot_percentiles(self):
        return str2bool(self._get_parameter("percentiles", ["show_percentiles"]))

    @property
    def plot_x_frequency(self):
        return str2bool(
            self._get_parameter(
                "x_frequency", ["xaxis_frequency", "frequency", "plot.horzaxis"]
            )
        )

    @property
    def plot_x_min(self):
        value = self._get_parameter("period_min", ["xmin", "x_min", "plot.period.min"])
        try:
            if value is not None and value != "":
                return float(value)
            return value
        except Exception:
            msg = "period_min"
            raise InvalidParameterValueError(msg, value)

    @property
    def plot_x_max(self):
        value = self._get_parameter("period_max", ["xmax", "x_max", "plot.period.max"])
        try:
            if value is not None and value != "":
                return float(value)
            return value
        except Exception:
            msg = "period_max"
            raise InvalidParameterValueError(msg, value)

    @property
    def plot_y_min(self):
        value = self._get_parameter("power_min", ["ymin", "y_min", "plot.power.min"])
        try:
            if value is not None and value != "":
                return float(value)
            return value
        except Exception:
            msg = "power_min"
            raise InvalidParameterValueError(msg, value)

    @property
    def plot_y_max(self):
        value = self._get_parameter("power_max", ["ymax", "y_max", "plot.power.max"])
        try:
            if value is not None and value != "":
                return float(value)
            return value
        except Exception:
            msg = "power_max"
            raise InvalidParameterValueError(msg, value)
