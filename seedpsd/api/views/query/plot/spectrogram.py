import traceback

import verboselogs
from pyramid.httpexceptions import (
    HTTPBadRequest,
    HTTPException,
    HTTPNoContent,
    HTTPNotFound,
    HTTPServerError,
)
from pyramid.view import view_config

from seedpsd.errors import (
    InvalidParameterValueError,
    MissingParameterError,
    NoDataError,
)
from seedpsd.settings import SpectrogramPlotSettings
from seedpsd.tools.common import str2bool

from . import PlotView

logger = verboselogs.VerboseLogger(__name__)


@view_config(
    route_name="spectrogram", renderer="seedpsd:api/templates/views/spectrogram.jinja2"
)
class SpectrogramView(PlotView):
    def __call__(self):
        logger.debug(self.request)

        # Process a query
        if (
            self.request.params
            and "locale" not in self.request.params
            and "customize" not in self.request.params
        ):
            # Get the plot settings
            try:
                plot_settings = SpectrogramPlotSettings(
                    self.settings,
                    start_time=self.start_time,
                    end_time=self.end_time,
                    image_format=self.image_format,
                    image_transparent=self.image_transparent,
                    image_dpi=self.image_dpi,
                    image_width=self.image_width,
                    image_height=self.image_height,
                    plot_colormap=self.plot_colormap,
                    plot_colormap_min=self.plot_colormap_min,
                    plot_colormap_max=self.plot_colormap_max,
                    plot_font_size=self.plot_font_size,
                    plot_grid=self.plot_grid,
                    plot_y_invert=self.plot_y_invert,
                )
                return self._render(plot_settings, "spectrogram")

            except NoDataError as e:
                nodata = self.request.params.get("nodata", None)
                if nodata is not None:
                    raise HTTPNotFound(str(e))
                raise HTTPNoContent(str(e))
            except InvalidParameterValueError as e:
                raise HTTPBadRequest(str(e))
            except MissingParameterError as e:
                raise HTTPBadRequest(str(e))
            except HTTPException:
                raise
            except Exception as e:
                logger.exception("Spectrogram Plot Failed")
                raise HTTPServerError(str(e), comment=traceback.format_exc())

        # No query parameters, display the help HTML page
        else:
            self.request._LOCALE_ = self.locale
            return {
                "locale": self.locale,
                "colormaps": self.worker_plot.colormaps(),
                "default_colormap": "viridis_r",
                "network": self._get_parameter("network", ["net"], mandatory=False),
                "station": self._get_parameter("station", ["sta"], mandatory=False),
                "location": self._get_parameter("location", ["loc"], mandatory=False),
                "channel": self._get_parameter("channel", ["cha"], mandatory=False),
                "command": "spectrogram",
                "settings": self.settings,
            }

    # SPECTROGRAM PLOT PARAMETERS ######################################################################################

    @property
    def plot_y_invert(self):
        return str2bool(self._get_parameter("y_invert", ["yinvert"]))
