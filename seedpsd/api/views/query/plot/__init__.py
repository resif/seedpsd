from io import BytesIO
from tempfile import NamedTemporaryFile
from zipfile import ZIP_DEFLATED, ZipFile

import verboselogs
from matplotlib.pyplot import colormaps as matplotlib_colormaps
from pyramid.response import Response

from seedpsd.api.views.query import QueryView
from seedpsd.errors import InvalidParameterValueError, NoDataError
from seedpsd.tools.common import str2bool

logger = verboselogs.VerboseLogger(__name__)


class PlotView(QueryView):
    # RENDERING ########################################################################################################
    def _render(self, plot_settings, plot_type):
        """
        Render a plot (or a list of) using the request parameters
        :param plot_settings: The plot settings
        :param plot_type: The plot type
        :return: A Response object
        """

        # Get the source(s)
        sources = self.get_sources()

        # No source
        if not sources:
            msg = f"No source found for {self.network or ''}.{self.station or ''}.{self.location or ''}.{self.channel or ''}"
            raise NoDataError(msg)

        # Multiple sources
        if len(sources) > 1:
            # Initialize the output container
            output = BytesIO()

            # For each source, build an image and add it to the Zip
            with ZipFile(output, "a", ZIP_DEFLATED, False) as zip_output:
                for source in sources:
                    with NamedTemporaryFile() as tmp_output:
                        try:
                            img_filename = self._render_source(
                                tmp_output, source, plot_settings, plot_type
                            )
                            zip_output.write(tmp_output.name, img_filename)
                        except NoDataError as e:
                            logger.verbose(e)

            # Render the output
            output.seek(0)
            filename = f"{self.network or ''}.{self.station or ''}.{self.location or ''}.{self.channel or ''}_{self.start_time}_{self.end_time}_{plot_type}.zip"
            return Response(
                body_file=output,
                content_type="application/zip",
                content_disposition=f'attachement; filename="{filename}"',
            )

        # Unique source
        output = BytesIO()
        filename = self._render_source(output, sources[0], plot_settings, plot_type)
        output.seek(0)
        mime_type = self.worker_plot.mime_type(plot_settings)
        return Response(
            body_file=output,
            content_type=mime_type,
            content_disposition=f'filename="{filename}"',
        )

    def _render_source(self, output, source, plot_settings, plot_type):
        """
        Render a plot for a source
        :param output: The output container
        :param source: The Source object
        :param plot_settings: The plot settings
        :param plot_type: The plot type
        :return: A filename
        """

        # Load PPSD from database
        ppsds = self.get_ppsds(source)

        # Merge plots of multiple sampling rates
        if len(ppsds) > 1:
            figures = []
            for ppsd in ppsds:
                figure = self.worker_plot.build_plot(ppsd, plot_settings, plot_type)
                figures.append(figure)

            self.worker_plot.save(figures, plot_settings, output)
            return self.worker_plot.filename(ppsds, plot_settings, plot_type)

        # Plot an unique sampling rate
        figure = self.worker_plot.build_plot(ppsds[0], plot_settings, plot_type)
        self.worker_plot.save(figure, plot_settings, output)
        return self.worker_plot.filename(ppsds[0], plot_settings, plot_type)

    # PROPERTIES #######################################################################################################

    @property
    def worker_plot(self):
        return self.request.registry.get("worker_plot")

    # IMAGE PARAMETERS #################################################################################################

    @property
    def image_format(self):
        return self._get_parameter("format")

    @property
    def image_transparent(self):
        return str2bool(self._get_parameter("transparent"))

    @property
    def image_dpi(self):
        value = self._get_parameter("dpi")
        try:
            if value is not None and value != "":
                return float(value)
            return value
        except Exception:
            msg = "dpi"
            raise InvalidParameterValueError(msg, value)

    @property
    def image_width(self):
        value = self._get_parameter("width", ["plot.width"])
        try:
            if value is not None and value != "":
                return float(value)
            return value
        except Exception:
            msg = "width"
            raise InvalidParameterValueError(msg, value)

    @property
    def image_height(self):
        value = self._get_parameter("height", ["plot.height"])
        try:
            if value is not None and value != "":
                return float(value)
            return value
        except Exception:
            msg = "height"
            raise InvalidParameterValueError(msg, value)

    # PLOT PARAMETERS ##################################################################################################

    @property
    def plot_colormap(self):
        value = self._get_parameter("colormap", ["cmap", "plot.color.palette"])
        if (
            value is not None
            and value not in ("", "pqlx")
            and value not in matplotlib_colormaps()
        ):
            msg = "colormap"
            raise InvalidParameterValueError(msg, value)
        return value

    @property
    def plot_colormap_min(self):
        value = self._get_parameter("colormap_min", ["cmin", "colormapmin", "cmapmin"])
        try:
            if value is not None and value != "":
                return float(value)
            return value
        except Exception:
            msg = "colormap_min"
            raise InvalidParameterValueError(msg, value)

    @property
    def plot_colormap_max(self):
        value = self._get_parameter("colormap_max", ["cmax", "colormapmax", "cmapmax"])
        try:
            if value is not None and value != "":
                return float(value)
            return value
        except Exception:
            msg = "colormap_max"
            raise InvalidParameterValueError(msg, value)

    @property
    def plot_font_size(self):
        value = self._get_parameter(
            "font_size",
            ["fontsize", "font", "plot.labelfont.size", "plot.titlefont.size"],
        )
        try:
            if value is not None and value != "":
                return float(value)
            return value
        except Exception:
            msg = "font_size"
            raise InvalidParameterValueError(msg, value)

    @property
    def plot_grid(self):
        return str2bool(self._get_parameter("grid"))
