import verboselogs
from pyramid.httpexceptions import HTTPNoContent, HTTPNotFound
from pyramid.response import Response
from pyramid.view import view_config
from tablib import Dataset

from seedpsd.errors import InvalidParameterValueError
from seedpsd.models.db.shared.source import Source
from seedpsd.models.db.tenant.stream import Stream
from seedpsd.tools.common import str2bool, to_datetime

from . import QueryView

logger = verboselogs.VerboseLogger(__name__)


@view_config(
    route_name="coverage", renderer="seedpsd:api/templates/views/coverage.jinja2"
)
class CoverageView(QueryView):
    def __call__(self):
        logger.debug(self.request)

        # Process a query
        if self.request.params and "locale" not in self.request.params:
            # Initialize the output
            output = ""

            # Initialize the dataset
            data = Dataset()
            data.headers = [
                "Network",
                "Station",
                "Location",
                "Channel",
                "Sampling rate",
                "Start time",
                "End time",
                "Is valid",
                "Last update",
            ]

            # Find sources related to requested parameters
            try:
                with self.db_session(self.db_engine()) as db_session:
                    sources = db_session.find_all(
                        Source.find(
                            network=self.network,
                            station=self.station,
                            location=self.location,
                            channel=self.channel,
                            start=self.start_time,
                            end=self.end_time,
                        )
                    )
            except Exception as e:
                sources = []
                logger.debug(e)

            if not sources:
                nodata = self.request.params.get("nodata", None)
                if nodata is not None:
                    msg = "No source found with these parameters"
                    raise HTTPNotFound(msg)
                msg = "No source found with these parameters"
                raise HTTPNoContent(msg)

            # For each source
            for source in sources:
                # Reset the rows container
                rows = []

                # Find streams related to the source and requested parameters
                with self.db_session(
                    self.db_engine(source.network.schema)
                ) as db_session:
                    streams = db_session.find_all(
                        Stream.find(
                            source=source,
                            start_after=self.start_time,
                            end_before=self.end_time,
                        )
                    )

                    # For each stream
                    for stream in streams:
                        source = stream.source

                        # Build a row
                        row = [
                            source.network.code,  # 0
                            source.station,  # 1
                            source.location,  # 2
                            source.channel,  # 3
                            stream.sampling_rate,  # 4
                            stream.start_utc,  # 5
                            stream.end_utc,  # 6
                            stream.valid,  # 7
                            stream.file.date_processed_utc,  # 8
                        ]

                        # Try to merge the new row with the previous one
                        # FDSN: "When determining continuous time spans from otherwise fragmented data, such as data records,
                        # it is recommended to use a time tear tolerance equal to or less than 1⁄2 the sampling period."
                        if self.extent and rows:
                            previous_row = rows[-1]
                            if (
                                abs(
                                    max(row[5], previous_row[6])
                                    - min(row[5], previous_row[6])
                                )
                                <= 0.5 / stream.sampling_rate
                                and row[0] == previous_row[0]
                                and row[1] == previous_row[1]
                                and row[2] == previous_row[2]
                                and row[3] == previous_row[3]
                                and row[4] == previous_row[4]
                            ):
                                rows[-1][6] = row[6]
                            else:
                                rows.append(row)
                        else:
                            rows.append(row)

                    # Append the rows to the dataset
                    data.extend(rows)

            # Check dataset length
            if data.height > 0:
                # Render the dataset
                output += data.export(self.format)

                # Render the output
                return Response(body=output, content_type="text/plain", charset="utf8")

            # No data
            self.request._LOCALE_ = self.locale
            nodata = self.request.params.get("nodata", None)
            if nodata is not None:
                msg = "No PSD found with these parameters"
                raise HTTPNotFound(msg)
            msg = "No PSD found with these parameters"
            raise HTTPNoContent(msg)

        # No query parameters, display the help HTML page
        self.request._LOCALE_ = self.locale
        return {
            "locale": self.locale,
            "command": "coverage",
        }

    # OPTIONAL PARAMETERS ##############################################################################################

    @property
    def worker_content(self):
        return self.request.registry.get("worker_content")

    @property
    def format(self):
        return self._get_parameter("format", default="csv")

    @property
    def extent(self):
        return str2bool(self._get_parameter("extent", default=False))

    @property
    def start_time(self):
        start = self._get_parameter(
            "start", ["starttime", "start_time"], mandatory=False
        )
        try:
            return to_datetime(start)
        except Exception:
            msg = "start"
            raise InvalidParameterValueError(msg, start)

    @property
    def end_time(self):
        end = self._get_parameter("end", ["endtime", "end_time"], mandatory=False)
        try:
            return to_datetime(end)
        except Exception:
            msg = "end"
            raise InvalidParameterValueError(msg, end)
