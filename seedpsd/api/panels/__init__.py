class PanelAbstract:
    def __init__(self, context, request):
        self.context = context
        self.request = request  # type: Request
        self.layout = request.layout_manager.layout

    @property
    def settings(self):
        return self.request.registry.get("settings")
