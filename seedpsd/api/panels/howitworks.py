from pyramid_layout.panel import panel_config

from . import PanelAbstract


@panel_config(
    name="howitworks", renderer="seedpsd:api/templates/panels/howitworks.jinja2"
)
class PanelHowItWorks(PanelAbstract):
    def __call__(self):
        return {
            "use_defaults": self.settings.ppsd_use_defaults,
        }
