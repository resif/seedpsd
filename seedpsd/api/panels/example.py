from pyramid_layout.panel import panel_config

from . import PanelAbstract


@panel_config(name="example", renderer="seedpsd:api/templates/panels/example.jinja2")
class PanelExample(PanelAbstract):
    def __call__(self, command=None):
        return {
            "command": command,
            "network": self.settings.example_network,
            "station": self.settings.example_station,
            "location": self.settings.example_location,
            "channel": self.settings.example_channel,
            "start": self.settings.example_start,
            "end": self.settings.example_end,
        }
