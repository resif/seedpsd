class InvalidParameterValueError(Exception):
    def __init__(self, parameter, value=None):
        if value is not None:
            message = f"Invalid value '{value}' for parameter '{parameter}'"
        else:
            message = f"Invalid value for parameter '{parameter}'"

        super().__init__(message)
        self.code = 400


class MissingParameterError(Exception):
    def __init__(self, parameter):
        message = f"Missing parameter '{parameter}'"
        super().__init__(message)
        self.code = 400


class NoDataError(Exception):
    def __init__(self, message, code=204):
        super().__init__(message)
        self.code = code


class TooManyResultsError(Exception):
    def __init__(self, parameter):
        message = f"Too many '{parameter}'"
        super().__init__(message)
        self.code = 413


class SkippedTraceError(Exception):
    def __init__(self, stats):
        message = f"Skipped trace: {stats.network}.{stats.station}.{stats.location or ''}.{stats.channel} | {stats.starttime} - {stats.endtime} | {stats.sampling_rate}Hz, {stats.npts} samples"
        super().__init__(message)


class EmptySettingValueError(Exception):
    def __init__(self, key, message=None):
        message = message or f"No value for mandatory setting '{key}'"
        super().__init__(message)


class UndefinedSettingValueError(Exception):
    def __init__(self, key, message=None):
        message = message or f"Missing mandatory setting '{key}'."
        super().__init__(message)
