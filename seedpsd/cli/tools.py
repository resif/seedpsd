import logging
import os.path
import sys
from logging.handlers import WatchedFileHandler

import coloredlogs
import verboselogs
from alembic.script import ScriptDirectory
from alembicverify.util import get_current_revision, get_head_revision
from click_loglevel import LogLevel
from sqlalchemy import create_engine

from seedpsd.settings import AlembicSettings
from seedpsd.tools.common import datetime_now

log_levels = LogLevel(extra=["VERBOSE"])


def check_db(settings):
    # Configure Alembic
    alembic_cfg = AlembicSettings(settings)
    engine = create_engine(settings.database_uri)
    script = ScriptDirectory.from_config(alembic_cfg)
    logging.getLogger("alembic.runtime.migration").setLevel(logging.ERROR)

    # Get head revision
    head = get_head_revision(alembic_cfg, engine, script)

    # Get current revision
    current = get_current_revision(alembic_cfg, engine, script)

    # Compare the revisions
    logging.debug(f"Database schema versions: current={current} | head={head}")
    if current != head:
        logging.error(
            "The database schema needs update. Please run 'seedpsd-cli admin migrate --upgrade'"
        )
        msg = "The database schema needs update. Please run 'seedpsd-cli admin migrate --upgrade'"
        raise Exception(msg)

    # Return the settings
    return settings


def configure_logger(settings, log_level=None, sql_log_level=None, amqp_log_level=None):
    # Define log format
    log_format = "%(asctime)s %(levelname)s %(message)s"

    # Get log levels
    log_level = log_level or settings.log_level
    sql_log_level = sql_log_level or settings.sql_log_level
    amqp_log_level = amqp_log_level or settings.amqp_log_level

    # Configure the loggers
    logging.basicConfig()
    verboselogs.install()
    coloredlogs.install(level=log_level, fmt=log_format, stream=sys.stdout)
    # logging.captureWarnings(True)

    # Set application logging level
    if log_level is not None:
        logging.getLogger("seedpsd").setLevel(log_level)

    # Set ORM logging level
    if sql_log_level is not None:
        logging.getLogger("sqlalchemy").setLevel(sql_log_level)

    # Set AMQP logging level
    if amqp_log_level is not None:
        logging.getLogger("pika").setLevel(amqp_log_level)
        logging.getLogger("proton").setLevel(amqp_log_level)

    # Configure file handler
    if settings.log_path:
        if settings.log_path.endswith(".log"):
            file_path = settings.log_path
        else:
            file_path = os.path.join(
                settings.log_path, f"seedpsd_{datetime_now().isoformat()}.log"
            )
        logging.info(f"Logging to: {file_path}")
        file_handler = WatchedFileHandler(file_path)
        file_handler.setFormatter(logging.Formatter(log_format))
        logging.getLogger().addHandler(file_handler)

    return logging.getLogger("seedpsd")


def configure_sentry(settings):
    if settings.sentry_dsn is not None:
        try:
            import sentry_sdk
            from sentry_sdk.integrations.logging import ignore_logger
            from sentry_sdk.integrations.pyramid import PyramidIntegration

            ignore_logger("root")

            sentry_sdk.init(
                dsn=settings.sentry_dsn,
                enable_tracing=settings.sentry_tracing,
                environment=settings.sentry_environment,
                sample_rate=float(settings.sentry_sample_rate),
                traces_sample_rate=float(settings.sentry_traces_sample_rate),
                profiles_sample_rate=float(settings.sentry_profiles_sample_rate),
                integrations=[
                    PyramidIntegration(transaction_style="route_name"),
                ],
            )
            logging.info("Sentry is configured to collect errors.")
        except ModuleNotFoundError:
            logging.warning(
                "Sentry is unavailable. Please install the package 'sentry-sdk'"
            )
