import os
import sys

import click

from seedpsd.cli.tools import check_db, configure_logger, configure_sentry, log_levels
from seedpsd.settings.workers import Settings

# CONFIGURATION
########################################################################################################################


# Group all sub-commands declared below
@click.group(help="Server management")
def server():
    pass


# LISTEN AMQP QUEUE FOR DATA
########################################################################################################################


@server.command(help="Handle data updates by listening an AMQP queue")
@click.option("--simulate", is_flag=True, help="Enable the simulation mode")
@click.option("--level", "log_level", type=log_levels, help="Log verbosity level")
@click.option(
    "--sql-level",
    "sql_log_level",
    type=log_levels,
    help="SQLAlchemy log verbosity level",
)
@click.option(
    "--amqp-level", "amqp_log_level", type=log_levels, help="AMQP log verbosity level"
)
@click.option("--debug", is_flag=True, help="Show stack trace")
@click.option(
    "--config-path",
    "config_path",
    type=click.Path(exists=True, readable=True),
    help="Custom config path",
)
def amqp_data(simulate, log_level, sql_log_level, amqp_log_level, debug, config_path):
    # Load settings
    settings = Settings(config_path, debug=debug)

    # Configure loggers
    logger = configure_logger(settings, log_level, sql_log_level, amqp_log_level)

    try:
        # Configure Sentry
        configure_sentry(settings)

        # Check database
        check_db(settings)

        # Starting a worker and listen the AMQP queue
        from seedpsd.workers.amqp.factory import SeedPsdWorkerAmqpFactory

        worker = SeedPsdWorkerAmqpFactory.factory_data(settings)
        worker.start(simulate)

        logger.info("Done.")
        sys.exit(os.EX_OK)
    except Exception:
        logger.exception("Worker AMQP Data crashed")
        sys.exit(os.EX_SOFTWARE)


# LISTEN AMQP QUEUE FOR METADATA
########################################################################################################################


@server.command(help="Handle metadata updates by listening an AMQP queue")
@click.option("--simulate", is_flag=True, help="Enable the simulation mode")
@click.option("--level", "log_level", type=log_levels, help="Log verbosity level")
@click.option(
    "--sql-level",
    "sql_log_level",
    type=log_levels,
    help="SQLAlchemy log verbosity level",
)
@click.option(
    "--amqp-level", "amqp_log_level", type=log_levels, help="AMQP log verbosity level"
)
@click.option("--debug", is_flag=True, help="Show stack trace")
@click.option(
    "--config-path",
    "config_path",
    type=click.Path(exists=True, readable=True),
    help="Custom config path",
)
def amqp_metadata(
    simulate, log_level, sql_log_level, amqp_log_level, debug, config_path
):
    # Load settings
    settings = Settings(config_path, debug=debug)

    # Configure loggers
    logger = configure_logger(settings, log_level, sql_log_level, amqp_log_level)

    try:
        # Configure Sentry
        configure_sentry(settings)

        # Check database
        check_db(settings)

        # Starting a worker and listen the AMQP queue
        from seedpsd.workers.amqp.factory import SeedPsdWorkerAmqpFactory

        worker = SeedPsdWorkerAmqpFactory.factory_metadata(settings)
        worker.start(simulate)

        logger.info("Done.")
        sys.exit(os.EX_OK)
    except Exception:
        logger.exception("Worker AMQP Metadata crashed")
        sys.exit(os.EX_SOFTWARE)


# START WEB SERVER
########################################################################################################################


@server.command(help="Launch the Web server")
@click.option("--host", help="The listen host", default="0.0.0.0")
@click.option("--port", help="The listen port", default=8000)
@click.option("--reload", is_flag=True, help="Reload mode")
@click.option("--level", "log_level", type=log_levels, help="Log verbosity level")
@click.option(
    "--sql-level",
    "sql_log_level",
    type=log_levels,
    help="SQLAlchemy log verbosity level",
)
@click.option("--debug", is_flag=True, help="Show stack trace")
@click.option(
    "--config-path",
    "config_path",
    type=click.Path(exists=True, readable=True),
    help="Custom config path",
)
@click.option("--locale", help="The default language")
def web(host, port, reload, log_level, sql_log_level, debug, config_path, locale):
    # Load settings
    settings = Settings(config_path, debug=debug)

    # Configure loggers
    logger = configure_logger(settings, log_level, sql_log_level)

    try:
        # Configure Sentry
        configure_sentry(settings)

        # Check database
        check_db(settings)

        # Load WSGI app
        from seedpsd.api import init_wsgi_api

        wsgi_api = init_wsgi_api(
            settings, config_path=config_path, reload=reload, debug=debug, locale=locale
        )

        # Serve the api
        from waitress import serve

        serve(wsgi_api, host=host, port=port)

        sys.exit(os.EX_OK)
    except Exception:
        logger.exception("Web failed")
        sys.exit(os.EX_SOFTWARE)
