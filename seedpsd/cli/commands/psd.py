import os
import sys

import click
from click_aliases import ClickAliasedGroup

from seedpsd.cli.tools import check_db, configure_logger, configure_sentry, log_levels
from seedpsd.settings.plots.histogram import HistogramPlotSettings
from seedpsd.settings.plots.spectrogram import SpectrogramPlotSettings
from seedpsd.settings.workers import Settings

# CONFIGURATION
########################################################################################################################


# Group all sub-commands declared below
@click.group(help="Computed PSD exploitation", cls=ClickAliasedGroup)
def psd():
    pass


# PLOT HISTOGRAM
########################################################################################################################


@psd.command(help="Plot histogram (PDF)", aliases=["histogram", "pdf"])
@click.argument("network")
@click.argument("station")
@click.argument("location")
@click.argument("channel")
@click.argument("start_time")
@click.argument("end_time")
@click.option(
    "--merge/--no-merge",
    "image_merge",
    default=True,
    help="Merge images resulting of multiple sampling rates",
)
@click.option("--format", "image_format", help="Image format (png, jpg, pdf)")
@click.option(
    "--transparent/--no-transparent",
    "image_transparent",
    default=None,
    help="Enable image transparency",
)
@click.option("--dpi", "image_dpi", help="Image DPI")
@click.option("--width", "image_width", help="Image width")
@click.option("--height", "image_height", help="Image height")
@click.option("--grid/--no-grid", "plot_grid", default=None, help="Show the grid")
@click.option(
    "--coverage/--no-coverage", "plot_coverage", default=None, help="Show coverage"
)
@click.option(
    "--percentiles/--no-percentiles",
    "plot_percentiles",
    default=None,
    help="Show percentiles",
)
@click.option(
    "--noise-models/--no-noise-models",
    "plot_noise_models",
    default=None,
    help="Show noise models",
)
@click.option("--mode/--no-mode", "plot_mode", default=None, help="Show mode")
@click.option("--mean/--no-mean", "plot_mean", default=None, help="Show mean")
@click.option(
    "--cumulative/--no-cumulative",
    "plot_cumulative",
    default=None,
    help="Show cumulative",
)
@click.option(
    "--x-frequency/--x-period",
    "plot_x_frequency",
    default=None,
    help="Set the X axis in Frequency (Hz) instead of Period (s)",
)
@click.option("--x-min", "plot_x_min", help="Set X min")
@click.option("--x-max", "plot_x_max", help="Set X max")
@click.option("--y-min", "plot_y_min", help="Set Y min")
@click.option("--y-max", "plot_y_max", help="Set Y max")
@click.option("--colormap", "plot_colormap", help="Set colourmap")
@click.option("--colormap-min", "plot_colormap_min", help="Set colourmap min")
@click.option("--colormap-max", "plot_colormap_max", help="Set colourmap max")
@click.option("--font-size", "plot_font_size", help="Set font size")
@click.option("--outdated", is_flag=True, help="Include dirty/outdated calculations")
@click.option(
    "--output",
    "output_path",
    default=os.getcwd(),
    help="Set output directory (default=current dir)",
)
@click.option("--level", "log_level", type=log_levels, help="Log verbosity level")
@click.option(
    "--sql-level",
    "sql_log_level",
    type=log_levels,
    help="SQLAlchemy log verbosity level",
)
@click.option("--debug", is_flag=True, help="Show stack trace")
@click.option(
    "--config-path",
    "config_path",
    type=click.Path(exists=True, readable=True),
    help="Custom config path",
)
def plot_histogram(
    network,
    station,
    location,
    channel,
    start_time,
    end_time,
    image_merge,
    image_format,
    image_transparent,
    image_dpi,
    image_width,
    image_height,
    plot_grid,
    plot_coverage,
    plot_percentiles,
    plot_noise_models,
    plot_mode,
    plot_mean,
    plot_cumulative,
    plot_x_frequency,
    plot_x_min,
    plot_x_max,
    plot_y_min,
    plot_y_max,
    plot_colormap,
    plot_colormap_min,
    plot_colormap_max,
    plot_font_size,
    outdated,
    output_path,
    log_level,
    sql_log_level,
    debug,
    config_path,
):
    # Load settings
    settings = Settings(config_path, debug=debug)

    # Configure loggers
    logger = configure_logger(settings, log_level, sql_log_level)

    try:
        # Configure Sentry
        configure_sentry(settings)

        # Check database
        check_db(settings)

        # Get plot settings
        plot_settings = HistogramPlotSettings(
            settings,
            start_time=start_time,
            end_time=end_time,
            image_format=image_format,
            image_transparent=image_transparent,
            image_dpi=image_dpi,
            image_width=image_width,
            image_height=image_height,
            plot_colormap=plot_colormap,
            plot_colormap_min=plot_colormap_min,
            plot_colormap_max=plot_colormap_max,
            plot_font_size=plot_font_size,
            plot_grid=plot_grid,
            plot_coverage=plot_coverage,
            plot_cumulative=plot_cumulative,
            plot_mean=plot_mean,
            plot_mode=plot_mode,
            plot_noise_models=plot_noise_models,
            plot_percentiles=plot_percentiles,
            plot_x_frequency=plot_x_frequency,
            plot_x_min=plot_x_min,
            plot_x_max=plot_x_max,
            plot_y_min=plot_y_min,
            plot_y_max=plot_y_max,
        )

        # Load sources from database
        from seedpsd.workers.ppsd import SeedPsdWorkerPPSD

        worker_ppsd = SeedPsdWorkerPPSD(settings)
        sources = worker_ppsd.get_sources(
            network, station, location, channel, start_time, end_time
        )

        # Load PPSD and build plot for each source
        for source in sources:
            logger.info(f"Building histogram for {source.nslc}")
            ppsds = worker_ppsd.from_database(
                source, start_time, end_time, with_outdated=outdated
            )
            build_plot(
                "histogram", plot_settings, ppsds, settings, image_merge, output_path
            )

        logger.info("Done.")
        sys.exit(os.EX_OK)
    except Exception:
        logger.exception("Plot histogram failed")
        sys.exit(os.EX_SOFTWARE)


# PLOT SPECTROGRAM
########################################################################################################################


@psd.command(help="Plot spectrogram", aliases=["spectrogram"])
@click.argument("network")
@click.argument("station")
@click.argument("location")
@click.argument("channel")
@click.argument("start_time")
@click.argument("end_time")
@click.option(
    "--merge/--no-merge",
    "image_merge",
    default=True,
    help="Merge images resulting of multiple sampling rates",
)
@click.option("--format", "image_format", help="Image format (png, jpg, pdf)")
@click.option(
    "--transparent/--no-transparent",
    "image_transparent",
    default=None,
    help="Enable image transparency",
)
@click.option("--dpi", "image_dpi", help="Image DPI")
@click.option("--width", "image_width", help="Image width")
@click.option("--height", "image_height", help="Image height")
@click.option("--grid/--no-grid", "plot_grid", default=None, help="Show the grid")
@click.option(
    "--y-invert/--y-not-invert", "plot_y_invert", default=None, help="Invert the Y axis"
)
@click.option("--colormap", "plot_colormap", help="Set colourmap")
@click.option("--colormap-min", "plot_colormap_min", help="Set colourmap min")
@click.option("--colormap-max", "plot_colormap_max", help="Set colourmap max")
@click.option("--font-size", "plot_font_size", help="Set font size")
@click.option("--outdated", is_flag=True, help="Include dirty/outdated calculations")
@click.option(
    "--output",
    "output_path",
    default=os.getcwd(),
    help="Set output directory (default=current dir)",
)
@click.option("--level", "log_level", type=log_levels, help="Log verbosity level")
@click.option(
    "--sql-level",
    "sql_log_level",
    type=log_levels,
    help="SQLAlchemy log verbosity level",
)
@click.option("--debug", is_flag=True, help="Show stack trace")
@click.option(
    "--config-path",
    "config_path",
    type=click.Path(exists=True, readable=True),
    help="Custom config path",
)
def plot_spectrogram(
    network,
    station,
    location,
    channel,
    start_time,
    end_time,
    image_merge,
    image_format,
    image_transparent,
    image_dpi,
    image_width,
    image_height,
    plot_grid,
    plot_y_invert,
    plot_colormap,
    plot_colormap_min,
    plot_colormap_max,
    plot_font_size,
    outdated,
    output_path,
    log_level,
    sql_log_level,
    debug,
    config_path,
):
    # Load settings
    settings = Settings(config_path, debug=debug)

    # Configure loggers
    logger = configure_logger(settings, log_level, sql_log_level)

    try:
        # Configure Sentry
        configure_sentry(settings)

        # Check database
        check_db(settings)

        # Get plot settings
        plot_settings = SpectrogramPlotSettings(
            settings,
            start_time=start_time,
            end_time=end_time,
            image_format=image_format,
            image_transparent=image_transparent,
            image_dpi=image_dpi,
            image_width=image_width,
            image_height=image_height,
            plot_colormap=plot_colormap,
            plot_colormap_min=plot_colormap_min,
            plot_colormap_max=plot_colormap_max,
            plot_font_size=plot_font_size,
            plot_grid=plot_grid,
            plot_y_invert=plot_y_invert,
        )

        # Load sources from database
        from seedpsd.workers.ppsd import SeedPsdWorkerPPSD

        worker_ppsd = SeedPsdWorkerPPSD(settings)
        sources = worker_ppsd.get_sources(
            network, station, location, channel, start_time, end_time
        )

        # Load PPSD and build plot for each source
        for source in sources:
            logger.info(f"Building spectrogram for {source.nslc}")
            ppsds = worker_ppsd.from_database(
                source, start_time, end_time, with_outdated=outdated
            )
            build_plot(
                "spectrogram", plot_settings, ppsds, settings, image_merge, output_path
            )

        logger.info("Done.")
        sys.exit(os.EX_OK)
    except Exception:
        logger.exception("Plot spectrogram failed")
        sys.exit(os.EX_SOFTWARE)


# BUILD PLOT
########################################################################################################################


def build_plot(plot_type, plot_settings, ppsds, settings, image_merge, output_path):
    # Initialize the worker
    from seedpsd.workers.plot import SeedPsdWorkerPlot

    worker_plot = SeedPsdWorkerPlot(settings)

    # Multiple PPSD to plot
    if image_merge and len(ppsds) > 1:
        figures = []
        for ppsd in ppsds:
            figure = worker_plot.build_plot(ppsd, plot_settings, plot_type)
            figures.append(figure)

        filename = worker_plot.filename(ppsds, plot_settings, plot_type)
        worker_plot.save(figures, plot_settings, os.path.join(output_path, filename))

    # Unique PPSD to plot
    else:
        for ppsd in ppsds:
            figure = worker_plot.build_plot(ppsd, plot_settings, plot_type)
            filename = worker_plot.filename(ppsd, plot_settings, plot_type)
            worker_plot.save(figure, plot_settings, os.path.join(output_path, filename))


# NPZ
########################################################################################################################


@psd.command(help="Generate a NPZ file", aliases=["npz"])
@click.argument("network")
@click.argument("station")
@click.argument("location")
@click.argument("channel")
@click.argument("start_time")
@click.argument("end_time")
@click.option(
    "--output",
    "output_path",
    default=os.getcwd(),
    help="Set output directory (default=current dir)",
)
@click.option("--level", "log_level", type=log_levels, help="Log verbosity level")
@click.option(
    "--sql-level",
    "sql_log_level",
    type=log_levels,
    help="SQLAlchemy log verbosity level",
)
@click.option("--debug", is_flag=True, help="Show stack trace")
@click.option(
    "--config-path",
    "config_path",
    type=click.Path(exists=True, readable=True),
    help="Custom config path",
)
def build_npz(
    network,
    station,
    location,
    channel,
    start_time,
    end_time,
    output_path,
    log_level,
    sql_log_level,
    debug,
    config_path,
):
    # Load settings
    settings = Settings(config_path, debug=debug)

    # Configure loggers
    logger = configure_logger(settings, log_level, sql_log_level)

    try:
        # Configure Sentry
        configure_sentry(settings)

        # Check database
        check_db(settings)

        # Load sources from database
        from seedpsd.workers.ppsd import SeedPsdWorkerPPSD

        worker_ppsd = SeedPsdWorkerPPSD(settings)
        sources = worker_ppsd.get_sources(
            network, station, location, channel, start_time, end_time
        )

        # Load PPSD and build NPZ files for each source
        for source in sources:
            logger.info(f"Building NPZ for {source.nslc}")
            ppsds = worker_ppsd.from_database(source, start_time, end_time)
            for ppsd in ppsds:
                file_name = worker_ppsd.npz_filename(ppsd)
                worker_ppsd.save_to_npz(ppsd, file_name, output_path)

        logger.info("Done.")
        sys.exit(os.EX_OK)
    except Exception:
        logger.exception("Build npz failed")
        sys.exit(os.EX_SOFTWARE)


# EXPORT VALUES
########################################################################################################################


@psd.command(help="Export PSD values", aliases=["export", "values", "value"])
@click.argument("network")
@click.argument("station")
@click.argument("location")
@click.argument("channel")
@click.argument("start_time")
@click.argument("end_time")
@click.option(
    "--type",
    "output_type",
    type=click.Choice(["psd", "mean", "mode", "histogram"], case_sensitive=False),
    default="psd",
    help="Output type",
)
@click.option(
    "--format",
    "output_format",
    type=click.Choice(["csv", "json"], case_sensitive=False),
    default="csv",
    help="Output format",
)
@click.option(
    "--output",
    "output_path",
    default=os.getcwd(),
    help="Set output directory (default=current dir)",
)
@click.option("--level", "log_level", type=log_levels, help="Log verbosity level")
@click.option(
    "--sql-level",
    "sql_log_level",
    type=log_levels,
    help="SQLAlchemy log verbosity level",
)
@click.option("--debug", is_flag=True, help="Show stack trace")
@click.option(
    "--config-path",
    "config_path",
    type=click.Path(exists=True, readable=True),
    help="Custom config path",
)
def export_values(
    network,
    station,
    location,
    channel,
    start_time,
    end_time,
    output_type,
    output_format,
    output_path,
    log_level,
    sql_log_level,
    debug,
    config_path,
):
    # Load settings
    settings = Settings(config_path, debug=debug)

    # Configure loggers
    logger = configure_logger(settings, log_level, sql_log_level)

    try:
        # Configure Sentry
        configure_sentry(settings)

        # Check database
        check_db(settings)

        # Load sources from database
        from seedpsd.workers.ppsd import SeedPsdWorkerPPSD

        worker_ppsd = SeedPsdWorkerPPSD(settings)
        sources = worker_ppsd.get_sources(
            network, station, location, channel, start_time, end_time
        )

        # Initialize value extractor
        from seedpsd.workers.value import SeedPsdWorkerValue

        worker_value = SeedPsdWorkerValue(settings)

        # Load PPSD and build NPZ files for each source
        for source in sources:
            ppsds = worker_ppsd.from_database(source, start_time, end_time)
            for ppsd in ppsds:
                file_name = worker_value.build_filename(
                    ppsd, output_format, output_type
                )
                with open(os.path.join(output_path, file_name), "w") as output_file:
                    output_file.write(worker_value.build_header(ppsd, output_format))
                    output_file.write(
                        worker_value.export(ppsd, output_format, output_type)
                    )

        logger.info("Done.")
        sys.exit(os.EX_OK)
    except Exception:
        logger.exception("Export values failed")
        sys.exit(os.EX_SOFTWARE)
