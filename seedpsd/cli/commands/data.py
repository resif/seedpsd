import os
import sys

import click
from click_aliases import ClickAliasedGroup

from seedpsd.cli.tools import check_db, configure_logger, configure_sentry, log_levels
from seedpsd.settings.workers import Settings

# CONFIGURATION
########################################################################################################################


# Group all sub-commands declared below
@click.group(help="Load/Update data", cls=ClickAliasedGroup)
def data():
    pass


# LOAD DATA FROM DISK BY NSLC
########################################################################################################################


@data.command(help="Process MSEED files by year and network")
@click.option("--year", help="Filter by year")
@click.option("--network", help="Filter by network code")
@click.option("--station", help="Filter by station code")
@click.option("--location", help="Filter by location code")
@click.option("--channel", help="Filter by channel code")
@click.option(
    "--update",
    "force_update",
    is_flag=True,
    help="Force update of already processed files",
)
@click.option("--simulate", is_flag=True, help="Enable the simulation mode")
@click.option("--parallel", is_flag=True, help="Enable parallelization")
@click.option("--max-cpu", "max_cpu", help="CPU limit (only with parallel)")
@click.option("--level", "log_level", type=log_levels, help="Log verbosity level")
@click.option(
    "--sql-level",
    "sql_log_level",
    type=log_levels,
    help="SQLAlchemy log verbosity level",
)
@click.option("--debug", is_flag=True, help="Show stack trace")
@click.option(
    "--config-path",
    "config_path",
    type=click.Path(exists=True, readable=True),
    help="Custom config path",
)
def load(
    year,
    network,
    station,
    location,
    channel,
    force_update,
    simulate,
    parallel,
    max_cpu,
    log_level,
    sql_log_level,
    debug,
    config_path,
):
    # Load settings
    settings = Settings(config_path, debug=debug)

    # Configure loggers
    logger = configure_logger(settings, log_level, sql_log_level)

    try:
        # Configure Sentry
        configure_sentry(settings)

        # Check database
        check_db(settings)

        # Starting a worker
        from seedpsd.workers.data.mseed import SeedPsdWorkerDataMseed

        worker = SeedPsdWorkerDataMseed(settings, parallel=parallel, max_cpu=max_cpu)

        # Process
        worker.process_nslc(
            year, network, station, location, channel, force_update, simulate, debug
        )

        logger.info("Done.")
        sys.exit(os.EX_OK)
    except Exception:
        logger.exception("Load failed")
        sys.exit(os.EX_SOFTWARE)


# LOAD MSEED
########################################################################################################################


@data.command(help="Process MSEED files by path (file or directory)", aliases=["mseed"])
@click.argument("path")
@click.option(
    "--update",
    "force_update",
    is_flag=True,
    help="Force update of already processed files",
)
@click.option(
    "--list",
    "list_file",
    is_flag=True,
    help="The file contains a list of full paths to MSEED files",
)
@click.option("--simulate", is_flag=True, help="Enable the simulation mode")
@click.option("--parallel", is_flag=True, help="Enable parallelization")
@click.option("--max-cpu", "max_cpu", help="CPU limit (only with parallel)")
@click.option("--level", "log_level", type=log_levels, help="Log verbosity level")
@click.option(
    "--sql-level",
    "sql_log_level",
    type=log_levels,
    help="SQLAlchemy log verbosity level",
)
@click.option("--debug", is_flag=True, help="Show stack trace")
@click.option(
    "--config-path",
    "config_path",
    type=click.Path(exists=True, readable=True),
    help="Custom config path",
)
def load_mseed(
    path,
    force_update,
    list_file,
    simulate,
    parallel,
    max_cpu,
    log_level,
    sql_log_level,
    debug,
    config_path,
):
    # Load settings
    settings = Settings(config_path, debug=debug)

    # Configure loggers
    logger = configure_logger(settings, log_level, sql_log_level)

    try:
        # Configure Sentry
        configure_sentry(settings)

        # Check database
        check_db(settings)

        # Starting a worker
        from seedpsd.workers.data.mseed import SeedPsdWorkerDataMseed

        worker = SeedPsdWorkerDataMseed(settings, parallel=parallel, max_cpu=max_cpu)

        # Process the path
        if list_file:
            files = []
            with open(path) as file:
                files = file.readlines()
            worker.process_files(files, force_update, simulate, debug)
        else:
            worker.process_path(path, force_update, simulate, debug)

        logger.info("Done.")
        sys.exit(os.EX_OK)
    except Exception:
        logger.exception("Mseed file load failed")
        sys.exit(os.EX_SOFTWARE)


# LOAD NPZ
########################################################################################################################


@data.command(help="Process NPZ files by path (file or directory)", aliases=["npz"])
@click.argument("path")
@click.option(
    "--update",
    "force_update",
    is_flag=True,
    help="Force update of already processed files",
)
@click.option(
    "--dirty", "force_dirty", is_flag=True, help="Force import of dirty files"
)
@click.option("--simulate", is_flag=True, help="Enable the simulation mode")
@click.option("--parallel", is_flag=True, help="Enable parallelization")
@click.option("--max-cpu", "max_cpu", help="CPU limit (only with parallel)")
@click.option("--level", "log_level", type=log_levels, help="Log verbosity level")
@click.option(
    "--sql-level",
    "sql_log_level",
    type=log_levels,
    help="SQLAlchemy log verbosity level",
)
@click.option("--debug", is_flag=True, help="Show stack trace")
@click.option(
    "--config-path",
    "config_path",
    type=click.Path(exists=True, readable=True),
    help="Custom config path",
)
def load_npz(
    path,
    force_update,
    force_dirty,
    simulate,
    parallel,
    max_cpu,
    log_level,
    sql_log_level,
    debug,
    config_path,
):
    # Load settings
    settings = Settings(config_path, debug=debug)

    # Configure loggers
    logger = configure_logger(settings, log_level, sql_log_level)

    try:
        # Configure Sentry
        configure_sentry(settings)

        # Check database
        check_db(settings)

        # Starting a worker
        from seedpsd.workers.data.npz import SeedPsdWorkerDataNPZ

        worker = SeedPsdWorkerDataNPZ(settings, parallel=parallel, max_cpu=max_cpu)

        # Process the path
        worker.process_path(path, force_update, force_dirty, simulate, debug)

        logger.info("Done.")
        sys.exit(os.EX_OK)
    except Exception:
        logger.exception("Npz file load failed")
        sys.exit(os.EX_SOFTWARE)


# UPDATE
########################################################################################################################


@data.command(help="Find dirty files in database and reprocess them", aliases=["dirty"])
@click.option("--network", help="Filter by network code")
@click.option("--station", help="Filter by station code")
@click.option("--location", help="Filter by location code")
@click.option("--channel", help="Filter by channel code")
@click.option("--year", help="Filter by year")
@click.option(
    "--delete", is_flag=True, help="Enable the deletion of really dirty files"
)
@click.option("--simulate", is_flag=True, help="Enable the simulation mode")
@click.option("--parallel", is_flag=True, help="Enable parallelization")
@click.option("--max-cpu", "max_cpu", help="CPU limit (only with parallel)")
@click.option("--level", "log_level", type=log_levels, help="Log verbosity level")
@click.option(
    "--sql-level",
    "sql_log_level",
    type=log_levels,
    help="SQLAlchemy log verbosity level",
)
@click.option("--debug", is_flag=True, help="Show stack trace")
@click.option(
    "--config-path",
    "config_path",
    type=click.Path(exists=True, readable=True),
    help="Custom config path",
)
def update_dirty(
    network,
    station,
    location,
    channel,
    year,
    delete,
    simulate,
    parallel,
    max_cpu,
    log_level,
    sql_log_level,
    debug,
    config_path,
):
    # Load settings
    settings = Settings(config_path, debug=debug)

    # Configure loggers
    logger = configure_logger(settings, log_level, sql_log_level)

    try:
        # Configure Sentry
        configure_sentry(settings)

        # Check database
        check_db(settings)

        # Starting a worker
        from seedpsd.workers.data.mseed import SeedPsdWorkerDataMseed

        worker = SeedPsdWorkerDataMseed(settings, parallel=parallel, max_cpu=max_cpu)

        # Process
        worker.update_dirty(
            network=network,
            station=station,
            location=location,
            channel=channel,
            year=year,
            simulate=simulate,
            debug=debug,
            delete=delete,
        )
        logger.info("Done.")

        sys.exit(os.EX_OK)
    except Exception:
        logger.exception("Dirty files update failed")
        sys.exit(os.EX_SOFTWARE)


@data.command(
    help="Find orphan files in database and link them to related Source",
    aliases=["orphan"],
)
@click.option("--network", help="Filter by network code")
@click.option("--station", help="Filter by station code")
@click.option("--location", help="Filter by location code")
@click.option("--channel", help="Filter by channel code")
@click.option("--year", help="Filter by year")
@click.option(
    "--delete", is_flag=True, help="Enable the deletion of really orphan files"
)
@click.option("--simulate", is_flag=True, help="Enable the simulation mode")
@click.option("--level", "log_level", type=log_levels, help="Log verbosity level")
@click.option(
    "--sql-level",
    "sql_log_level",
    type=log_levels,
    help="SQLAlchemy log verbosity level",
)
@click.option("--debug", is_flag=True, help="Show stack trace")
@click.option(
    "--config-path",
    "config_path",
    type=click.Path(exists=True, readable=True),
    help="Custom config path",
)
def update_orphan(
    network,
    station,
    location,
    channel,
    year,
    delete,
    simulate,
    log_level,
    sql_log_level,
    debug,
    config_path,
):
    # Load settings
    settings = Settings(config_path, debug=debug)

    # Configure loggers
    logger = configure_logger(settings, log_level, sql_log_level)

    try:
        # Configure Sentry
        configure_sentry(settings)

        # Check database
        check_db(settings)

        # Starting a worker
        from seedpsd.workers.data.mseed import SeedPsdWorkerDataMseed

        worker = SeedPsdWorkerDataMseed(settings)

        # Process
        worker.update_orphan(
            network=network,
            station=station,
            location=location,
            channel=channel,
            year=year,
            simulate=simulate,
            debug=debug,
            delete=delete,
        )
        logger.info("Done.")

        sys.exit(os.EX_OK)
    except Exception:
        logger.exception("Orphan update failed")
        sys.exit(os.EX_SOFTWARE)
