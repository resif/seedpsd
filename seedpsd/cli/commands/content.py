import os
import sys

import click

from seedpsd.cli.tools import check_db, configure_logger, configure_sentry, log_levels
from seedpsd.settings.workers import Settings

# CONFIGURATION
########################################################################################################################


# Group all sub-commands declared below
@click.group(help="Database content management")
def content():
    pass


# FILES
########################################################################################################################


@content.command(help="List files")
@click.option("--network", help="Filter by network code")
@click.option("--station", help="Filter by station code")
@click.option("--location", help="Filter by location code")
@click.option("--channel", help="Filter by channel code")
@click.option("--date", type=click.DateTime(), help="End at datetime")
@click.option("--before", type=click.DateTime(), help="End before at datetime")
@click.option("--after", type=click.DateTime(), help="End after at datetime")
@click.option("--level", "log_level", type=log_levels, help="Log verbosity level")
@click.option(
    "--sql-level",
    "sql_log_level",
    type=log_levels,
    help="SQLAlchemy log verbosity level",
)
@click.option("--debug", is_flag=True, help="Show stack trace")
@click.option(
    "--config-path",
    "config_path",
    type=click.Path(exists=True, readable=True),
    help="Custom config path",
)
def list_file(
    network,
    station,
    location,
    channel,
    date,
    before,
    after,
    log_level,
    sql_log_level,
    debug,
    config_path,
):
    # Load settings
    settings = Settings(config_path, debug=debug)

    # Configure loggers
    logger = configure_logger(settings, log_level, sql_log_level)

    try:
        # Configure Sentry
        configure_sentry(settings)

        # Check database
        check_db(settings)

        # Starting a worker
        from seedpsd.workers.content import SeedPsdWorkerContent

        worker = SeedPsdWorkerContent(settings)

        # List the files
        files = worker.list_file(
            network, station, location, channel, date, before, after
        )

        if files:
            logger.info(f"Found {len(files)} file(s)")
            for file in files:
                logger.info(file)
        else:
            logger.info("No file found.")

        logger.info("Done.")
        sys.exit(os.EX_OK)
    except Exception:
        logger.exception("List files failed")
        sys.exit(os.EX_SOFTWARE)


# NETWORKS
########################################################################################################################


@content.command(help="List networks")
@click.option("--code", help="Filter by network code")
@click.option(
    "--temporary/--permanent",
    "temporary",
    default=None,
    help="Filter by temporary or pernament status",
)
@click.option("--start", type=click.DateTime(), help="Start at datetime")
@click.option(
    "--start-before",
    "start_before",
    type=click.DateTime(),
    help="Start before at datetime",
)
@click.option(
    "--start-after",
    "start_after",
    type=click.DateTime(),
    help="Start after at datetime",
)
@click.option("--end", type=click.DateTime(), help="End at datetime")
@click.option(
    "--end-before", "end_before", type=click.DateTime(), help="End before at datetime"
)
@click.option(
    "--end-after", "end_after", type=click.DateTime(), help="End after at datetime"
)
@click.option("--level", "log_level", type=log_levels, help="Log verbosity level")
@click.option(
    "--sql-level",
    "sql_log_level",
    type=log_levels,
    help="SQLAlchemy log verbosity level",
)
@click.option("--debug", is_flag=True, help="Show stack trace")
@click.option(
    "--config-path",
    "config_path",
    type=click.Path(exists=True, readable=True),
    help="Custom config path",
)
def list_network(
    code,
    temporary,
    start,
    start_before,
    start_after,
    end,
    end_before,
    end_after,
    log_level,
    sql_log_level,
    debug,
    config_path,
):
    # Load settings
    settings = Settings(config_path, debug=debug)

    # Configure loggers
    logger = configure_logger(settings, log_level, sql_log_level)

    try:
        # Configure Sentry
        configure_sentry(settings)

        # Check database
        check_db(settings)

        # Starting a worker
        from seedpsd.workers.content import SeedPsdWorkerContent

        worker = SeedPsdWorkerContent(settings)

        # List the files
        files = worker.list_network(
            code,
            start,
            start_before,
            start_after,
            end,
            end_before,
            end_after,
            temporary,
        )

        if files:
            logger.info(f"Found {len(files)} network(s)")
            for file in files:
                logger.info(file)
        else:
            logger.info("No network found.")

        logger.info("Done.")
        sys.exit(os.EX_OK)
    except Exception:
        logger.exception("List networks failed")
        sys.exit(os.EX_SOFTWARE)


# SOURCES
########################################################################################################################


@content.command(help="List sources")
@click.option("--network", help="Filter by network code")
@click.option("--station", help="Filter by station code")
@click.option("--location", help="Filter by location code")
@click.option("--channel", help="Filter by channel code")
@click.option("--level", "log_level", type=log_levels, help="Log verbosity level")
@click.option(
    "--sql-level",
    "sql_log_level",
    type=log_levels,
    help="SQLAlchemy log verbosity level",
)
@click.option("--debug", is_flag=True, help="Show stack trace")
@click.option(
    "--config-path",
    "config_path",
    type=click.Path(exists=True, readable=True),
    help="Custom config path",
)
def list_source(
    network, station, location, channel, log_level, sql_log_level, debug, config_path
):
    # Load settings
    settings = Settings(config_path, debug=debug)

    # Configure loggers
    logger = configure_logger(settings, log_level, sql_log_level)

    try:
        # Configure Sentry
        configure_sentry(settings)

        # Check database
        check_db(settings)

        # Starting a worker
        from seedpsd.workers.content import SeedPsdWorkerContent

        worker = SeedPsdWorkerContent(settings)

        # List the sources
        sources = worker.list_source(network, station, location, channel)

        if sources:
            logger.info(f"Found {len(sources)} source(s)")
            for source in sources:
                logger.info(source)
        else:
            logger.info("No source found.")

        logger.info("Done.")
        sys.exit(os.EX_OK)
    except Exception:
        logger.exception("List sources failed")
        sys.exit(os.EX_SOFTWARE)
