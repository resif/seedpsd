import os
import sys

import click
from click_aliases import ClickAliasedGroup

from seedpsd.cli.tools import check_db, configure_logger, configure_sentry, log_levels
from seedpsd.settings.workers import Settings

# CONFIGURATION
########################################################################################################################


# Group all sub-commands declared below
@click.group(help="Load/Update metadata", cls=ClickAliasedGroup)
def metadata():
    pass


# LOAD/UPDATE EPOCHS FROM INVENTORY
########################################################################################################################


@metadata.command(help="Update epochs by querying the inventory", aliases=["load"])
@click.option("--network", help="Filter by network code")
@click.option("--station", help="Filter by station code")
@click.option("--location", help="Filter by location code")
@click.option("--channel", help="Filter by channel code")
@click.option("--simulate", is_flag=True, help="Enable the simulation mode")
@click.option("--parallel", is_flag=True, help="Enable parallelization")
@click.option("--max-cpu", "max_cpu", help="CPU limit (only with parallel)")
@click.option("--level", "log_level", type=log_levels, help="Log verbosity level")
@click.option(
    "--sql-level",
    "sql_log_level",
    type=log_levels,
    help="SQLAlchemy log verbosity level",
)
@click.option("--debug", is_flag=True, help="Show stack trace")
@click.option(
    "--config-path",
    "config_path",
    type=click.Path(exists=True, readable=True),
    help="Custom config path",
)
def update(
    network,
    station,
    location,
    channel,
    simulate,
    parallel,
    max_cpu,
    log_level,
    sql_log_level,
    debug,
    config_path,
):
    # Load settings
    settings = Settings(config_path, debug=debug)

    # Configure loggers
    logger = configure_logger(settings, log_level, sql_log_level)

    try:
        # Configure Sentry
        configure_sentry(settings)

        # Check database
        check_db(settings)

        # Starting a worker
        from seedpsd.workers.metadata import SeedPsdWorkerMetadata

        worker = SeedPsdWorkerMetadata(settings, parallel=parallel, max_cpu=max_cpu)

        # Load/Update epochs
        worker.update(network, station, location, channel, simulate)

        logger.info("Done.")
        sys.exit(os.EX_OK)
    except Exception:
        logger.exception("Metadata update failed")
        sys.exit(os.EX_SOFTWARE)
