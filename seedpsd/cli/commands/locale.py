import logging
import os
import sys
from pathlib import Path

import click
from babel.messages.catalog import Catalog
from babel.messages.extract import extract_from_dir
from babel.messages.mofile import write_mo
from babel.messages.pofile import read_po, write_po
from click_aliases import ClickAliasedGroup

from seedpsd.cli.tools import configure_logger, configure_sentry, log_levels
from seedpsd.settings.workers import Settings

# CONFIGURATION
########################################################################################################################


# Group all sub-commands declared below
@click.group(help="Localization", cls=ClickAliasedGroup)
def locale():
    pass


# EXTRACT
########################################################################################################################


@locale.command(help="Extract messages from source files and generate a POT file")
@click.option("--locale", help="Locale")
@click.option(
    "--level",
    "log_level",
    type=log_levels,
    default=logging.INFO,
    help="Log verbosity level",
)
@click.option("--debug", is_flag=True, help="Show stack trace")
@click.option(
    "--config-path",
    "config_path",
    type=click.Path(exists=True, readable=True),
    help="Custom config path",
)
def extract(locale, log_level, debug, config_path):
    def _write_catalog(catalog, locale, domain, settings):
        logger.debug(f"_write_catalog({catalog}, {locale}, {domain} {settings})")
        logger.info(f"Writing '{locale}' catalog for '{domain}'")

        if isinstance(settings.locale_path, Path):
            output_path = os.path.join(
                settings.locale_path.as_posix(), locale, "LC_MESSAGES", f"{domain}.po"
            )
            os.makedirs(
                os.path.abspath(
                    os.path.join(settings.locale_path.as_posix(), locale, "LC_MESSAGES")
                ),
                exist_ok=True,
            )
        else:
            output_path = os.path.join(
                settings.locale_path, locale, "LC_MESSAGES", f"{domain}.po"
            )
            os.makedirs(
                os.path.abspath(
                    os.path.join(settings.locale_path, locale, "LC_MESSAGES")
                ),
                exist_ok=True,
            )

        logger.verbose("Writing catalog to %s", output_path)
        if os.path.exists(output_path):
            with open(output_path, "rb") as old_file:
                old_catalog = read_po(old_file, locale=locale, domain=domain)
            old_catalog.update(catalog, no_fuzzy_matching=True)

            with open(output_path, "wb") as output:
                write_po(output, old_catalog, omit_header=True, sort_output=True)
        else:
            with open(output_path, "wb") as output:
                write_po(output, catalog, omit_header=True, sort_output=True)

    # Loading settings
    settings = Settings(config_path, debug=debug)

    # Configure loggers
    logger = configure_logger(settings, log_level)

    try:
        # Configure Sentry
        configure_sentry(settings)

        # Configure extraction backends
        method_map = [
            ("**.jinja2", "jinja2"),
        ]

        # Configure sources of messages
        domains = {
            "api_templates": os.path.abspath(
                os.path.join(settings._config_path, "seedpsd", "api", "templates")
            ),
        }

        # Analyze each source
        for domain, domain_path in domains.items():
            logger.info(f"Extracting messages for '{domain}'")

            # Initialize a catalog
            catalog = Catalog()

            # Extract messages from sources
            extracted = extract_from_dir(dirname=domain_path, method_map=method_map)

            # Analyze each extracted message
            for filename, lineno, message, comments, context in extracted:
                # Add the message to the catalog
                catalog.add(
                    message,
                    locations=[
                        (
                            os.path.join(
                                os.path.relpath(domain_path, settings._config_path),
                                filename,
                            ),
                            lineno,
                        )
                    ],
                    auto_comments=comments,
                    context=context,
                )

            # Write output file for the requested locale
            if locale:
                for lang in str(locale).split(","):
                    _write_catalog(catalog, lang, domain, settings)

            # Or, write output file for all available locales
            else:
                for lang in settings.locale_available:
                    _write_catalog(catalog, lang, domain, settings)

        logger.info("Done.")
        sys.exit(os.EX_OK)
    except Exception:
        logger.exception("Local exctraction failed")
        sys.exit(os.EX_SOFTWARE)


# COMPILE
########################################################################################################################


@locale.command(help="Compile message catalogs to MO files")
@click.option("--locale", help="Locale")
@click.option(
    "--level",
    "log_level",
    type=log_levels,
    default=logging.INFO,
    help="Log verbosity level",
)
@click.option("--debug", is_flag=True, help="Show stack trace")
@click.option(
    "--config-path",
    "config_path",
    type=click.Path(exists=True, readable=True),
    help="Custom config path",
)
def compile(locale, log_level, debug, config_path):
    def _compile_locale(locale, settings):
        logger.debug(f"_compile_locale({locale}, {settings})")
        domains = ["api_templates"]

        for domain in domains:
            logger.info(f"Compiling '{locale}' messages for '{domain}'")
            if isinstance(settings.locale_path, Path):
                locale_path = os.path.abspath(
                    os.path.join(settings.locale_path.as_posix(), locale, "LC_MESSAGES")
                )
            else:
                locale_path = os.path.abspath(
                    os.path.join(settings.locale_path, locale, "LC_MESSAGES")
                )

            os.makedirs(locale_path, exist_ok=True)

            input_path = os.path.join(locale_path, f"{domain}.po")
            logger.verbose("Reading text catalog from %s", input_path)
            with open(input_path, "rb") as infile:
                catalog = read_po(infile, locale, domain=domain)

            output_path = os.path.join(locale_path, f"{domain}.mo")
            logger.verbose("Writing binary catalog to %s", output_path)
            with open(output_path, "wb") as outfile:
                write_mo(outfile, catalog)

    # Loading settings
    settings = Settings(config_path, debug=debug)

    # Configure loggers
    logger = configure_logger(settings, log_level)

    try:
        # Configure Sentry
        configure_sentry(settings)

        # For the requested locale
        if locale:
            for lang in str(locale).split(","):
                _compile_locale(lang, settings)

        # Or, for all available locales
        else:
            for lang in list(settings.locale_available):
                _compile_locale(lang, settings)

        logger.info("Done.")
        sys.exit(os.EX_OK)
    except Exception:
        logger.exception("Locale compilation failed")
        sys.exit(os.EX_SOFTWARE)
