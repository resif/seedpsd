#############################################
FROM ghcr.io/astral-sh/uv:python3.11-bookworm-slim AS builder

# Enable bytecode compilation
# Copy from the cache instead of linking since it's a mounted volume
ENV UV_COMPILE_BYTECODE=1 \
    UV_LINK_MODE=copy

WORKDIR /app
RUN touch README.md
# Sync only project dependencies
RUN --mount=type=cache,target=/root/.cache/uv \
    --mount=type=bind,source=uv.lock,target=uv.lock \
    --mount=type=bind,source=pyproject.toml,target=pyproject.toml \
    uv sync --frozen --no-install-project --no-dev
ADD . /app
# Sync project
RUN --mount=type=cache,target=/root/.cache/uv \
    uv sync --frozen --no-dev

#############################################
FROM python:3.11-slim AS seedpsd

RUN addgroup --system app && \
    adduser --system --group app
USER app
# Copy the application from the builder
COPY --from=builder --chown=app:app /app /app

# Place executables in the environment at the front of the path
ARG CI_COMMIT_SHORT_SHA
ENV SENTRY_RELEASE=$CI_COMMIT_SHORT_SHA \
    SEEDPSD_COMMIT=$CI_COMMIT_SHORT_SHA \
    PATH="/app/.venv/bin:$PATH"

# For seedpsd-web
#ENTRYPOINT ["seedpsd-cli", "server", "web"]
# For seedpsd-amqp-data
#ENTRYPOINT ["seedpsd-cli", "server", "amqp-data"]
# For seedpsd-amqp-metadata
#ENTRYPOINT ["seedpsd-cli", "server", "amqp-metadata"]
