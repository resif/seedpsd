# SeedPSD changelog

## Releases

### 3.2.0 (2024-10-31)

* Major changes of the PSD calculation parameters and support of overlapped data (BLOCKING CHANGES: Recalculation required!)
* Major redesign of the database structure (multi-tenancy: one schema per network)
* Major redesign of the HTML templates allowing translation and customization of the layout and CSS
* Support of SDS data structure and empty location codes
* Support for reading metadata from a StationXML file rather than from an FDSN webservice
* Support of RabbitMQ (AMQP)
* Dropped support of Qpid (AMQP)
* Sentry log collection support
* A lot of new settings
* A lot of bugfixes
* A lot of documentation enhancements

Notes:
* Last release by Philippe BOLLARD
* See [known limitations](doc/known-limitations.md)
* Changed git workflow from gitflow to gitlab-flow. See [Gitlab Flow](https://about.gitlab.com/blog/2020/03/05/what-is-gitlab-flow/)
* Requirements managed with [uv](https://docs.astral.sh/uv/)

### 3.1.0 (2023-02-01)

* Major redesign of the database session management
* Allow customization of the plot footer
* Change default colormap to 'viridis_r' (instead of 'pqlx')
* Handle null date(time) when no endtime is specified
* Handle update of dirty/orphan files
* Documentation enhancements

### 3.0.0 (2022-08-05)

Major redesign of the whole application:

* Store PSD into a database instead of NPZ files
* Metadata update tracking with obsolete data reprocessing
* Web service included with :
 - URL builder and documentation
 - plot generation (with a lot of options)
 - PSD values extraction as text file

### 2.0.0 (2021-07-16)

Refactoring:

* Move from ArgParse to Click
* ...

New features:

* Add AMQP listener
* Find and clean dirty files

* Second prototype (released by Philippe BOLLARD)

### 1.0.0 (2021-02-01)

* First prototype (released by Jérôme TOUVIER)

### 0.0.x (Unreleased)

* Internal developments
