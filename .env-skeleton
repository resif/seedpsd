########################################################################################################################
## SEEDPSD - CONFIGURATION SKELETON
## Please fill all needed parameters below or set/override them using environment variables
########################################################################################################################


## Database ############################################################################################################

## Mandatory: Database name
SEEDPSD_DATABASE_NAME=seedpsd

## Mandatory: User login
SEEDPSD_DATABASE_USER=

## Optional: User password
SEEDPSD_DATABASE_PASS=

## Mandatory: Server hostname
SEEDPSD_DATABASE_HOST=

## Optional: Server port
#SEEDPSD_DATABASE_PORT=

## Optional: Connection pool type(queue|static|null, default=queue)
#SEEDPSD_DATABASE_POOL_TYPE=queue

## Optional: Connection pool size min (default=5)
#SEEDPSD_DATABASE_POOL_SIZE_MIN=5

## Optional: Connection pool size max (default=10)
#SEEDPSD_DATABASE_POOL_SIZE_MAX=10

## Optional: Connection pool recycle timeout in seconds (default=-1)
#SEEDPSD_DATABASE_POOL_RECYCLE=-1

## Optional: Connection pool timeout in seconds (default=30)
#SEEDPSD_DATABASE_POOL_TIMEOUT=30

## Optional: Check connection availability by pre-ping strategy (default=true)
#SEEDPSD_DATABASE_POOL_PRE_PING=true

## Optional: Connection pool enabled with parallel mode (default=false)
#SEEDPSD_DATABASE_POOL_WITH_PARALLEL=false


## StationXML Metadata #################################################################################################

## Mandatory: ObsPy Client to use
## See: https://docs.obspy.org/packages/obspy.clients.fdsn.html
SEEDPSD_FDSN_CLIENT=EIDA-ROUTING

## Optional: Debug mode of ObsPy client (default=false)
#SEEDPSD_FDSN_CLIENT_DEBUG=false

## Optional: Root directory containing StationXML metadata files to use instead of using FDSN ObsPy Client
#SEEDPSD_PATH_METADATA=


## MSEED Data files ####################################################################################################

## Mandatory: Root directory containing MSEED data files
SEEDPSD_PATH_DATA=

## Optional: Directories/files are organized using SDS (default=false)
#SEEDPSD_USE_SDS=false

## Optional: Directories/files are organized using network extended code (default=false)
#SEEDPSD_USE_EXTENDED=false


## PPSD Calculation and plot rendering #################################################################################

## Optional: Use ObsPy default value for PPSD calculation instead of SeedPSD customization by channel band
## Recommended value (using an empty database): false
## Default value (for compatibility using an already populated database): true
SEEDPSD_PPSD_USE_DEFAULTS=false

## Optional: Exclude no-seismic channels (default=true)
SEEDPSD_PPSD_ONLY_ALLOWED_CHANNELS=true

## Optional: Allowed channel code: bands (default=BCDEFGHLMPQRSTUV)
#SEEDPSD_PPSD_ALLOWED_CHANNEL_BANDS=BCDEFGHLMPQRSTUV

## Optional: Allowed channel code: instruments (default=DGHJLMNP)
#SEEDPSD_PPSD_ALLOWED_CHANNEL_INSTRUMENTS=DGHJLMNP

## Optional: Allowed channel code: orientations (default=ABCDEFHINORTUVWZ123)
#SEEDPSD_PPSD_ALLOWED_CHANNEL_ORIENTATIONS=ABCDEFHINORTUVWZ123

## Optional: Datacenter name to use on plot footer (default=SeedPSD)
#SEEDPSD_PLOT_FOOTER_DC=

## Optional: Licence to use on plot footer
#SEEDPSD_PLOT_FOOTER_LICENSE=


## Webservice HTML pages ###############################################################################################

## Optional: Path used in generated URLs (default=/)
#SEEDPSD_API_PATH=/


## Optional: Default language (default=en)
#SEEDPSD_LOCALE_DEFAULT=en

## Optional: Available languages (default=en)
#SEEDPSD_LOCALE_AVAILABLE=en,fr


## Optional: Template skin name
#SEEDPSD_SKIN_NAME=

## Optional: Path containing skin templates and resources (instead of using a skin name)
#SEEDPSD_SKIN_PATH=


## Optional: Example customization
#SEEDPSD_EXAMPLE_NETWORK=
#SEEDPSD_EXAMPLE_STATION=
#SEEDPSD_EXAMPLE_LOCATION=
#SEEDPSD_EXAMPLE_CHANNEL=
#SEEDPSD_EXAMPLE_START=
#SEEDPSD_EXAMPLE_END=


## Logging #############################################################################################################

## Optional: Path (directory or '.log' file) to use for logging into a file
#SEEDPSD_LOG_PATH=

## Optional: Log stack trace when an error occurred (default=false)
#SEEDPSD_LOG_STACKTRACE=false

## Optional: Application logging level (DEBUG|VERBOSE|INFO|WARNING|ERROR) (default=INFO)
#SEEDPSD_LOG_LEVEL=INFO

## Optional: Database logging level (default=WARNING)
#SEEDPSD_SQL_LOG_LEVEL=WARNING


## Optional: Sentry data source name (to enable lgging with Sentry)
## See: https://docs.sentry.io/product/sentry-basics/concepts/dsn-explainer/
#SEEDPSD_SENTRY_DSN=

## Optional: Enable tracing with Sentry (default=false)
#SEEDPSD_SENTRY_TRACING=true

## Optional: Sentry running environment
#SEEDPSD_SENTRY_ENVIRONMENT=production

## Optional: Sentry sample rate
#SEEDPSD_SENTRY_SAMPLE_RATE=1.0


## AMQP automation #####################################################################################################

## Mandatory if used: Server URL
#SEEDPSD_AMQP_SERVER_URL=localhost
#SEEDPSD_AMQP_PORT=  # default=5672
#SEEDPSD_AMQP_VHOST= # default=/
#SEEDPSD_AMQP_USER=
#SEEDPSD_AMQP_PASSWORD=

# Optional: Heartbeat timeout value (default=2400)
#SEEDPSD_AMQP_HEARTBEAT=2400

# Optional: blocked connection timeout value (default=1200)
#SEEDPSD_AMQP_TIMEOUT=1200

# Optional: Log level for AMQP backend (default=WARNING)
#SEEDPSD_AMQP_LOG_LEVEL=WARNING

## Optional: Exchanger name (default=post-integration)
#SEEDPSD_AMQP_EXCHANGE_NAME=post-integration

## Optional: Exchanger type (direct|fanout|headers|topic, default=direct)
#SEEDPSD_AMQP_EXCHANGE_TYPE=direct

## Optional: Exchanger durable flag (default=true)
#SEEDPSD_AMQP_EXCHANGE_DURABLE=true


## Optional: Data queue name (default=seedpsd-data)
#SEEDPSD_AMQP_DATA_QUEUE_NAME=seedpsd-data

## Optional: Data queue routing key (default=miniseed)
#SEEDPSD_AMQP_DATA_QUEUE_ROUTING=miniseed

## Optional: Data queue durable flag (default=true)
#SEEDPSD_AMQP_DATA_QUEUE_DURABLE=true

## Optional: Metadata queue name (default=seedpsd-metadata)
#SEEDPSD_AMQP_METADATA_QUEUE_NAME=seedpsd-metadata

## Optional: Routing key for metadata processing (default=stationxml)
#SEEDPSD_AMQP_METADATA_QUEUE_ROUTING=stationxml

## Optional: Metadata queue durable flag (default=true)
#SEEDPSD_AMQP_METADATA_QUEUE_DURABLE=true

## Optional: Chain data update after metadata update (default=false)
#SEEDPSD_AMQP_METADATA_UPDATE_DATA=false

## Optional: Allow orphan files deletion after metadata update (default=false)
#SEEDPSD_AMQP_METADATA_DELETE_ORPHAN=false

## Optional: Allow dirty files deletion after metadata update (default=false)
#SEEDPSD_AMQP_METADATA_DELETE_DIRTY=false
