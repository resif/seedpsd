[build-system]
requires = ["hatchling"]
build-backend = "hatchling.build"

[project]
name = "seedpsd"
dynamic = ["version"]
description = "SeedPSD"
readme = "README.md"
license = {file = "LICENSE"}
authors = [
    { name = "Philippe Bollard", email = "dc@resif.fr" },
]
maintainers = [
    { name = "Résif-DC", email = "dc@resif.fr" },
]
classifiers = [
    "Development Status :: 4 - Beta",
    "Intended Audience :: Developers",
    "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
    "Natural Language :: English",
    "Operating System :: OS Independent",
    "Programming Language :: Python :: 3",
]
dependencies = [
    "alembic",
    "alembic-verify",
    "babel>=2.15.0",
    "click",
    "click-aliases",
    "click-loglevel",
    "coloredlogs",
    "fdsnnetextender",
    "multiprocess",
    "obspy!=1.4.1",
    "pendulum<3.0.0",
    "pika>=1.3.2",
    "psycopg[binary]>=3.2.1",
    "pypdf3",
    "pyramid>=2.0.2",
    "pyramid-jinja2>=2.10.1",
    "pyramid-layout>=1.0",
    "python-dotenv>=1.0.1",
    "sentry-sdk",
    "simple-singleton",
    "sqlalchemy-utils",
    "sqlalchemy>=2.0",
    "tablib",
    "verboselogs",
    "waitress>=3.0.0",
]
packages = [{include = "seedpsd"}]
requires-python = ">= 3.11"

[project.scripts]
seedpsd-cli = "seedpsd.cli:cli"

[project.urls]
Homepage = "https://gricad-gitlab.univ-grenoble-alpes.fr/OSUG/RESIF/seedpsd"
Documentation = "https://gricad-gitlab.univ-grenoble-alpes.fr/OSUG/RESIF/seedpsd"
Repository = "https://gricad-gitlab.univ-grenoble-alpes.fr/OSUG/RESIF/seedpsd"
"Bug Tracker" = "https://gricad-gitlab.univ-grenoble-alpes.fr/OSUG/RESIF/seedpsd/issues"
Changelog = "https://gricad-gitlab.univ-grenoble-alpes.fr/OSUG/RESIF/seedpsd/-/blob/main/CHANGELOG.md"

[tool.uv]
managed = true
dev-dependencies = [
    "pytest",
    "pytest-cov",
]

[tool.hatch.version]
path = "seedpsd/__init__.py"

[tool.hatch.build.targets.sdist]
include = [
    "/seedpsd",
]

[tool.pytest.ini_options]
minversion = "6.0"
addopts = "--cov"
testpaths = ["tests"]
filterwarnings = ["ignore::DeprecationWarning"]

[tool.coverage.run]
branch = true
source = ["seedpsd"]
omit = ["*/tests/*"]

[tool.ruff]
target-version = "py311"
exclude = ["spectral_estimation.py"]
output-format = "concise"

[tool.ruff.lint]
select = ["ALL"]
ignore = [
  "ANN",    # Type annotations
  "COM812", # Trailing comma missing
  "D",      # PydocStyle
  "ISC001",
]


[tool.ruff.lint.per-file-ignores]
"tests/**.py" = ["PLR2004", "S101", "S108", "SLF001"]

[tool.djlint]
indent = 2
profile = "jinja"
blank_line_after_tag = "load,endblock"
blank_line_before_tag = "block"
ignore = "T028"
extension = "jinja2"
files = ["seedpsd/api/templates/"]
use_gitignore = true

[tool.djlint.per-file-ignores]
"seedpsd/api/templates/views/wadl.jinja2" = "H021"
