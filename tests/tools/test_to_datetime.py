# ruff: noqa: DTZ001
import datetime

import pendulum
from obspy import UTCDateTime

from seedpsd.tools.common import to_datetime


class TestToDatetime:
    """
    Tests of the function "to_datetime()"
    """

    def test_none(self):
        assert to_datetime(None) is None

    def test_pendulum(self):
        d = pendulum.DateTime(2023, 1, 1)
        assert to_datetime(d) == d

    def test_utcdatetime(self):
        d = UTCDateTime(2023, 1, 1)
        assert to_datetime(d) == pendulum.DateTime(2023, 1, 1)

    def test_date(self):
        d = datetime.date(2023, 1, 1)
        assert to_datetime(d) == pendulum.DateTime(2023, 1, 1)

    def test_datetime(self):
        d = datetime.datetime(2023, 1, 1)
        assert to_datetime(d) == pendulum.DateTime(2023, 1, 1)

    def test_str_date(self):
        d = "2023-01-01"
        assert to_datetime(d) == pendulum.DateTime(2023, 1, 1)

    def test_str_datetime(self):
        d = "2023-01-01T00:00:00Z"
        assert to_datetime(d) == pendulum.DateTime(2023, 1, 1)
