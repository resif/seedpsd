import pytest

from seedpsd.tools.common import is_allowed_channel


class TestIsAllowedChannel:
    """
    Tests of the function "is_seismic_channel()"
    """

    BANDS = ("A", "B")
    INSTRUMENTS = ("C", "D")
    ORIENTATIONS = ("E", "F")

    @pytest.mark.parametrize("band", BANDS)
    @pytest.mark.parametrize("instrument", INSTRUMENTS)
    @pytest.mark.parametrize("orientation", ORIENTATIONS)
    def test_allowed(self, band, instrument, orientation):
        assert (
            is_allowed_channel(
                f"{band}{instrument}{orientation}",
                bands="".join(self.BANDS),
                instruments="".join(self.INSTRUMENTS),
                orientations="".join(self.ORIENTATIONS),
            )
            is True
        )

    @pytest.mark.parametrize("band", "X")
    @pytest.mark.parametrize("instrument", INSTRUMENTS)
    @pytest.mark.parametrize("orientation", ORIENTATIONS)
    def test_non_allowed_band(self, band, instrument, orientation):
        assert (
            is_allowed_channel(
                f"{band}{instrument}{orientation}",
                bands="".join(self.BANDS),
                instruments="".join(self.INSTRUMENTS),
                orientations="".join(self.ORIENTATIONS),
            )
            is False
        )

    @pytest.mark.parametrize("band", BANDS)
    @pytest.mark.parametrize("instrument", "X")
    @pytest.mark.parametrize("orientation", ORIENTATIONS)
    def test_non_allowed_instrument(self, band, instrument, orientation):
        assert (
            is_allowed_channel(
                f"{band}{instrument}{orientation}",
                bands="".join(self.BANDS),
                instruments="".join(self.INSTRUMENTS),
                orientations="".join(self.ORIENTATIONS),
            )
            is False
        )

    @pytest.mark.parametrize("band", BANDS)
    @pytest.mark.parametrize("instrument", INSTRUMENTS)
    @pytest.mark.parametrize("orientation", "X")
    def test_non_allowed_orientation(self, band, instrument, orientation):
        assert (
            is_allowed_channel(
                f"{band}{instrument}{orientation}",
                bands="".join(self.BANDS),
                instruments="".join(self.INSTRUMENTS),
                orientations="".join(self.ORIENTATIONS),
            )
            is False
        )
