# ruff: noqa: N802
from seedpsd.tools.common import value_or_default


class TestValueOrDefault:
    """
    Tests of the function "value_or_default()"
    """

    def test_value_FR(self):
        assert value_or_default("FR") == "FR"

    def test_value_2023(self):
        assert value_or_default(2023) == 2023

    def test_default_FR(self):
        assert value_or_default(None, "FR") == "FR"

    def test_default_2023(self):
        assert value_or_default(None, 2023) == 2023
