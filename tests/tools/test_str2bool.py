from seedpsd.tools.common import str2bool


class TestStr2Bool:
    """
    Tests of the function "str2bool()"
    """

    def test_yes(self):
        assert str2bool("yes") is True

    def test_yes_uppercase(self):
        assert str2bool("YES") is True

    def test_true(self):
        assert str2bool("true") is True

    def test_on(self):
        assert str2bool("on") is True

    def test_1(self):
        assert str2bool("1") is True

    def test_false(self):
        assert str2bool("false") is False

    def test_other_false(self):
        assert str2bool("other") is False

    def test_none(self):
        assert str2bool(None) is None

    def test_boolean_true(self):
        assert str2bool(True) is True

    def test_boolean_false(self):
        assert str2bool(False) is False
