import logging
from pathlib import Path

from pytest import fixture, raises

from seedpsd.errors import EmptySettingValueError, UndefinedSettingValueError
from seedpsd.settings.workers import Settings


class TestSettingsWorker:
    """
    Tests of the class "Settings"
    """

    @fixture
    def settings_empty(self):
        file_path = Path(__file__).parent.resolve()
        return Settings(
            config_path=file_path, config_file=".env-empty", debug=True, prefix="TEST"
        )

    @fixture
    def settings_full(self):
        file_path = Path(__file__).parent.resolve()
        return Settings(config_path=file_path, config_file=".env-full", debug=True)

    # GET VALUES #######################################################################################################

    def test_empty(self, settings_empty):
        # Mandatory
        with raises((EmptySettingValueError, UndefinedSettingValueError)):
            settings_empty._get_value("EMPTY_1", mandatory=True)

        # With default callable
        def _default():
            pass

        value_2 = settings_empty._get_value("EMPTY_2", default=_default)
        assert value_2 is None

        # With default value
        value_3 = settings_empty._get_value("EMPTY_3", default="default")
        assert value_3 == "default"

        # Without default
        value_4 = settings_empty._get_value("EMPTY_4")
        assert value_4 is None

    def test_unknown_key(self, settings_empty):
        # Mandatory
        with raises((EmptySettingValueError, UndefinedSettingValueError)):
            _ = settings_empty._get_value("UNKNOWN_KEY_1", mandatory=True)

        # With default callable
        def _default():
            pass

        value_2 = settings_empty._get_value("UNKNOWN_KEY_2", default=_default)
        assert value_2 is None

        # With default value
        value_3 = settings_empty._get_value("UNKNOWN_KEY_3", default="default")
        assert value_3 == "default"

        # Without default
        value_4 = settings_empty._get_value("UNKNOWN_KEY_4")
        assert value_4 is None

        # Without default, using prefix
        value_5 = settings_empty._get_value("UNKNOWN_KEY_5", "UNKNOWN_KEY_5")
        assert value_5 is None

    # AMQP #############################################################################################################

    def test_amqp_server_url(self, settings_empty, settings_full):
        assert settings_full.amqp_server_url == "amqp-server.mynetwork.lan:1234"

        with raises((EmptySettingValueError, UndefinedSettingValueError)):
            _ = settings_empty.amqp_server_url

    def test_amqp_log_level(self, settings_empty, settings_full):
        assert settings_full.amqp_log_level == "DEBUG"
        assert settings_empty.amqp_log_level == logging.WARNING

    def test_amqp_data_queue_name(self, settings_empty, settings_full):
        assert settings_full.amqp_data_queue_name == "data-queue-name"
        assert settings_empty.amqp_data_queue_name == "seedpsd-data"

    def test_amqp_data_queue_routing(self, settings_empty, settings_full):
        assert settings_full.amqp_data_queue_routing == "data-queue-routing"
        assert settings_empty.amqp_data_queue_routing == "miniseed"

    def test_amqp_data_queue_durable(self, settings_empty, settings_full):
        assert settings_full.amqp_data_queue_durable is True
        assert settings_empty.amqp_data_queue_durable is True

    def test_amqp_exchange_name(self, settings_empty, settings_full):
        assert settings_full.amqp_exchange_name == "data-exchange-name"
        assert settings_empty.amqp_exchange_name == "post-integration"

    def test_amqp_exchange_type(self, settings_empty, settings_full):
        assert settings_full.amqp_exchange_type == "data-exchange-type"
        assert settings_empty.amqp_exchange_type == "direct"

    def test_amqp_exchange_durable(self, settings_empty, settings_full):
        assert settings_full.amqp_exchange_durable is True
        assert settings_empty.amqp_exchange_durable is True

    def test_amqp_metadata_queue_name(self, settings_empty, settings_full):
        assert settings_full.amqp_metadata_queue_name == "metadata-queue-name"
        assert settings_empty.amqp_metadata_queue_name == "seedpsd-metadata"

    def test_amqp_metadata_queue_routing(self, settings_empty, settings_full):
        assert settings_full.amqp_metadata_queue_routing == "metadata-queue-routing"
        assert settings_empty.amqp_metadata_queue_routing == "stationxml"

    def test_amqp_metadata_queue_durable(self, settings_empty, settings_full):
        assert settings_full.amqp_metadata_queue_durable is True
        assert settings_empty.amqp_metadata_queue_durable is True

    def test_amqp_metadata_update_data(self, settings_empty, settings_full):
        assert settings_full.amqp_metadata_update_data is True
        assert settings_empty.amqp_metadata_update_data is False

    def test_amqp_metadata_delete_orphan(self, settings_empty, settings_full):
        assert settings_full.amqp_metadata_delete_orphan is True
        assert settings_empty.amqp_metadata_delete_orphan is False

    def test_amqp_metadata_delete_dirty(self, settings_empty, settings_full):
        assert settings_full.amqp_metadata_delete_dirty is True
        assert settings_empty.amqp_metadata_delete_dirty is False

    # API ##############################################################################################################

    def test_api_path(self, settings_empty, settings_full):
        assert settings_full.api_path == "/seedpsd"
        assert settings_empty.api_path == "/"

    # DATA #############################################################################################################

    def test_data_path(self, settings_empty, settings_full):
        assert settings_full.data_path == "/here/data"

        with raises((EmptySettingValueError, UndefinedSettingValueError)):
            _ = settings_empty.data_path

    def test_data_sds(self, settings_empty, settings_full):
        assert settings_full.data_sds is True
        assert settings_empty.data_sds is False

    def test_data_extended(self, settings_empty, settings_full):
        assert settings_full.data_extended is True
        assert settings_empty.data_extended is False

    # DATABASE #########################################################################################################

    def test_database_host(self, settings_empty, settings_full):
        assert settings_full.database_host == "db-server.mynetwork.lan"

        with raises((EmptySettingValueError, UndefinedSettingValueError)):
            _ = settings_empty.database_host

    def test_database_port(self, settings_empty, settings_full):
        assert settings_full.database_port == "5132"
        assert settings_empty.database_port is None

    def test_database_name(self, settings_empty, settings_full):
        assert settings_full.database_name == "db-name"

        with raises((EmptySettingValueError, UndefinedSettingValueError)):
            _ = settings_empty.database_name

    def test_database_user(self, settings_empty, settings_full):
        assert settings_full.database_user == "db-user"

        with raises((EmptySettingValueError, UndefinedSettingValueError)):
            _ = settings_empty.database_user

    def test_database_pass(self, settings_empty, settings_full):
        assert settings_full.database_pass == "db-pass"
        assert settings_empty.database_pass is None

    def test_database_driver(self, settings_empty, settings_full):
        assert settings_full.database_driver == "psycopg2"
        assert settings_empty.database_driver == "psycopg"

    def test_database_uri(self, settings_empty, settings_full):
        assert (
            settings_full.database_uri
            == "postgresql+psycopg2://db-user:db-pass@db-server.mynetwork.lan:5132/db-name"
        )

    def test_database_pool_type(self, settings_empty, settings_full):
        assert settings_full.database_pool_type == "null"
        assert settings_empty.database_pool_type == "queue"

    def test_database_pool_size_min(self, settings_empty, settings_full):
        assert settings_full.database_pool_size_min == 2
        assert settings_empty.database_pool_size_min == 5

    def test_database_pool_size_max(self, settings_empty, settings_full):
        assert settings_full.database_pool_size_max == 20
        assert settings_empty.database_pool_size_max == 10

    def test_database_pool_recycle(self, settings_empty, settings_full):
        assert settings_full.database_pool_recycle == 180
        assert settings_empty.database_pool_recycle == -1

    def test_database_pool_timeout(self, settings_empty, settings_full):
        assert settings_full.database_pool_timeout == 15
        assert settings_empty.database_pool_timeout == 30

    def test_database_pool_pre_ping(self, settings_empty, settings_full):
        assert settings_full.database_pool_pre_ping is False
        assert settings_empty.database_pool_pre_ping is True

    def test_database_pool_with_parallel(self, settings_empty, settings_full):
        assert settings_full.database_pool_with_parallel is True
        assert settings_empty.database_pool_with_parallel is False

    # EXAMPLE ##########################################################################################################

    def test_example_network(self, settings_empty, settings_full):
        assert settings_full.example_network == "RA"
        assert settings_empty.example_network is None

    def test_example_station(self, settings_empty, settings_full):
        assert settings_full.example_station == "NCAD"
        assert settings_empty.example_station is None

    def test_example_location(self, settings_empty, settings_full):
        assert settings_full.example_location == "01"
        assert settings_empty.example_location is None

    def test_example_channel(self, settings_empty, settings_full):
        assert settings_full.example_channel == "HNE"
        assert settings_empty.example_channel is None

    def test_example_start(self, settings_empty, settings_full):
        assert settings_full.example_start == "2020-02-03"
        assert settings_empty.example_start is None

    def test_example_end(self, settings_empty, settings_full):
        assert settings_full.example_end == "2021-04-05"
        assert settings_empty.example_end is None

    # FDSN OBSPY CLIENT ################################################################################################

    def test_fdsn_client(self, settings_empty, settings_full):
        assert settings_full.fdsn_client == "RESIF"

        with raises((EmptySettingValueError, UndefinedSettingValueError)):
            _ = settings_empty.fdsn_client

    def test_fdsn_client_debug(self, settings_empty, settings_full):
        assert settings_full.fdsn_client_debug is True
        assert settings_empty.fdsn_client_debug is False

    # LOCALIZATION #####################################################################################################

    def test_locale_default(self, settings_empty, settings_full):
        assert settings_full.locale_default == "fr"
        assert settings_empty.locale_default == "en"

    def test_locale_available(self, settings_empty, settings_full):
        assert settings_full.locale_available == ["en", "fr"]
        assert settings_empty.locale_available == ["en"]

    def test_locale_path(self, settings_empty, settings_full):
        assert settings_full.locale_path == "/here/locale"
        assert settings_empty.locale_path == str(
            Path(__file__)
            .parent.parent.parent.joinpath("seedpsd")
            .joinpath("locale")
            .resolve()
        )

    # LOGGING ##########################################################################################################

    def test_log_path(self, settings_empty, settings_full):
        assert settings_full.log_path == "/here/logs"
        assert settings_empty.log_path is None

    def test_log_stacktrace(self, settings_empty, settings_full):
        assert settings_full.log_stacktrace is True
        assert settings_empty.log_stacktrace is True

    def test_log_level(self, settings_empty, settings_full):
        assert settings_full.log_level == "VERBOSE"
        assert settings_empty.log_level == logging.INFO

    def test_sql_log_level(self, settings_empty, settings_full):
        assert settings_full.sql_log_level == "DEBUG"
        assert settings_empty.sql_log_level == logging.WARNING

    # METADATA #########################################################################################################

    def test_metadata_path(self, settings_empty, settings_full):
        assert settings_full.metadata_path == "/here/metadata"
        assert settings_empty.metadata_path is None

    # PLOT CUSTOMIZATION ###############################################################################################

    def test_plot_footer_dc(self, settings_empty, settings_full):
        assert settings_full.plot_footer_dc == "My DC"
        assert settings_empty.plot_footer_dc == "SeedPSD"

    def test_plot_footer_license(self, settings_empty, settings_full):
        assert settings_full.plot_footer_license == "cc-by"
        assert settings_empty.plot_footer_license is None

    def test_plot_image_format(self, settings_empty, settings_full):
        assert settings_full.plot_image_format == "jpg"
        assert settings_empty.plot_image_format == "png"

    def test_plot_image_transparent(self, settings_empty, settings_full):
        assert settings_full.plot_image_transparent is False
        assert settings_empty.plot_image_transparent is True

    def test_plot_image_dpi(self, settings_empty, settings_full):
        assert settings_full.plot_image_dpi == 200
        assert settings_empty.plot_image_dpi == 100

    def test_plot_footer_width(self, settings_empty, settings_full):
        assert settings_full.plot_image_width == 1080
        assert settings_empty.plot_image_width == 900

    def test_plot_footer_height(self, settings_empty, settings_full):
        assert settings_full.plot_image_height == 720
        assert settings_empty.plot_image_height == 600

    def test_plot_footer_font_size(self, settings_empty, settings_full):
        assert settings_full.plot_font_size == 11
        assert settings_empty.plot_font_size == 10

    # PLOT HISTOGRAM CUSTOMIZATION #####################################################################################

    def test_plot_histogram_colormap(self, settings_empty, settings_full):
        assert settings_full.plot_histogram_colormap == "pqlx"
        assert settings_empty.plot_histogram_colormap == "viridis_r"

    def test_plot_histogram_colormap_min(self, settings_empty, settings_full):
        assert settings_full.plot_histogram_colormap_min == 10
        assert settings_empty.plot_histogram_colormap_min is None

    def test_plot_histogram_colormap_max(self, settings_empty, settings_full):
        assert settings_full.plot_histogram_colormap_max == 50
        assert settings_empty.plot_histogram_colormap_max == 30

    def test_plot_histogram_grid(self, settings_empty, settings_full):
        assert settings_full.plot_histogram_grid is False
        assert settings_empty.plot_histogram_grid is True

    def test_plot_histogram_coverage(self, settings_empty, settings_full):
        assert settings_full.plot_histogram_coverage is False
        assert settings_empty.plot_histogram_coverage is True

    def test_plot_histogram_percentiles(self, settings_empty, settings_full):
        assert settings_full.plot_histogram_percentiles is True
        assert settings_empty.plot_histogram_percentiles is False

    def test_plot_histogram_noise_models(self, settings_empty, settings_full):
        assert settings_full.plot_histogram_noise_models is False
        assert settings_empty.plot_histogram_noise_models is True

    def test_plot_histogram_mode(self, settings_empty, settings_full):
        assert settings_full.plot_histogram_mode is True
        assert settings_empty.plot_histogram_mode is False

    def test_plot_histogram_mean(self, settings_empty, settings_full):
        assert settings_full.plot_histogram_mean is True
        assert settings_empty.plot_histogram_mean is False

    def test_plot_histogram_cumulative(self, settings_empty, settings_full):
        assert settings_full.plot_histogram_cumulative is True
        assert settings_empty.plot_histogram_cumulative is False

    def test_plot_histogram_x_frequency(self, settings_empty, settings_full):
        assert settings_full.plot_histogram_x_frequency is True
        assert settings_empty.plot_histogram_x_frequency is False

    def test_plot_histogram_x_min(self, settings_empty, settings_full):
        assert settings_full.plot_histogram_x_min == 0.2
        assert settings_empty.plot_histogram_x_min == 0.01

    def test_plot_histogram_x_max(self, settings_empty, settings_full):
        assert settings_full.plot_histogram_x_max == 200
        assert settings_empty.plot_histogram_x_max == 179

    def test_plot_histogram_y_min(self, settings_empty, settings_full):
        assert settings_full.plot_histogram_y_min == -150
        assert settings_empty.plot_histogram_y_min == -200

    def test_plot_histogram_y_max(self, settings_empty, settings_full):
        assert settings_full.plot_histogram_y_max == -30
        assert settings_empty.plot_histogram_y_max == -50

    # PLOT SPECTROGRAM CUSTOMIZATION ###################################################################################

    def test_plot_spectrogram_colormap(self, settings_empty, settings_full):
        assert settings_full.plot_spectrogram_colormap == "plasma"
        assert settings_empty.plot_spectrogram_colormap == "jet"

    def test_plot_spectrogram_colormap_min(self, settings_empty, settings_full):
        assert settings_full.plot_spectrogram_colormap_min == 25
        assert settings_empty.plot_spectrogram_colormap_min is None

    def test_plot_spectrogram_colormap_max(self, settings_empty, settings_full):
        assert settings_full.plot_spectrogram_colormap_max == 60
        assert settings_empty.plot_spectrogram_colormap_max is None

    def test_plot_spectrogram_grid(self, settings_empty, settings_full):
        assert settings_full.plot_spectrogram_grid is False
        assert settings_empty.plot_spectrogram_grid is True

    def test_plot_spectrogram_y_invert(self, settings_empty, settings_full):
        assert settings_full.plot_spectrogram_y_invert is False
        assert settings_empty.plot_spectrogram_y_invert is True

    # PPSD CUSTOMIZATION ###############################################################################################

    def test_ppsd_use_defaults(self, settings_empty, settings_full):
        assert settings_full.ppsd_use_defaults is False
        assert settings_empty.ppsd_use_defaults is True

    def test_ppsd_only_allowed_channels(self, settings_empty, settings_full):
        assert settings_full.ppsd_only_allowed_channels is True
        assert settings_empty.ppsd_only_allowed_channels is True

    def test_ppsd_allowed_channel_bands(self, settings_empty, settings_full):
        assert settings_full.ppsd_allowed_channel_bands == "ABC"
        assert settings_empty.ppsd_allowed_channel_bands == "BCDEFGHLMPQRSTUV"

    def test_ppsd_allowed_channel_instruments(self, settings_empty, settings_full):
        assert settings_full.ppsd_allowed_channel_instruments == "DEF"
        assert settings_empty.ppsd_allowed_channel_instruments == "DGHJLMNP"

    def test_ppsd_allowed_channel_orientations(self, settings_empty, settings_full):
        assert settings_full.ppsd_allowed_channel_orientations == "GHI123"
        assert settings_empty.ppsd_allowed_channel_orientations == "ABCDEFHINORTUVWZ123"

    def test_ppsd_is_allowed_channel(self, settings_empty, settings_full):
        assert settings_full.ppsd_is_allowed_channel("ADG") is True
        assert settings_full.ppsd_is_allowed_channel("XXX") is False
        assert settings_empty.ppsd_is_allowed_channel("HHZ") is True
        assert settings_empty.ppsd_is_allowed_channel("XXX") is False

    # SENTRY ###########################################################################################################

    def test_sentry_dsn(self, settings_empty, settings_full):
        assert settings_full.sentry_dsn == "sentry-dsn"
        assert settings_empty.sentry_dsn is None

    def test_sentry_tracing(self, settings_empty, settings_full):
        assert settings_full.sentry_tracing is True
        assert settings_empty.sentry_tracing is False

    def test_sentry_environment(self, settings_empty, settings_full):
        assert settings_full.sentry_environment == "dev"
        assert settings_empty.sentry_environment is None

    def test_sentry_sample_rate(self, settings_empty, settings_full):
        assert settings_full.sentry_sample_rate == "1.0"
        assert settings_full.sentry_traces_sample_rate == "0.2"
        assert settings_full.sentry_profiles_sample_rate == "0.4"
        assert settings_empty.sentry_sample_rate is None
        assert settings_empty.sentry_traces_sample_rate is None
        assert settings_empty.sentry_profiles_sample_rate is None

    # SKIN CUSTOMIZATION ###############################################################################################

    def test_skin_name(self, settings_empty, settings_full):
        assert settings_full.skin_name == "my-skin"
        assert settings_empty.skin_name is None

    def test_skin_path(self, settings_empty, settings_full):
        assert settings_full.skin_path == "/here/skin"
        assert settings_empty.skin_path is None
