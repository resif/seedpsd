# ruff: noqa: N801
import os

from seedpsd.models.files.factory import MSEEDFile, MSEEDFileFactory


class Test_MSEEDFileFactory_FromPath:
    """
    Tests of the method "MSEEDFileFactory.from_path()"
    """

    def test_failure(self):
        generated_file = MSEEDFileFactory.from_path("a_wrong_filename")
        assert generated_file is None

    def test_existing_file_path(self):
        with open("/tmp/FR.CIEL.00.HHZ.D.2023.001", "w"):
            expected_file = MSEEDFile(
                1,
                2023,
                "FR",
                "CIEL",
                "00",
                "HHZ",
                parent_path="/tmp",
                file_name="FR.CIEL.00.HHZ.D.2023.001",
            )
            generated_file = MSEEDFileFactory.from_path(
                "/tmp/FR.CIEL.00.HHZ.D.2023.001"
            )

        os.remove("/tmp/FR.CIEL.00.HHZ.D.2023.001")

        assert generated_file._id == expected_file._id
        assert str(generated_file._file_name) == expected_file._file_name
        assert str(generated_file._parent_path) == expected_file._parent_path

    def test_existing_parent_path(self):
        with open("/tmp/FR.CIEL.00.HHZ.D.2023.001", "w"):
            expected_file = MSEEDFile(
                1,
                2023,
                "FR",
                "CIEL",
                "00",
                "HHZ",
                parent_path="/tmp",
                file_name="FR.CIEL.00.HHZ.D.2023.001",
            )
            generated_file = MSEEDFileFactory.from_path(
                "FR.CIEL.00.HHZ.D.2023.001", parent_path="/tmp"
            )

        os.remove("/tmp/FR.CIEL.00.HHZ.D.2023.001")

        assert generated_file._id == expected_file._id
        assert str(generated_file._file_name) == expected_file._file_name
        assert str(generated_file._parent_path) == expected_file._parent_path

    def test_generated_parent_path_with_sds(self):
        os.makedirs("/tmp/2023/FR/CIEL/HHZ.D/", exist_ok=True)
        with open("/tmp/2023/FR/CIEL/HHZ.D/FR.CIEL.00.HHZ.D.2023.001", "w"):
            expected_file = MSEEDFile(
                1,
                2023,
                "FR",
                "CIEL",
                "00",
                "HHZ",
                parent_path="/tmp/2023/FR/CIEL/HHZ.D",
                file_name="FR.CIEL.00.HHZ.D.2023.001",
            )
            generated_file = MSEEDFileFactory.from_path(
                "FR.CIEL.00.HHZ.D.2023.001", parent_path="/tmp", sds=True
            )

        os.remove("/tmp/2023/FR/CIEL/HHZ.D/FR.CIEL.00.HHZ.D.2023.001")

        assert generated_file._id == expected_file._id
        assert str(generated_file._file_name) == expected_file._file_name
        assert str(generated_file._parent_path) == expected_file._parent_path

    def test_generated_parent_path_without_sds(self):
        os.makedirs("/tmp/FR/2023/CIEL/HHZ.D/", exist_ok=True)
        with open("/tmp/FR/2023/CIEL/HHZ.D/FR.CIEL.00.HHZ.D.2023.001", "w"):
            expected_file = MSEEDFile(
                1,
                2023,
                "FR",
                "CIEL",
                "00",
                "HHZ",
                parent_path="/tmp/FR/2023/CIEL/HHZ.D",
                file_name="FR.CIEL.00.HHZ.D.2023.001",
            )
            generated_file = MSEEDFileFactory.from_path(
                "FR.CIEL.00.HHZ.D.2023.001", parent_path="/tmp", sds=False
            )

        os.remove("/tmp/FR/2023/CIEL/HHZ.D/FR.CIEL.00.HHZ.D.2023.001")

        assert generated_file._id == expected_file._id
        assert str(generated_file._file_name) == expected_file._file_name
        assert str(generated_file._parent_path) == expected_file._parent_path

    def test_generated_parent_path_with_extended(self):
        os.makedirs("/tmp/XP2023/2024/FR01A/HHZ.D/", exist_ok=True)
        with open("/tmp/XP2023/2024/FR01A/HHZ.D/XP.FR01A.00.HHZ.D.2024.001", "w"):
            expected_file = MSEEDFile(
                1,
                2024,
                "XP",
                "FR01A",
                "00",
                "HHZ",
                parent_path="/tmp/XP2023/2024/FR01A/HHZ.D",
                file_name="XP.FR01A.00.HHZ.D.2024.001",
            )
            generated_file = MSEEDFileFactory.from_path(
                "XP.FR01A.00.HHZ.D.2024.001",
                parent_path="/tmp",
                sds=False,
                extended=True,
            )

        os.remove("/tmp/XP2023/2024/FR01A/HHZ.D/XP.FR01A.00.HHZ.D.2024.001")

        assert generated_file._id == expected_file._id
        assert str(generated_file._file_name) == expected_file._file_name
        assert str(generated_file._parent_path) == expected_file._parent_path

    def test_generated_parent_path_without_extended(self):
        os.makedirs("/tmp/XP/2024/FR01A/HHZ.D/", exist_ok=True)
        with open("/tmp/XP/2024/FR01A/HHZ.D/XP.FR01A.00.HHZ.D.2024.001", "w"):
            expected_file = MSEEDFile(
                1,
                2024,
                "XP",
                "FR01A",
                "00",
                "HHZ",
                parent_path="/tmp/XP/2024/FR01A/HHZ.D",
                file_name="XP.FR01A.00.HHZ.D.2024.001",
            )
            generated_file = MSEEDFileFactory.from_path(
                "XP.FR01A.00.HHZ.D.2024.001",
                parent_path="/tmp",
                sds=False,
                extended=False,
            )

        os.remove("/tmp/XP/2024/FR01A/HHZ.D/XP.FR01A.00.HHZ.D.2024.001")

        assert generated_file._id == expected_file._id
        assert str(generated_file._file_name) == expected_file._file_name
        assert str(generated_file._parent_path) == expected_file._parent_path
