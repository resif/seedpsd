# ruff: noqa: N801,N802,N803
from pytest import fixture

from seedpsd.models.db.shared.file import File
from seedpsd.models.db.shared.network import Network
from seedpsd.models.db.shared.source import Source
from seedpsd.models.files.factory import MSEEDFile, MSEEDFileFactory


class Test_MSEEDFileFactory_FromDatabase:
    """
    Tests of the method "MSEEDFileFactory.from_database()"
    """

    # TEST PERMANENT NETWORK ###########################################################################################

    @fixture(scope="class")
    def network_FR(self):
        return Network("FR", "1962-01-01T00:00:00", "2500-12-31T23:59:59")

    @fixture(scope="class")
    def source_FR_CIEL_00_HHZ(self, network_FR):
        return Source(network_FR, "CIEL", "00", "HHZ")

    @fixture(scope="class")
    def file_FR_CIEL_00_HHZ_2023_001(self, source_FR_CIEL_00_HHZ):
        file = File(name="FR.CIEL.00.HHZ.D.2023.001", year=2023, day=1)
        file.source = source_FR_CIEL_00_HHZ
        return file

    def test_without_source(self):
        assert (
            MSEEDFileFactory.from_database(
                File(name="FR.CIEL.00.HHZ.D.2023.001", year=2023, day=1), "/parent"
            )
            is None
        )

    def test_with_sds(self, file_FR_CIEL_00_HHZ_2023_001):
        expected_file = MSEEDFile(
            1,
            2023,
            "FR",
            "CIEL",
            "00",
            "HHZ",
            parent_path="/tmp/2023/FR/CIEL/HHZ.D",
            file_name="FR.CIEL.00.HHZ.D.2023.001",
        )
        generated_file = MSEEDFileFactory.from_database(
            file_FR_CIEL_00_HHZ_2023_001, parent_path="/tmp", sds=True
        )

        assert generated_file._id == expected_file._id
        assert str(generated_file._file_name) == expected_file._file_name
        assert str(generated_file._parent_path) == expected_file._parent_path

    def test_without_sds(self, file_FR_CIEL_00_HHZ_2023_001):
        expected_file = MSEEDFile(
            1,
            2023,
            "FR",
            "CIEL",
            "00",
            "HHZ",
            parent_path="/tmp/FR/2023/CIEL/HHZ.D",
            file_name="FR.CIEL.00.HHZ.D.2023.001",
        )
        generated_file = MSEEDFileFactory.from_database(
            file_FR_CIEL_00_HHZ_2023_001, parent_path="/tmp", sds=False
        )

        assert generated_file._id == expected_file._id
        assert str(generated_file._file_name) == expected_file._file_name
        assert str(generated_file._parent_path) == expected_file._parent_path

    # TEST TEMPORARY NETWORK ###########################################################################################

    @fixture(scope="class")
    def network_Z4(self):
        return Network("Z4", "2023-01-01T00:00:00", "2023-12-31T00:00:00")

    @fixture(scope="class")
    def source_Z4_PSIS1_00_HHZ(self, network_Z4):
        return Source(network_Z4, "PSIS1", "00", "HHZ")

    @fixture(scope="class")
    def file_Z4_PSIS1_00_HHZ_2023_300(self, source_Z4_PSIS1_00_HHZ):
        file = File(name="Z4.PSIS1.00.HHZ.D.2023.300", year=2023, day=300)
        file.source = source_Z4_PSIS1_00_HHZ
        return file

    def test_with_extended(self, file_Z4_PSIS1_00_HHZ_2023_300):
        expected_file = MSEEDFile(
            300,
            2023,
            "Z4",
            "PSIS1",
            "00",
            "HHZ",
            parent_path="/tmp/Z42023/2023/PSIS1/HHZ.D",
            file_name="Z4.PSIS1.00.HHZ.D.2023.300",
        )
        generated_file = MSEEDFileFactory.from_database(
            file_Z4_PSIS1_00_HHZ_2023_300, parent_path="/tmp", sds=False, extended=True
        )

        assert generated_file._id == expected_file._id
        assert str(generated_file._file_name) == expected_file._file_name
        assert str(generated_file._parent_path) == expected_file._parent_path

    def test_without_extended(self, file_Z4_PSIS1_00_HHZ_2023_300):
        expected_file = MSEEDFile(
            300,
            2023,
            "Z4",
            "PSIS1",
            "00",
            "HHZ",
            parent_path="/tmp/Z4/2023/PSIS1/HHZ.D",
            file_name="Z4.PSIS1.00.HHZ.D.2023.300",
        )
        generated_file = MSEEDFileFactory.from_database(
            file_Z4_PSIS1_00_HHZ_2023_300, parent_path="/tmp", sds=False, extended=False
        )

        assert generated_file._id == expected_file._id
        assert str(generated_file._file_name) == expected_file._file_name
        assert str(generated_file._parent_path) == expected_file._parent_path
