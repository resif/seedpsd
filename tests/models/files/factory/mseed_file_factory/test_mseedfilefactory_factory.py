# ruff: noqa: N801
from seedpsd.models.files.factory import MSEEDFile, MSEEDFileFactory


class Test_MSEEDFileFactory_Factory:
    """
    Tests of the method "MSEEDFileFactory.factory()"
    """

    def test_simple(self):
        params = {
            "day": 1,
            "year": 2023,
            "network": "FR",
            "station": "CIEL",
            "location": "00",
            "channel": "HHZ",
        }

        expected_file = MSEEDFile(
            params.get("day"),
            params.get("year"),
            params.get("network"),
            params.get("station"),
            params.get("location"),
            params.get("channel"),
        )
        generated_file = MSEEDFileFactory.factory(
            params.get("day"),
            params.get("year"),
            params.get("network"),
            params.get("station"),
            params.get("location"),
            params.get("channel"),
        )

        assert generated_file.__dict__ == expected_file.__dict__

    def test_with_quality(self):
        params = {
            "day": 1,
            "year": 2023,
            "network": "FR",
            "station": "CIEL",
            "location": "00",
            "channel": "HHZ",
            "quality": "R",
        }

        expected_file = MSEEDFile(
            params.get("day"),
            params.get("year"),
            params.get("network"),
            params.get("station"),
            params.get("location"),
            params.get("channel"),
            quality=params.get("quality"),
        )
        generated_file = MSEEDFileFactory.factory(
            params.get("day"),
            params.get("year"),
            params.get("network"),
            params.get("station"),
            params.get("location"),
            params.get("channel"),
            quality=params.get("quality"),
        )

        assert generated_file.__dict__ == expected_file.__dict__

    def test_with_filename(self):
        params = {
            "day": 1,
            "year": 2023,
            "network": "FR",
            "station": "CIEL",
            "location": "00",
            "channel": "HHZ",
            "filename": "a_good_filename",
        }

        expected_file = MSEEDFile(
            params.get("day"),
            params.get("year"),
            params.get("network"),
            params.get("station"),
            params.get("location"),
            params.get("channel"),
            file_name=params.get("filename"),
        )
        generated_file = MSEEDFileFactory.factory(
            params.get("day"),
            params.get("year"),
            params.get("network"),
            params.get("station"),
            params.get("location"),
            params.get("channel"),
            file_name=params.get("filename"),
        )

        assert generated_file.__dict__ == expected_file.__dict__

    def test_with_parent_path(self):
        params = {
            "day": 1,
            "year": 2023,
            "network": "FR",
            "station": "CIEL",
            "location": "00",
            "channel": "HHZ",
            "parent_path": "/parent",
        }

        expected_file = MSEEDFile(
            params.get("day"),
            params.get("year"),
            params.get("network"),
            params.get("station"),
            params.get("location"),
            params.get("channel"),
            parent_path=params.get("parent_path"),
        )
        generated_file = MSEEDFileFactory.factory(
            params.get("day"),
            params.get("year"),
            params.get("network"),
            params.get("station"),
            params.get("location"),
            params.get("channel"),
            parent_path=params.get("parent_path"),
        )

        assert generated_file.__dict__ == expected_file.__dict__
