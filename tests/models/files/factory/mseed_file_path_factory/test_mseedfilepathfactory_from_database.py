# ruff: noqa: N801,N802,N803
from pytest import fixture

from seedpsd.models.db.shared.file import File
from seedpsd.models.db.shared.network import Network
from seedpsd.models.db.shared.source import Source
from seedpsd.models.files.factory import MSEEDFilePathFactory


class Test_MSEEDFilePathFactory_FromDatabase:
    """
    Tests of the method "MSEEDFilePathFactory.from_database()"
    """

    # TEST FR ##########################################################################################################

    @fixture(scope="class")
    def network_FR(self):
        return Network("FR", "1962-01-01T00:00:00", "2500-12-31T23:59:59")

    @fixture(scope="class")
    def source_FR_CIEL_00_HHZ(self, network_FR):
        return Source(network_FR, "CIEL", "00", "HHZ")

    @fixture(scope="class")
    def file_FR_CIEL_00_HHZ_2023_001(self, source_FR_CIEL_00_HHZ):
        file = File(name="FR.CIEL.00.HHZ.D.2023.001", year=2023, day=1)
        file.source = source_FR_CIEL_00_HHZ
        return file

    def test_FR_with_sds(self, file_FR_CIEL_00_HHZ_2023_001):
        params = {
            "parent_path": "/parent",
            "file": file_FR_CIEL_00_HHZ_2023_001,
            "sds": True,
            "extended": False,
        }
        generated_path = MSEEDFilePathFactory.from_database(
            params.get("parent_path"),
            params.get("file"),
            params.get("sds"),
            params.get("extended"),
        )
        expected_path = f"{params.get('parent_path')}/2023/FR/CIEL/HHZ.D"

        assert generated_path == expected_path

    def test_FR_without_sds(self, file_FR_CIEL_00_HHZ_2023_001):
        params = {
            "parent_path": "/parent",
            "file": file_FR_CIEL_00_HHZ_2023_001,
            "sds": False,
            "extended": False,
        }
        generated_path = MSEEDFilePathFactory.from_database(
            params.get("parent_path"),
            params.get("file"),
            params.get("sds"),
            params.get("extended"),
        )
        expected_path = f"{params.get('parent_path')}/FR/2023/CIEL/HHZ.D"

        assert generated_path == expected_path

    # TEST Z4 ##########################################################################################################

    @fixture(scope="class")
    def network_Z4(self):
        return Network("Z4", "2023-01-01T00:00:00", "2023-12-31T00:00:00")

    @fixture(scope="class")
    def source_Z4_PSIS1_00_HHZ(self, network_Z4):
        return Source(network_Z4, "PSIS1", "00", "HHZ")

    @fixture(scope="class")
    def file_Z4_PSIS1_00_HHZ_2023_300(self, source_Z4_PSIS1_00_HHZ):
        file = File(name="Z4.PSIS1.00.HHZ.D.2023.300", year=2023, day=300)
        file.source = source_Z4_PSIS1_00_HHZ
        return file

    def test_Z4_with_extended(self, file_Z4_PSIS1_00_HHZ_2023_300):
        params = {
            "parent_path": "/parent",
            "file": file_Z4_PSIS1_00_HHZ_2023_300,
            "sds": False,
            "extended": True,
        }
        generated_path = MSEEDFilePathFactory.from_database(
            params.get("parent_path"),
            params.get("file"),
            params.get("sds"),
            params.get("extended"),
        )
        expected_path = f"{params.get('parent_path')}/Z42023/2023/PSIS1/HHZ.D"

        assert generated_path == expected_path

    def test_Z4_without_extended(self, file_Z4_PSIS1_00_HHZ_2023_300):
        params = {
            "parent_path": "/parent",
            "file": file_Z4_PSIS1_00_HHZ_2023_300,
            "sds": False,
            "extended": False,
        }
        generated_path = MSEEDFilePathFactory.from_database(
            params.get("parent_path"),
            params.get("file"),
            params.get("sds"),
            params.get("extended"),
        )
        expected_path = f"{params.get('parent_path')}/Z4/2023/PSIS1/HHZ.D"

        assert generated_path == expected_path
