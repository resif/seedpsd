# ruff: noqa: N801
from seedpsd.models.files.factory import MSEEDFilePathFactory


class Test_MSEEDFilePathFactory_FromId:
    """
    Tests of the method "MSEEDFilePathFactory.from_id()"
    """

    # PERMANENT NETWORK ################################################################################################

    def test_with_sds(self):
        params = {
            "parent_path": "/parent",
            "mseed_id": "FR.CIEL.00.HHZ.D.2023.001",
            "raise_error": False,
            "sds": True,
        }
        generated_path = MSEEDFilePathFactory.from_id(
            params.get("parent_path"),
            params.get("mseed_id"),
            params.get("raise_error"),
            params.get("sds"),
        )
        expected_path = f"{params.get('parent_path')}/2023/FR/CIEL/HHZ.D"

        assert generated_path == expected_path

    def test_without_sds(self):
        params = {
            "parent_path": "/parent",
            "mseed_id": "FR.CIEL.00.HHZ.D.2023.001",
            "raise_error": False,
            "sds": False,
        }
        generated_path = MSEEDFilePathFactory.from_id(
            params.get("parent_path"),
            params.get("mseed_id"),
            params.get("raise_error"),
            params.get("sds"),
        )
        expected_path = f"{params.get('parent_path')}/FR/2023/CIEL/HHZ.D"

        assert generated_path == expected_path

    # TEMPORARY NETWORK ################################################################################################

    def test_with_extended(self):
        params = {
            "parent_path": "/parent",
            "mseed_id": "XP.FR01A.00.HHZ.D.2024.001",
            "raise_error": False,
            "sds": False,
            "extended": True,
        }
        generated_path = MSEEDFilePathFactory.from_id(
            params.get("parent_path"),
            params.get("mseed_id"),
            params.get("raise_error"),
            params.get("sds"),
            params.get("extended"),
        )
        expected_path = f"{params.get('parent_path')}/XP2023/2024/FR01A/HHZ.D"

        assert generated_path == expected_path

    def test_without_extended(self):
        params = {
            "parent_path": "/parent",
            "mseed_id": "XP.FR01A.00.HHZ.D.2024.001",
            "raise_error": False,
            "sds": False,
            "extended": False,
        }
        generated_path = MSEEDFilePathFactory.from_id(
            params.get("parent_path"),
            params.get("mseed_id"),
            params.get("raise_error"),
            params.get("sds"),
            params.get("extended"),
        )
        expected_path = f"{params.get('parent_path')}/XP/2024/FR01A/HHZ.D"

        assert generated_path == expected_path
