# ruff: noqa: N801
from seedpsd.models.files.factory import MSEEDFilePathFactory


class Test_MSEEDFilePathFactory_Factory:
    """
    Tests of the method "MSEEDFilePathFactory.factory()"
    """

    def _run_factory(self, params, path):
        generated_path = MSEEDFilePathFactory.factory(
            params.get("parent_path"),
            params.get("year"),
            params.get("network"),
            params.get("station"),
            params.get("channel"),
            params.get("sds"),
        )
        assert generated_path == path

    # TEST SDS #########################################################################################################

    def test_with_sds(self):
        params = {
            "parent_path": "/parent",
            "year": 2023,
            "network": "FR",
            "sds": True,
        }
        path = f"{params['parent_path']}/{params['year']}/{params['network']}"
        self._run_factory(params, path)

    def test_without_sds(self):
        params = {
            "parent_path": "/parent",
            "year": 2023,
            "network": "FR",
            "sds": False,
        }
        path = f"{params['parent_path']}/{params['network']}/{params['year']}"
        self._run_factory(params, path)

    # TEST STATION #####################################################################################################

    def test_station(self):
        params = {
            "parent_path": "/parent",
            "year": 2023,
            "network": "FR",
            "station": "CIEL",
            "sds": False,
        }
        path = f"{params['parent_path']}/{params['network']}/{params['year']}/{params['station']}"
        self._run_factory(params, path)

    def test_channel(self):
        params = {
            "parent_path": "/parent",
            "year": 2023,
            "network": "FR",
            "station": "CIEL",
            "channel": "HHZ",
            "sds": False,
        }
        path = f"{params['parent_path']}/{params['network']}/{params['year']}/{params['station']}/{params['channel']}.D"
        self._run_factory(params, path)
