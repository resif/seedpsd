from pytest import fixture

from seedpsd.models.files.npz import NPZFile


class TestNpzFile:
    """
    Tests of the class "MSEEDFile"
    """

    @fixture
    def npz_file(self):
        return NPZFile(
            2023,
            "FR",
            "CIEL",
            "00",
            "HHZ",
            file_name="FR.CIEL.00.HHZ.D.2023.npz",
            parent_path="/parent",
        )

    def test_id(self, npz_file):
        assert npz_file._id == "FR.CIEL.00.HHZ.D.2023"

    def test_generated_file_name(self, npz_file):
        assert npz_file._generated_file_name == "FR.CIEL.00.HHZ.D.2023.npz"

    def test_file_name(self, npz_file):
        assert npz_file.file_name == "FR.CIEL.00.HHZ.D.2023.npz"

    def test_file_path(self, npz_file):
        assert npz_file.file_path == "/parent/FR.CIEL.00.HHZ.D.2023.npz"

    def test_exists(self, npz_file):
        assert npz_file.exists is False

    def test_is_readable(self, npz_file):
        assert npz_file.is_readable is False

    def test_is_writeable(self, npz_file):
        assert npz_file.is_writeable is False

    def test_is_temporary_network(self, npz_file):
        assert npz_file.is_temporary_network is False
