import datetime

from pytest import fixture

from seedpsd.models.files.mseed import MSEEDFile


class TestMseedFile:
    """
    Tests of the class "MSEEDFile"
    """

    @fixture
    def mseed_file(self):
        return MSEEDFile(
            1,
            2023,
            "FR",
            "CIEL",
            "00",
            "HHZ",
            file_name="FR.CIEL.00.HHZ.D.2023.001",
            parent_path="/parent",
        )

    def test_day(self, mseed_file):
        assert mseed_file.day == "001"

    def test_id(self, mseed_file):
        assert mseed_file._id == "FR.CIEL.00.HHZ.D.2023.001"

    def test_generated_file_name(self, mseed_file):
        assert mseed_file._generated_file_name == "FR.CIEL.00.HHZ.D.2023.001"

    def test_date(self, mseed_file):
        assert mseed_file.date == datetime.date(2023, 1, 1)

    def test_file_name(self, mseed_file):
        assert mseed_file.file_name == "FR.CIEL.00.HHZ.D.2023.001"

    def test_file_path(self, mseed_file):
        assert mseed_file.file_path == "/parent/FR.CIEL.00.HHZ.D.2023.001"

    def test_exists(self, mseed_file):
        assert mseed_file.exists is False

    def test_is_readable(self, mseed_file):
        assert mseed_file.is_readable is False

    def test_is_writeable(self, mseed_file):
        assert mseed_file.is_writeable is False

    def test_is_temporary_network(self, mseed_file):
        assert mseed_file.is_temporary_network is False
