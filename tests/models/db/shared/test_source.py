from pytest import fixture

from seedpsd.models.db.shared.network import Network
from seedpsd.models.db.shared.source import Source


class TestDbSharedSource:
    """
    Tests of the class "Source"
    """

    @fixture
    def network1(self):
        return Network("FR", start="1962-01-01", end="2500-12-31")

    @fixture
    def network2(self):
        return Network("G", start="1982-01-01", end="2500-12-31")

    @fixture
    def source1(self, network1):
        return Source(network1, "CIEL", "00", "HHZ")

    @fixture
    def source2(self, network2):
        return Source(network2, "TAM", "", "MHZ")

    def test_network(self, source1, source2, network1, network2):
        assert source1.network == network1
        assert source2.network == network2

    def test_station(self, source1, source2):
        assert source1.station == "CIEL"
        assert source2.station == "TAM"

    def test_location(self, source1, source2):
        assert source1.location == "00"
        assert source2.location == ""

    def test_channel(self, source1, source2):
        assert source1.channel == "HHZ"
        assert source2.channel == "MHZ"
