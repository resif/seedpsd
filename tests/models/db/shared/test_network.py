import datetime

from pytest import fixture

from seedpsd.models.db.shared.network import Network


class TestDbSharedNetwork:
    """
    Tests of the class "Network"
    """

    @fixture
    def network1(self):
        return Network("FR", start="1962-01-01", end="2500-12-31")

    @fixture
    def network2(self):
        return Network("Z3", start="2015-01-01", end="2022-12-31")

    def test_code(self, network1, network2):
        assert network1.code == "FR"
        assert network2.code == "Z3"

    def test_extended_code(self, network1, network2):
        assert network1.extended_code == "FR"
        assert network2.extended_code == "Z32015"

    def test_start(self, network1, network2):
        assert network1.start == datetime.date(1962, 1, 1)
        assert network2.start == datetime.date(2015, 1, 1)

    def test_end(self, network1, network2):
        assert network1.end is None
        assert network2.end == datetime.date(2022, 12, 31)

    def test_year(self, network1, network2):
        assert network1.year == 1962
        assert network2.year == 2015

    def test_temporary(self, network1, network2):
        assert network1.temporary is False
        assert network2.temporary is True

    def test_schema(self, network1, network2):
        assert network1.schema == "network_FR"
        assert network2.schema == "network_Z32015"
