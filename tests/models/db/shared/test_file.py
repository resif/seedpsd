import datetime

from pytest import fixture

from seedpsd.models.db.shared.file import File


class TestDbSharedFile:
    """
    Tests of the class "File"
    """

    @fixture
    def file1(self):
        return File(
            "FR.CIEL.00.HHE.D.2021.010",
            date="2021-01-10",
            processed=True,
            available_metadata=True,
        )

    @fixture
    def file2(self):
        return File(
            "XP.FR04A.00.HHZ.D.2023.060",
            year="2023",
            day="060",
            processed=False,
            available_metadata=False,
        )

    def test_name(self, file1, file2):
        assert file1.name == "FR.CIEL.00.HHE.D.2021.010"
        assert file2.name == "XP.FR04A.00.HHZ.D.2023.060"

    def test_date(self, file1, file2):
        assert file1.date == datetime.date(2021, 1, 10)
        assert file1.year == 2021
        assert file1.day == 10

        assert file2.date == datetime.date(2023, 3, 1)
        assert file2.year == 2023
        assert file2.day == 60

    def test_invalidate(self, file1):
        assert file1.processed is True
        file1.invalidate()
        assert file1.processed is False
