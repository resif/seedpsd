# ruff: noqa: DTZ001
import datetime

from pytest import fixture

from seedpsd.models.db.tenant.stream import Stream


class TestDbTenantStream:
    """
    Tests of the class "Stream"
    """

    @fixture
    def stream1(self):
        return Stream("2020-01-01T00:00:00", "2020-12-31T23:59:59", 100)

    def test_start(self, stream1):
        assert stream1.start == datetime.datetime(2020, 1, 1, 0, 0, 0)

    def test_end(self, stream1):
        assert stream1.end == datetime.datetime(2020, 12, 31, 23, 59, 59)

    def test_sampling_rate(self, stream1):
        assert stream1.sampling_rate == 100
