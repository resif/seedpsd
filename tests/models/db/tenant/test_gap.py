# ruff: noqa: DTZ001
import datetime

from pytest import fixture

from seedpsd.models.db.tenant.gap import Gap


class TestDbTenantGap:
    """
    Tests of the class "Gap"
    """

    @fixture
    def gap1(self):
        return Gap("2020-01-01T11:22:33", "2020-01-02T22:33:44")

    def test_start(self, gap1):
        assert gap1.start == datetime.datetime(2020, 1, 1, 11, 22, 33)

    def test_end(self, gap1):
        assert gap1.end == datetime.datetime(2020, 1, 2, 22, 33, 44)
