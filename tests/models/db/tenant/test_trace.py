# ruff: noqa: DTZ001
import datetime

from pytest import fixture

from seedpsd.models.db.tenant.trace import Trace


class TestDbTenantTrace:
    """
    Tests of the class "Trace"
    """

    @fixture
    def trace1(self):
        return Trace("2020-01-01T11:22:33")

    def test_timestamp(self, trace1):
        assert trace1.timestamp == datetime.datetime(2020, 1, 1, 11, 22, 33)

    def test_data(self, trace1):
        trace1.data = [0.0, 1.1, 2.2, 3.3]
        assert trace1.data == [0.0, 1.1, 2.2, 3.3]
