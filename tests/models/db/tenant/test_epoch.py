# ruff: noqa: DTZ001
import datetime

from pytest import fixture

from seedpsd.models.db.shared.network import Network
from seedpsd.models.db.shared.source import Source
from seedpsd.models.db.tenant.epoch import Epoch


class TestDbTenantEpoch:
    """
    Tests of the class "Epoch"
    """

    @fixture
    def network1(self):
        return Network("FR", start="1962-01-01", end="2500-12-31")

    @fixture
    def source1(self, network1):
        return Source(network1, "CIEL", "00", "HHZ")

    @fixture
    def epoch1(self):
        return Epoch("2020-01-01T00:00:00", "2020-12-31T23:59:59", "INSTRUMENT1")

    @fixture
    def epoch2(self):
        return Epoch("2020-01-01T00:00:00", "2500-12-31T23:59:59", "INSTRUMENT2")

    def test_source(self, epoch1, source1):
        epoch1.source = source1
        assert epoch1.source == source1

    def test_start(self, epoch1):
        assert epoch1.start == datetime.datetime(2020, 1, 1, 0, 0, 0)

    def test_end(self, epoch1, epoch2):
        assert epoch1.end == datetime.datetime(2020, 12, 31, 23, 59, 59)
        assert epoch2.end is None

    def test_instrument(self, epoch1):
        assert epoch1.instrument == "INSTRUMENT1"

    def test_update_instrument(self, epoch1, epoch2):
        changed1 = epoch1.update(instrument="NEW-INSTRUMENT")
        assert changed1 is True
        assert epoch1.instrument == "NEW-INSTRUMENT"

        changed2 = epoch2.update(instrument="INSTRUMENT2")
        assert changed2 is False
        assert epoch2.instrument == "INSTRUMENT2"

    def test_update_end(self, epoch1, epoch2):
        changed1a = epoch1.update(end="2020-12-31T23:59:59")
        assert changed1a is False
        assert epoch1.end == datetime.datetime(2020, 12, 31, 23, 59, 59)

        changed1b = epoch1.update(end="2025-12-31T23:59:59")
        assert changed1b is True
        assert epoch1.end == datetime.datetime(2025, 12, 31, 23, 59, 59)

        changed2a = epoch2.update(end="2500-12-31T23:59:59")
        assert changed2a is False
        assert epoch2.end is None

        changed2b = epoch2.update(end="2025-12-31T23:59:59")
        assert changed2b is True
        assert epoch2.end == datetime.datetime(2025, 12, 31, 23, 59, 59)

    def test_update_end_2500(self, epoch1, epoch2):
        changed1 = epoch1.update(end="2500-12-31T23:59:59")
        assert changed1 is True
        assert epoch1.end is None

        changed2 = epoch2.update(end="2500-12-31T23:59:59")
        assert changed2 is False
        assert epoch2.end is None
