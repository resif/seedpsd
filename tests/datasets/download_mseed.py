import os
import os.path
import sys
from datetime import timedelta

import yaml
from obspy import UTCDateTime
from obspy.clients.fdsn import Client


def load_yml(file_path):
    with open(file_path) as f:
        return yaml.safe_load(f)


def daterange(start_date, end_date):
    for n in range(int((end_date.datetime - start_date.datetime).days)):
        yield start_date + timedelta(n)


def download_mseed(client_key, datasets):
    # Initialize client
    client = Client(client_key)

    # Initialize output directory
    output_dir = os.path.join(
        os.path.dirname(os.path.abspath(__file__)), "mseed.local", client_key
    )
    os.makedirs(output_dir, exist_ok=True)

    # For each data source
    for dataset in datasets:
        # Initialize dataset output directory
        dataset_dir = os.path.join(
            output_dir, dataset["network"], dataset["station"], dataset["channel"]
        )
        os.makedirs(dataset_dir, exist_ok=True)

        # For each day
        for day in daterange(
            UTCDateTime(dataset["start"]), UTCDateTime(dataset["end"])
        ):
            # Build file name
            filename = f"{dataset['network']}.{dataset['station']}.{dataset['location'] or ''}.{dataset['channel']}.D.{day.year}.{str(day.julday).zfill(3)}"

            # Download 1 day of data
            data = client.get_waveforms(
                network=dataset["network"],
                station=dataset["station"],
                location=dataset["location"],
                channel=dataset["channel"],
                starttime=day,
                endtime=day + timedelta(days=1),
            )

            # Write data to file
            data.write(os.path.join(dataset_dir, filename), format="MSEED")


if __name__ == "__main__":
    yml_config = load_yml(sys.argv[1])

    for client_key, datasets in yml_config.items():
        download_mseed(client_key, datasets)
