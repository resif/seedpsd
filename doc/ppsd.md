# SeedPSD documentation

[Back to index](index.md)

## PPSD configuration

### Footer of generated plots

To configure the footer on the generated plots, set the following variables:

```bash
# Optional: Datacenter name to use on plot footer (default=SeedPSD)
SEEDPSD_PLOT_FOOTER_DC=SeedPSD

# Optional: Licence to use on plot footer
SEEDPSD_PLOT_FOOTER_LICENSE=cc-by
```

### Allowed channels

Only seismological data sources are supported by ObsPy for the calculation of PSDs. SeedPSD is therefore restricted by default to the following channel codes:

- band (1st letter): B, C, D, E, F, G, H, L, M, P, Q, R, S, T, U, V
- instrument (2nd letter): D, G, H, J, L, M, N, P
- orientation (3rd letter): A, B, C, D, E, F, H, I, N, O, R, T, U, V, W, Z, 1, 2, 3

To remove an element or add the support of other cases, configure the three following variables as needed:

```bash
# Optional: Allowed channel code: bands
SEEDPSD_PPSD_ALLOWED_CHANNEL_BANDS=ABC

# Optional: Allowed channel code: instruments
SEEDPSD_PPSD_ALLOWED_CHANNEL_INSTRUMENTS=DEF

# Optional: Allowed channel code: orientations
SEEDPSD_PPSD_ALLOWED_CHANNEL_ORIENTATIONS=GHI123
```

To fully disable this restriction and enable the support of all possible cases, configure the following variable to false:

```bash
# Optional: Exclude no-seismic channels (default=true)
SEEDPSD_PPSD_ONLY_ALLOWED_CHANNELS=false
```
Please note that the calculation of PSDs may not work at all if the data source is not compatible with PPSD internal algorithm!


### Calculation parameters

The SeedPSD engine relies on the PPSD class from ObsPy. At both feeding and operation, any PPSD object is initialized using the parameters as listed below.

> If this is your first time using SeedPSD and/or if your database is empty, we recommend that you use customized PPSD values.
> 
> However, if your database already contains PSDs calculated with an older version of SeedPSD, but you don't want to recalculate everything, you'll need to continue using the default PPSD values. 

#### With default PPSD values (for legacy databases)

##### Configuration

Use the following setting:

```bash
# Optional: Use ObsPy default value for PPSD calculation instead of SeedPSD customization by channel band
# Default value (for compatibility using an already populated database): true
SEEDPSD_PPSD_USE_DEFAULTS=true
```

##### Parameters value

| Band code | ppsd_length | period_smoothing_width_octaves | period_step_octaves | period_limits | overlap |
|-----------|-------------|--------------------------------|---------------------|---------------|---------|
| all       | 3600.0      | 1.0                            | 0.125               | None          | 0.5     | 


#### With customized PPSD values (for new databases - recommended)

> Please ensure your database is empty before enabling this feature.
>
> If your database contains statistics computed with the default parameters of PPSD:
> 
>- empty or recreate your database, and recomputed every statistics with the customized parameters (recommended)
>- or, use the setting "SEEDPSD_PPSD_USE_DEFAULTS=true" to keep the old algorithm using default parameters of PPSD for all data sources.

##### Configuration

Use the following setting:

```bash
# Optional: Use ObsPy default value for PPSD calculation instead of SeedPSD customization by channel band
# Recommended value (using an empty database): false
SEEDPSD_PPSD_USE_DEFAULTS=false
```

##### Parameters value

| Band code  | ppsd_length          | period_smoothing_width_octaves | period_step_octaves | period_limits                     | overlap |
|------------|----------------------|--------------------------------|---------------------|-----------------------------------|---------|
| D          | 3600.0 * 0.5         | 1.0                            | 1/4                 | (2/sampling_rate, ppsd_length/4)  | 0.5     | 
| C          | 3600.0 * 0.25        | 1.0                            | 1/4                 | (2/sampling_rate, ppsd_length/4)  | 0.5     | 
| E          | 3600.0 (default)     | 1/2                            | 1/8                 | (2/sampling_rate, ppsd_length/12) | 0.5     | 
| H          | 3600.0 (default)     | 1/2                            | 1/8                 | (2/sampling_rate, ppsd_length/12) | 0.5     | 
| B          | 3600.0 * 2           | 1/2                            | 1/32                | (2/sampling_rate, ppsd_length/24) | 0.5     | 
| M          | 3600.0 * 2           | 1/2                            | 1/32                | (2/sampling_rate, ppsd_length/24) | 0.5     | 
| L          | 3600.0 * 24 - 1000.0 | 1/4                            | 1/64                | (2/sampling_rate, ppsd_length/48) | 0.0     | 
| V          | 3600.0 * 24 - 1000.0 | 1/4                            | 1/64                | (2/sampling_rate, ppsd_length/48) | 0.0     |

For all other band codes, PPSD default values are used:

| Band code | ppsd_length | period_smoothing_width_octaves | period_step_octaves | period_limits | overlap |
|-----------|-------------|--------------------------------|---------------------|---------------|---------|
| other     | 3600.0      | 1.0                            | 0.125               | None          | 0.5     | 

##### Scientific validation

###### Preliminary works

* by Aurélien Mordret to determine the best segment length for each sampling frequency:
![Segment length / Sampling frequency](./resources/2023-04_SampleNum_SamplingFreq.png)

* by Aurélien Mordret and Laurent Stehly to determine the best PPSD values:
[PPSD tests](./resources/2023-11_PPSD_tests.pdf)

* by Laurent Stehly to finely tuned each parameter using test cases (see bellow)

##### Test cases

For each source, a small subset of ~10 files (extracted from Résif/EPOS-France datacenter) has been used.
The material stored under "[tests/datasets/](../tests/datasets/)" allows you to fetch them again and/or to test other cases.

| Band code | Sampling rate (Hz) | Source          | Year |
|-----------|--------------------|-----------------| ---- |
| B         | 20                 | FR.ARBF.00.BHZ  | 2021 |
| B         | 40                 | X7.PY95.00.BHZ  | 2013 |
| B         | 50                 | 7C.AIR.00.BHZ   | 2010 |
| B         | 62.5               | 7C.WADA.00.BHZ  | 2010 |
| C         | 250                | RA.YTMZ.00.CNZ  | 2018 |
| C         | 250                | YZ.TETR1.00.CHZ | 2022 |
| C         | 400                | MT.SZB.00.CHZ   | 2009 |
| C         | 500                | 1N.PEG1.00.CHZ  | 2019 |
| D         | 250                | 7H.PBOU.00.DHZ  | 2023 |
| D         | 500                | ZO.AR001.00.DPZ | 2018 |
| E         | 80                 | 7C.BLS.00.EHZ   | 2010 |
| E         | 100                | 1N.HAR1.00.EHZ  | 2016 |
| E         | 125                | CL.AGEO.00.EHZ  | 2010 |
| E         | 200                | 8N.HB01.00.EHZ  | 2023 |
| F         | 2000               | MT.SZB.00.FHZ   | 2009 |
| H         | 100                | X7.PY95.00.HHZ  | 2013 |
| H         | 125                | RA.YTMZ.00.HNZ  | 2018 |
| H         | 200                | FR.SZBH.00.HHZ  | 2021 |
| H         | 200                | FR.SZBH.00.HNZ  | 2021 |
| L         | 1                  | X7.PY95.00.LHZ  | 2013 |
| M         | 4                  | FR.SZBH.00.MMZ  | 2021 |
| M         | 5                  | G.TAM..MHZ      | 2009 |
| S         | 31.2               | YT.103.00.SHZ   | 2001 |
| S         | 40                 | XK.LP13.00.SHZ  | 2009 |
| S         | 50                 | XG.01001.00.SPZ | 2020 |
| S         | 62.5               | YB.S2.00.SHZ    | 2001 |
| V         | 0.1                | FR.WLS.00.VHZ   | 2020 |


###  Technical implementation details

The related source code can be found in the file "[seedpsd/tools/ppsd.py](../seedpsd/tools/ppsd.py)"

#### class PPSDFactory

This class is a kind of wrapper around the PPSD class. Its 'ppsd_params()' method implements the customization of PPSD parameters for each band code.

#### class CustomPPSD

This class extends and overloads the original PPSD class to
- provide extraction of numerical values for histogram,
- allow use of transparent background on histogram plots,
- reformat the plot title.

#### class PPSD (from ObsPy)

Please note that ObsPy 1.4.1 is incompatible with SQLAlchemy 2 and therefore cannot be installed with SeedPSD. 
The dependency on ObsPy is therefore currently blocked in version 1.4.0.

However, ObsPy version 1.4.1 introduces fixes to PPSD. 
The file concerned has therefore been integrated into SeedPSD ("[seedpsd/tools/spectral_estimation.py](../seedpsd/tools/spectral_estimation.py)") 
and it is this local version which will be used for the calculations instead of that coming from the installed ObsPy package.
