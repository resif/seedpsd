= SeedPSD documentation

link:./index.md[Back to index]

== Command-line usage

General syntax:

[,bash]
----
$ seedpsd-cli [OPTIONS] COMMAND [ARGS]
----

Use the **--help** option to show the specific syntax of each command.

=== Available commands

[,bash]
----
$ seedpsd-cli --help
    Usage: seedpsd-cli [OPTIONS] COMMAND [ARGS]...

      Welcome to SeedPSD command-line interface. Please use one of the following
      commands.

    Options:
      --help  Show this message and exit.

    Commands:
      admin     Database administration
      content   Database content management
      data      Load/Update data
      locale    Localization
      metadata  Load/Update metadata
      psd       Computed PSD exploitation
      server    Server management
----

==== Admin

[,bash]
----
$ seedpsd-cli admin
    Usage: seedpsd-cli admin [OPTIONS] COMMAND [ARGS]...

      Database administration

    Options:
      --help  Show this message and exit.

    Commands:
      create (database-create,schema-create) Create the database and/or its structure
      drop (database-drop,delete,schema-drop) Drop the database and/or its structure
      migrate (schema-migrate)        Apply/Generate a database schema migration
      version (schema-version)        Show/Update the database schema version
----

===== Create

[,bash]
----
$ seedpsd-cli admin create --help

Usage: seedpsd-cli admin create [OPTIONS]

  Create the database and/or its structure

Options:
  --encoding TEXT                 Database encoding (default: utf8)
  --template TEXT                 Database template (default: template0)

  --config-path PATH              Custom config path
  --level [DEBUG|INFO|WARNING|ERROR|CRITICAL|VERBOSE] Log verbosity level
  --sql-level [DEBUG|INFO|WARNING|ERROR|CRITICAL] SQLAlchemy log verbosity level
  --debug                         Show stack trace

  --help                          Show this message and exit.
----

===== Drop

[,bash]
----
$ seedpsd-cli admin drop --help

Usage: seedpsd-cli admin drop [OPTIONS]

  Drop the database and/or its structure

Options:
  --schema TEXT                   Name of a schema to drop
  --force                         Force the deletion without manual confirmation

  --config-path PATH              Custom config path
  --level [DEBUG|INFO|WARNING|ERROR|CRITICAL|VERBOSE] Log verbosity level
  --sql-level [DEBUG|INFO|WARNING|ERROR|CRITICAL] SQLAlchemy log verbosity level
  --debug                         Show stack trace

  --help                          Show this message and exit.
----

===== Migrate

[,bash]
----
$ seedpsd-cli admin migrate --help

Usage: seedpsd-cli admin migrate [OPTIONS]

  Apply/Generate a database schema migration

Options:
  --upgrade                       Upgrade database schema up to a version
  --downgrade                     Downgrade database schema up to a version
  --autogenerate                  Generate a database schema migration
  --version TEXT                  Target version (default=head)

  --config-path PATH              Custom config path
  --level [DEBUG|INFO|WARNING|ERROR|CRITICAL|VERBOSE] Log verbosity level
  --sql-level [DEBUG|INFO|WARNING|ERROR|CRITICAL] SQLAlchemy log verbosity level
  --debug                         Show stack trace

  --help                          Show this message and exit.
----

===== Version

[,bash]
----
$ seedpsd-cli admin version --help

Usage: seedpsd-cli admin version [OPTIONS]

  Show/Update the database schema version

Options:
  --stamp TEXT                    Stamp the database schema to a specific version
  --current                       Show current version

  --config-path PATH              Custom config path
  --level [DEBUG|INFO|WARNING|ERROR|CRITICAL|VERBOSE] Log verbosity level
  --sql-level [DEBUG|INFO|WARNING|ERROR|CRITICAL] SQLAlchemy log verbosity level
  --debug                         Show stack trace

  --help                          Show this message and exit.
----

==== Content

[,bash]
----
$ seedpsd-cli content
    Usage: seedpsd-cli content [OPTIONS] COMMAND [ARGS]...

      Database content management

    Options:
      --help  Show this message and exit.

    Commands:
      list-file     List files
      list-network  List networks
      list-source   List sources
----

===== List-file

[,bash]
----
$ seedpsd-cli content list-file --help

Usage: seedpsd-cli content list-file [OPTIONS]

  List files

Options:
  --network TEXT                  Filter by network code
  --station TEXT                  Filter by station code
  --location TEXT                 Filter by location code
  --channel TEXT                  Filter by channel code

  --date [%Y-%m-%d|%Y-%m-%dT%H:%M:%S|%Y-%m-%d %H:%M:%S] End at datetime
  --before [%Y-%m-%d|%Y-%m-%dT%H:%M:%S|%Y-%m-%d %H:%M:%S] End before at datetime
  --after [%Y-%m-%d|%Y-%m-%dT%H:%M:%S|%Y-%m-%d %H:%M:%S] End after at datetime

  --config-path PATH              Custom config path
  --level [DEBUG|INFO|WARNING|ERROR|CRITICAL|VERBOSE] Log verbosity level
  --sql-level [DEBUG|INFO|WARNING|ERROR|CRITICAL] SQLAlchemy log verbosity level
  --debug                         Show stack trace

  --help                          Show this message and exit.
----

===== List-network

[,bash]
----
$ seedpsd-cli content list-network --help

Usage: seedpsd-cli content list-network [OPTIONS]

  List networks

Options:
  --code TEXT                     Filter by network code
  --temporary / --permanent       Filter by temporary or pernament status
  --start [%Y-%m-%d|%Y-%m-%dT%H:%M:%S|%Y-%m-%d %H:%M:%S] Start at datetime
  --start-before [%Y-%m-%d|%Y-%m-%dT%H:%M:%S|%Y-%m-%d %H:%M:%S] Start before at datetime
  --start-after [%Y-%m-%d|%Y-%m-%dT%H:%M:%S|%Y-%m-%d %H:%M:%S] Start after at datetime
  --end [%Y-%m-%d|%Y-%m-%dT%H:%M:%S|%Y-%m-%d %H:%M:%S] End at datetime
  --end-before [%Y-%m-%d|%Y-%m-%dT%H:%M:%S|%Y-%m-%d %H:%M:%S] End before at datetime
  --end-after [%Y-%m-%d|%Y-%m-%dT%H:%M:%S|%Y-%m-%d %H:%M:%S]  End after at datetime

  --config-path PATH              Custom config path
  --level [DEBUG|INFO|WARNING|ERROR|CRITICAL|VERBOSE] Log verbosity level
  --sql-level [DEBUG|INFO|WARNING|ERROR|CRITICAL] SQLAlchemy log verbosity level
  --debug                         Show stack trace

  --help                          Show this message and exit.
----

===== List-source

[,bash]
----
$ seedpsd-cli content list-source --help

Usage: seedpsd-cli content list-source [OPTIONS]

  List sources

Options:
  --network TEXT                  Filter by network code
  --station TEXT                  Filter by station code
  --location TEXT                 Filter by location code
  --channel TEXT                  Filter by channel code

  --config-path PATH              Custom config path
  --level [DEBUG|INFO|WARNING|ERROR|CRITICAL|VERBOSE] Log verbosity level
  --sql-level [DEBUG|INFO|WARNING|ERROR|CRITICAL] SQLAlchemy log verbosity level
  --debug                         Show stack trace

  --help                          Show this message and exit.
----


==== Data

[,bash]
----
$ seedpsd-cli data
    Usage: seedpsd-cli data [OPTIONS] COMMAND [ARGS]...

      Load/Update data

    Options:
      --help  Show this message and exit.

    Commands:
      load                    Process MSEED files by year and network
      load-mseed (mseed)      Process MSEED files by path (file or directory)
      load-npz (npz)          Process NPZ files by path (file or directory)
      update-dirty (dirty)    Find dirty files in database and reprocess them
      update-orphan (orphan)  Find orphan files in database and link them to related...
----

===== Load

[,bash]
----
$ seedpsd-cli data load --help

Usage: seedpsd-cli data load [OPTIONS]

  Process MSEED files by year and network

Options:
  --year TEXT                     Filter by year
  --network TEXT                  Filter by network code
  --station TEXT                  Filter by station code
  --location TEXT                 Filter by location code
  --channel TEXT                  Filter by channel code
  --update                        Force update of already processed files
  --simulate                      Enable the simulation mode

  --parallel                      Enable parallelization
  --max-cpu TEXT                  CPU limit (only with parallel)

  --config-path PATH              Custom config path
  --level [DEBUG|INFO|WARNING|ERROR|CRITICAL|VERBOSE] Log verbosity level
  --sql-level [DEBUG|INFO|WARNING|ERROR|CRITICAL] SQLAlchemy log verbosity level
  --debug                         Show stack trace

  --help                          Show this message and exit.
----

===== Load-mseed

[,bash]
----
$ seedpsd-cli data mseed --help

Usage: seedpsd-cli data mseed [OPTIONS] PATH

  Process MSEED files by path (file or directory)

Options:
  --update                        Force update of already processed files
  --list                          The file contains a list of full paths to MSEED files
  --simulate                      Enable the simulation mode

  --parallel                      Enable parallelization
  --max-cpu TEXT                  CPU limit (only with parallel)

  --config-path PATH              Custom config path
  --level [DEBUG|INFO|WARNING|ERROR|CRITICAL|VERBOSE] Log verbosity level
  --sql-level [DEBUG|INFO|WARNING|ERROR|CRITICAL] SQLAlchemy log verbosity level
  --debug                         Show stack trace

  --help                          Show this message and exit.
----

===== Load-npz

[,bash]
----
$ seedpsd-cli data npz --help

Usage: seedpsd-cli data npz [OPTIONS] PATH

  Process NPZ files by path (file or directory)

Options:
  --update                        Force update of already processed files
  --dirty                         Force import of dirty files
  --simulate                      Enable the simulation mode

  --parallel                      Enable parallelization
  --max-cpu TEXT                  CPU limit (only with parallel)

  --config-path PATH              Custom config path
  --level [DEBUG|INFO|WARNING|ERROR|CRITICAL|VERBOSE] Log verbosity level
  --sql-level [DEBUG|INFO|WARNING|ERROR|CRITICAL] SQLAlchemy log verbosity level
  --debug                         Show stack trace

  --help                          Show this message and exit
----

===== Update-dirty

[,bash]
----
$ seedpsd-cli data dirty --help

Usage: seedpsd-cli data dirty [OPTIONS]

  Find dirty files in database and reprocess them

Options:
  --network TEXT                  Filter by network code
  --station TEXT                  Filter by station code
  --location TEXT                 Filter by location code
  --channel TEXT                  Filter by channel code
  --year TEXT                     Filter by year

  --delete                        Enable the deletion of really dirty files
  --simulate                      Enable the simulation mode

  --parallel                      Enable parallelization
  --max-cpu TEXT                  CPU limit (only with parallel)

  --config-path PATH              Custom config path
  --level [DEBUG|INFO|WARNING|ERROR|CRITICAL|VERBOSE] Log verbosity level
  --sql-level [DEBUG|INFO|WARNING|ERROR|CRITICAL] SQLAlchemy log verbosity level
  --debug                         Show stack trace

  --help                          Show this message and exit.
----

===== Update-orphan

[,bash]
----
$ seedpsd-cli data orphan --help
Usage: seedpsd-cli data orphan [OPTIONS]

  Find orphan files in database and link them to related Source

Options:
  --network TEXT                  Filter by network code
  --station TEXT                  Filter by station code
  --location TEXT                 Filter by location code
  --channel TEXT                  Filter by channel code
  --year TEXT                     Filter by year

  --delete                        Enable the deletion of really orphan files
  --simulate                      Enable the simulation mode

  --config-path PATH              Custom config path
  --level [DEBUG|INFO|WARNING|ERROR|CRITICAL|VERBOSE] Log verbosity level
  --sql-level [DEBUG|INFO|WARNING|ERROR|CRITICAL] SQLAlchemy log verbosity level
  --debug                         Show stack trace

  --help                          Show this message and exit.
----

==== Metadata

[,bash]
----
$ seedpsd-cli metadata
    Usage: seedpsd-cli metadata [OPTIONS] COMMAND [ARGS]...

      Load/Update metadata

    Options:
      --help  Show this message and exit.

    Commands:
      update  Update epochs by querying the inventory
----

===== Update

[,bash]
----
$ seedpsd-cli metadata update --help

Usage: seedpsd-cli metadata update [OPTIONS]

  Update epochs by querying the inventory

Options:
  --network TEXT                  Filter by network code
  --station TEXT                  Filter by station code
  --location TEXT                 Filter by location code
  --channel TEXT                  Filter by channel code
  --simulate                      Enable the simulation mode

  --parallel                      Enable parallelization
  --max-cpu TEXT                  CPU limit (only with parallel)

  --config-path PATH              Custom config path
  --level [DEBUG|INFO|WARNING|ERROR|CRITICAL|VERBOSE] Log verbosity level
  --sql-level [DEBUG|INFO|WARNING|ERROR|CRITICAL] SQLAlchemy log verbosity level
  --debug                         Show stack trace

  --help                          Show this message and exit
----

==== PSD

[,bash]
----
$ seedpsd-cli psd
    Usage: seedpsd-cli psd [OPTIONS] COMMAND [ARGS]...

      Computed PSD exploitation

    Options:
      --help  Show this message and exit.

    Commands:
      build-npz (npz)                 Generate a NPZ file
      export-values (export,value,values) Export PSD values
      plot-histogram (histogram,pdf)  Plot histogram (PDF)
      plot-spectrogram (spectrogram)  Plot spectrogram
----

===== Build-npz

[,bash]
----
$ seedpsd-cli psd npz --help

Usage: seedpsd-cli psd npz [OPTIONS] NETWORK STATION LOCATION CHANNEL START_TIME END_TIME

  Generate a NPZ file

Options:
  --output TEXT                   Set output directory (default=current dir)

  --config-path PATH              Custom config path
  --level [DEBUG|INFO|WARNING|ERROR|CRITICAL|VERBOSE] Log verbosity level
  --sql-level [DEBUG|INFO|WARNING|ERROR|CRITICAL] SQLAlchemy log verbosity level
  --debug                         Show stack trace

  --help                          Show this message and exit.
----

===== Export-values

[,bash]
----
$ seedpsd-cli psd value --help

Usage: seedpsd-cli psd value [OPTIONS] NETWORK STATION LOCATION CHANNEL START_TIME END_TIME

  Export PSD values

Options:
  --type [psd|mean|mode|histogram]
                                  Output type
  --format [csv|json]             Output format
  --output TEXT                   Set output directory (default=current dir)

  --config-path PATH              Custom config path
  --level [DEBUG|INFO|WARNING|ERROR|CRITICAL|VERBOSE] Log verbosity level
  --sql-level [DEBUG|INFO|WARNING|ERROR|CRITICAL] SQLAlchemy log verbosity level
  --debug                         Show stack trace

  --help                          Show this message and exit.
----

===== Plot-histogram

[,bash]
----
$ seedpsd-cli psd histogram --help

Usage: seedpsd-cli psd histogram [OPTIONS] NETWORK STATION LOCATION CHANNEL START_TIME END_TIME

  Plot histogram (PDF)

Options:
  --merge / --no-merge            Merge images resulting of multiple sampling rates
  --format TEXT                   Image format (png, jpg, pdf)
  --transparent / --no-transparent Enable image transparency
  --dpi TEXT                      Image DPI
  --width TEXT                    Image width
  --height TEXT                   Image height
  --grid / --no-grid              Show the grid
  --coverage / --no-coverage      Show coverage
  --percentiles / --no-percentiles Show percentiles
  --noise-models / --no-noise-models Show noise models
  --mode / --no-mode              Show mode
  --mean / --no-mean              Show mean
  --cumulative / --no-cumulative  Show cumulative
  --x-frequency / --x-period      Set the X axis in Frequency (Hz) instead of Period (s)
  --x-min TEXT                    Set X min
  --x-max TEXT                    Set X max
  --y-min TEXT                    Set Y min
  --y-max TEXT                    Set Y max
  --colormap TEXT                 Set colourmap
  --colormap-min TEXT             Set colourmap min
  --colormap-max TEXT             Set colourmap max
  --font-size TEXT                Set font size
  --outdated                      Include dirty/outdated calculations
  --output TEXT                   Set output directory (default=current dir)

  --config-path PATH              Custom config path
  --level [DEBUG|INFO|WARNING|ERROR|CRITICAL|VERBOSE] Log verbosity level
  --sql-level [DEBUG|INFO|WARNING|ERROR|CRITICAL] SQLAlchemy log verbosity level
  --debug                         Show stack trace

  --help                          Show this message and exit.
----

===== Plot-spectrogram

[,bash]
----
$ seedpsd-cli psd spectrogram --help

Usage: seedpsd-cli psd spectrogram [OPTIONS] NETWORK STATION LOCATION CHANNEL START_TIME END_TIME

  Plot spectrogram

Options:
  --merge / --no-merge            Merge images resulting of multiple sampling rates
  --format TEXT                   Image format (png, jpg, pdf)
  --transparent / --no-transparent Enable image transparency
  --dpi TEXT                      Image DPI
  --width TEXT                    Image width
  --height TEXT                   Image height
  --grid / --no-grid              Show the grid
  --y-invert / --y-not-invert     Invert the Y axis
  --colormap TEXT                 Set colourmap
  --colormap-min TEXT             Set colourmap min
  --colormap-max TEXT             Set colourmap max
  --font-size TEXT                Set font size
  --outdated                      Include dirty/outdated calculations
  --output TEXT                   Set output directory (default=current dir)

  --config-path PATH              Custom config path
  --level [DEBUG|INFO|WARNING|ERROR|CRITICAL|VERBOSE] Log verbosity level
  --sql-level [DEBUG|INFO|WARNING|ERROR|CRITICAL] SQLAlchemy log verbosity level
  --debug                         Show stack trace

  --help                          Show this message and exit.
----

==== Server

[,bash]
----
$ seedpsd-cli server
    Usage: seedpsd-cli server [OPTIONS] COMMAND [ARGS]...

      Server management

    Options:
      --help  Show this message and exit.

    Commands:
      amqp-data      Handle data updates by listening an AMQP queue
      amqp-metadata  Handle metadata updates by listening an AMQP queue
      web            Launch the Web server
----

===== Amqp-data

[,bash]
----
seedpsd-cli server amqp-data --help
Usage: seedpsd-cli server amqp-data [OPTIONS]

  Handle data updates by listening an AMQP queue

Options:
  --simulate                      Enable the simulation mode

  --config-path PATH              Custom config path
  --level [DEBUG|INFO|WARNING|ERROR|CRITICAL|VERBOSE] Log verbosity level
  --sql-level [DEBUG|INFO|WARNING|ERROR|CRITICAL] SQLAlchemy log verbosity level
  --amqp-level [DEBUG|INFO|WARNING|ERROR|CRITICAL|] AMQP log verbosity level
  --debug                         Show stack trace

  --help                          Show this message and exit.
----

===== Amqp-metadata

[,bash]
----
seedpsd-cli server amqp-metadata --help
Usage: seedpsd-cli server amqp-metadata [OPTIONS]

  Handle metadata updates by listening an AMQP queue

Options:
  --simulate                      Enable the simulation mode
  --update                        Enable the update of data after an update of meatadata

  --config-path PATH              Custom config path
  --level [DEBUG|INFO|WARNING|ERROR|CRITICAL|VERBOSE] Log verbosity level
  --sql-level [DEBUG|INFO|WARNING|ERROR|CRITICAL] SQLAlchemy log verbosity level
  --amqp-level [DEBUG|INFO|WARNING|ERROR|CRITICAL|] AMQP log verbosity level
  --debug                         Show stack trace

  --help                          Show this message and exit.
----

===== Web

[,bash]
----
$ seedpsd-cli server web --help

Usage: seedpsd-cli server web [OPTIONS]

  Launch the Web server

Options:
  --host TEXT                     The listen host
  --port INTEGER                  The listen port
  --reload                        Reload mode
  --locale TEXT                   The default language

  --config-path PATH              Custom config path
  --level [DEBUG|INFO|WARNING|ERROR|CRITICAL|VERBOSE] Log verbosity level
  --sql-level [DEBUG|INFO|WARNING|ERROR|CRITICAL] SQLAlchemy log verbosity level
  --debug                         Show stack trace

  --help                          Show this message and exit.
----

==== Locale

[,bash]
----
$ seedpsd-cli locale
    Usage: seedpsd-cli locale [OPTIONS] COMMAND [ARGS]...

    Localization

    Options:
      --help  Show this message and exit.

    Commands:
      compile  Compile message catalogs to MO files
      extract  Extract messages from source files and generate a POT file
----

===== Compile

[,bash]
----
$ seedpsd-cli locale compile --help

Usage: seedpsd-cli locale compile [OPTIONS]

  Compile message catalogs to MO files

Options:
  --locale TEXT                   Locale

  --config-path PATH              Custom config path
  --level [DEBUG|INFO|WARNING|ERROR|CRITICAL|VERBOSE] Log verbosity level
  --debug                         Show stack trace

  --help                          Show this message and exit.
----

===== Extract

[,bash]
----
$ seedpsd-cli locale extract --help

Usage: seedpsd-cli locale extract [OPTIONS]

  Extract messages from source files and generate a POT file

Options:
  --locale TEXT                   Locale

  --config-path PATH              Custom config path
  --level [DEBUG|INFO|WARNING|ERROR|CRITICAL|VERBOSE] Log verbosity level
  --debug                         Show stack trace

  --help                          Show this message and exit.
----