= SeedPSD documentation

link:./index.md[Back to index]

== Installation

=== From sources

**seedpsd** sources are distributed under the terms of link:https://choosealicense.com/licenses/gpl-3.0[GPLv3 licence]
and are available at https://gricad-gitlab.univ-grenoble-alpes.fr/OSUG/RESIF/seedpsd

First, ensure Git and Python 3 are installed on your system.

[,bash]
----
sudo apt install git-core python3 postgresql
----

Then, install https://docs.astral.sh/uv/getting-started/installation/[uv]. Uv is a tool for dependency management and packaging python applications.
[,bash]
----
curl -LsSf https://astral.sh/uv/install.sh | sh
echo 'eval "$(uv generate-shell-completion bash)"' >> ~/.bashrc
echo 'eval "$(uvx --generate-shell-completion bash)"' >> ~/.bashrc
----

Then, clone the project from the Gitlab repository:

[,bash]
----
git clone git@gricad-gitlab.univ-grenoble-alpes.fr:OSUG/RESIF/seedpsd.git
cd seedpsd
----

Install the project and its dependencies:

[,bash]
----
uv sync --frozen

# show the dependency tree for the project
uv tree
----

== Database setup

=== Initialization

Create a PostGreSQL role and database dedicated to SeedPSD:

[,bash]
----
# su - postgres
createuser --pwprompt seedpsd
createdb -O seedpsd seedpsd
----

See : https://wiki.debian.org/PostgreSql

=== Users and privileges

The SeedPSD workers will connect to the database using the privileged user ``seedpsd``.

The webinterface will use an unprivileged role, called ``wspsd``.

[,bash]
----
# su - postgres
createuser --pwprompt wspsd
----

Connect to the seedpsd database as ``seedpsd`` and pass the following commands.

[,sql]
----
seedspd=> GRANT CONNECT ON DATABASE seedpsd TO wspsd;
seedpsd=> ALTER DEFAULT PRIVILEGES FOR ROLE seedpsd GRANT USAGE ON SCHEMAS TO wspsd;
seedpsd=> ALTER DEFAULT PRIVILEGES FOR ROLE seedpsd GRANT SELECT ON TABLES TO wspsd;
----

=== Create the database schema

When the link:config.adoc[mandatory configuration] of SeedPSD is ready, initialize the structure of the ``seedpsd`` database:

[,bash]
----
# source .venv/bin/activate
seedpsd-cli admin create
----

If your PostgreSQL user has adminstrator rights, the **create** command will create automatically the database itself if needed.

