# Docker use
To make seedpsd discovery easier, a dockerfile is present in this repository

Make sure you set up a database as explained in "Database setup" section of [install section](/doc/install.adoc#user-content-database-setup)

```bash
sudo apt install podman buildah
```

## Buildah/Podman
Once buildah and podman installed, you can build the seedpsd docker image with the following command
```shell
buildah bud -t seedpsd --layers --build-arg SENTRY_RELEASE=123456789 --network=host
```

Create an env file that will be used by the docker image
```shell
cp .env-skeleton .env.docker
```

... and run it with the following command (make sure you have the appropriate .env.docker env file)
In this .env.docker file, `localhost` is replaced  by `host.containers.internal` to allow access to local database
```shell
podman run -p 8000:8000 --env-file=".env.docker" --rm seedpsd sh -c "seedpsd-cli admin create"  # to create the necessary tables
podman run -p 8000:8000 --env-file=".env.docker" --rm seedpsd sh -c "seedpsd-cli server web"
```
You may replace `seedpsd-cli server web` with any seedpsd command


To stop the docker image, list all running containers with the `podman container list` command
Take the name of the seedpsd running container and execute the following command : 

```shell
podman container kill <name of the seedpsd container>
```
