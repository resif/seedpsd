= SeedPSD documentation

link:./index.md[Back to index]

== Configuration

Create a file '.env' under the link:../[top directory] to configure SeedPSD by setting the following environment variables.

You will find a skeleton inside the **link:../.env-skeleton[.env-skeleton]** file.
Rename it to '.env' then fill and uncomment the necessary lines.

=== Mandatory configuration

==== ObsPy FDSN client

Configure the link:https://docs.obspy.org/packages/obspy.clients.fdsn.html[FDSN client] to use to fetch metadata by setting the following variable:

[,bash]
----
# Mandatory: ObsPy Client to use
SEEDPSD_FDSN_CLIENT=RESIF
----

==== Root directory containing MSEED data files

Configure the root directory containing MSEED data files by setting the following variable:

[,bash]
----
# Mandatory: Root directory containing MSEED data files
SEEDPSD_PATH_DATA=
----

==== Database

Configure the database to use by setting the following variables:

[,bash]
----
# Mandatory: Database name
SEEDPSD_DATABASE_NAME=seedpsd

# Mandatory: Server hostname
SEEDPSD_DATABASE_HOST=

# Mandatory: User login
SEEDPSD_DATABASE_USER=

# Optional: User password
#SEEDPSD_DATABASE_PASS=
----

Please read the dedicated page for further link:database.adoc[database configuration].

=== Optional configuration

Still in the '.env' file, use the following variables to configure optional features.

==== Using an SDS tree structure

If your MSEED files are stored in a SDS tree, enable the following variable:

[,bash]
----
# Optional: Directories/files are organized using SDS (default=false)
SEEDPSD_USE_SDS=true
----

Depending on the configuration, the path generated will take the following form:

- with SDS:
....
/[SEEDPSD_PATH_DATA]/[YEAR]/[NETWORK_CODE]/[STATION_CODE]/[CHANNEL_CODE].D/[MSEED_FILE_NAME]
....

- without SDS:
....
/[SEEDPSD_PATH_DATA]/[NETWORK_CODE]/[YEAR]/[STATION_CODE]/[CHANNEL_CODE].D/[MSEED_FILE_NAME]
....

==== Using a tree structure with extended network codes

If your network-level directories are named using extended code (only for temporary networks), enable the following variable:

[,bash]
----
#Optional: Directories/files are organized using network extended code (default=false)
SEEDPSD_USE_EXTENDED=true
----

For example, with the temporary AlpArray network (Z3 - 2015), the network code used in the generated paths will take the following form:

- with extended: Z32015
- without extended: Z3

==== PPSD customization

Please read the dedicated page for further link:ppsd.md[PPSD configuration].

===== With a new and empty database

If this is your first time using SeedPSD and/or if your database is empty, we recommend that you enable customized PPSD values by using the following setting:

[,bash]
----
# Optional: Use ObsPy default value for PPSD calculation instead of SeedPSD customization by channel band
# Recommended value (using an empty database): false
SEEDPSD_PPSD_USE_DEFAULTS=false
----

===== With a legacy and already populated database

If your database contains statistics computed with the default parameters of PPSD:

- empty or recreate your database, and recomputed every statistics with the customized parameters (recommended)
- or, use the following setting to keep the old algorithm using default parameters of PPSD for all data sources.

[,bash]
----
# Optional: Use ObsPy default value for PPSD calculation instead of SeedPSD customization by channel band
# Default value (for compatibility using an already populated database): true
SEEDPSD_PPSD_USE_DEFAULTS=true
----

==== Frontend webservice

Please read the dedicated page for link:server-web.adoc[frontend webservice configuration].

==== Plugging SeedPSD to AMQP

Please read the dedicated page for link:server-amqp.adoc[AMQP configuration].

==== Logging level

Adjust the logging level using a value among ERROR, WARNING, INFO, VERBOSE, DEBUG for:

- the SeedPSD application with 'SEEDPSD_LOG_LEVEL'
- the database layer (SQLAlchemy) using 'SEEDPSD_SQL_LOG_LEVEL'
- the ObsPy FDSN client using 'SEEDPSD_FDSN_CLIENT_DEBUG'

[,bash]
----
# Optional: Log stack trace when an error occurred (default=false)
SEEDPSD_LOG_STACKTRACE=false

# Optional: Application logging level (DEBUG|VERBOSE|INFO|WARNING|ERROR) (default=INFO)
SEEDPSD_LOG_LEVEL=INFO

# Optional: Database logging level (default=WARNING)
SEEDPSD_SQL_LOG_LEVEL=WARNING
----

- Logging levels are defined on the official documentation: https://docs.python.org/3/library/logging.html#logging-levels
- SeedPSD also defined an additional level 'VERBOSE' between 'DEBUG' and 'INFO' (see https://verboselogs.readthedocs.io/ for more details)

Use the following variable to enable logging to a file (into a directory or a specific file):

[,bash]
----
# Optional: Path (directory or '.log' file) to use for logging into a file
SEEDPSD_LOG_PATH=/tmp
----

Use the following variable to enable logging of stack trace (useful for debug):

[,bash]
----
# Optional: Log stack trace when an error occurred (default=false)
SEEDPSD_LOG_STACKTRACE=true
----

==== Collecting errors with Sentry

Use the following variables to enable collecting errors with Sentry:

[,bash]
----
# Optional: Sentry data source name (to enable lgging with Sentry)
# See: https://docs.sentry.io/product/sentry-basics/concepts/dsn-explainer/
SEEDPSD_SENTRY_DSN=

# Optional: Enable tracing with Sentry (default=false)
SEEDPSD_SENTRY_TRACING=true

# Optional: Sentry running environment
SEEDPSD_SENTRY_ENVIRONMENT=production

# Optional: Sentry sample rates
SEEDPSD_SENTRY_SAMPLE_RATE=1.0
SEEDPSD_SENTRY_TRACES_SAMPLE_RATE=0.1
SEEDPSD_SENTRY_PROFILES_SAMPLE_RATE=0.1
----

Please read the official documentation: https://docs.sentry.io/platforms/python/

==== Using a local inventory instead of querying a webservice

[WARNING]
====
Enabling this feature will DISABLE any request to a webservice, even if the 'SEEDPSD_FDSN_CLIENT' variable is properly configured.
====

For some special cases, if your metadata are not available through a FDSN Station webservice, SeedPSD allows you to use
a local inventory stored into StationXML files. To enable this feature, set the path containing StationXML file(s)
using the following variable:

[,bash]
----
# Optional: Root directory containing StationXML metadata files to use instead of using FDSN ObsPy Client
SEEDPSD_PATH_METADATA=
----

The selected path must be either:

- one unique StationXML containing the full inventory
- a directory containing several StationXML files named using the following pattern: [NETWORK_CODE].[STATION_CODE].xml

The inventory must be formatted using link:http://docs.fdsn.org/projects/stationxml/en/latest/[StationXML] and must contain
an instrumental response for each channel.

=== Configuration skeleton (.env-skeleton)

[,bash]
----
include::../.env-skeleton[]
----
