# SeedPSD documentation

[Back to index](index.md)

## Software dependencies

### ObsPy

* https://docs.obspy.org/
* https://docs.obspy.org/tutorial/code_snippets/probabilistic_power_spectral_density.html
* https://github.com/obspy/obspy/wiki/Notes-on-Parallel-Processing-with-Python-and-ObsPy

### Database

* https://docs.sqlalchemy.org/en/20/
* https://sqlalchemy-utils.readthedocs.io/
* https://alembic.sqlalchemy.org/en/latest/

* https://alembic-verify.readthedocs.io/en/latest/
* https://pypi.org/project/alembic-verify/

* https://www.psycopg.org/psycopg3/docs/index.html
* https://pypi.org/project/psycopg/


### Command-line interface

* https://click.palletsprojects.com/
* https://coloredlogs.readthedocs.io/
* https://verboselogs.readthedocs.io

* https://pypi.org/project/click/
* https://pypi.org/project/click-aliases/
* https://pypi.org/project/click-loglevel/
* https://pypi.org/project/coloredlogs/
* https://pypi.org/project/verboselogs/

### Web interface

* https://trypyramid.com/

* https://pypi.org/project/pyramid/
* https://pypi.org/project/pyramid-jinja2/
* https://pypi.org/project/pyramid_layout/
* https://pypi.org/project/waitress/

* https://docs.pylonsproject.org/projects/pyramid_layout/en/latest/
* https://awesome-pyramid.readthedocs.io/en/latest/awesome.html

### AMQP

* https://pypi.org/project/pika/
* https://www.rabbitmq.com/tutorials

### Miscellaneous

* https://pypi.org/project/python-dotenv/
* https://pypi.org/project/simple-singleton/

* https://pillow.readthedocs.io/en/stable/
* https://pypi.org/project/Pillow/

* https://pypi.org/project/multiprocess/

* https://tablib.readthedocs.io/en/stable/
* https://pypi.org/project/tablib/

* https://pendulum.eustace.io/
* https://pypi.org/project/pendulum/

* https://babel.pocoo.org/en/latest/
* https://pypi.org/project/Babel/

### Tests

* https://docs.pytest.org/en/latest/
* https://pytest-cov.readthedocs.io/en/latest/
* https://docs.sentry.io/platforms/python/
* https://pyyaml.org/

* https://pypi.org/project/pytest/
* https://pypi.org/project/pytest-cov/
* https://pypi.org/project/sentry-sdk/
* https://pypi.org/project/PyYAML/

### Code quality

* https://docs.astral.sh/ruff/
* https://www.djlint.com/
* https://pre-commit.com/

* https://pypi.org/project/ruff/
* https://pypi.org/project/djlint/
* https://pypi.org/project/pre-commit/
